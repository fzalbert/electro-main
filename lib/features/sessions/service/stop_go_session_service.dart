import 'dart:convert';

import 'package:electro/data/locale/app_database.dart';
import 'package:electro/data/locale/dao/stop_go.dart';
import 'package:electro/data/locale/entities/session_status.dart';
import 'package:electro/data/locale/entities/stop_go.dart';
import 'package:electro/data/singletons/storage.dart';
import 'package:electro/features/sessions/service/object_service.dart';
import 'package:electro/features/sessions/service/position_service.dart';
import 'package:electro/features/sessions/view_models/stop_go_session.dart';
import 'package:electro/models/inventorization/item.dart';
import 'package:electro/utils/guid_helper.dart';

class StopGoSessionService {
  final MapObjectService objectService = MapObjectService();

  StopGoSessionService();

  Future<StopGoSessionEntity?> create() async {
    var dao = await _getDao();

    var userId = StorageRepository.getString(KeysConstants.userIdKey);

    String? groupId;
    try {
      Map<String, dynamic> itemMap =
          jsonDecode(StorageRepository.getString(KeysConstants.applicationKey));
      Item sharedItem = Item.fromJson(itemMap);
      if (sharedItem.statusId == "2" || sharedItem.statusId == "3") {
        groupId = sharedItem.id;
      }
    } catch (e) {
      print("не заявка");
    }

    if (groupId != null) {
      var _session = await dao.findById(groupId);
      if (_session != null) {
        await dao.deleteByObjectId(groupId);
      }
    }

    var session = StopGoSessionEntity(
      id: groupId ?? GuidHelper.timeBased(),
      drawTypeId: "",
      createdAt: DateTime.now().toIso8601String(),
      status: SessionStatus.inProgress.index,
      userId: userId,
    );

    var id = session.id;
    await dao.insert(session);

    return dao.findById(id);
  }

  Future<StopGoSession?> findById(String sessionId) async {
    var dao = await _getDao();
    var entity = await dao.findById(sessionId);

    var userId = StorageRepository.getString(KeysConstants.userIdKey);

    if (entity == null || entity.userId != userId) {
      return null;
    }

    var coords = await PositionService().findBySessionId(entity.id);
    var objects = await objectService.findBySessionId(sessionId);

    return StopGoSession.fromEntity(entity, coords, objects);
  }

  Future<void> updateStatus(String sessionId, SessionStatus newStatus) async {
    var dao = await _getDao();
    var entity = await dao.findById(sessionId);
    if(entity != null){
      if(newStatus != SessionStatus.sended) {
        entity.status = newStatus.index;
        await dao.update(entity);
      }
      else {
        await dao.deleteByObjectId(entity.id);
      }
    }
  }

  Future<void> updateDrawId(String sessionId, String drawTypeId) async {
    var dao = await _getDao();
    var entity = await dao.findById(sessionId);
    if (entity != null) {
      entity.drawTypeId = drawTypeId;
      await dao.update(entity);
    }
  }

  Future<List<StopGoSessionEntity>> getAll() async {
    var dao = await _getDao();
    var list = await dao.findAll();

    var userId = StorageRepository.getString(KeysConstants.userIdKey);

    return list.where((element) => element.userId == userId).toList();
  }

  Future<StopGoSessionDao> _getDao() async {
    var _database = await DatabaseBuilder().create();
    return _database.stopGoSessionDao;
  }

  Future<AppDatabase> _getDatabase() async {
    return await DatabaseBuilder().create();
  }
}
