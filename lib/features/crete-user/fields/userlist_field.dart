import 'package:electro/app/theme/theme.dart';
import 'package:flutter/material.dart';

class UsersList extends StatefulWidget {
  final String fieldName;
  final List<dynamic> chooseFields;
  final bool isOne;
  final dynamic document;
  final Map? controllers;
  final Function? callback;
  const UsersList({
    Key? key,
    required this.fieldName,
    required this.chooseFields,
    required this.isOne,
    this.document,
    this.controllers,
    this.callback,
  }) : super(key: key);
  @override
  _UsersListState createState() => _UsersListState();
}

class _UsersListState extends State<UsersList> {
  List<int> selectedIndexes = [];

  @override
  void initState() {
    super.initState();
    if (widget.isOne) {
      selectedIndexes.add(widget.chooseFields.length - 1);
    }
  }

  @override
  Widget build(BuildContext context) {
    if (widget.isOne) {
      return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              widget.fieldName.toUpperCase(),
              style: Theme.of(context).textTheme.headline3!.copyWith(
                    fontSize: 12,
                  ),
            ),
            Container(
              height: 150,
              child: ListView.builder(
                itemCount: widget.chooseFields.length,
                itemBuilder: _createListView,
              ),
            ),
            const SizedBox(height: 28),
          ],
        )
      ]);
    } else {
      return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              widget.fieldName.toUpperCase(),
              style: Theme.of(context).textTheme.headline3!.copyWith(
                    fontSize: 12,
                  ),
            ),
            Container(
              height: 150,
              child: ListView.builder(
                itemCount: widget.chooseFields.length,
                itemBuilder: _createListView,
              ),
            ),
            const SizedBox(height: 28),
          ],
        )
      ]);
    }
  }

  Widget _createListView(BuildContext context, int index) {
    return GestureDetector(
      onTap: () {
        setState(() {
          if (widget.isOne) {
            selectedIndexes = [];
            selectedIndexes.add(index);
            if (widget.callback != null) {
              widget.callback!(
                widget.document.typeParameters.specificationsId,
                widget.document.typeParameters.id,
                <String>[widget.chooseFields[index]["id"].toString()],
              );
            }
          } else {
            if (selectedIndexes.contains(index)) {
              selectedIndexes.removeWhere((item) => item == index);
            } else {
              selectedIndexes.add(index);
            }
            if (widget.callback != null) {
              List<String> ids = [];
              for (int i = 0; i < selectedIndexes.length; i++) {
                ids.add(widget.chooseFields[selectedIndexes[i]]["id"]);
              }
              widget.callback!(
                widget.document.typeParameters.specificationsId,
                widget.document.typeParameters.id,
                ids,
              );
            }
          }
        });
      },
      child: Container(
        margin: const EdgeInsets.symmetric(vertical: 4),
        padding: const EdgeInsets.symmetric(vertical: 8),
        color:
            selectedIndexes.contains(index) ? Colors.black12 : Colors.white60,
        child: Text(
          widget.chooseFields[index]["optionText"],
          style: Theme.of(context)
              .textTheme
              .headline3!
              .copyWith(fontSize: 14, color: AppTheme.textGrey),
        ),
      ),
    );
  }
}
