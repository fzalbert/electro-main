library packages;

import 'package:electro/app/constants/app_icons.dart';
import 'package:electro/app/theme/theme.dart';
import 'package:electro/data/utils/functions.dart';
import 'package:electro/features/projects/components/slider.dart';
import 'package:electro/features/projects/components/switcher.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutter_switch/flutter_switch.dart';
import '../detail_bottom_sheet.dart';
import 'utils/data_util.dart';
import 'utils/util.dart';

export 'utils/data_util.dart';
export 'utils/util.dart';

enum DataType {
  DataList,
  DataMap,
}

class Config {
  final DataType dataType;
  final String parentId;
  final String uri;
  final String name;
  final String id;
  final String children;
  final String layerId;
  final String activated;
  final String value;
  final String disable;

  const Config({
    this.dataType = DataType.DataMap,
    this.parentId = 'parentId',
    this.uri = 'uri',
    this.name = 'name',
    this.id = 'id',
    this.children = 'children',
    this.layerId = "layerId",
    this.activated = "activated",
    this.value = 'value',
    this.disable = 'disable',
  });
}

class FlutterTree extends StatefulWidget {
  final Map<String, dynamic> treeData;
  final List<Map<String, dynamic>> listData;
  final List<Map<String, dynamic>> initialListData;
  final Function(Map<String, double>) onChecked;
  final Config config;

  const FlutterTree({
    Key? key,
    this.treeData = const <String, dynamic>{},
    this.config = const Config(),
    this.listData = const <Map<String, dynamic>>[],
    this.initialListData = const <Map<String, dynamic>>[],
    required this.onChecked,
  }) : super(key: key);

  @override
  _FlutterTreeState createState() => _FlutterTreeState();
}

class _FlutterTreeState extends State<FlutterTree> {
  Map<String, dynamic> sourceTreeMap = {};
  bool checkedBox = false;
  int selectValue = 0;
  Map treeMap = {};
  double currentValue = 1;

  @override
  initState() {
    super.initState();
    // set default select
    if (widget.config.dataType == DataType.DataList) {
      var listToMap =
          DataUtil.transformListToMap(widget.listData, widget.config);
      sourceTreeMap = listToMap;
      factoryTreeData(sourceTreeMap);
      widget.initialListData.forEach((element) {
        element['checked'] = false;
      });

      for (var element in treeMap.values.toList()) {
        if (element["activated"] != null && element["activated"]) {
          setCheckStatus(element);
        }
      }
      for (var element in treeMap.values.toList().reversed) {
        if (element["children"] != null) {
          bool allChidrenTrue = true;
          element['children'].forEach((element) {
            if (allChidrenTrue) {
              if (element["activated"]) {
                allChidrenTrue = true;
              } else {
                allChidrenTrue = false;
              }
            }
          });
          if (allChidrenTrue) {
            element["activated"] = true;
            setCheckStatus(element);
          }
        }
      }
    } else {
      sourceTreeMap = widget.treeData;
    }
  }

  setCheckStatus(item) {
    item['checked'] = true;
    if (item['children'] != null) {
      item['children'].forEach((element) {
        setCheckStatus(element);
      });
    }
  }

  factoryTreeData(treeModel) {
    treeModel['open'] = false;
    treeModel['checked'] = false;
    treeMap.putIfAbsent(treeModel[widget.config.id], () => treeModel);
    (treeModel[widget.config.children] ?? []).forEach((element) {
      factoryTreeData(element);
    });
  }

  Future<Widget> loadItem(String? uri, String name) async {
    return Container(
      width: MediaQuery.of(context).size.width * 0.5,
      child: Row(
        children: [
          if (uri != null && uri != "")
            Image.network(
              uri,
              width: 20,
              height: 20,
            ),
          const SizedBox(
            width: 12,
          ),
          Expanded(
            child: Text(
              name,
              style: Theme.of(context)
                  .textTheme
                  .headline4!
                  .copyWith(fontSize: 16, color: AppTheme.regularText),
            ),
          ),
        ],
      ),
    );
  }

  Widget loadItemWithoutImage(String name) {
    return Container(
      width: MediaQuery.of(context).size.width * 0.5,
      child: Row(
        children: [
          Expanded(
            child: Text(
              name,
              style: Theme.of(context)
                  .textTheme
                  .headline4!
                  .copyWith(fontSize: 16, color: AppTheme.regularText),
            ),
          ),
        ],
      ),
    );
  }

  Widget buildTreeParent(Map<String, dynamic> e, bool isShift) {
    return Column(
      children: [
        GestureDetector(
          onTap: () => onOpenNode(e),
          child: Container(
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.only(left: isShift ? 10 : 0, top: 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    (e[widget.config.children] ?? []).isNotEmpty
                        ? isOpenChildrenIcon(e)
                        : const SizedBox.shrink(),
                    const SizedBox(
                      width: 16,
                    ),
                    e[widget.config.children] == null
                        ? GestureDetector(
                            onTap: () {
                              MyFunctions.popBottomSheet(
                                  DetailBottomSheet(
                                    prefix: Container(
                                      margin: EdgeInsets.only(bottom: 20),
                                      child:
                                          Image.network(e[widget.config.uri]),
                                    ),
                                    title: e[widget.config.name],
                                  ),
                                  true,
                                  context);
                            },
                            child: childrenBody(e))
                        : childrenBody(e),
                    Spacer(),
                    switcher(
                      startingState: e["checked"],
                      disabled: e['disable'],
                      onToggle: () {
                        selectSwitcher(e, e[widget.config.layerId]);
                      },
                    ),
                  ],
                ),
                e[widget.config.children] == null
                    ? e["checked"]
                        ? ProjectsSlider(
                            currentValue: e["value"],
                            callback: (double newValue) {
                              getCheckedItems();
                              setState(() {
                                e["value"] = newValue;
                              });
                            })
                        : Container()
                    : Container(),
                (e['open'] ?? false)
                    ? Column(
                        children: buildTreeNode(e),
                      )
                    : const SizedBox.shrink(),
              ],
            ),
          ),
        ),
      ],
    );
  }

  Widget childrenBody(Map<String, dynamic> data) {
    return data[widget.config.children] != null
        ? Text('${data[widget.config.name]}',
            style: Theme.of(context).textTheme.headline1!.copyWith(
                fontSize: 16,
                color: (data['open'] ?? false)
                    ? AppTheme.regularTextDarkBlue
                    : AppTheme.regularText))
        : FutureBuilder<Widget>(
            future: loadItem(data[widget.config.uri], data[widget.config.name]),
            builder: (BuildContext context, AsyncSnapshot<Widget> snapshot) {
              if (snapshot.hasData) {
                return snapshot.data!;
              } else if (snapshot.hasError) {
                return loadItemWithoutImage("");
              } else {
                return loadItemWithoutImage("");
              }
            },
          );
  }

  buildTreeNode(Map<String, dynamic> data) {
    return (data[widget.config.children] ?? []).map<Widget>(
      (e) {
        return buildTreeParent(e, true);
      },
    ).toList();
  }

  Widget isOpenChildrenIcon(Map<String, dynamic> e) {
    return (e['open'] ?? false)
        ? Container(
            padding: const EdgeInsets.all(4),
            width: 24,
            height: 24,
            child: SvgPicture.asset(
              AppIcons.arrowDown,
            ))
        : Container(
            padding: const EdgeInsets.all(4),
            width: 24,
            height: 24,
            child: SvgPicture.asset(
              AppIcons.arrowRight,
            ));
  }

  onOpenNode(Map<String, dynamic> model) {
    if ((model[widget.config.children] ?? []).isEmpty) return;
    model['open'] = !model['open'];
    setState(() {
      sourceTreeMap = sourceTreeMap;
    });
  }

  selectSwitcher(Map<String, dynamic> dataModel, String layerId) {
    print("LAYER ID = $layerId");
    bool checked = dataModel['checked']!;
    if ((dataModel[widget.config.children] ?? []).isNotEmpty) {
      var stack = MStack();
      stack.push(dataModel);
      while (stack.top > 0) {
        Map<String, dynamic> node = stack.pop();
        for (var item in node[widget.config.children] ?? []) {
          stack.push(item);
        }
        if (checked) {
          if (!node['disable']) node['checked'] = false;
        } else {
          if (!node['disable']) node['checked'] = true;
        }
      }
    } else {
      if (checked) {
        if (!dataModel['disable']) dataModel['checked'] = false;
      } else {
        if (!dataModel['disable']) dataModel['checked'] = true;
      }
    }

    if (dataModel[widget.config.parentId]! > 0) {
      updateParentNode(dataModel);
    }
    setState(() {
      sourceTreeMap = sourceTreeMap;
    });
    getCheckedItems();
  }

  getCheckedItems() {
    var stack = MStack();
    Map<String, double> checkedList = {};
    stack.push(sourceTreeMap);
    while (stack.top > 0) {
      var node = stack.pop();
      for (var item in (node[widget.config.children] ?? [])) {
        stack.push(item);
      }
      if (node['checked']) {
        if (node[widget.config.layerId] != "node") {
          checkedList[node[widget.config.layerId]] = node["value"];
        }
      }
    }

    widget.onChecked(checkedList);
  }

  updateParentNode(Map<String, dynamic> dataModel) {
    var par = treeMap[dataModel[widget.config.parentId]];
    if (par == null) return;
    int checkLen = 0;
    for (var item in (par[widget.config.children] ?? [])) {
      if (item['checked']) {
        checkLen++;
      }
    }

    if (checkLen == (par[widget.config.children] ?? []).length) {
      if (!par['disable']) par['checked'] = true;
    } else {
      if (!par['disable']) par['checked'] = false;
    }

    if (treeMap[par[widget.config.parentId]] != null ||
        treeMap[par[widget.config.parentId]] == 0) {
      updateParentNode(par);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 20, bottom: 20),
      color: Colors.transparent,
      child: SingleChildScrollView(
        child: buildTreeParent(sourceTreeMap, false),
      ),
    );
  }
}
