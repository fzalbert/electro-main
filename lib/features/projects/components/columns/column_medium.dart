import 'package:electro/features/projects/components/columns/column_item.dart';
import 'package:electro/features/projects/components/columns/sub_column.dart';
import 'package:electro/features/projects/components/slider.dart';
import 'package:electro/features/projects/components/switcher.dart';
import 'package:electro/app/constants/app_icons.dart';
import 'package:electro/app/theme/theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:electro/models/columns/project_directory.dart';
import 'package:electro/models/columns/layer_directory.dart';

class ProjectsColumnMedium extends StatefulWidget {
  final ProjectDirectoryResponseDto? projectDirectory;
  final List<ProjectDirectoryResponseDto>? childrenDirectory;
  final List<LayerDirectoryModel>? projectLayers;

  ProjectsColumnMedium({
    Key? key,
    this.projectDirectory,
    this.childrenDirectory,
    this.projectLayers,
  }) : super(key: key);

  @override
  _ProjectsColumnMediumState createState() => _ProjectsColumnMediumState();
}

class _ProjectsColumnMediumState extends State<ProjectsColumnMedium> {
  bool secondColumnOpen = false;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    print("PROJECT LAYERS = ${widget.projectLayers}");
    if (widget.projectDirectory != null) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Column(
            children: [
              Container(
                margin: const EdgeInsets.fromLTRB(11, 0, 0, 0),
                child: Row(
                  children: [
                    GestureDetector(
                        behavior: HitTestBehavior.translucent,
                        onTap: () {
                          setState(() {
                            secondColumnOpen = !secondColumnOpen;
                          });
                        },
                        child: secondColumnOpen
                            ? Container(
                                padding: const EdgeInsets.all(4),
                                width: 24,
                                height: 24,
                                child: SvgPicture.asset(
                                  AppIcons.arrowDown,
                                ))
                            : Container(
                                padding: const EdgeInsets.all(4),
                                width: 24,
                                height: 24,
                                child: SvgPicture.asset(
                                  AppIcons.arrowRight,
                                ))),
                    const SizedBox(
                      width: 16,
                    ),
                    Text(
                      widget.projectDirectory!.name,
                      style: Theme.of(context).textTheme.headline3!.copyWith(
                          fontSize: 16,
                          color: secondColumnOpen
                              ? AppTheme.regularTextDarkBlue
                              : AppTheme.regularText),
                    ),
                    const Spacer(),
                    ProjectsSwitcher(
                      startingState: widget.projectDirectory!.activated,
                    )
                  ],
                ),
              ),
              secondColumnOpen
                  ? Column(
                      children: [
                        Container(
                            padding: const EdgeInsets.only(left: 31),
                            child: Column(
                              children: [
                                ProjectsSlider(),
                                ListView.builder(
                                    shrinkWrap: true,
                                    physics:
                                        const NeverScrollableScrollPhysics(),
                                    itemCount:
                                        widget.projectDirectory!.layers.length,
                                    itemBuilder: (context, index) {
                                      return Container(
                                        margin: const EdgeInsets.symmetric(
                                            vertical: 16),
                                        child: ColumnItem(
                                          title: widget.projectDirectory!
                                              .layers[index].name,
                                          prefix: Container(
                                            width: 15,
                                            height: 15,
                                            child: Image.network(widget
                                                .projectDirectory!
                                                .layers[index]
                                                .legendLink),
                                          ),
                                        ),
                                      );
                                    }),
                              ],
                            )),
                        Container(
                          padding: const EdgeInsets.only(left: 17),
                          child: ListView.builder(
                              physics: const NeverScrollableScrollPhysics(),
                              shrinkWrap: true,
                              itemCount: widget.childrenDirectory!.length,
                              itemBuilder: (context, index) {
                                return SubColumn(
                                    projectDirectory:
                                        widget.childrenDirectory![index]);
                              }),
                        )
                      ],
                    )
                  : const SizedBox(),
            ],
          )
        ],
      );
    } else {
      return Column(
        children: [
          Container(
              padding: const EdgeInsets.only(left: 31),
              child: Column(
                children: [
                  ProjectsSlider(),
                  ListView.builder(
                      shrinkWrap: true,
                      physics: const NeverScrollableScrollPhysics(),
                      itemCount: widget.projectLayers!.length,
                      itemBuilder: (context, index) {
                        return Container(
                          margin: const EdgeInsets.symmetric(vertical: 16),
                          child: ColumnItem(
                            title: widget.projectLayers![index].name,
                            prefix: Container(
                              width: 15,
                              height: 15,
                              child: Image.network(
                                  widget.projectLayers![index].legendLink),
                            ),
                          ),
                        );
                      }),
                ],
              )),
        ],
      );
    }
  }
}
