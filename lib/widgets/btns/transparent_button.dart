import 'package:flutter/material.dart';

class TransparentButton extends StatelessWidget {
  final String title;
  final String prefix;

  final VoidCallback onTap;

  const TransparentButton(
      {Key? key, required this.title, required this.prefix, required this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          FlatButton(
            splashColor: Colors.transparent,
            highlightColor: Colors.transparent,
            child: Text(
              title,
              textAlign: TextAlign.center,
              style: const TextStyle(
                fontWeight: FontWeight.w400,
                color: Colors.white,
                fontSize: 12.0,
                decoration: TextDecoration.underline,
              ),
            ),
            onPressed: onTap,
          )
        ],
      ),
    );
  }
}