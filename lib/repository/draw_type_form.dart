import 'dart:io';

import 'package:dio/dio.dart';
import 'package:electro/data/locale/entities/file.dart';
import 'package:electro/data/singletons/dio_settings.dart';
import 'package:electro/data/singletons/storage.dart';
import 'package:electro/models/object/draw_type_form.dart';
import 'package:electro/repository/draw_type.dart';
import 'package:electro/utils/guid_helper.dart';
import 'package:flutter/material.dart';


class DrawTypeFormRepository {
  Future<DrawTypeForm> get(String drawTypeId) async {
    await StorageRepository.getInstance();
    Dio _dio = DioSettings.createDio(connectTimeout: 30000);
    final Response response = await _dio.get(
      'GetDrawType',
      queryParameters: {"drawTypeId": drawTypeId},
      options: Options(headers: {
        'Authorization': 'Bearer ${StorageRepository.getString('token')}'
      }),
    );

    try {
      if (response.statusCode! >= 200 && response.statusCode! < 300) {
        return DrawTypeForm.fromJson(response.data);
      } else {
        throw NetworkException(response: response);
      }
    } catch (e) {
      var context = GlobalInfo.key.currentContext;
      if (context != null) {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text(e.toString()),
          duration: const Duration(milliseconds: 700),
        ));
      }
      print(e.toString());
      rethrow;
    }
  }

  void create(
    String drawTypeId,
  ) async {
    await StorageRepository.getInstance();

    String groupId = GuidHelper.timeBased();
    String directoryId = drawTypeId;
    String mapObjectTypeId = GuidHelper.timeBased();

    Dio _dio = Dio(DioSettings().dioBaseOptions);
    final Response response = await _dio.get(
      'CreateDrawObject',
      options: Options(headers: {
        'Authorization': 'Bearer ${StorageRepository.getString('token')}'
      }),
    );
  }

  Future<FileModel?> postFile({required File file}) async {
    try {
      String fileName = file.path.split('/').last;
      FormData formData = FormData.fromMap({
        "file": await MultipartFile.fromFile(file.path, filename: fileName),
      });
      Dio _dio = DioSettings.createDio();
      final Response response = await _dio.post(
        'UploadDrawFile',
        data: formData,
        options: Options(headers: {
          'Authorization': 'Bearer ${StorageRepository.getString('token')}'
        }),
      );

      var data = response.data;
      return FileModel(
        size: data["size"],
        sourceName: data["sourcename"],
        originalName: data["originalname"],
        uri: file.path,
      );
    } catch (ex) {
      print(ex.toString());
      return null;
    }
  }

  Future<FileModel?> postFileModel({required FileModel file}) async {
    try {
      String fileName = file.originalName;
      FormData formData = FormData.fromMap({
        "file": await MultipartFile.fromFile(file.uri!, filename: fileName),
      });
      Dio _dio = Dio(DioSettings().dioBaseOptions);
      final Response response = await _dio.post(
        'UploadDrawFile',
        data: formData,
        options: Options(headers: {
          'Authorization': 'Bearer ${StorageRepository.getString('token')}'
        }),
      );

      var data = response.data;
      return FileModel(
        size: data["size"],
        sourceName: data["sourcename"],
        originalName: data["originalname"],
        uri: file.uri,
      );
    } catch (ex) {
      print(ex.toString());
      return null;
    }
  }
}
