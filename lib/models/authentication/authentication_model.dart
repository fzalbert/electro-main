enum AuthenticationStatus {
  unknown,
  authenticated,
  unauthenticated,
}

class AuthenticationModel {
  final AuthenticationStatus status;

  const AuthenticationModel({
    required this.status,
  });
}
