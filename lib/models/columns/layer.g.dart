// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'layer.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LayerModel _$LayerModelFromJson(Map<String, dynamic> json) => LayerModel(
      uri: json['uri'] as String? ?? '',
      protocolName: json['protocolName'] as String? ?? '',
      mapProjectLayerConfigs: (json['mapProjectLayerConfigs'] as List<dynamic>?)
              ?.map((e) => e as Map<String, dynamic>)
              .toList() ??
          [],
      id: json['id'] as String? ?? '',
      name: json['name'] as String? ?? '',
      legendLink: json['legendLink'] as String? ?? '',
    );

Map<String, dynamic> _$LayerModelToJson(LayerModel instance) =>
    <String, dynamic>{
      'uri': instance.uri,
      'id': instance.id,
      'name': instance.name,
      'legendLink': instance.legendLink,
      'protocolName': instance.protocolName,
      'mapProjectLayerConfigs': instance.mapProjectLayerConfigs,
    };
