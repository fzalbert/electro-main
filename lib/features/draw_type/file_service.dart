import 'package:electro/data/locale/app_database.dart';
import 'package:electro/data/locale/entities/file.dart';

class FileService {

  Future<void> save(List<FileModel> files, String objectId) async {
    var db = await _getDb();


    for(var file in files){
      var model = FileEntity.fromModel(file, objectId);
      await db.file.insert(model);
    }
  }

  Future<AppDatabase> _getDb() async {
    var _database = await DatabaseBuilder().create();
    return _database;
  }
}