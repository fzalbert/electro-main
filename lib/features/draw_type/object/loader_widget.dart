

import 'package:flutter/cupertino.dart';

class LoaderWidget extends StatelessWidget {

  const LoaderWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
   return const SizedBox(
     height: 200,
     child: Center(
       child: CupertinoActivityIndicator(),
     ),
   );
  }

}