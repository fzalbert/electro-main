import 'package:electro/data/locale/entities/position.dart';
import 'package:latlong2/latlong.dart';
import 'package:proj4dart/proj4dart.dart';

class GeoJsonCreator {
  static String createPoint(LatLng latLng) {
    //var pointSrc = Point(x: latLng.latitude, y: latLng.longitude);
    var pointSrc = Point(x: latLng.longitude, y: latLng.latitude);
    var projSrc = Projection.get('EPSG:4326')!;
    var projDst = Projection.get('EPSG:3857')!;

    //print(pointSrc.toString());

    var pointForward = projSrc.transform(projDst, pointSrc);
    //print(pointForward.toString());

    //return "{\"type\":\"FeatureCollection\",\"features\":[{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Point\",\"coordinates\":[3383867.283277,8403028.338388]}}]}";
    return "{\"type\":\"FeatureCollection\",\"features\":[{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Point\",\"coordinates\":[${pointForward.x},${pointForward.y}]}}]}";
    //return "{ \"type\": \"FeatureCollection\", \"features\": [ { \"type\": \"Point\", \"coordinates\": [${pointForward.x}, ${pointForward.y}]}]}";
  }

  static List<double> createPointCoord(LatLng latLng) {
    var pointSrc = Point(x: latLng.longitude, y: latLng.latitude);
    var projSrc = Projection.get('EPSG:4326')!;
    var projDst = Projection.get('EPSG:3857')!;

    var pointForward = projSrc.transform(projDst, pointSrc);
    return [pointForward.x, pointForward.y];
  }

  static LatLng createCoord(List<double> point) {
    var pointSrc = Point(x: point[0], y: point[1]);
    var projSrc = Projection.get('EPSG:3857')!;
    var projDst = Projection.get('EPSG:4326')!;

    var pointForward = projSrc.transform(projDst, pointSrc);
    return LatLng(pointForward.y, pointForward.x);
  }

  static String createRoute(List<PositionEntity> list) {
    String jsonRoute = "";

    jsonRoute +=
        "{\"type\":\"FeatureCollection\",\"features\":[{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"LineString\",\"coordinates\":[";

    for (int i = 0; i < list.length; i++) {
      var element = list[i];
      var pointSrc = Point(x: element.longitude, y: element.latitude);
      var projSrc = Projection.get('EPSG:4326')!;
      var projDst = Projection.get('EPSG:3857')!;
      var pointForward = projSrc.transform(projDst, pointSrc);

      jsonRoute += "[${pointForward.x},${pointForward.y}]";

      if (i != list.length - 1) {
        jsonRoute = jsonRoute + ",";
      }
    }

    jsonRoute += "]}}]}";

    return jsonRoute;
  }
}
