import 'package:electro/data/locale/entities/object_type.dart';
import 'package:electro/features/draw_type/draw_type_tree_widget.dart';
import 'package:electro/models/object/object_type.dart';
import 'package:json_annotation/json_annotation.dart';

part 'draw_type.g.dart';

@JsonSerializable()
class DrawTypeResponseDto extends ObjectType {
  DrawTypeResponseDto({
    required this.id,
    required this.directoryName,
    required this.orderNumber,
    required this.objectTypes,
  });

  @JsonKey(name: 'Id')
  String id;
  @JsonKey(name: 'DirectoryName')
  String directoryName;
  @JsonKey(name: 'OrderNumber')
  int orderNumber;
  @JsonKey(name: 'ObjectTypes')
  List<ObjectTypeResponseDto> objectTypes;

  factory DrawTypeResponseDto.fromJson(Map<String, dynamic> json) =>
      _$DrawTypeResponseDtoFromJson(json);

  Map<String,dynamic> toJson() => _$DrawTypeResponseDtoToJson(this);

  @override
  Map<String, dynamic> getExtraData() {
    return {
      "id": id
    };
  }

  @override
  String getId() {
    return id;
  }

  @override
  String getParentId() {
    return "";
  }

  @override
  String getTitle() {
    return directoryName;
  }
}

class DrawTypeModel extends ObjectType{

  DrawTypeModel({
    required this.id,
    required this.directoryName,
    required this.orderNumber,
    required this.objectTypes,
  });

  @override
  String getId() {
    return id;
  }

  @override
  String getParentId() {
    return id;
  }

  @override
  String getTitle() {
    return directoryName;
  }

  String id;
  String directoryName;
  int orderNumber;
  List<ObjectTypeResponseDto> objectTypes;
}