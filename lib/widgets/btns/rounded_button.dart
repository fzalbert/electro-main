import 'package:electro/bloc/authentication/auth_bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:formz/formz.dart';

import '../../../app/constants/colors.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class RoundedButton extends StatelessWidget {
  final String title;
  final double? width;
  final double? height;
  final double cornerRadius;
  final Color color;
  final Color? borderColor;
  final Color titleColor;
  final double fontSize;
  final VoidCallback? onTap;
  final isLoading;

  const RoundedButton(
      {Key? key,
      this.title = "",
      this.width,
      this.onTap,
      this.borderColor,
      this.color = mainColor,
      this.titleColor = Colors.white,
      this.height,
      this.isLoading = false,
      this.cornerRadius = 0,
      this.fontSize = 16})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        width: width,
        height: height,
        decoration: BoxDecoration(
            color: color,
            borderRadius: BorderRadius.circular(cornerRadius),
            border: Border.all(color: color)),
        child: FlatButton(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(12.0),
              side: BorderSide(color: borderColor ?? color)),
          child: isLoading
              ? const Center(
                  child: CupertinoActivityIndicator(),
                )
              : Text(
                  title,
                  style: TextStyle(
                      fontWeight: FontWeight.w500,
                      color: titleColor,
                      fontSize: fontSize),
                ),
          onPressed: onTap,
        ));
  }
}
