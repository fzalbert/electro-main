// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'map_object_data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MapObjectData _$MapObjectDataFromJson(Map<String, dynamic> json) =>
    MapObjectData(
      id: json['Id'] as String,
      parentId: json['ParentId'] as String?,
      motId: json['MotId'] as String,
      coords: json['Coords'] as String,
      comment: json['Comment'] as String?,
      mapObjectDataValues: (json['MapObjectDataValues'] as List<dynamic>)
          .map((e) => MapObjectDataValue.fromJson(e as Map<String, dynamic>))
          .toList(),
      mapObjectFiles: json['MapObjectFiles'] as List<dynamic>,
      renters: (json['Renters'] as List<dynamic>)
          .map((e) => RenterRequestDto.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$MapObjectDataToJson(MapObjectData instance) =>
    <String, dynamic>{
      'Id': instance.id,
      'ParentId': instance.parentId,
      'Renters': instance.renters,
      'MotId': instance.motId,
      'Coords': instance.coords,
      'Comment': instance.comment,
      'MapObjectDataValues': instance.mapObjectDataValues,
      'MapObjectFiles': instance.mapObjectFiles,
    };
