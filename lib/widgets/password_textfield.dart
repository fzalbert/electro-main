import 'package:electro/app/constants/app_icons.dart';
import 'package:electro/app/theme/theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class PasswordTextField extends StatefulWidget {
  final TextEditingController controller;
  final String? currentText;
  PasswordTextField({Key? key, required this.controller, this.currentText})
      : super(key: key);

  @override
  _PasswordTextFieldState createState() => _PasswordTextFieldState();
}

class _PasswordTextFieldState extends State<PasswordTextField> {
  InputBorder border({required Color color}) {
    return OutlineInputBorder(
        borderRadius: BorderRadius.circular(9),
        borderSide: BorderSide(color: color, width: 1));
  }

  bool hidePassword = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: TextField(
        obscureText: hidePassword,
        
        onChanged: (s) {
          setState(() {});
        },
        textAlignVertical: TextAlignVertical.center,
        decoration: InputDecoration(
            hintText: 'Введите',hintStyle: Theme.of(context)
                .textTheme
                .headline3!
                .copyWith(fontSize: 14, color: AppTheme.textGrey),
            filled: true,
            fillColor: const Color(0xffF8F8F8),
            counterText: '',
            contentPadding: const EdgeInsets.symmetric(vertical: 12,horizontal: 10),
            suffixIcon: hidePassword
                ? GestureDetector(
                    onTap: () {
                      setState(() {
                        hidePassword = !hidePassword;
                      });
                    },
                    child: Container(
                        padding: const EdgeInsets.symmetric(vertical: 10,),
                        child: SvgPicture.asset(AppIcons.eyeOff)),
                  )
                : GestureDetector(
                    onTap: () {
                      setState(() {
                        hidePassword = !hidePassword;
                      });
                    },
                    child: Container(
                        padding: const EdgeInsets.symmetric(vertical: 10),
                        child: SvgPicture.asset(AppIcons.eyeOn)),
                  ),
            focusedBorder: border(color: const Color(0xffF3F3F3)),
            border: border(color: const Color(0xffF3F3F3)),
            enabledBorder: border(color: const Color(0xffF3F3F3)),
            disabledBorder: border(color: const Color(0xffF3F3F3))),
      ),
    );
  }
}
