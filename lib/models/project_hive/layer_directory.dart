import 'package:hive/hive.dart';
import 'package:json_annotation/json_annotation.dart';

part 'layer_directory.g.dart';

@HiveType(typeId: 3)
class LayerDirectoryHiveModel extends HiveObject {
  @HiveField(0)
  final String id;

  @HiveField(1)
  final String mapProjectId;

  @HiveField(2)
  final String mapProjectDirectoryId;

  @HiveField(3)
  final String name;

  @HiveField(4)
  final String uri;

  @HiveField(5)
  final String protocol;

  @HiveField(6)
  final bool enabledDefault;

  @HiveField(7)
  final bool activated;

  @HiveField(8)
  final String createdBy;

  @HiveField(9)
  final int orderNumber;

  @HiveField(10)
  final String legendLink;

  @HiveField(11)
  final String timeCreated;

  @HiveField(12)
  final String geoserverLayerId;

  @HiveField(13)
  final String moduleId;

  @HiveField(14)
  final String cqlFilter;

  @HiveField(15)
  final int dataSearchMode;

  @HiveField(16)
  final bool showInfoBlock;

  // final List mapProjectLayerConfigs
  LayerDirectoryHiveModel(
      {required this.activated,
      required this.cqlFilter,
      required this.createdBy,
      required this.dataSearchMode,
      required this.enabledDefault,
      required this.geoserverLayerId,
      required this.id,
      required this.legendLink,
      required this.mapProjectDirectoryId,
      required this.mapProjectId,
      required this.moduleId,
      required this.name,
      required this.orderNumber,
      required this.protocol,
      required this.showInfoBlock,
      required this.timeCreated,
      required this.uri});
}
