import 'dart:io';

import 'package:electro/data/locale/app_database.dart';
import 'package:electro/data/locale/entities/object_type.dart';
import 'package:electro/features/draw_type/object_form_service.dart';
import 'package:electro/models/object/draw_type.dart';
import 'package:electro/models/object/object_type.dart';
import 'package:electro/repository/draw_type.dart';

class ObjectTypeService {
  final _repository = DrawTypeRepository();

  Future<bool> update() async {
    var db = await _getDb();
    try {
      var drawTypes = await _repository.getAll();
      var items = _convertDrawTypesData(drawTypes);

      var result = await db.database.delete(ObjectTypeEntity.TABLE_NAME);
      // await db.objectType.clear();

      for (var e in items) {
        var entity = ObjectTypeEntity(
            id: e.id, directoryId: e.directoryId, title: e.title);
        await db.objectType.insert(entity);
      }

      ObjectFormService().update(items.map((e) => e.id));


      return true;
    }
    // on NetworkException catch(ex){
    //
    // } on SocketException catch(ex){
    //
    // }

    catch (ex) {
      print(ex.toString());
      return false;

    }
  }

  Future<List<ObjectTypeModel>> getAll() async {
    var db = await _getDb();
    var items = await db.objectType.findAll();
    if (items.isNotEmpty) {
      return items
          .map((e) => ObjectTypeModel(
              id: e.id, directoryId: e.directoryId, title: e.title))
          .toList();
    } else {
      try {
        var drawTypes = await _repository.getAll();
        var items = _convertDrawTypesData(drawTypes);
        return items;
      } on NetworkException catch (ex) {
        return [];
      } on SocketException catch (ex) {
        return [];
      } catch (ex) {
        return [];
      }
    }
  }

  Future<ObjectTypeModel?> getRoute() async {
    var db = await _getDb();
    var items = await db.objectType.findAll();

    try {
      var entity = items.firstWhere((element) => element.title.toLowerCase() == "маршрут");
      return ObjectTypeModel.fromEntity(entity);
    }
    catch(ex){
      return null;
    }

  }

  List<ObjectTypeModel> _convertDrawTypesData(List<DrawTypeResponseDto> items) {
    List<ObjectTypeModel> list = <ObjectTypeModel>[];
    for (var item in items) {
      for (var child in item.objectTypes) {
        _addChild(child, list, item);
      }
    }
    return list;
  }

  void _updateDb(List<ObjectTypeModel> items) async {
    var db = await _getDb();
    var result = await db.database.delete(ObjectTypeEntity.TABLE_NAME);
    // await db.objectType.clear();

    for (var e in items) {
      var entity = ObjectTypeEntity(
          id: e.id, directoryId: e.directoryId, title: e.title);
      await db.objectType.insert(entity);
    }
  }

  void _addChild(ObjectTypeResponseDto child, List<ObjectTypeModel> items,
      DrawTypeResponseDto parent) {
    if (child.childrenTypes.isNotEmpty) {
      for (var element in child.childrenTypes) {
        _addChild(element, items, parent);
      }
    }

    if (child.isCanDraw) {
      items.add(ObjectTypeModel(
          id: child.id, directoryId: parent.id, title: child.name));
    }
  }

  Future<AppDatabase> _getDb() async {
    var _database = await DatabaseBuilder().create();
    return _database;
  }
}
