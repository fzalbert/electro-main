import 'dart:io';

import 'package:electro/bloc/object/object_cubit.dart';
import 'package:electro/features/crete-user/fields/fields.dart';
import 'package:electro/features/crete-user/fields/image_upload.dart';
import 'package:electro/features/projects/components/editing_textfield.dart';
import 'package:electro/features/projects/components/photo_placeholder.dart';
import 'package:electro/models/object/object_info.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/src/provider.dart';
import 'package:mime/mime.dart';

class PreviewObjectInfoWidget extends StatelessWidget {
  final ObjectInfo object;
  PreviewObjectInfoWidget({Key? key, required this.object}) : super(key: key);
  List<File> allFiles = [];

  @override
  Widget build(BuildContext context) {
    if (object.files != null) print("FILES = ${object.files!}");
    return Container(
      padding: EdgeInsets.only(top: 10),
      decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(15))),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Container(
                  margin: EdgeInsets.only(left: 15, bottom: 20, top: 20),
                  child: Text(
                    object.title ?? object.layerItems[0][1],
                    style: Theme.of(context).textTheme.headline3!.copyWith(
                          fontSize: 20,
                        ),
                  ),
                ),
              ],
            ),
            Column(
              children: [
                for (List mapElement in object.layerItems)
                  Container(
                    margin:
                        const EdgeInsets.only(left: 15, bottom: 10, right: 15),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          mapElement[0],
                          style:
                              Theme.of(context).textTheme.headline3!.copyWith(
                                    fontSize: 12,
                                  ),
                        ),
                        const SizedBox(
                          height: 8,
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: EditingTextField(
                                currentText: mapElement[1],
                                controller: TextEditingController(),
                                enabled: false,
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(height: 8),
                      ],
                    ),
                  )
              ],
            ),
            if (object.files != null) Text("файлы")
            /*FutureBuilder(
                future: asyncLoadFiles(context, object.files!),
                builder:
                    (BuildContext context, AsyncSnapshot<List<File>> snapshot) {
                  if (snapshot.hasData ||
                      snapshot.connectionState == ConnectionState.done) {
                    allFiles = snapshot.data!;

                    return GridView.builder(
                        shrinkWrap: true,
                        itemCount: allFiles.length,
                        physics: const NeverScrollableScrollPhysics(),
                        gridDelegate:
                            const SliverGridDelegateWithFixedCrossAxisCount(
                                crossAxisCount: 3,
                                crossAxisSpacing: 20,
                                mainAxisSpacing: 10,
                                childAspectRatio: 103 / 103),
                        itemBuilder: (context, index) {
                          return PhotoPlaceholder(
                            child:
                                lookupMimeType(allFiles[index].path.toString())!
                                        .startsWith('image/')
                                    ? Image.file(allFiles[index])
                                    : Text(allFiles[index]
                                        .path
                                        .toString()
                                        .split("/")
                                        .last),
                            isUploaded: true,
                          );
                        });
                  } else if (snapshot.hasError) {
                    return Container();
                  } else {
                    return CupertinoActivityIndicator();
                  }
                },
              )*/
          ],
        ),
      ),
    );
  }
}

Future<List<File>> asyncLoadFiles(
    BuildContext context, List<dynamic> filesMap) async {
  print("FILES MAP = $filesMap");
  List<File> _files = [];
  for (Map mapFile in filesMap) {
    File file = await context
        .read<ObjectCubit>()
        .dowloadFile(idFile: mapFile["Id"], objectId: mapFile["ObjectId"]);

    _files.add(file);
  }

  print("FILES = $_files");

  return _files;
}
