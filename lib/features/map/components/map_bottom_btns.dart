import 'dart:ui';

import 'package:electro/app/constants/app_icons.dart';
import 'package:electro/app/constants/colors.dart';
import 'package:electro/data/singletons/stop_and_go.dart';
import 'package:electro/data/utils/functions.dart';
import 'package:electro/features/add-objects/inventory_bottomsheet.dart';
import 'package:electro/features/crete-user/user_bottom_sheet.dart';
import 'package:electro/features/map/components/rounded_btn_loader.dart';
import 'package:electro/features/projects/projects_bottomsheet.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import 'rounded_btn.dart';

Widget bottomBtnsCreate(
  GestureTapCallback _getMyLocation,
  GestureTapCallback _zoomInDidTap,
  GestureTapCallback _zoomOutDidTap,
  GestureTapCallback _zoomOutDidTap2,
  VoidCallback extraAction,
  BuildContext context, {
  required List<String> activeLayers,
      bool cacheUpdating = false,
      int cacheUpdateState = 2,
      required VoidCallback updateCacheAction,

}) {
  double margin = 15.0;

  return Padding(
    padding: const EdgeInsets.only(bottom: 50),
    child: Container(
      height: 56,
      child: Row(
        mainAxisSize: MainAxisSize.min,

        mainAxisAlignment: MainAxisAlignment.center,
        // crossAxisAlignment: CrossAxisAlignment.center,

        children: [
          // back
          _createBtn(() {}, AppIcons.arrowRight, () {
            FocusScope.of(context).requestFocus(FocusNode());
            MyFunctions.showLogOut(context: context);
          }),
          const SizedBox(
            width: 15,
          ),
          _createBtn(() {}, "assets/icons/ic_wallet.png", () {
            FocusScope.of(context).requestFocus(FocusNode());
            MyFunctions.popBottomSheet(
                ProjectsBottomSheet(
                  activeLayers: activeLayers,
                ),
                true,
                context,
                isProjectList: true);
          }),

          const SizedBox(
            width: 15,
          ),

          // const SizedBox(
          //   width: 15,
          // ),

          // stop and go
          _createBtn(() {}, AppIcons.plus, () {
            FocusScope.of(context).requestFocus(FocusNode());
            MyFunctions.popBottomSheet(
                InventoryBottomSheet(
                  isActive: StopAndGo.isStarted,
                ),
                true,
                context);
          }),

          const SizedBox(
            width: 15,
          ),

          //location
          _createBtn(_getMyLocation, "assets/icons/ic_location.png", () {
            FocusScope.of(context).requestFocus(FocusNode());
          }),

          const SizedBox(
            width: 15,
          ),
          _createBtnLoader(AppIcons.sync, cacheUpdating, cacheUpdateState, updateCacheAction),

        ],
      ),
    ),
  );
}

Widget _createBtn(VoidCallback tap, String asset, VoidCallback extraAction) {
  return RoundedBtn(
    onTap: () {
      tap();

      extraAction();
    },
    asset: asset,
  );
}
Widget _createBtnLoader(String asset, bool cacheUpdating, int cacheUpdateState, VoidCallback tap) {
  return RoundedBtnLoader(
    state: cacheUpdateState,
    loadind:  cacheUpdating,
    onTap: () {
      tap();

    },
    asset: asset,
  );
}
