import 'package:electro/models/columns/layer_directory.dart';
import 'package:electro/models/columns/project_directory.dart';
import 'package:electro/models/project_hive/layer_directory.dart';
import 'package:electro/models/project_hive/project_directory.dart';
import 'package:hive/hive.dart';

part 'directory_response_model.g.dart';

@HiveType(typeId: 1)
class DirectoryResponseDtoHiveModel extends HiveObject {
  DirectoryResponseDtoHiveModel({
    required this.activated,
    required this.center,
    required this.createdBy,
    required this.id,
    required this.mapProjectDirectories,
    required this.moduleId,
    required this.name,
    required this.orderNumber,
    required this.timeCreated,
    required this.zoomLevel,
    required this.mapProjectLayers,
  });

  @HiveField(0)
  final String id;

  @HiveField(1)
  final String name;

  @HiveField(2)
  final String createdBy;

  @HiveField(3)
  final String timeCreated;

  @HiveField(4)
  final String center;

  @HiveField(5)
  final int zoomLevel;

  @HiveField(6)
  final bool activated;

  @HiveField(7)
  final int orderNumber;

  @HiveField(8)
  final String moduleId;

  @HiveField(9)
  final List<ProjectDirectoryResponseDtoHiveModel> mapProjectDirectories;

  @HiveField(10)
  final List<LayerDirectoryHiveModel> mapProjectLayers;
}
