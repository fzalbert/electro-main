import 'dart:io';

import 'package:electro/bloc/project-list/projects_events.dart';
import 'package:electro/bloc/project-list/projects_state.dart';
import 'package:electro/data/database/database.dart';
import 'package:electro/models/columns/directory.dart';
import 'package:electro/models/columns/layer.dart';
import 'package:electro/models/project_hive/directory_response_model.dart';
import 'package:electro/models/project_hive/layer_directory.dart';
import 'package:electro/models/project_hive/project_directory.dart';
import 'package:electro/models/project_hive/projects.dart';
import 'package:electro/repository/projects.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:formz/formz.dart';
import 'package:electro/models/columns/layer_directory.dart';
import 'package:electro/models/columns/project_directory.dart';
import 'package:hive/hive.dart';

class ProjectListBloc extends Bloc<ProjectListEvents, ProjectListState> {
  final ProjectsRepository repository;
  final Database db;
  final _box = Hive.box<ProjectsModel>('projectList');

  ProjectListBloc(this.repository, this.db)
      : super(ProjectListState(
          projectList: [],
          status: FormzStatus.pure,
          layerModels: [],
          markers: [],
          layerIds: {},
        )) {
    on<GetProjectListEvent>((event, emit) async {
      emit(state.copyWith(status: FormzStatus.submissionInProgress));
      try {
        var result = await repository.getProjectList();
        List<DirectoryResponseDtoHiveModel> res = [];

        for (var _res in result) {
          res.add(DirectoryResponseDtoHiveModel(
            activated: _res.activated,
            center: _res.center,
            createdBy: _res.createdBy,
            id: _res.id,
            mapProjectDirectories:
                getProjectDirectory(_res.mapProjectDirectories),
            moduleId: _res.moduleId,
            name: _res.name,
            orderNumber: _res.orderNumber,
            timeCreated: _res.timeCreated,
            zoomLevel: _res.zoomLevel,
            mapProjectLayers: getLayerDirectory(_res.mapProjectLayers),
          ));
        }

        ProjectsModel _projects = ProjectsModel(projects: res);

        await _box.put("projectCached", _projects);
        var projectCached = _box.get("projectCached");
        //print("PROJECT IN CACHE $projectCached");

        _addToDatabase(result);
        final layers = <LayerModel>[];
        for (final project in result) {
          layers.addAll(await repository.getLayersList(projectId: project.id));
        }
        emit(state.copyWith(
            status: FormzStatus.submissionSuccess,
            projectList: result,
            layerModels: layers,
            layerIds: {}));
      } catch (e) {
        print("ERROR = $e");
        var result = await getFromDatabase();
        emit(state.copyWith(projectList: result, status: FormzStatus.invalid));
      }
    });
  }

  List<LayerDirectoryHiveModel> getLayerDirectory(
      List<LayerDirectoryModel> _mapLayer) {
    List<LayerDirectoryHiveModel> _res = [];
    for (LayerDirectoryModel _map in _mapLayer) {
      _res.add(LayerDirectoryHiveModel(
          activated: _map.activated,
          cqlFilter: _map.cqlFilter,
          createdBy: _map.createdBy,
          dataSearchMode: _map.dataSearchMode,
          enabledDefault: _map.enabledDefault,
          geoserverLayerId: _map.geoserverLayerId,
          id: _map.id,
          legendLink: _map.legendLink,
          mapProjectDirectoryId: _map.mapProjectDirectoryId,
          mapProjectId: _map.mapProjectId,
          moduleId: _map.moduleId,
          name: _map.name,
          orderNumber: _map.orderNumber,
          protocol: _map.protocol,
          showInfoBlock: _map.showInfoBlock,
          timeCreated: _map.timeCreated,
          uri: _map.uri));
    }

    return _res;
  }

  List<ProjectDirectoryResponseDtoHiveModel> getProjectDirectory(
      List<ProjectDirectoryResponseDto> _mapProject) {
    List<ProjectDirectoryResponseDtoHiveModel> _res = [];
    for (ProjectDirectoryResponseDto _map in _mapProject) {
      _res.add(ProjectDirectoryResponseDtoHiveModel(
        activated: _map.activated,
        createdBy: _map.createdBy,
        layers: getLayerDirectory(_map.layers),
        childrenDirectories: getProjectDirectory(_map.childrenDirectories),
        id: _map.id,
        enabledDefault: _map.enabledDefault,
        mapProjectDirectoryId: _map.mapProjectDirectoryId,
        mapProjectId: _map.mapProjectId,
        name: _map.name,
        orderNumber: _map.orderNumber,
        rootServiceDirectory: _map.rootServiceDirectory,
        serviceId: _map.serviceId,
        timeCreated: _map.timeCreated,
      ));
    }

    return _res;
  }

  List<LayerDirectoryModel> getLayerDirectoryDB(
      List<LayerDirectoryHiveModel> _mapLayer) {
    List<LayerDirectoryModel> _res = [];
    for (LayerDirectoryHiveModel _map in _mapLayer) {
      _res.add(LayerDirectoryModel(
          activated: _map.activated,
          cqlFilter: _map.cqlFilter,
          createdBy: _map.createdBy,
          dataSearchMode: _map.dataSearchMode,
          enabledDefault: _map.enabledDefault,
          geoserverLayerId: _map.geoserverLayerId,
          id: _map.id,
          legendLink: _map.legendLink,
          mapProjectDirectoryId: _map.mapProjectDirectoryId,
          mapProjectId: _map.mapProjectId,
          moduleId: _map.moduleId,
          name: _map.name,
          orderNumber: _map.orderNumber,
          protocol: _map.protocol,
          showInfoBlock: _map.showInfoBlock,
          timeCreated: _map.timeCreated,
          uri: _map.uri));
    }

    return _res;
  }

  List<ProjectDirectoryResponseDto> getProjectDirectoryDB(
      List<ProjectDirectoryResponseDtoHiveModel> _mapProject) {
    List<ProjectDirectoryResponseDto> _res = [];
    for (ProjectDirectoryResponseDtoHiveModel _map in _mapProject) {
      _res.add(ProjectDirectoryResponseDto(
        activated: _map.activated,
        createdBy: _map.createdBy,
        layers: getLayerDirectoryDB(_map.layers),
        childrenDirectories: getProjectDirectoryDB(_map.childrenDirectories),
        id: _map.id,
        enabledDefault: _map.enabledDefault,
        mapProjectDirectoryId: _map.mapProjectDirectoryId,
        mapProjectId: _map.mapProjectId,
        name: _map.name,
        orderNumber: _map.orderNumber,
        rootServiceDirectory: _map.rootServiceDirectory,
        serviceId: _map.serviceId,
        timeCreated: _map.timeCreated,
      ));
    }

    return _res;
  }

  void updateLayerIds(Map<String, double> newLayerIds) async {
    emit(state.copyWith(status: FormzStatus.submissionInProgress));
    try {
      emit(state.copyWith(
        status: FormzStatus.submissionSuccess,
        layerIds: newLayerIds,
      ));
    } catch (e) {
      var result = await getFromDatabase();
      emit(state.copyWith(projectList: result, status: FormzStatus.invalid));
    }
  }

  void _addToDatabase(List<DirectoryResponseDto> result) async {
    var isEmpty = await Database.tableIsEmpty(
        query: Database.directoryQuery, table: 'directoryList');
    if (isEmpty) {
      for (var directory in result) {
        await Database.insert(
            table: "directoryList",
            query: Database.directoryQuery,
            data: {
              'localId': directory.id,
              'name': directory.name,
              'createdBy': directory.createdBy,
              'timeCreated': directory.timeCreated,
              'center': directory.center,
              'zoomLevel': directory.zoomLevel,
              'activated': directory.activated == true ? 1 : 0,
              'orderNumber': directory.orderNumber,
              'moduleId': directory.moduleId,
            });
        for (var projectDirectory in directory.mapProjectDirectories) {
          await Database.insert(
              table: "projectList",
              data: {
                'enabledDefault':
                    projectDirectory.enabledDefault == true ? 1 : 0,
                'localId': projectDirectory.id,
                'mapProjectId': projectDirectory.mapProjectId,
                'mapProjectDirectoryId': projectDirectory.mapProjectDirectoryId,
                'name': projectDirectory.name,
                'activated': projectDirectory.activated == true ? 1 : 0,
                'orderNumber': projectDirectory.orderNumber,
                'serviceId': projectDirectory.serviceId,
                'rootServiceDirectory':
                    projectDirectory.rootServiceDirectory == true ? 1 : 0,
                'createdBy': projectDirectory.createdBy,
                'timeCreated': projectDirectory.timeCreated,
              },
              query: Database.projectDirectoryQuery);

          for (var childProjectDirectory
              in projectDirectory.childrenDirectories) {
            await Database.insert(
                table: "childProjectList",
                data: {
                  'enabledDefault':
                      childProjectDirectory.enabledDefault == true ? 1 : 0,
                  'localId': childProjectDirectory.id,
                  'mapProjectId': childProjectDirectory.mapProjectId,
                  'mapProjectDirectoryId':
                      childProjectDirectory.mapProjectDirectoryId,
                  'name': childProjectDirectory.name,
                  'activated': childProjectDirectory.activated == true ? 1 : 0,
                  'orderNumber': childProjectDirectory.orderNumber,
                  'serviceId': childProjectDirectory.serviceId,
                  'rootServiceDirectory':
                      childProjectDirectory.rootServiceDirectory == true
                          ? 1
                          : 0,
                  'createdBy': childProjectDirectory.createdBy,
                  'timeCreated': childProjectDirectory.timeCreated,
                },
                query: Database.projectDirectoryQuery);

            for (var childLayerDirectory in childProjectDirectory.layers) {
              await Database.insert(
                table: "childLayerList",
                data: {
                  'localId': childLayerDirectory.id,
                  'mapProjectId': childLayerDirectory.mapProjectId,
                  'mapProjectDirectoryId':
                      childLayerDirectory.mapProjectDirectoryId,
                  'name': childLayerDirectory.name,
                  'activated': childLayerDirectory.activated == true ? 1 : 0,
                  'orderNumber': childLayerDirectory.orderNumber,
                  'protocol': childLayerDirectory.protocol,
                  'uri': childLayerDirectory.uri,
                  'createdBy': childLayerDirectory.createdBy,
                  'timeCreated': childLayerDirectory.timeCreated,
                  'enabledDefault':
                      childLayerDirectory.enabledDefault == true ? 1 : 0,
                  'legendLink': childLayerDirectory.legendLink,
                  'geoserverLayerId': childLayerDirectory.geoserverLayerId,
                  'cqlFilter': childLayerDirectory.cqlFilter,
                  'moduleId': childLayerDirectory.moduleId,
                  'dataSearchMode': childLayerDirectory.dataSearchMode,
                  'showInfoBlock':
                      childLayerDirectory.showInfoBlock == true ? 1 : 0,
                },
                query: Database.layerDirectoryQuery,
              );
            }
          }

          for (var layerDirectory in projectDirectory.layers) {
            await Database.insert(
              table: "layerList",
              data: {
                'localId': layerDirectory.id,
                'mapProjectId': layerDirectory.mapProjectId,
                'mapProjectDirectoryId': layerDirectory.mapProjectDirectoryId,
                'name': layerDirectory.name,
                'activated': layerDirectory.activated == true ? 1 : 0,
                'orderNumber': layerDirectory.orderNumber,
                'protocol': layerDirectory.protocol,
                'uri': layerDirectory.uri,
                'createdBy': layerDirectory.createdBy,
                'timeCreated': layerDirectory.timeCreated,
                'enabledDefault': layerDirectory.enabledDefault == true ? 1 : 0,
                'legendLink': layerDirectory.legendLink,
                'geoserverLayerId': layerDirectory.geoserverLayerId,
                'cqlFilter': layerDirectory.cqlFilter,
                'moduleId': layerDirectory.moduleId,
                'dataSearchMode': layerDirectory.dataSearchMode,
                'showInfoBlock': layerDirectory.showInfoBlock == true ? 1 : 0,
              },
              query: Database.layerDirectoryQuery,
            );
          }
        }
      }
    } else {
      for (var directory in result) {
        await Database.update(
            id: directory.id,
            table: "directoryList",
            checkDatabase: Database.directoryQuery,
            dataToUpdate: {
              'localId': directory.id,
              'name': directory.name,
              'createdBy': directory.createdBy,
              'timeCreated': directory.timeCreated,
              'center': directory.center,
              'zoomLevel': directory.zoomLevel,
              'activated': directory.activated == true ? 1 : 0,
              'orderNumber': directory.orderNumber,
              'moduleId': directory.moduleId,
            });
        for (var projectDirectory in directory.mapProjectDirectories) {
          for (var childProjectDirectory
              in projectDirectory.childrenDirectories) {
            await Database.update(
              id: childProjectDirectory.id,
              table: "childProjectList",
              checkDatabase: Database.childProjectDirectoryQuery,
              dataToUpdate: {
                'enabledDefault':
                    childProjectDirectory.enabledDefault == true ? 1 : 0,
                'localId': childProjectDirectory.id,
                'mapProjectId': childProjectDirectory.mapProjectId,
                'mapProjectDirectoryId':
                    childProjectDirectory.mapProjectDirectoryId,
                'name': childProjectDirectory.name,
                'activated': childProjectDirectory.activated == true ? 1 : 0,
                'orderNumber': childProjectDirectory.orderNumber,
                'serviceId': childProjectDirectory.serviceId,
                'rootServiceDirectory':
                    childProjectDirectory.rootServiceDirectory == true ? 1 : 0,
                'createdBy': childProjectDirectory.createdBy,
                'timeCreated': childProjectDirectory.timeCreated,
              },
            );

            for (var childLayerDirectory in childProjectDirectory.layers) {
              await Database.update(
                checkDatabase: Database.childLayerDirectoryQuery,
                id: childLayerDirectory.id,
                table: "childLayerList",
                dataToUpdate: {
                  'localId': childLayerDirectory.id,
                  'mapProjectId': childLayerDirectory.mapProjectId,
                  'mapProjectDirectoryId':
                      childLayerDirectory.mapProjectDirectoryId,
                  'name': childLayerDirectory.name,
                  'activated': childLayerDirectory.activated == true ? 1 : 0,
                  'orderNumber': childLayerDirectory.orderNumber,
                  'protocol': childLayerDirectory.protocol,
                  'uri': childLayerDirectory.uri,
                  'createdBy': childLayerDirectory.createdBy,
                  'timeCreated': childLayerDirectory.timeCreated,
                  'enabledDefault':
                      childLayerDirectory.enabledDefault == true ? 1 : 0,
                  'legendLink': childLayerDirectory.legendLink,
                  'geoserverLayerId': childLayerDirectory.geoserverLayerId,
                  'cqlFilter': childLayerDirectory.cqlFilter,
                  'moduleId': childLayerDirectory.moduleId,
                  'dataSearchMode': childLayerDirectory.dataSearchMode,
                  'showInfoBlock':
                      childLayerDirectory.showInfoBlock == true ? 1 : 0,
                },
              );
            }
          }

          await Database.update(
              id: projectDirectory.id,
              table: "projectList",
              dataToUpdate: {
                'localId': projectDirectory.id,
                'mapProjectId': projectDirectory.mapProjectId,
                'mapProjectDirectoryId': projectDirectory.mapProjectDirectoryId,
                'name': projectDirectory.name,
                'activated': projectDirectory.activated == true ? 1 : 0,
                'orderNumber': projectDirectory.orderNumber,
                'serviceId': projectDirectory.serviceId,
                'rootServiceDirectory':
                    projectDirectory.rootServiceDirectory == true ? 1 : 0,
                'createdBy': projectDirectory.createdBy,
                'timeCreated': projectDirectory.timeCreated,
              },
              checkDatabase: Database.projectDirectoryQuery);
          for (var layerDirectory in projectDirectory.layers) {
            await Database.update(
              id: layerDirectory.id,
              checkDatabase: Database.layerDirectoryQuery,
              table: 'layerList',
              dataToUpdate: {
                'localId': layerDirectory.id,
                'mapProjectId': layerDirectory.mapProjectId,
                'mapProjectDirectoryId': layerDirectory.mapProjectDirectoryId,
                'name': layerDirectory.name,
                'activated': layerDirectory.activated == true ? 1 : 0,
                'orderNumber': layerDirectory.orderNumber,
                'protocol': layerDirectory.protocol,
                'uri': layerDirectory.uri,
                'createdBy': layerDirectory.createdBy,
                'timeCreated': layerDirectory.timeCreated,
                'enabledDefault': layerDirectory.enabledDefault == true ? 1 : 0,
                'legendLink': layerDirectory.legendLink,
                'geoserverLayerId': layerDirectory.geoserverLayerId,
                'cqlFilter': layerDirectory.cqlFilter,
                'moduleId': layerDirectory.moduleId,
                'dataSearchMode': layerDirectory.dataSearchMode,
                'showInfoBlock': layerDirectory.showInfoBlock == true ? 1 : 0,
              },
            );
          }
        }
      }
    }
  }

  Future<List<DirectoryResponseDto>> getFromDatabase() async {
    List<ProjectDirectoryResponseDto> projectDirectories = [];
    List<LayerDirectoryModel> layerDirectories = [];

    List<DirectoryResponseDto> directories = [];

    var projectDirectoryDB = await Database.getItems(
        query: Database.projectDirectoryQuery, table: 'projectList');
    var projectDB = await Database.getItems(
        query: Database.directoryQuery, table: 'directoryList');
    var layerDirectoryDB = await Database.getItems(
        query: Database.layerDirectoryQuery, table: 'layerList');
    var childLayerDirectoryDB = await Database.getItems(
        query: Database.layerDirectoryQuery, table: 'childLayerList');
    var childProjectDirectoryDB = await Database.getItems(
        query: Database.projectDirectoryQuery, table: 'childProjectList');

    for (var item in projectDirectoryDB) {
      List<ProjectDirectoryResponseDto> childProjectDirectories = [];

      for (var item1 in layerDirectoryDB) {
        if (item1['mapProjectDirectoryId'] == item['localId']) {
          layerDirectories.add(
            LayerDirectoryModel(
                activated: item['activated'] == 1 ? true : false,
                createdBy: item['createdBy'] ?? "",
                id: item['localId'] ?? "",
                mapProjectDirectoryId: item['mapProjectDirectoryId'] ?? "",
                mapProjectId: item['mapProjectId'] ?? "",
                name: item['name'] ?? "",
                orderNumber: item['orderNumber'],
                timeCreated: item['timeCreated'] ?? "",
                dataSearchMode: item['dataSearchMode'] ?? 0,
                cqlFilter: item['cqlFilter'] ?? "",
                enabledDefault: item['enabledDefault'] == 1 ? true : false,
                geoserverLayerId: item['geoserverLayerId'] ?? "",
                legendLink: item['legendLink'] ?? "",
                moduleId: item['moduleId'] ?? "",
                protocol: item['protocol'] ?? "",
                showInfoBlock: item['showInfoBlock'] == 1 ? true : false,
                uri: item['uri'] ?? ""),
          );
        }
      }
//childDirectories Data GET start

      for (var childProjectDirectory in childProjectDirectoryDB) {
        List<LayerDirectoryModel> childLayerDirectories = [];
        for (var childLayer in childLayerDirectoryDB) {
          if (childLayer['mapProjectDirectoryId'] ==
              childProjectDirectory['localId']) {
            childLayerDirectories.add(
              LayerDirectoryModel(
                  activated: childLayer['activated'] == 1 ? true : false,
                  createdBy: childLayer['createdBy'] ?? "",
                  id: childLayer['localId'] ?? "",
                  mapProjectDirectoryId:
                      childLayer['mapProjectDirectoryId'] ?? "",
                  mapProjectId: childLayer['mapProjectId'] ?? "",
                  name: childLayer['name'] ?? "",
                  orderNumber: childLayer['orderNumber'],
                  timeCreated: childLayer['timeCreated'] ?? "",
                  dataSearchMode: childLayer['dataSearchMode'] ?? 0,
                  cqlFilter: childLayer['cqlFilter'] ?? "",
                  enabledDefault:
                      childLayer['enabledDefault'] == 1 ? true : false,
                  geoserverLayerId:
                      childProjectDirectory['geoserverLayerId'] ?? "",
                  legendLink: childProjectDirectory['legendLink'] ?? "",
                  moduleId: childProjectDirectory['moduleId'] ?? "",
                  protocol: childProjectDirectory['protocol'] ?? "",
                  showInfoBlock: childProjectDirectory['showInfoBlock'] == 1
                      ? true
                      : false,
                  uri: childProjectDirectory['uri'] ?? ""),
            );
          }
        }
        if (item['localId'] == childProjectDirectory['mapProjectDirectoryId']) {
          childProjectDirectories.add(ProjectDirectoryResponseDto(
            childrenDirectories: [],
            layers: childLayerDirectories,
            enabledDefault:
                childProjectDirectory['enabledDefault'] == 1 ? true : false,
            activated: childProjectDirectory['activated'] == 1 ? true : false,
            createdBy: childProjectDirectory['createdBy'] ?? "",
            id: childProjectDirectory['localId'] ?? "",
            mapProjectDirectoryId:
                childProjectDirectory['mapProjectDirectoryId'],
            mapProjectId: childProjectDirectory['mapProjectId'] ?? "",
            name: childProjectDirectory['name'] ?? "",
            orderNumber: childProjectDirectory['orderNumber'] ?? "",
            rootServiceDirectory:
                childProjectDirectory['rootServiceDirectory'] == 1
                    ? true
                    : false,
            serviceId: childProjectDirectory['serviceId'] ?? "",
            timeCreated: childProjectDirectory['timeCreated'] ?? "",
          ));
        }
      }
//childDirectories Data GET end

      projectDirectories.add(ProjectDirectoryResponseDto(
        childrenDirectories: childProjectDirectories,
        layers: layerDirectories,
        enabledDefault: item['enabledDefault'] == 1 ? true : false,
        activated: item['activated'] == 1 ? true : false,
        createdBy: item['createdBy'] ?? "",
        id: item['localId'] ?? "",
        mapProjectDirectoryId: item['mapProjectDirectoryId'],
        mapProjectId: item['mapProjectId'] ?? "",
        name: item['name'] ?? "",
        orderNumber: item['orderNumber'] ?? "",
        rootServiceDirectory: item['rootServiceDirectory'] == 1 ? true : false,
        serviceId: item['serviceId'] ?? "",
        timeCreated: item['timeCreated'] ?? "",
      ));
    }

// collecting all data retrieved from db to list of directory models to return
    for (var item in projectDB) {
      directories.add(
        DirectoryResponseDto(
            name: item['name'] ?? '',
            orderNumber: item['orderNumber'] ?? 0,
            timeCreated: item['timeCreated'] ?? '',
            activated: item['activated'] == 1 ? true : false,
            center: item['center'] ?? '',
            createdBy: item['createdbBy'] ?? '',
            id: item['localId'] ?? '',
            moduleId: item['moduleId'] ?? '',
            zoomLevel: item['zoomLevel'],
            mapProjectDirectories: projectDirectories
                .where((element) => element.mapProjectId == item['localId'])
                .toList(),
            mapProjectLayers: layerDirectories
                .where((element) => element.mapProjectId == item['localId'])
                .toList()),
      );
    }

    ProjectsModel? _projects = _box.get("projectCached");
    print("PROJECT IN DATABASE = ${_projects!.projects}");
    List<DirectoryResponseDto> _res = [];
    for (DirectoryResponseDtoHiveModel project in _projects.projects) {
      _res.add(
        DirectoryResponseDto(
            activated: project.activated,
            center: project.center,
            createdBy: project.createdBy,
            id: project.id,
            mapProjectDirectories:
                getProjectDirectoryDB(project.mapProjectDirectories),
            moduleId: project.moduleId,
            name: project.name,
            orderNumber: project.orderNumber,
            timeCreated: project.timeCreated,
            zoomLevel: project.zoomLevel,
            mapProjectLayers: getLayerDirectoryDB(project.mapProjectLayers)),
      );
    }

    return _res;
  }

  List<TileLayerOptions> _getWmsOverlayOptions(List<LayerModel> layers) {
    /// Get Test String
    String getTestString(List<Map<String, dynamic>> configs) {
      String testString = "";
      for (final config in configs) {
        if (config["nick"] != "layers") {
          testString += "${config["value"]};";
        }
      }
      return testString;
    }

    /// Transform config to Map
    Map<String, dynamic> configsListToMap(List<Map<String, dynamic>> configs) {
      return {for (final config in configs) config["nick"]: config["value"]};
    }

    List<Map<String, dynamic>> layersGroup = [];
    for (final layer in layers) {
      String testString = getTestString(layer.mapProjectLayerConfigs);
      var foundIndex = -1;
      for (int i = 0; i < layersGroup.length; i++) {
        if (layersGroup[i]["testString"] == testString &&
            layersGroup[i]["uri"] == layer.uri) {
          foundIndex = i;
          break;
        }
      }
      final configsMap = configsListToMap(layer.mapProjectLayerConfigs);
      if (foundIndex == -1) {
        layersGroup.add(<String, dynamic>{
          'testString': testString,
          'layers': <String>[configsMap["layers"]],
          'uri': layer.uri,
          for (final mapEntry in configsMap.entries)
            if (mapEntry.key != 'layers') mapEntry.key: mapEntry.value,
        });
      } else {
        layersGroup[foundIndex]["layers"].add(configsMap['layers']);
      }
    }

    final List<TileLayerOptions> tileLayerOptions = [];
    for (final layerGroup in layersGroup) {
      tileLayerOptions.add(TileLayerOptions(
        backgroundColor: Colors.transparent,
        wmsOptions: WMSTileLayerOptions(
            baseUrl: layerGroup['uri'] + "?",
            layers: layerGroup['layers'],
            format: layerGroup['format'],
            version: "1.12.2",
            transparent: layerGroup['transparent'],
            otherParameters: {
              for (final option in layerGroup.entries)
                if (!["layers", "format", "transparent", "uri"]
                    .contains(option.key))
                  option.key: option.value.toString(),
            }),
      ));
    }
    return tileLayerOptions;
  }
}
