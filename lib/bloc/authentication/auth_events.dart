import 'package:equatable/equatable.dart';

class AuthEvents extends Equatable {
  @override
  // TODO: implement props
  List<Object?> get props => [];
}

class LoginUserEvent extends AuthEvents {
  final String login;
  final String password;
  LoginUserEvent({required this.login, required this.password});
}

class LogOutUserEvent extends AuthEvents {

  LogOutUserEvent();
}
