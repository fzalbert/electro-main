import 'package:json_annotation/json_annotation.dart';

part 'login_response.g.dart';

@JsonSerializable()
class LoginResponseDto {
  @JsonKey()
  final String token;
  @JsonKey()
  final String userId;

  LoginResponseDto({
    required this.token,
    required this.userId
  });

  factory LoginResponseDto.fromJson(Map<String, dynamic> json) =>
      _$LoginResponseDtoFromJson(json);

  Map<String,dynamic> toJson() => _$LoginResponseDtoToJson(this);
}
