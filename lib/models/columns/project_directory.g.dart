// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'project_directory.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProjectDirectoryResponseDto _$ProjectDirectoryResponseDtoFromJson(
        Map<String, dynamic> json) =>
    ProjectDirectoryResponseDto(
      activated: json['Activated'] as bool,
      createdBy: json['CreatedBy'] as String? ?? '',
      layers: (json['Layers'] as List<dynamic>?)
              ?.map((e) =>
                  LayerDirectoryModel.fromJson(e as Map<String, dynamic>))
              .toList() ??
          [],
      childrenDirectories: (json['ChildrenDirectories'] as List<dynamic>)
          .map((e) =>
              ProjectDirectoryResponseDto.fromJson(e as Map<String, dynamic>))
          .toList(),
      id: json['Id'] as String? ?? '',
      enabledDefault: json['EnabledDefault'] as bool,
      mapProjectDirectoryId: json['MapProjectDirectoryId'] as String? ?? '',
      mapProjectId: json['MapProjectId'] as String? ?? '',
      name: json['Name'] as String? ?? '',
      orderNumber: json['OrderNumber'] as int? ?? 0,
      rootServiceDirectory: json['RootServiceDirectory'] as bool,
      serviceId: json['ServiceId'] as String? ?? '',
      timeCreated: json['TimeCreated'] as String? ?? '',
    );

Map<String, dynamic> _$ProjectDirectoryResponseDtoToJson(
        ProjectDirectoryResponseDto instance) =>
    <String, dynamic>{
      'Id': instance.id,
      'MapProjectId': instance.mapProjectId,
      'MapProjectDirectoryId': instance.mapProjectDirectoryId,
      'Name': instance.name,
      'Activated': instance.activated,
      'OrderNumber': instance.orderNumber,
      'ServiceId': instance.serviceId,
      'RootServiceDirectory': instance.rootServiceDirectory,
      'CreatedBy': instance.createdBy,
      'TimeCreated': instance.timeCreated,
      'EnabledDefault': instance.enabledDefault,
      'Layers': instance.layers,
      'ChildrenDirectories': instance.childrenDirectories,
    };
