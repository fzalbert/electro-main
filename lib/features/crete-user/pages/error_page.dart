import 'package:electro/app/theme/theme.dart';
import 'package:electro/features/crete-user/fields/image_upload.dart';
import 'package:electro/features/projects/components/editing_textfield.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class ErrorPage extends StatelessWidget {
  final String? errorText;
  const ErrorPage({Key? key, this.errorText}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.4,
      padding: const EdgeInsets.fromLTRB(16, 44, 16, 0),
      child: Center(
        child: Text(
          errorText ?? "Попробуйте еще раз",
          style: Theme.of(context).textTheme.headline3!.copyWith(
                fontSize: 20,
              ),
        ),
      ),
    );
  }
}
