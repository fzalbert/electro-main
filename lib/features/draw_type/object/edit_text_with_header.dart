import 'package:electro/app/theme/theme.dart';
import 'package:electro/data/utils/functions.dart';
import 'package:electro/features/projects/components/editing_textfield.dart';
import 'package:electro/utils/string_helper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class EditTextWithHeader extends StatefulWidget {
  String title;
  String type;
  dynamic value;
  bool? isRequired;
  Map<String, String>? optionSelect;

  TextEditingController controller;
  bool isEditable;

  EditTextWithHeader({
    Key? key,
    required this.title,
    required this.type,
    required this.value,
    required this.controller,
    this.isRequired,
    this.optionSelect,
    this.isEditable = true,
  }) : super(key: key);

  @override
  _EditTextWithHeaderState createState() => _EditTextWithHeaderState();
}

class _EditTextWithHeaderState extends State<EditTextWithHeader> {
  String? selectValue;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (widget.type == 'select') {
      print("Select value = ${widget.controller.text}");
      selectValue = widget.controller.text != ""
          ? widget.controller.text
          : widget.optionSelect?.values.first;
      widget.controller.text = widget.optionSelect!.values.first;
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            widget.isRequired != null && widget.isRequired!
                ? widget.title.capitalize() + "*"
                : widget.title.capitalize(),
            style: Theme.of(context)
                .textTheme
                .headline3!
                .copyWith(fontSize: 12, color: AppTheme.textGreyDark),
          ),
          const SizedBox(
            height: 8,
          ),
          Row(
            children: [
              if (widget.type == 'select')
                Expanded(
                    child: _createSelector(
                        context, widget.controller, widget.title))
              else if (widget.type == 'date')
                Expanded(
                  child: InputDatePickerFormField(
                    firstDate: DateTime(2009),
                    lastDate: DateTime(2020, 12, 12),
                    initialDate: DateTime(2010, 12, 12),
                  ),
                )
              else if (widget.type == 'smalltext' ||
                  widget.type == 'textarea' ||
                  widget.type == 'number' ||
                  widget.type == 'realnumber')
                Expanded(
                  child: EditingTextField(
                    currentText: widget.value,
                    controller: widget.controller,
                    enabled: widget.isEditable,
                    isRequired: widget.isRequired,
                  ),
                ),
            ],
          ),
        ],
      ),
    );
  }

  void updateSelectValue(String newSelectValue) {
    setState(() {
      selectValue = newSelectValue;
      widget.controller.text = newSelectValue;
    });
  }

  Widget _createSelector(
      BuildContext context, TextEditingController controller, String title) {
    return CupertinoButton(
      onPressed: () {
        MyFunctions.showSelector(
          title: title,
          context: context,
          values: widget.optionSelect!.values.toList(),
          callback: updateSelectValue,
        );
      },
      padding: EdgeInsets.all(0),
      child: Container(
        padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(9.0),
            border: Border.all(
                color: const Color(0xffF3F3F3),
                style: BorderStyle.solid,
                width: 1),
            color: const Color(0xffF8F8F8)),
        child: Row(
          children: [
            Expanded(
              child: Text(
                selectValue!,
                style: Theme.of(context).textTheme.headline4!.copyWith(
                      fontSize: 16,
                    ),
              ),
            ),
            const Spacer(),
            const Icon(Icons.arrow_drop_down, color: AppTheme.textGreyDark)
          ],
        ),
      ),
    );
  }
}

class SelectorSheet extends StatefulWidget {
  final String title;
  final List<String> values;
  final Function callback;
  const SelectorSheet(
      {Key? key,
      required this.title,
      required this.values,
      required this.callback})
      : super(key: key);

  @override
  _SelectorSheetState createState() => _SelectorSheetState();
}

class _SelectorSheetState extends State<SelectorSheet> {
  @override
  Widget build(BuildContext context) {
    print("VALS = ${widget.values}");
    return Container(
      height: MediaQuery.of(context).size.height * 0.6,
      padding: const EdgeInsets.fromLTRB(16, 44, 16, 0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            widget.title,
            style: Theme.of(context).textTheme.headline3!.copyWith(
                  fontSize: 20,
                ),
          ),
          SizedBox(
            height: 20,
          ),
          Expanded(
            child: ListView.separated(
              itemCount: widget.values.length,
              itemBuilder: (BuildContext context, int index) {
                return CupertinoButton(
                  onPressed: () {
                    widget.callback(widget.values[index]);
                    Navigator.pop(context);
                  },
                  padding: EdgeInsets.all(0),
                  child: Container(
                    child: Text(
                      widget.values[index],
                      style: Theme.of(context).textTheme.headline4!.copyWith(
                            fontSize: 16,
                          ),
                    ),
                  ),
                );
              },
              separatorBuilder: (BuildContext context, int index) =>
                  const Divider(),
            ),
          )
        ],
      ),
    );
  }
}
