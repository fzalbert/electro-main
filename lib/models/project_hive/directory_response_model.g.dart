// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'directory_response_model.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class DirectoryResponseDtoHiveModelAdapter
    extends TypeAdapter<DirectoryResponseDtoHiveModel> {
  @override
  final int typeId = 1;

  @override
  DirectoryResponseDtoHiveModel read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return DirectoryResponseDtoHiveModel(
      activated: fields[6] as bool,
      center: fields[4] as String,
      createdBy: fields[2] as String,
      id: fields[0] as String,
      mapProjectDirectories:
          (fields[9] as List).cast<ProjectDirectoryResponseDtoHiveModel>(),
      moduleId: fields[8] as String,
      name: fields[1] as String,
      orderNumber: fields[7] as int,
      timeCreated: fields[3] as String,
      zoomLevel: fields[5] as int,
      mapProjectLayers: (fields[10] as List).cast<LayerDirectoryHiveModel>(),
    );
  }

  @override
  void write(BinaryWriter writer, DirectoryResponseDtoHiveModel obj) {
    writer
      ..writeByte(11)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.name)
      ..writeByte(2)
      ..write(obj.createdBy)
      ..writeByte(3)
      ..write(obj.timeCreated)
      ..writeByte(4)
      ..write(obj.center)
      ..writeByte(5)
      ..write(obj.zoomLevel)
      ..writeByte(6)
      ..write(obj.activated)
      ..writeByte(7)
      ..write(obj.orderNumber)
      ..writeByte(8)
      ..write(obj.moduleId)
      ..writeByte(9)
      ..write(obj.mapProjectDirectories)
      ..writeByte(10)
      ..write(obj.mapProjectLayers);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is DirectoryResponseDtoHiveModelAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
