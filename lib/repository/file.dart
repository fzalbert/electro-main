import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:dio/dio.dart';
import 'package:electro/data/singletons/dio_settings.dart';
import 'package:electro/data/singletons/storage.dart';
import 'package:path_provider/path_provider.dart';

class FileRepository {
  final Dio _dio = DioSettings.createDio(connectTimeout: 30000);

  Future<Map> postFile({required File file}) async {
    String fileName = file.path.split('/').last;
    FormData formData = FormData.fromMap({
      "file": await MultipartFile.fromFile(file.path, filename: fileName),
    });
    final Response response = await _dio.post(
      'UploadFileDocument',
      data: formData,
      options: Options(headers: {
        'Authorization': 'Bearer ${StorageRepository.getString('token')}'
      }),
    );

    return response.data;
  }

  Future<File> downloadFile(
      {required String idFile, required String arendatorId}) async {
    final Response response = await _dio.get(
      'DownloadFileDocument',
      queryParameters: {
        "id": idFile,
        "typesId": arendatorId,
      },
      options: Options(headers: {
        'Authorization': 'Bearer ${StorageRepository.getString('token')}'
      }),
    );

    File file = await _createFileFromString(
        encodedStr: response.data["file"], fileName: response.data["name"]);

    return file;
  }

  Future<File> _createFileFromString(
      {required String encodedStr, required String fileName}) async {
    Uint8List bytes = base64.decode(encodedStr);
    String dir = (await getApplicationDocumentsDirectory()).path;
    File file = File("$dir/" + fileName);

    await file.writeAsBytes(bytes);
    return file;
  }
}
