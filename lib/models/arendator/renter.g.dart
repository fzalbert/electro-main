// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'renter.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RenterRequestDto _$RenterRequestDtoFromJson(Map<String, dynamic> json) =>
    RenterRequestDto(
      idList:
          (json['RenterIds'] as List<dynamic>).map((e) => e as String).toList(),
      typeId: json['RenterTypesId'] as String,
    );

Map<String, dynamic> _$RenterRequestDtoToJson(RenterRequestDto instance) =>
    <String, dynamic>{
      'RenterIds': instance.idList,
      'RenterTypesId': instance.typeId,
    };
