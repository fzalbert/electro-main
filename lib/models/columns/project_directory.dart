import 'package:electro/models/columns/layer_directory.dart';
import 'package:json_annotation/json_annotation.dart';

part 'project_directory.g.dart';

@JsonSerializable()
class ProjectDirectoryResponseDto {
  @JsonKey(defaultValue: '', name: 'Id')
  final String id;

  @JsonKey(defaultValue: '', name: 'MapProjectId')
  final String mapProjectId;

  @JsonKey(defaultValue: '', name: 'MapProjectDirectoryId')
  final String mapProjectDirectoryId;

  @JsonKey(defaultValue: '', name: 'Name')
  final String name;

  @JsonKey(name: 'Activated')
  final bool activated;

  @JsonKey(defaultValue: 0, name: 'OrderNumber')
  final int orderNumber;

  @JsonKey(defaultValue: '', name: 'ServiceId')
  final String serviceId;

  @JsonKey(name: 'RootServiceDirectory')
  final bool rootServiceDirectory;

  @JsonKey(defaultValue: '', name: 'CreatedBy')
  final String createdBy;

  @JsonKey(defaultValue: '', name: 'TimeCreated')
  final String timeCreated;

  @JsonKey(name: 'EnabledDefault')
  final bool enabledDefault;

  @JsonKey(defaultValue: [], name: 'Layers')
  final List<LayerDirectoryModel> layers;

  @JsonKey(name: 'ChildrenDirectories')
  final List<ProjectDirectoryResponseDto> childrenDirectories;

  ProjectDirectoryResponseDto(
      {required this.activated,
      required this.createdBy,
      required this.layers,
      required this.childrenDirectories,
      required this.id,
      required this.enabledDefault,
      required this.mapProjectDirectoryId,
      required this.mapProjectId,
      required this.name,
      required this.orderNumber,
      required this.rootServiceDirectory,
      required this.serviceId,
      required this.timeCreated});

  factory ProjectDirectoryResponseDto.fromJson(Map<String, dynamic> json) =>
      _$ProjectDirectoryResponseDtoFromJson(json);
}
