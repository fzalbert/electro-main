import 'package:electro/app/constants/colors.dart';
import 'package:electro/bloc/authentication/auth_bloc.dart';
import 'package:electro/bloc/authentication/auth_events.dart';
import 'package:electro/bloc/authentication/auth_state.dart';
import 'package:electro/data/singletons/storage.dart';
import 'package:electro/features/map/map.dart';
import 'package:electro/repository/auth.dart';
import 'package:electro/widgets/btns/rounded_button.dart';
import 'package:electro/widgets/text_field_with_header.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:formz/formz.dart';

class AuthBodyWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _AuthBodyState();
}

class _AuthBodyState extends State<AuthBodyWidget> {
  final TextEditingController _loginController = TextEditingController();
  late final TextEditingController _passwordController =
      TextEditingController();
  late AuthenticationRepository repository = AuthenticationRepository();
  late AuthBloc bloc;
  final _kName = GlobalKey<FormState>();
  final _kPassword = GlobalKey<FormState>();

  // var _maskFormatter = new MaskTextInputFormatter(
  //     mask: '+# (###) ###-##-##', filter: {"#": RegExp(r'[0-9]')});

  static const double _horizontalMargin = 15;
  double _topMargin = 143;

  @override
  void initState() {
    bloc = AuthBloc(repository: repository);

    super.initState();
  }

  bool checkFields() {
    var currentPassed = 0;
    List<GlobalKey<FormState>> list = [
      _kName,
      _kPassword,
    ];
    for (var item in list) {
      if (item.currentState!.validate()) {
        currentPassed++;
      }
    }
    if (currentPassed == 2) {
      return true;
    } else {
      return
      false;
    }
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    double margin = 38.0;

    SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
      statusBarColor: Colors.white,
      statusBarBrightness: Brightness.dark,
    ));

    return BlocProvider.value(
      value: bloc,
      child: BlocConsumer<AuthBloc, AuthState>(listener: (context1, state) {
        if (state.loginStatus == FormzStatus.submissionSuccess) {
          var result = StorageRepository.getString('token');
          if (result.isNotEmpty) {
            Navigator.pushAndRemoveUntil(
                context,
                CupertinoPageRoute(builder: (c) => MapScreenWidget()),
                (route) => false);
          }
        } else if (state.loginStatus == FormzStatus.invalid) {
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text(
              'Неверное имя пользователя или пароль',
              style: Theme.of(context).textTheme.headline3!.copyWith(color: Colors.white),
            ),
            duration: Duration(milliseconds: 2000),
          ));
        }
      }, builder: (context, state) {
        return Stack(
          children: [
            Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: _horizontalMargin),
              child: SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(height: _topMargin),
                    const Text(
                      "Введите ваше данные",
                      textAlign: TextAlign.start,
                      style: TextStyle(
                        fontSize: 20,
                        color: Colors.black,
                        fontWeight: FontWeight.w800,
                      ),
                    ),
                    const SizedBox(height: 8),
                    const Text(
                      "Чтобы получить доступ\nк данным необходимо войти",
                      textAlign: TextAlign.start,
                      style: TextStyle(
                        fontSize: 14,
                        color: Color.fromRGBO(102, 102, 102, 1),
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    const SizedBox(height: 28),
                    const Text(
                      "ВАШЕ ИМЯ",
                      style: TextStyle(
                          fontWeight: FontWeight.w500,
                          color: Color.fromARGB(255, 102, 102, 102),
                          fontSize: 12,
                          letterSpacing: 0.8),
                    ),
                    const SizedBox(
                      height: 8.0,
                    ),
                    HeaderedTextField(
                      formKey: _kName,
                      errorText: '',
                      hasError: state.loginStatus == FormzStatus.invalid,
                      isPassword: false,
                      controller: _loginController,
                      hint: "Введите имя",
                    ),
                    const SizedBox(height: 12),
                    const Text(
                      "Пароль",
                      style: TextStyle(
                          fontWeight: FontWeight.w500,
                          color: Color.fromARGB(255, 102, 102, 102),
                          fontSize: 12,
                          letterSpacing: 0.8),
                    ),
                    const SizedBox(
                      height: 8.0,
                    ),
                    HeaderedTextField(
                      formKey: _kPassword,
                      hasError: state.loginStatus == FormzStatus.invalid,
                      errorText: '',
                      isPassword: true,
                      controller: _passwordController,
                      hint: "Введите пароль",
                    ),
                    const SizedBox(height: 28),
                    RoundedButton(
                      title: "Продолжить",
                      height: 48,
                      width: double.infinity,
                      cornerRadius: 10,
                      isLoading: state.loginStatus == FormzStatus.submissionInProgress,
                      onTap: () {
                        FocusScope.of(context).requestFocus(FocusNode());

                        if (checkFields()) {
                          print('ssss');
                          context.read<AuthBloc>().add(LoginUserEvent(
                              login: _loginController.text,
                              password: _passwordController.text));
                        }
                      },
                    ),
                    const SizedBox(height: 16),
                    SizedBox(
                      height: 44,
                      width: double.infinity,
                      child: OutlinedButton(
                        onPressed: () {},
                        style: ButtonStyle(
                          shape: MaterialStateProperty.all(
                            RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10.0)),
                          ),
                        ),
                        child: const Text(
                          "Войти через ЕСИА",
                          style: TextStyle(
                              fontWeight: FontWeight.w500,
                              color: mainColor,
                              fontSize: 16),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            )
          ],
        );
      }),
    );
  }
}
