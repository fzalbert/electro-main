class DocumentAdd {
  String? typeParametersId;
  String? stringData;
  int? numberData;
  double? doubleData;
  String? dateData;
  String? dateTimeData;
  String? coords;
  List<dynamic>? files;
  String? parameterOptionsId;
  List<String>? parameterOptionsIds;

  DocumentAdd({
    this.typeParametersId,
    this.stringData,
    this.numberData,
    this.doubleData,
    this.dateData,
    this.dateTimeData,
    this.coords,
    this.files,
    this.parameterOptionsId,
    this.parameterOptionsIds,
  });

  Map<String, dynamic> toMap() {
    return {
      "TypeParametersId": typeParametersId,
      "StringData": stringData,
      "NumberData": numberData,
      "DoubleData": doubleData,
      "DateData": dateData,
      "DateTimeData": dateTimeData,
      "Coords": coords,
      "Files": files,
      "ParameterOptionsId": parameterOptionsId,
      "ParameterOptionsIds": parameterOptionsIds,
    };
  }
}
