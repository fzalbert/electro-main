import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_map_tile_caching/flutter_map_tile_caching.dart';

import 'tile_loaders.dart';

class Panel extends StatelessWidget {
  const Panel({
    Key? key,
    required this.mcm,
    required this.controller,
    required this.mapSource,
  }) : super(key: key);

  final MapCachingManager mcm;
  final MapController controller;
  final String? mapSource;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(16),
      ),
      padding: const EdgeInsets.only(
        top: 20,
        left: 16,
        right: 16,
        bottom: 20,
      ),
      child: SafeArea(
        child: Container(
          child: TileLoader(
            mcm: mcm,
            controller: controller,
            mapSource: mapSource,
          ),
        ),
      ),
    );
  }
}
