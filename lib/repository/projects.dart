import 'package:dio/dio.dart';
import 'package:electro/data/singletons/dio_settings.dart';
import 'package:electro/data/singletons/storage.dart';
import 'package:electro/models/columns/directory.dart';
import 'package:electro/models/columns/layer.dart';
import 'package:electro/models/object/object_info.dart';
import 'package:electro/utils/geojson_helper.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:latlong2/latlong.dart';

class ProjectsRepository {
  Future<List<DirectoryResponseDto>> getProjectList() async {
    await StorageRepository.getInstance();
    Dio _dio = DioSettings.createDio(connectTimeout: 30000);

    final Response response = await _dio.get(
      'ProjectList/',
      options: Options(headers: {
        'Authorization': 'Bearer ${StorageRepository.getString('token')}'
      }),
    );

    try {
      if (response.statusCode! >= 200 && response.statusCode! < 300) {
        List<DirectoryResponseDto> list = [];
        List<dynamic> result = response.data;
        //print("RESULT IN GET PROJECT LIST ${result[1]["Layers"]}");
        //print(result[1]["ChildrenDirectories"][0]["Layers"][2]);
        for (int i = 0; i < result.length; i++) {
          list.add(DirectoryResponseDto.fromJson(result[i]));
        }
        print("RESULT IN GET PROJECT LIST ${list[1].mapProjectLayers}");
        return list;
      } else {
        throw Exception();
      }
    } catch (e) {
      print(e.toString());
      throw Exception();
    }
  }

  Future<List<List<String>>> getObjectsByCoordAndLayers(
      {required LatLng pos, required List<String> layerIds}) async {
    List<double> res = GeoJsonCreator.createPointCoord(pos);
    Dio _dio = DioSettings.createDio(connectTimeout: 3000);
    String resultLayersString = "";
    resultLayersString += "[";
    for (String layer in layerIds) {
      resultLayersString += "'$layer',";
    }
    resultLayersString += "]";
    print("RES STRING  $resultLayersString");
    final Response response = await _dio.get(
      'LayersClickData/',
      queryParameters: {
        'lat': res[0],
        "lng": res[1],
        'layerIds': resultLayersString,
      },
      options: Options(headers: {
        'Authorization': 'Bearer ${StorageRepository.getString('token')}'
      }),
    );

    try {
      if (response.statusCode! >= 200 && response.statusCode! < 300) {
        List<List<String>> list = [];
        dynamic result = response.data;
        //print("RESULT IN GET LAYERS LIST ${result[1]}");
        print("DATA = ${result}");
        if (result == false) {
          throw Exception();
        }
        print("LENGTH = ${result.length}");
        try {
          for (var _res in result) {
            list.add([_res["name"], _res["keyIdentifier"]]);
          }
          return list;
        } catch (e) {
          return [
            [result[0]["ObjectId"]]
          ];
        }
      } else {
        throw Exception();
      }
    } catch (e) {
      print(e.toString());
      throw Exception();
    }
  }

  Future<ObjectInfo> getObjectByType(
      {required String typeId, required List<String> layerIds}) async {
    Dio _dio = DioSettings.createDio(connectTimeout: 30000);
    String resultLayersString = "";
    resultLayersString += "[";
    for (String layer in layerIds) {
      resultLayersString += "'$layer',";
    }
    resultLayersString += "]";
    print("RES STRING  $resultLayersString");

    try {
      final Response response = await _dio.get(
        'LayersDataByKeyIdentifier/',
        queryParameters: {
          'keyIdentifier': typeId,
          'layerIds': resultLayersString,
        },
        options: Options(headers: {
          'Authorization': 'Bearer ${StorageRepository.getString('token')}'
        }),
      );

      if (response.statusCode! >= 200 && response.statusCode! < 300) {
        List<String> list = [];
        dynamic result = response.data;
        //print("RESULT IN GET LAYERS LIST ${result[1]}");
        print("DATA = ${result}");
        ObjectInfo object = ObjectInfo.fromJson(result[0]);

        return object;
      } else {
        throw Exception();
      }
    } catch (e) {
      print(e.toString());
      throw Exception();
    }
  }

  Future<List<LayerModel>> getLayersList(
      {String? projectId, String? directoryId, String? layerId}) async {
    await StorageRepository.getInstance();
    Dio _dio = DioSettings.createDio(connectTimeout: 30000);
    final Response response = await _dio.get(
      'GetLayers/',
      queryParameters: {
        if (projectId != null) 'projectId': projectId,
        if (directoryId != null) 'directoryId': directoryId,
        if (layerId != null) 'layerId': layerId,
      },
      options: Options(headers: {
        'Authorization': 'Bearer ${StorageRepository.getString('token')}'
      }),
    );

    try {
      if (response.statusCode! >= 200 && response.statusCode! < 300) {
        List<LayerModel> list = [];
        List<dynamic> result = response.data;
        //print("RESULT IN GET LAYERS LIST ${result[1]}");
        for (var element in result) {
          list.add(LayerModel.fromJson(element));
        }
        return list;
      } else {
        throw Exception();
      }
    } catch (e) {
      print(e.toString());
      throw Exception();
    }
  }
}
