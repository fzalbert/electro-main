import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:electro/data/singletons/storage.dart';
import 'package:dio/dio.dart';
import 'package:electro/data/singletons/dio_settings.dart';
import 'package:electro/models/arendator/arendator_types.dart';
import 'package:electro/models/arendator/document_structure.dart';
import 'package:path_provider/path_provider.dart';


class ArendatorRepository {
  final Dio _dio = Dio(DioSettings().dioBaseOptions);

  Future<List<dynamic>> getTypesArendator() async {
    print('getTypesArendator');
    try {
      final Response response = await _dio.get(
        'DocumentTypes?limit=10&offset=0&order=ASC&sort&search/',
        options: Options(headers: {
          'Authorization': 'Bearer ${StorageRepository.getString('token')}'
        }),
      );



        if (response.statusCode! >= 200 && response.statusCode! < 300) {
          StorageRepository.putString('cacheDocumentTypes', jsonEncode(response.data));
          Arendator arendator = Arendator.fromJson(response.data);
          print('test rent');
          print(arendator.rows);
          return arendator.rows;
        }

    } catch (e) {

      //print(e);
      Map<String, dynamic> cache = jsonDecode(StorageRepository.getString('cacheDocumentTypes')) as Map<String, dynamic>;
      Arendator arendator = Arendator.fromJson(cache);
      return arendator.rows;
    }




    return [];
  }

  Future<List<dynamic>> getDocumentStructureForArendator(
      {required String arendatorId}) async {
    print('getDocumentStructureForArendator');
    try {
      final Response response = await _dio.get(
        'GetDocumentAdd',
        queryParameters: {"id": arendatorId},
        options: Options(headers: {
          'Authorization': 'Bearer ${StorageRepository.getString('token')}'
        }),
      );

      if (response.statusCode! >= 200 && response.statusCode! < 300) {
        StorageRepository.putString('cachegetDocumentStructureForArendator'+arendatorId, jsonEncode(response.data));
        Document documentStructure = Document.fromJson(response.data);
        if (documentStructure.parameters != null) {
          //print(
          //"ПЕРЕКЛЮЧАТЕЛЬ = ${documentStructure.parameters![4].parameterOptions.length}");

          return documentStructure.parameters!;
        }
      }

    } catch (e) {

      //print(e);
      Map<String, dynamic> cache = jsonDecode(StorageRepository.getString('cachegetDocumentStructureForArendator'+arendatorId)) as Map<String, dynamic>;
      Document documentStructure = Document.fromJson(cache);

      if (documentStructure.parameters != null) {
        //print(
        //"ПЕРЕКЛЮЧАТЕЛЬ = ${documentStructure.parameters![4].parameterOptions.length}");

        return documentStructure.parameters!;
      }


    }

    return [];
  }

  Future<List<dynamic>> getDocumentEditForArendator(
      {required String arendatorId, required String typesId}) async {
    print('getDocumentEditForArendator');
    try {
      final Response response = await _dio.get(
        'GetDocumentEdit',
        queryParameters: {
          "id": typesId,
          "typesId": arendatorId,
        },
        options: Options(headers: {
          'Authorization': 'Bearer ${StorageRepository.getString('token')}'
        }),
      );

      if (response.statusCode! >= 200 && response.statusCode! < 300) {
        StorageRepository.putString('cachegetDocumentEditForArendator', jsonEncode(response.data));
        Document documentStructure = Document.fromJson(response.data);
        if (documentStructure.parameters != null) {
          print("PARAMS = ${documentStructure.parameters}");
          //print(
          //"ПЕРЕКЛЮЧАТЕЛЬ = ${documentStructure.parameters![4].parameterOptions.length}");

          return documentStructure.parameters!;
        }
      }

    } catch (e) {

      //print(e);
      Map<String, dynamic> cache = jsonDecode(StorageRepository.getString('cachegetDocumentEditForArendator')) as Map<String, dynamic>;
      Document documentStructure = Document.fromJson(cache);
      if (documentStructure.parameters != null) {
        print("PARAMS = ${documentStructure.parameters}");
        //print(
        //"ПЕРЕКЛЮЧАТЕЛЬ = ${documentStructure.parameters![4].parameterOptions.length}");

        return documentStructure.parameters!;
      }


    }

    return [];
  }

  Future<List<dynamic>> getColumns({required String arendatorId}) async {
    print('getColumns');
    try {
      final Response response = await _dio.get(
        'DocumentsTypeColumns',
        queryParameters: {
          "id": arendatorId,
        },
        options: Options(headers: {
          'Authorization': 'Bearer ${StorageRepository.getString('token')}'
        }),
      );

      if (response.statusCode! >= 200 && response.statusCode! < 300) {
        StorageRepository.putString('cachegetColumns'+arendatorId, jsonEncode(response.data));
        print('test rr');
        print(response.data);
        return response.data;
      }

    } catch (e) {

      //print(e);
      List<dynamic> cache = jsonDecode(StorageRepository.getString('cachegetColumns'+arendatorId)) as List<dynamic>;

      return cache;


    }



    return [];
  }

  Future<List<Map>> getDocumentsForArendator(
      {required String arendatorId}) async {
    print('getDocumentsForArendator');
    try {
      print("ID = $arendatorId");
      final Response response = await _dio.get(
        'DocumentsType',
        queryParameters: {
          "id": arendatorId,
          "limit": 10,
          "offset": 0,
          "order": "ASC&sort&search"
        },
        options: Options(headers: {
          'Authorization': 'Bearer ${StorageRepository.getString('token')}'
        }),
      );
      print("DATA = ${response.data}");
      StorageRepository.putString('cachegetDocumentsForArendator'+arendatorId, jsonEncode(response.data));
      List<dynamic> documents = response.data['rows'];
      List<dynamic> columns = await getColumns(arendatorId: arendatorId);
      List<Map> map = [];

      for (dynamic document in documents) {
        Map documentMap = <String, dynamic>{};
        for (int i = 1; i < columns.length; i++) {
          documentMap[columns[i]["title"]] = document[columns[i]["field"]];
          documentMap["documentId"] = document["id"];
          documentMap["arendatorId"] = arendatorId;
        }
        map.add(documentMap);
      }
      print("DOCUMENTS = ${map}");

      return map;

    } catch (e) {

      //print(e);
      Map<String, dynamic> cache = jsonDecode(StorageRepository.getString('cachegetDocumentsForArendator'+arendatorId)) as Map<String, dynamic>;
      List<dynamic> documents = cache['rows'];
      List<dynamic> columns = await getColumns(arendatorId: arendatorId);
      List<Map> map = [];

      for (dynamic document in documents) {
        Map documentMap = <String, dynamic>{};
        for (int i = 1; i < columns.length; i++) {
          documentMap[columns[i]["title"]] = document[columns[i]["field"]];
          documentMap["documentId"] = document["id"];
          documentMap["arendatorId"] = arendatorId;
        }
        map.add(documentMap);
      }
      print("DOCUMENTS = ${map}");

      return map;


    }




  }

  Future<void> postDocument({required Map documentMap}) async {


    final Response response = await _dio.post(
      'DocumentAdd',
      data: jsonEncode(documentMap),
      options: Options(headers: {
        'Authorization': 'Bearer ${StorageRepository.getString('token')}'
      }),
    );

    print(response.data);
  }

  Future<void> postEditDocument({required Map documentMap}) async {
    final Response response = await _dio.post(
      'DocumentEdit',
      data: jsonEncode(documentMap),
      options: Options(headers: {
        'Authorization': 'Bearer ${StorageRepository.getString('token')}'
      }),
    );

    print("AFTER EDIT = ${response.statusCode}");
  }
}
