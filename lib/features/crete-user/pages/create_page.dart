import 'dart:convert';
import 'dart:io';

import 'package:electro/app/theme/theme.dart';
import 'package:electro/bloc/arendator/arendator_cubit.dart';
import 'package:electro/features/crete-user/fields/checkbox_field.dart';
import 'package:electro/features/crete-user/fields/fields.dart';
import 'package:electro/features/crete-user/fields/image_upload.dart';
import 'package:electro/features/crete-user/fields/userlist_field.dart';
import 'package:electro/features/projects/components/editing_textfield.dart';
import 'package:electro/features/projects/components/photo_placeholder.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_multi_formatter/formatters/masked_input_formatter.dart';
import 'package:latlong2/latlong.dart';
import 'package:provider/src/provider.dart';

class CreateArendatorView extends StatefulWidget {
  final List<dynamic> documents;
  final String typesId;
  final bool? isEdit;
  final String arendatorId;
  const CreateArendatorView({
    Key? key,
    required this.documents,
    required this.typesId,
    this.isEdit = false,
    required this.arendatorId,
  }) : super(key: key);

  @override
  _CreateArendatorViewState createState() => _CreateArendatorViewState();
}

class _CreateArendatorViewState extends State<CreateArendatorView> {
  Map documentAddMap = {};
  Map controllers = {};
  List<File> allFiles = [];

  void addParameter(Map parameter) {
    documentAddMap["parameters"].add(parameter);
  }

  void updateTextController(
      String specificationsId, String id, String text) async {
    controllers[specificationsId][id].value = TextEditingValue(
      text: text,
      selection: TextSelection.collapsed(offset: text.length),
    );
  }

  void updateCheckboxController(
      String specificationsId, String id, List<String> ids) {
    if (specificationsId == "6") {
      controllers["6"][id] = ids[0];
    } else {
      controllers["5"][id] = ids;
    }
  }

  void updateChooseController(
      String specificationsId, String id, List<String> ids) {
    if (specificationsId == "7") {
      controllers["7"][id] = ids[0];
    } else {
      controllers["8"][id] = ids;
    }
  }

  void updateImageController(
      String specificationsId, String id, List<File> files) async {
    List<Map> filesInfo = [];

    for (File file in files) {
      Map fileInfo =
          await context.read<ArendatorCubit>().postFileToApi(file: file);
      filesInfo.add(fileInfo);
    }
    controllers["11"][id] = filesInfo;
  }

  void updatePositionController(
      String specificationsId, String id, LatLng pos) {
    Map locationMap = {};
    locationMap["type"] = "FeatureCollection";
    locationMap["features"] = [
      {
        "type": "Feature",
        "properties": {},
        "geometry": {
          "type": "Polygon",
          "coordinates": [
            [
              [pos.latitude, pos.longitude],
              [pos.latitude, pos.longitude],
            ]
          ]
        }
      },
    ];
    var res =
        "{\"type\":\"FeatureCollection\",\"features\":[{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Polygon\",\"coordinates\":[[[latitude,longitude],[latitude,longitude],[latitude,longitude],[latitude,longitude]]]}}]}";
    res = res.replaceAll("latitude", pos.latitude.toString());
    res = res.replaceAll("longitude", pos.longitude.toString());
    controllers["12"][id] = res;
  }

  Future<List<File>> asyncLoadFiles(String id, List<dynamic> filesMap) async {
    List<File> _files = [];
    for (Map mapFile in filesMap) {
      File file = await context
          .read<ArendatorCubit>()
          .dowloadFile(idFile: mapFile["id"], arendatorId: widget.arendatorId);

      _files.add(file);
    }

    return _files;
  }

  @override
  void initState() {
    super.initState();
    documentAddMap["Id"] = widget.typesId;
    documentAddMap["TypesId"] = widget.arendatorId;
    documentAddMap["token"] = null;
    documentAddMap["parameters"] = [];

    for (int i = 1; i < 13; i++) {
      controllers["$i"] = {};
    }

    for (var document in widget.documents) {
      if (document.typeParameters.specificationsId == "1") {
        controllers["1"][document.typeParameters.id] = TextEditingController();

        if (document.numberData != null) {
          print("NUMBER DATA ${document.numberData}");
          updateTextController(
              "1", document.typeParameters.id, document.numberData.toString());
          setState(() {});
        }
      }
      if (document.typeParameters.specificationsId == "2") {
        controllers["2"][document.typeParameters.id] = TextEditingController();

        if (document.doubleData != null) {
          updateTextController(
              "2", document.typeParameters.id, document.doubleData.toString());
          setState(() {});
        }
      }
      if (document.typeParameters.specificationsId == "3") {
        controllers["3"][document.typeParameters.id] = TextEditingController();

        if (document.stringData != null) {
          updateTextController(
              "3", document.typeParameters.id, document.stringData.toString());
          setState(() {});
        }
      }
      if (document.typeParameters.specificationsId == "4") {
        controllers["4"][document.typeParameters.id] = TextEditingController();

        if (document.stringData != null) {
          updateTextController(
              "4", document.typeParameters.id, document.stringData.toString());
          setState(() {});
        }
      }
      if (document.typeParameters.specificationsId == "5") {
        controllers["5"][document.typeParameters.id] = <String>[];
      }
      if (document.typeParameters.specificationsId == "6") {
        controllers["6"][document.typeParameters.id] = "";
      }
      if (document.typeParameters.specificationsId == "7") {
        controllers["7"][document.typeParameters.id] = "";
      }
      if (document.typeParameters.specificationsId == "8") {
        controllers["8"][document.typeParameters.id] = <String>[];
      }
      if (document.typeParameters.specificationsId == "9") {
        controllers["9"][document.typeParameters.id] = TextEditingController();
      }
      if (document.typeParameters.specificationsId == "10") {
        controllers["10"][document.typeParameters.id] = TextEditingController();
      }
      if (document.typeParameters.specificationsId == "11") {
        controllers["11"][document.typeParameters.id] = <File>[];
        print("FILES = ${document.files}");
        if (document.files != null) {
          //_asyncLoadFiles(document.typeParameters.id, document.files);
        }
      }
      if (document.typeParameters.specificationsId == "12") {
        controllers["12"][document.typeParameters.id] = "";
      }
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.8,
      padding: const EdgeInsets.fromLTRB(16, 44, 16, 0),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Создание арендатора',
              style: Theme.of(context).textTheme.headline3!.copyWith(
                    fontSize: 20,
                  ),
            ),
            const SizedBox(height: 28),
            for (var document in widget.documents)
              if (document.typeParameters.specificationsId == "4")
                //небольшой текст
                textField(
                  context: context,
                  document: document,
                  fieldName: document.name,
                  controllers: controllers,
                  keyboardType: TextInputType.text,
                  callback: updateTextController,
                )
              else if (document.typeParameters.specificationsId == "1")
                // число
                textField(
                  context: context,
                  document: document,
                  fieldName: document.name,
                  controllers: controllers,
                  keyboardType: TextInputType.number,
                  inputFormatters: <TextInputFormatter>[
                    FilteringTextInputFormatter.digitsOnly
                  ],
                  callback: updateTextController,
                )
              else if (document.typeParameters.specificationsId == "2")
                //десятичное число
                textField(
                  context: context,
                  document: document,
                  fieldName: document.name,
                  controllers: controllers,
                  keyboardType: TextInputType.number,
                  inputFormatters: <TextInputFormatter>[
                    FilteringTextInputFormatter.digitsOnly
                  ],
                  callback: updateTextController,
                )
              else if (document.typeParameters.specificationsId == "3")
                //текст
                textField(
                  context: context,
                  document: document,
                  fieldName: document.name,
                  controllers: controllers,
                  keyboardType: TextInputType.text,
                  callback: updateTextController,
                )
              else if (document.typeParameters.specificationsId == "9")
                //дата
                textField(
                  context: context,
                  document: document,
                  fieldName: document.name,
                  controllers: controllers,
                  keyboardType: TextInputType.number,
                  inputFormatters: [
                    MaskedInputFormatter('##-##-####'),
                    //FilteringTextInputFormatter.digitsOnly
                  ],
                  callback: updateTextController,
                )
              else if (document.typeParameters.specificationsId == "10")
                //дата и время
                textField(
                  context: context,
                  document: document,
                  fieldName: document.name,
                  controllers: controllers,
                  keyboardType: TextInputType.number,
                  inputFormatters: [
                    MaskedInputFormatter('##-##-####,##:##:##'),
                    //FilteringTextInputFormatter.digitsOnly
                  ],
                  callback: updateTextController,
                )
              else if (document.typeParameters.specificationsId == "5")
                //флажки
                CustomCheckBoxField(
                  parameterOptions: document.parameterOptions,
                  fieldName: document.name,
                  isOne: false,
                  document: document,
                  controllers: controllers,
                  callback: updateCheckboxController,
                )
              else if (document.typeParameters.specificationsId == "6")
                //переключатель
                CustomCheckBoxField(
                  parameterOptions: document.parameterOptions,
                  fieldName: document.name,
                  isOne: true,
                  document: document,
                  controllers: controllers,
                  callback: updateCheckboxController,
                )
              else if (document.typeParameters.specificationsId == "7")
                //выбор
                UsersList(
                  fieldName: document.name,
                  chooseFields: document.parameterOptions,
                  isOne: true,
                  document: document,
                  controllers: controllers,
                  callback: updateChooseController,
                )
              else if (document.typeParameters.specificationsId == "8")
                //множественный выбор
                UsersList(
                  fieldName: document.name,
                  chooseFields: document.parameterOptions,
                  isOne: false,
                  document: document,
                  controllers: controllers,
                  callback: updateChooseController,
                )
              else if (document.typeParameters.specificationsId == "11")
                FutureBuilder(
                  future: asyncLoadFiles(
                      document.typeParameters.id, document.files),
                  builder: (BuildContext context,
                      AsyncSnapshot<List<File>> snapshot) {
                    if (snapshot.hasData ||
                        snapshot.connectionState == ConnectionState.done) {
                      allFiles = snapshot.data!;

                      return fileField(
                        context: context,
                        fieldName: document.name,
                        document: document,
                        controllers: controllers,
                        callback: updateImageController,
                        files: allFiles,
                      );
                    } else if (snapshot.hasError) {
                      return fileField(
                        context: context,
                        fieldName: document.name,
                        document: document,
                        controllers: controllers,
                        callback: updateImageController,
                        files: allFiles,
                      );
                    } else {
                      return ImageUploadItem();
                    }
                  },
                )
              else if (document.typeParameters.specificationsId == "12")
                GeoButton(
                  context: context,
                  callback: updatePositionController,
                  document: document,
                  controllers: controllers,
                )
              else
                Column(
                  children: [
                    Text(
                      document.name,
                      style: Theme.of(context).textTheme.headline3!.copyWith(
                            fontSize: 20,
                          ),
                    ),
                    const SizedBox(height: 28),
                    Text(
                      "${document.typeParameters.specificationsId}",
                      style: Theme.of(context).textTheme.headline3!.copyWith(
                            fontSize: 10,
                          ),
                    ),
                  ],
                ),
            createButton(
                fieldName: widget.isEdit! ? "Редактировать" : "Создать",
                context: context,
                onPressed: () {
                  documentAddMap["parameters"] = [];
                  setState(() {});
                  for (var document in widget.documents) {
                    if (controllers["1"][document.typeParameters.id] != null) {
                      String text =
                          controllers["1"][document.typeParameters.id].text;

                      if (text != "") {
                        print("1 CONTROLLER = $text");
                        addParameter({
                          "TypeParametersId": document.typeParameters.id,
                          "NumberData": int.parse(
                              controllers["1"][document.typeParameters.id].text)
                        });
                      }
                    }
                    if (controllers["2"][document.typeParameters.id] != null) {
                      String text =
                          controllers["2"][document.typeParameters.id].text;

                      if (text != "") {
                        print("2 CONTROLLER = $text");
                        addParameter({
                          "TypeParametersId": document.typeParameters.id,
                          "DoubleData": double.parse(
                              controllers["2"][document.typeParameters.id].text)
                        });
                      }
                    }
                    if (controllers["3"][document.typeParameters.id] != null) {
                      String text =
                          controllers["3"][document.typeParameters.id].text;

                      if (text != "") {
                        print("3 CONTROLLER = $text");
                        addParameter({
                          "TypeParametersId": document.typeParameters.id,
                          "StringData":
                              controllers["3"][document.typeParameters.id].text
                        });
                      }
                    }
                    if (controllers["4"][document.typeParameters.id] != null) {
                      String text =
                          controllers["4"][document.typeParameters.id].text;
                      if (text != "") {
                        print("4 CONTROLLER = $text");
                        addParameter({
                          "TypeParametersId": document.typeParameters.id,
                          "StringData":
                              controllers["4"][document.typeParameters.id].text
                        });
                      }
                    }

                    if (controllers["6"][document.typeParameters.id] != null) {
                      String id = controllers["6"][document.typeParameters.id];
                      print("6 CONTROLLER = $id");
                      if (id != "") {
                        print("6 CONTROLLER = $id");
                        addParameter({
                          "TypeParametersId": document.typeParameters.id,
                          "ParameterOptionsId": controllers["6"]
                              [document.typeParameters.id]
                        });
                      }
                    }

                    if (controllers["12"][document.typeParameters.id] != null) {
                      String coordsString =
                          controllers["12"][document.typeParameters.id];
                      print("12 CONTROLLER = $coordsString");
                      if (coordsString != "") {
                        print("12 CONTROLLER = $coordsString");
                        addParameter({
                          "TypeParametersId": document.typeParameters.id,
                          "Coords": controllers["12"]
                              [document.typeParameters.id]
                        });
                      }
                    }

                    if (controllers["5"][document.typeParameters.id] != null) {
                      List<String> ids =
                          controllers["5"][document.typeParameters.id];
                      if (ids.isNotEmpty) {
                        print("5 CONTROLLER = $ids");
                        addParameter({
                          "TypeParametersId": document.typeParameters.id,
                          "ParameterOptionsIds": controllers["5"]
                              [document.typeParameters.id]
                        });
                      }
                    }

                    if (controllers["7"][document.typeParameters.id] != null) {
                      String id = controllers["7"][document.typeParameters.id];
                      print("7 CONTROLLER = $id");
                      if (id != "") {
                        print("7 CONTROLLER = $id");
                        addParameter({
                          "TypeParametersId": document.typeParameters.id,
                          "ParameterOptionsId": controllers["7"]
                              [document.typeParameters.id]
                        });
                      }
                    }

                    if (controllers["8"][document.typeParameters.id] != null) {
                      List<dynamic> ids =
                          controllers["8"][document.typeParameters.id];
                      if (ids.isNotEmpty) {
                        print("8 CONTROLLER = $ids");
                        addParameter({
                          "TypeParametersId": document.typeParameters.id,
                          "ParameterOptionsIds": controllers["8"]
                              [document.typeParameters.id]
                        });
                      }
                    }

                    if (controllers["9"][document.typeParameters.id] != null) {
                      String text =
                          controllers["9"][document.typeParameters.id].text;
                      if (text != "") {
                        print("9 CONTROLLER = $text");
                        var date = controllers["9"][document.typeParameters.id]
                            .text
                            .split("-");

                        addParameter({
                          "TypeParametersId": document.typeParameters.id,
                          "DateData": date[2] +
                              "-" +
                              date[1] +
                              "-" +
                              date[0] +
                              "T00:00:00"
                        });
                      }
                    }
                    if (controllers["10"][document.typeParameters.id] != null) {
                      String text =
                          controllers["10"][document.typeParameters.id].text;
                      if (text != "") {
                        print("10 CONTROLLER = $text");
                        var date = controllers["10"][document.typeParameters.id]
                            .text
                            .split(",");
                        var dateDate = date[0].split("-");
                        var dateTime = date[1];
                        addParameter({
                          "TypeParametersId": document.typeParameters.id,
                          "DateTimeData": dateDate[2] +
                              "-" +
                              dateDate[1] +
                              "-" +
                              dateDate[0] +
                              "T" +
                              dateTime,
                        });
                      }
                    }

                    if (controllers["11"][document.typeParameters.id] != null) {
                      List<dynamic> files =
                          controllers["11"][document.typeParameters.id];

                      if (files.isNotEmpty) {
                        print("11 CONTROLLER = $files");
                        addParameter({
                          "TypeParametersId": document.typeParameters.id,
                          "Files": files,
                        });
                      }
                    }
                  }

                  print("DOCUMENT MAP = $documentAddMap");
                  if (widget.isEdit!) {
                    context.read<ArendatorCubit>().postEditDocumentToApi(
                        arendatorId: widget.arendatorId,
                        documentMap: documentAddMap);
                  } else {
                    context.read<ArendatorCubit>().postDocumentToApi(
                        arendatorId: widget.typesId,
                        documentMap: documentAddMap);
                  }
                }),
            const SizedBox(
              height: 15,
            )
          ],
        ),
      ),
    );
  }
}
