import 'package:electro/data/locale/entities/map_object.dart';
import 'package:electro/models/arendator/renter.dart';
import 'package:electro/models/object/draw_type.dart';
import 'package:electro/utils/geojson_helper.dart';
import 'package:json_annotation/json_annotation.dart';

import 'map_object_data_value.dart';

part 'map_object_data.g.dart';

@JsonSerializable()
class MapObjectData {
  MapObjectData({
    required this.id,
    this.parentId,
    required this.motId,
    required this.coords,
    this.comment,
    required this.mapObjectDataValues,
    required this.mapObjectFiles,
    required this.renters,
  });

  @JsonKey(name: 'Id')
  String id;
  @JsonKey(name: 'ParentId')
  String? parentId;
  @JsonKey(name: 'Renters')
  List<RenterRequestDto> renters;

  /// GUID Идентификатор типа объекта. Идетификатор можно взять из модели
  /// запроса GetDrawTypes. [DrawTypeResponseDto] (Id)
  @JsonKey(name: 'MotId')
  String motId;
  @JsonKey(name: 'Coords')
  String coords;
  @JsonKey(name: 'Comment')
  String? comment;
  @JsonKey(name: 'MapObjectDataValues')
  List<MapObjectDataValue> mapObjectDataValues;
  @JsonKey(name: 'MapObjectFiles')
  List<dynamic> mapObjectFiles;

  static MapObjectData fromModel(MapObjectModel model) {

    var coords = model.coords.isNotEmpty
        ? GeoJsonCreator.createRoute(model.coords.toList())
        : GeoJsonCreator.createPoint(model.position);

    var files = model.filesInfo.map((e) => {
      "Size": e.size,
      "SourceName": e.sourceName,
      "OriginalName": e.originalName,
      "ObjWkt": coords
    }).toList();
    
    return MapObjectData(
      id: model.id,
      parentId: null,
      renters: model.renters == null
          ? []
          : model.renters!
              .map((e) => RenterRequestDto.fromModel(e))
              .toList(), // map
      motId: model.objectTypeId,
      coords: coords,
      comment: "",
      mapObjectDataValues:
          model.values.map((e) => MapObjectDataValue.from(e)).toList(),

      mapObjectFiles: files
    );
  }

  factory MapObjectData.fromJson(Map<String, dynamic> json) =>
      _$MapObjectDataFromJson(json);

  Map<String, dynamic> toJson() => _$MapObjectDataToJson(this);

  // Map<String, dynamic> toJson() => {
  //   "MapObjectFiles": null,
  //   "MapObjectDataValues": mapObjectDataValues,
  //   "Comment": comment,
  //   "Coords": coords,
  //   "MotId": motId,
  //   "ParentId": parentId,
  //   "Id": id,
  // };
}
