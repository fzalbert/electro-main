import 'package:electro/features/account/auth/logout_bottomsheet.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class BottomSheetHelper {
  static show(Widget widget, BuildContext context) {
    showModalBottomSheet(
        shape: shape,
        context: context,
        isScrollControlled: true,
        builder: (context) {
          return DraggableScrollableSheet(
              expand: false,
              maxChildSize: 0.95,
              builder:
                  (BuildContext context, ScrollController scrollController) {
                if (widget is ScrollableWidget) {
                  (widget as ScrollableWidget)
                      .setScrollController(scrollController);
                }

                return widget;
              });
        });
  }

  static showSelectable(BuildContext context, {required String text,
      GestureTapCallback? approve, GestureTapCallback? cancel}) {
    showModalBottomSheet(
        isScrollControlled: true,
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(30), topRight: Radius.circular(30))),
        context: context,
        builder: (x) {
          return SelectableBottomSheet(
            parentContext: context,
            text: text,
            approve: approve,
            cancel: cancel,
          );
        });
  }

  static const RoundedRectangleBorder shape = RoundedRectangleBorder(
      borderRadius: BorderRadius.only(
          topLeft: Radius.circular(30), topRight: Radius.circular(30)));
}

abstract class ScrollableWidget {
  void setScrollController(ScrollController controller);
}
