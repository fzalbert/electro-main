import 'dart:async';

import 'package:electro/data/locale/entities/map_object.dart';
import 'package:electro/data/locale/entities/map_object_value.dart';
import 'package:electro/data/locale/entities/session_status.dart';
import 'package:electro/features/sessions/service/position_service.dart';
import 'package:electro/features/sessions/service/stop_go_session_service.dart';
import 'package:electro/features/sessions/view_models/stop_go_session.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:latlong2/latlong.dart';

class StopAndGo {

  static StopGoSessionService sessionService = StopGoSessionService();
  static PositionService  positionService = PositionService();

  static StreamSubscription<Position>? _userPositionStream;
  static final List<LatLng> _positions = [];
  static bool _isStarted = false;
  static bool _isPaused = false;
  static StopGoSession? _session;
  static void Function(StopGoSession)? _onStart;
  static void Function(StopGoSession)? _onSelected;
  static void Function(Iterable<LatLng>?)? _onEnd;
  static VoidCallback? _onCancel;
  static void Function(Iterable<LatLng>)? _onNewPoints;
  static void Function(MapObjectModel)? _onNewObject;

  static bool get isStarted => _isStarted;
  static bool get isPaused => _isPaused;
  static set onStart (value) => _onStart = value;
  static set onSelected (value) => _onSelected = value;
  static set onEnd (value) => _onEnd = value;
  static set onCancel (value) => _onCancel = value;
  static set onNewPoints (value) => _onNewPoints = value;
  static set onNewObject (value) => _onNewObject = value;



  static void start() async {
    if (_isStarted) return;

    var session = await sessionService.create();
    if(session != null) {

      _isStarted = true;
      _session = StopGoSession.fromEntity(session, [], []);
      _onStart?.call(_session!);
      _startListeningPosition();
    }
  }

  static void startPrevious({String? sessionId}) async {
    if (_isStarted) return;

    if(sessionId == null || sessionId.isEmpty){
      var session = _session;

      if(session == null) {
        return;
      } else{
        _isStarted = true;
        _onStart?.call(session);

        _onNewPoints?.call(_positions);
        
        _startListeningPosition();
      }
    }
    else {

      var sessionService = StopGoSessionService();
      var session = await sessionService.findById(sessionId);

      if (session == null) {
        print("session id: $sessionId not found");
        return;
      }

      var positions = session.coords.map((e) =>
          LatLng(e.latitude, e.longitude));

      _session = session;

      _positions.clear();
      _positions.addAll(positions);

      _isStarted = true;

      _onStart?.call(session);
      _onNewPoints?.call(positions);

      _startListeningPosition();
    }
  }

  static void select(String sessionId) async {
    if (_isStarted) return;

    var sessionService = StopGoSessionService();
    var session = await sessionService.findById(sessionId);

    if(session == null) {
      print("session id: $sessionId not found");
      return;
    }

    var positions = session.coords.map((e) => LatLng(e.latitude, e.longitude));

    _session = session;

    _positions.clear();
    _positions.addAll(positions);

    _onSelected?.call(session);
    _onNewPoints?.call(positions);
  }

  static void changeStatus({required SessionStatus status}) async{
    var currentSession = _session;


    if(currentSession == null || !_isStarted) {

      if(status == SessionStatus.sended) {
        finish();
      }
      return;
    }

    if(status == SessionStatus.done) {
      await sessionService.updateStatus(currentSession.id, SessionStatus.done);
      finish();
    }
  }

  static void finish() async {

    var currentSession = _session;


    _isStarted = false;

    _session = null;
    _userPositionStream?.cancel();


    _positions.clear();
    _onEnd?.call(_positions);
  }
  static void pause() {

    var currentSession = _session;

    if (!_isStarted && currentSession != null) return;


    _isPaused = true;
    sessionService.updateStatus(currentSession!.id, SessionStatus.paused);


    _userPositionStream?.cancel();

  }

  static void resume() {

    var currentSession = _session;

    if (!_isStarted && currentSession != null) return;


    _isPaused = false;
    sessionService.updateStatus(currentSession!.id, SessionStatus.inProgress);


    _startListeningPosition();

  }

  static void cancel(){
    if (!_isStarted) {
      if(_session == null){
        return;
      }
      else {
        _positions.clear();
        _onCancel?.call();
      }
    }
    _isStarted = false;
    _session = null;
    _userPositionStream?.cancel();
    _positions.clear();
    _onCancel?.call();
  }

  static void newObjectCreated(MapObjectModel object){
    _onNewObject?.call(object);
  }

  static void _startListeningPosition(){
    _userPositionStream = Geolocator.getPositionStream(
      desiredAccuracy: LocationAccuracy.bestForNavigation,
      distanceFilter: 5,
      forceAndroidLocationManager: false,
      intervalDuration: const Duration(milliseconds: 0),
    ).listen((currentPosition) {

      LatLng? lastPosition;
      try {
        lastPosition = _positions.last;
      }
      catch(ex){
        lastPosition = null;
      }

      var a = currentPosition.accuracy;
      var isDistanceMore = true;

      if(lastPosition != null) {
        var distance = Geolocator.distanceBetween(
            currentPosition.latitude, currentPosition.longitude,
            lastPosition.latitude, lastPosition.longitude);

        isDistanceMore = distance > 3;
      }

      if(a < 20 && isDistanceMore) {
        _newPosition(currentPosition);
      }
    });
  }

  static void _newPosition(Position position){
    var latLng = LatLng(position.latitude, position.longitude);

    _positions.add(latLng);
    _onNewPoints?.call([latLng]);



    if(_session != null) {
      print(latLng);
      positionService.addToSession(_session!.id, latLng);
    }

  }

  static StopGoSession? getCurrentSession(){
    return _session;
  }
}