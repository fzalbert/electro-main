import 'package:floor/floor.dart';

@Entity(tableName: "position", foreignKeys: [
  // ForeignKey(
  //   childColumns: ["sessionId"],
  //   parentColumns: ["id"],
  //   entity: StopGoSessionEntity,
  //   onDelete: ForeignKeyAction.cascade,
  // )
])
class PositionEntity {
  @PrimaryKey()
  final String id;

  final String? sessionId;

  final double latitude;
  final double longitude;

  PositionEntity({
    required this.id,
    required this.latitude,
    required this.longitude,
    required this.sessionId,
  });
}
