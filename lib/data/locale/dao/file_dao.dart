

import 'package:electro/data/locale/entities/file.dart';
import 'package:floor/floor.dart';

@dao
abstract class FileDao {

  @Query('SELECT * FROM ${FileEntity.TABLE_NAME}')
  Future<List<FileEntity>> findAll();

  @Query('SELECT * FROM ${FileEntity.TABLE_NAME} WHERE id = :id')
  Stream<FileEntity?> findById(int id);

  @Query('SELECT * FROM ${FileEntity.TABLE_NAME} WHERE objectId = :objectId')
  Future<List<FileEntity>> findByMapObjectId(String objectId);

  @Insert()
  Future<void> insert(FileEntity file);

}