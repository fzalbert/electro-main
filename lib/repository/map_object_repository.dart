import 'dart:io';

import 'package:dio/dio.dart';
import 'package:electro/data/locale/entities/map_object.dart';
import 'package:electro/data/singletons/dio_settings.dart';
import 'package:electro/data/singletons/storage.dart';
import 'package:electro/models/draw/map_object.dart';

class MapObjectRepository {
  final Dio _dio = DioSettings.createDio(connectTimeout: 15000);

  Future<int> send(MapObjectModel model, String drawTypeId) async {
    var data = MapObject.fromModel(model, drawTypeId);
    print("StopGoSession " + data.toJson().toString());

    for (var element in data.mapObjectData) {
      print(element.toJson().toString());
    }

    try {
      final Response response = await _dio.post('CreateDrawObject',
          options: Options(headers: {
            'Authorization': 'Bearer ${StorageRepository.getString('token')}'
          }),
          data: data.toJson());

      return response.statusCode ?? 500;
    } catch (ex) {
      print(ex.toString());
      return 500;
    }
  }
}
