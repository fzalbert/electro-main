import 'package:electro/features/projects/components/columns/column_medium.dart';
import 'package:electro/features/projects/components/switcher.dart';
import 'package:electro/app/constants/app_icons.dart';
import 'package:electro/app/theme/theme.dart';
import 'package:electro/models/columns/directory.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:electro/models/columns/project_directory.dart';

class ProjectSectionItem extends StatefulWidget {
  final DirectoryResponseDto projects;
  const ProjectSectionItem({Key? key, required this.projects})
      : super(key: key);

  @override
  State<ProjectSectionItem> createState() => _ProjectSectionItemState();
}

class _ProjectSectionItemState extends State<ProjectSectionItem> {
  bool firstColumnOpen = false;

  @override
  Widget build(BuildContext context) {
    print("WIDGET LAYERS = ${widget.projects.mapProjectLayers}");
    return Container(
      child: Column(
        children: [
          Container(
            margin: const EdgeInsets.fromLTRB(0, 30, 0, 21),
            child: Row(
              children: [
                GestureDetector(
                    behavior: HitTestBehavior.translucent,
                    onTap: () {
                      setState(() {
                        firstColumnOpen = !firstColumnOpen;
                      });
                    },
                    child: Container(
                      child: firstColumnOpen
                          ? Container(
                              padding: const EdgeInsets.all(4),
                              width: 24,
                              height: 24,
                              child: SvgPicture.asset(
                                AppIcons.arrowDown,
                              ))
                          : Container(
                              padding: const EdgeInsets.all(4),
                              width: 24,
                              height: 24,
                              child: SvgPicture.asset(
                                AppIcons.arrowRight,
                              )),
                    )),
                const SizedBox(
                  width: 16,
                ),
                Text(
                  widget.projects.name,
                  style: Theme.of(context).textTheme.headline1!.copyWith(
                      fontSize: 16,
                      color: firstColumnOpen
                          ? AppTheme.regularTextDarkBlue
                          : AppTheme.regularText),
                ),
                const Spacer(),
                ProjectsSwitcher(
                  startingState: widget.projects.activated,
                )
              ],
            ),
          ),

//
//
//
//
//
          firstColumnOpen
              ? ListView.builder(
                  shrinkWrap: true,
                  physics: const NeverScrollableScrollPhysics(),
                  itemCount: widget.projects.mapProjectDirectories.isNotEmpty
                      ? widget.projects.mapProjectDirectories.length
                      : 1,
                  itemBuilder: (context, index) {
                    print(
                        "itemCount: ${widget.projects.mapProjectDirectories.length == 0}");
                    return Container(
                      margin: const EdgeInsets.symmetric(vertical: 21),
                      child: ProjectsColumnMedium(
                        childrenDirectory:
                            widget.projects.mapProjectDirectories.isNotEmpty
                                ? widget.projects.mapProjectDirectories[index]
                                    .childrenDirectories
                                : null,
                        projectDirectory:
                            widget.projects.mapProjectDirectories.isNotEmpty
                                ? widget.projects.mapProjectDirectories[index]
                                : null,
                        projectLayers:
                            widget.projects.mapProjectDirectories.isNotEmpty
                                ? null
                                : widget.projects.mapProjectLayers,
                      ),
                    );
                  })
              : const SizedBox()
        ],
      ),
    );
  }
}
