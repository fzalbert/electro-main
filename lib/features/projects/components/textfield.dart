import 'package:electro/app/constants/app_icons.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ProjectsTextField extends StatefulWidget {
  ProjectsTextField({Key? key}) : super(key: key);

  @override
  _ProjectsTextFieldState createState() => _ProjectsTextFieldState();
}

class _ProjectsTextFieldState extends State<ProjectsTextField> {
  final TextEditingController controller = TextEditingController();

  InputBorder border({required Color color}) {
    return OutlineInputBorder(
        borderRadius: BorderRadius.circular(9),
        borderSide: BorderSide(color: color, width: 1));
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 36,
      child: TextField(
        controller: controller,
        onChanged: (s) {
          setState(() {});
        },
        textAlignVertical: TextAlignVertical.center,
        decoration: InputDecoration(
            filled: true,
            fillColor: const Color(0xffF8F8F8),
            counterText: '',
            contentPadding: const EdgeInsets.symmetric(vertical: 12),
            suffixIcon: controller.text.isNotEmpty
                ? GestureDetector(
                    onTap: () {
                      setState(() {
                             controller.clear();
                      });
                 
                    },
                    child: Container(
                        padding: const EdgeInsets.symmetric(vertical: 8),
                        child: SvgPicture.asset(AppIcons.cancel)),
                  )
                : null,
            prefixIcon: Container(
                padding: const EdgeInsets.symmetric(vertical: 8),
                child: SvgPicture.asset(
                  AppIcons.search,
                )),
            focusedBorder: border(color: const Color(0xffF3F3F3)),
            border: border(color: const Color(0xffF3F3F3)),
            enabledBorder: border(color: const Color(0xffF3F3F3)),
            disabledBorder: border(color: const Color(0xffF3F3F3))),
      ),
    );
  }
}
