import 'dart:ui';

import 'package:flutter/material.dart';

const mainColor = Color.fromRGBO(43, 97, 163, 1.0);
const mainBackgroundColor = Colors.white;


const tableColor = Color.fromRGBO(248, 249, 252, 1.0);
const lightBlueColor = Color.fromRGBO(40, 98, 145, 0.4);
const disabledButtonColor = Color.fromRGBO(59, 122, 229, 1.0);
const borderColor = Color.fromRGBO(0, 45, 119, 1);
const placeholderColor = Color.fromRGBO(175, 175, 175, 1);
const lightGradientColor = Color.fromRGBO(25, 105, 203, 1.0);
const darkGradientColor = Color.fromRGBO(19, 47, 197, 1.0);
const mainGrey = Color.fromRGBO(175, 175, 175, 1.0);
const bottomNavBarShadowColor = Color.fromRGBO(0, 0, 0, 0.08);

const appBarTopGradient = Color.fromRGBO(43,115,238, 1.0);
const appBarBottomGradient = Color.fromRGBO(0,72,195, 1.0);

const errorColor = Color.fromRGBO(239, 49, 36, 1.0);
const iconGrey = Color(0xFF666666);