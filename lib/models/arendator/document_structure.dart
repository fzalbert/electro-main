import 'package:electro/models/arendator/document_parameters.dart';

class Document {
  String? id;
  String? typesId;
  String? layerId;
  String? geoserverLayerId;
  String? serviceTypesId;
  String? parentId;
  List<dynamic>? parameters;
  List<dynamic>? values;

  Document({
    this.id,
    this.typesId,
    this.layerId,
    this.geoserverLayerId,
    this.serviceTypesId,
    this.parentId,
    this.parameters,
  });

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'typesId': typesId,
      'layerId': layerId,
      'geoserverLayerId': geoserverLayerId,
      'serviceTypesId': serviceTypesId,
      'parentId': parentId,
      'parameters': parameters,
    };
  }

  factory Document.fromJson(Map<String, dynamic> json) {
    return Document(
      id: json["id"],
      typesId: json["typesId"],
      layerId: json["layerId"],
      geoserverLayerId: json["geoserverLayerId"],
      serviceTypesId: json["serviceTypesId"],
      parentId: json["parentId"],
      parameters: json["parameters"]
          .map((jsonElement) => DocumentParameters.fromJson(jsonElement))
          .toList(),
    );
  }
}
