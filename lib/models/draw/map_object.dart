import 'package:electro/data/locale/entities/map_object.dart';
import 'package:electro/features/sessions/view_models/stop_go_session.dart';
import 'package:electro/utils/guid_helper.dart';
import 'package:json_annotation/json_annotation.dart';
import 'map_object_data.dart';

part 'map_object.g.dart';

@JsonSerializable()
class MapObject {
  MapObject({
    this.token,
    required this.directoryId,
    required this.groupId,
    required this.mapObjectData,
  });

  @JsonKey(name: 'Token')
  String? token;
  @JsonKey(name: 'DirectoryId')
  String directoryId;
  @JsonKey(name: 'GroupId')
  String groupId;
  @JsonKey(name: 'MapObjectData')
  List<MapObjectData> mapObjectData;

  static MapObject fromSession(StopGoSession model) {
    return MapObject(
      directoryId: model.drawTypeId,
      groupId: model.id,
      mapObjectData:
          model.objects.map((e) => MapObjectData.fromModel(e)).toList(),
    );
  }

  static MapObject fromModel(MapObjectModel model, String drawTypeId) {
    return MapObject(
      directoryId: drawTypeId,
      groupId: GuidHelper.timeBased(),
      mapObjectData: [MapObjectData.fromModel(model)],
    );
  }

  factory MapObject.fromJson(Map<String, dynamic> json) =>
      _$MapObjectFromJson(json);

  Map<String, dynamic> toJson() => _$MapObjectToJson(this);
}
