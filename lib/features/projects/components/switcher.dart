import 'package:electro/data/utils/functions.dart';
import 'package:electro/features/projects/detail_bottom_sheet.dart';
import 'package:electro/app/theme/theme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_switch/flutter_switch.dart';

class ProjectsSwitcher extends StatefulWidget {
  final Widget? prefix;
  final String? title;
  final bool startingState;
  final Function? onToggle;
  final bool? redraw;

  ProjectsSwitcher({
    Key? key,
    this.prefix,
    this.title,
    required this.startingState,
    this.onToggle,
    this.redraw,
  }) : super(key: key);

  @override
  _ProjectsSwitcherState createState() => _ProjectsSwitcherState();
}

class _ProjectsSwitcherState extends State<ProjectsSwitcher> {
  bool currentValue = false;

  @override
  void initState() {
    currentValue = widget.startingState;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FlutterSwitch(
        activeColor: AppTheme.regularTextDarkBlue,
        inactiveColor: const Color(0xffE4E4E6),
        width: 32,
        height: 20,
        toggleSize: 15,
        padding: 2,
        value: currentValue,
        onToggle: (value) {
          if (widget.onToggle != null) {
            widget.onToggle!();
          }
          setState(() {
            currentValue = value;
          });
          if (widget.prefix != null && widget.title != null && currentValue) {
            MyFunctions.popBottomSheet(
                DetailBottomSheet(
                  prefix: widget.prefix,
                  title: widget.title,
                ),
                true,
                context);
          }
        });
  }
}

Widget switcher(
    {required bool startingState,
    required Function onToggle,
    required bool disabled}) {
  bool currentValue = startingState;
  return FlutterSwitch(
    activeColor: AppTheme.regularTextDarkBlue,
    inactiveColor: const Color(0xffE4E4E6),
    width: 32,
    height: 20,
    toggleSize: 15,
    padding: 2,
    disabled: disabled,
    value: currentValue,
    onToggle: (value) {
      onToggle();
    },
  );
}
