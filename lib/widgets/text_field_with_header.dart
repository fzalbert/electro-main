import 'package:electro/app/constants/app_icons.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../app/constants/colors.dart';

class HeaderedTextField extends StatefulWidget {
  final TextEditingController controller;
  final bool hasError;
  final String hint;
  final bool isPassword;
  final String? errorText;
  final GlobalKey<FormState> formKey;

  const HeaderedTextField({
    Key? key,
    required this.isPassword,
    required this.formKey,
    required this.controller,
    required this.hasError,
    required this.errorText,
    required this.hint,
  }) : super(key: key);

  @override
  State<HeaderedTextField> createState() => _HeaderedTextFieldState();
}

class _HeaderedTextFieldState extends State<HeaderedTextField> {
  bool isHidden = false;
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          width: double.infinity,
          child: Form(
            key: widget.formKey,
            child: TextFormField(
              obscureText: widget.isPassword ? !isHidden : false,
              validator: (text) {
                if (text == null || text.isEmpty) {
                  return 'Обязательно к заполнению';
                }  else {
                  return null;
                }
              },
              controller: widget.controller,
              decoration: InputDecoration(errorText: widget.hasError?'':null,helperText: '',
                counterText: '',
                suffixIcon: widget.isPassword
                    ? isHidden
                        ? GestureDetector(
                            onTap: () {
                              setState(() {
                                isHidden = !isHidden;
                              });
                            },
                            child: Container(
                                padding: const EdgeInsets.symmetric(
                                  vertical: 10,
                                ),
                                child: SvgPicture.asset(AppIcons.eyeOn)))
                        : GestureDetector(
                            onTap: () {
                              setState(() {
                                isHidden = !isHidden;
                              });
                            },
                            child: Container(
                                padding: const EdgeInsets.symmetric(
                                  vertical: 10,
                                ),
                                child: SvgPicture.asset(AppIcons.eyeOff)))
                    : null,
                errorStyle: const TextStyle(
                  fontSize: 12,
                  color: errorColor,
                ),
                fillColor: const Color.fromRGBO(255, 255, 255, 0.7),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8.0),
                ),
                hintText: widget.hint,
                contentPadding: const EdgeInsets.all(12),
                hintStyle: const TextStyle(
                  fontSize: 16,
                  color: Color.fromARGB(255, 203, 203, 203),
                  fontWeight: FontWeight.w400,
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
