import 'dart:async';
import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:electro/data/singletons/dio_settings.dart';
import 'package:electro/data/singletons/storage.dart';
import 'package:electro/models/authentication/authentication_model.dart';
import 'package:electro/models/authentication/login_response.dart';
import 'package:formz/formz.dart';

class AuthenticationRepository {
  final _controller = StreamController<AuthenticationStatus>();

  Stream<AuthenticationStatus> get status async* {
    await StorageRepository.getInstance();
    await Future.delayed(const Duration(milliseconds: 1500));
    yield AuthenticationStatus.unauthenticated;

    yield* _controller.stream;
  }

  Future<int> logIn({
    required String login,
    required String password,
  }) async {
    await StorageRepository.getInstance();

    Dio _dio = Dio(DioSettings().dioBaseOptions);

    String basicAuth = 'Basic ' + base64Encode(utf8.encode('$login:$password'));

    final Response response = await _dio.post(
      'Login/',
      options: Options(headers: <String, String>{'authorization': basicAuth}),
    );
    try {
      if (response.statusCode! >= 200 && response.statusCode! < 300) {

        var responseModel = LoginResponseDto.fromJson(response.data);

        StorageRepository.putString(KeysConstants.tokenKey, responseModel.token);
        StorageRepository.putString(KeysConstants.userIdKey, responseModel.userId);
        print(StorageRepository.getString(KeysConstants.tokenKey));

        return 1;
      } else if (response.statusCode == 401) {
        return 2;
      } else {
        print(response.statusCode);
        return 3;
      }
    } catch (e) {
      print(e.toString());
      return 3;
    }
  }

  Future<int> logOut() async {
    await StorageRepository.getInstance();
    Dio _dio = Dio(DioSettings().dioBaseOptions);
    var token = StorageRepository.getString('token');

    try {
      await _dio.post(
        'Logout/',
        queryParameters: {
          'token': token,
        },
      );

      StorageRepository.deleteString('token');
      return 1;
    } catch (e) {
      print(e.toString());
      return 1;
    }
  }

  void dispose() => _controller.close();
}
