// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'parameter.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Parameter _$ParameterFromJson(Map<String, dynamic> json) => Parameter(
      id: json['Id'] as String,
      parameterName: json['ParameterName'] as String,
      parameterType: json['ParameterType'] as String,
      required: json['Required'] as bool? ?? false,
      orderNumber: json['OrderNumber'] as int? ?? 0,
      minLenght: json['MinLenght'] as int?,
      maxLength: json['MaxLength'] as int?,
      regularExpression: json['RegularExpression'] as String?,
      regularExpressionText: json['RegularExpressionText'] as String?,
      isFixed: json['IsFixed'] as bool? ?? false,
      defaultValue: json['DefaultValue'],
      optionSelect: (json['OptionSelect'] as Map<String, dynamic>?)?.map(
            (k, e) => MapEntry(k, e as String),
          ) ??
          {},
    );

Map<String, dynamic> _$ParameterToJson(Parameter instance) => <String, dynamic>{
      'Id': instance.id,
      'ParameterName': instance.parameterName,
      'ParameterType': instance.parameterType,
      'Required': instance.required,
      'OrderNumber': instance.orderNumber,
      'MinLenght': instance.minLenght,
      'MaxLength': instance.maxLength,
      'RegularExpression': instance.regularExpression,
      'RegularExpressionText': instance.regularExpressionText,
      'IsFixed': instance.isFixed,
      'DefaultValue': instance.defaultValue,
      'OptionSelect': instance.optionSelect,
    };
