import 'package:electro/features/projects/components/columns/column_item.dart';
import 'package:electro/features/projects/components/slider.dart';
import 'package:electro/features/projects/components/switcher.dart';
import 'package:electro/app/constants/app_icons.dart';
import 'package:electro/app/theme/theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:electro/models/columns/project_directory.dart';
import 'package:electro/models/columns/layer_directory.dart';

class SubColumn extends StatefulWidget {
  final ProjectDirectoryResponseDto projectDirectory;

  SubColumn(
      {Key? key, required this.projectDirectory,})
      : super(key: key);

  @override
  _SubColumnState createState() => _SubColumnState();
}

class _SubColumnState extends State<SubColumn> {
  bool secondColumnOpen = false;
  @override
  void initState() {
   
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Column(
          children: [
            Container(
              margin: const EdgeInsets.fromLTRB(11, 0, 0, 0),
              child: Row(
                children: [
                  GestureDetector(behavior: HitTestBehavior.translucent,
                      onTap: () {
                        setState(() {
                          secondColumnOpen = !secondColumnOpen;
                        });
                      },
                      child: secondColumnOpen
                          ? Container(
                              padding: const EdgeInsets.all(4),
                              width: 24,
                              height: 24,
                              child: SvgPicture.asset(
                                AppIcons.arrowDown,
                              ))
                          : Container(
                              padding: const EdgeInsets.all(4),
                              width: 24,
                              height: 24,
                              child: SvgPicture.asset(
                                AppIcons.arrowRight,
                              ))),
                  const SizedBox(
                    width: 16,
                  ),
                  Text(
                    widget.projectDirectory.name,
                    style: Theme.of(context).textTheme.headline3!.copyWith(
                        fontSize: 16,
                        color: secondColumnOpen
                            ? AppTheme.regularTextDarkBlue
                            : AppTheme.regularText),
                  ),
                  const Spacer(),
                  ProjectsSwitcher(
                    startingState: widget.projectDirectory.activated,
                  )
                ],
              ),
            ),
           
            secondColumnOpen
                ? Container(
                    padding: const EdgeInsets.only(left: 31),
                    child: Column(
                      children: [ ProjectsSlider(),
                        ListView.builder(shrinkWrap:true,physics:const NeverScrollableScrollPhysics(),
                            itemCount: widget.projectDirectory.layers.length,
                            itemBuilder: (context, index) {
                              return Container(margin:const  EdgeInsets.symmetric(vertical: 16),
                                child: ColumnItem(
                                  title:  widget.projectDirectory.layers[index].name,
                                  prefix: Container(
                                    width: 15,
                                    height: 15,
                                    child: Image.network(
                                         widget.projectDirectory.layers[index].legendLink),
                                  ),
                                ),
                              );
                            }),
                      ],
                    )
                   
                    )
                : const SizedBox(),
                
          ],
        )
      ],
    );
  }
}
