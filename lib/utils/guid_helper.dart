import 'package:uuid/uuid.dart';

class GuidHelper {

  static String timeBased(){
    var uuid = const Uuid();
    return uuid.v1();
  }
}