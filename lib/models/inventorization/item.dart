import 'dart:io';

class Item {
  final String id;
  final String typesId;
  final String typesDataId;
  final String timeCreated;
  final bool deleted;
  final String userId;
  final String statusId;
  final List<String> layersId;
  final String objectGeometry;
  final String objectBbox;

  Item({
    required this.id,
    required this.typesId,
    required this.typesDataId,
    required this.timeCreated,
    required this.deleted,
    required this.userId,
    required this.statusId,
    required this.layersId,
    required this.objectGeometry,
    required this.objectBbox,
  });

  factory Item.fromJson(Map<String, dynamic> json) {
    return Item(
      id: json["id"],
      typesId: json["typesId"],
      typesDataId: json["typesDataId"],
      timeCreated: json["timeCreated"],
      deleted: json["deleted"],
      userId: json["userId"],
      statusId: json["statusId"],
      layersId: (json["layersId"] as List<dynamic>).cast<String>(),
      objectGeometry: json["objectGeometry"],
      objectBbox: json["objectBbox"],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "id": id,
      "typesId": typesId,
      "typesDataId": typesDataId,
      "timeCreated": timeCreated,
      "deleted": deleted,
      "userId": userId,
      "statusId": statusId,
      "layersId": layersId,
      "objectGeometry": objectGeometry,
      "objectBbox": objectBbox,
    };
  }
}
