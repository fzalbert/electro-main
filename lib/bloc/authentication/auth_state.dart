import 'package:equatable/equatable.dart';
import 'package:formz/formz.dart';

class AuthState extends Equatable {

  final FormzStatus loginStatus;
  final FormzStatus logoutStatus;
  const AuthState({required this.loginStatus, required this.logoutStatus});

  AuthState copyWith({
    FormzStatus? loginStatus,
    FormzStatus? logoutStatus,
  }) {
    return AuthState(
        loginStatus: loginStatus ?? this.loginStatus,
        logoutStatus: logoutStatus ?? this.logoutStatus);
  }
    @override
  List<Object?> get props => [
    loginStatus,logoutStatus,
  ];
}
