import 'package:equatable/equatable.dart';

class ProjectListEvents extends Equatable {
  @override
  List<Object?> get props => [];
}

class GetProjectListEvent extends ProjectListEvents {}
