

import 'package:electro/models/arendator/renter.dart';
import 'package:floor/floor.dart';

@dao
abstract class RenterDao {

  @Query('SELECT * FROM renter_table')
  Future<List<RenterEntity>> findAll();

  @Query('SELECT * FROM renter_table WHERE id = :id')
  Stream<RenterEntity?> findById(int id);

  @Insert()
  Future<void> insert(RenterEntity renter);

}