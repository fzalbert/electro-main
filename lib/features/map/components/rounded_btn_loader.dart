import 'dart:ui';

import 'package:electro/app/constants/app_icons.dart';
import 'package:electro/app/constants/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class RoundedBtnLoader extends StatelessWidget {
  final GestureTapCallback? onTap;
  final String asset;
  final bool loadind;
  final int state; //0 - fail, 1 - warning, 2 - success

  RoundedBtnLoader({Key? key, this.onTap, this.asset='', this.loadind=false, this.state=2}) : super(key: key);

  Color getStateColor(int state)
  {
    switch(state)
    {
      case 0:
        return Colors.red;

      case 1:
        return Colors.orangeAccent;

      case 2:
        return Colors.green;

      default:
        return Colors.black12;

    }

  }
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap, //!loadind && state!=2 ? onTap : (){},
      child:
      ClipRRect(
        borderRadius: BorderRadius.circular(50.0),
        child: BackdropFilter(
          filter: ImageFilter.blur(sigmaX: 12, sigmaY: 12),
          child: Stack(
            alignment: Alignment.center,
            children: <Widget>[
              loadind ? CircularProgressIndicator(color: mainColor,) : Container(),
              Container(
                width: 54,
                height: 54,
                decoration: BoxDecoration(
                  gradient: const LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [
                      Color.fromRGBO(255, 255, 255, 0.7),
                      Color.fromRGBO(215, 224, 237, 0.7),
                    ],
                  ),
                  border: Border.all(color: Colors.white),
                  borderRadius: BorderRadius.circular(50.0),
                  boxShadow: const [
                    BoxShadow(
                      color: Color(0x1A193D83),
                      offset: Offset(30, 30),
                      blurRadius: 50,
                    )
                  ],
                ),
                child: Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: SvgPicture.asset(asset, color: loadind ? Colors.black12 : getStateColor(state), width: 100, height: 100,),


                ),
              ),

            ],
          ),





        ),
      ),
    );
  }
}