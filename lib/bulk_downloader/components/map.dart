import 'package:electro/data/singletons/bulk_dowload_provider.dart';
import 'package:electro/data/singletons/general_provider.dart';
import 'package:electro/models/columns/layer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong2/latlong.dart';
import 'package:provider/provider.dart';
import 'package:flutter_map_tile_caching/flutter_map_tile_caching.dart';
import 'region_mode.dart';

class MapView extends StatelessWidget {
  const MapView({
    Key? key,
    required this.controller,
    required this.mcm,
    required this.layers,
    required this.layerIdsMap,
    required this.activeLayers,
    required this.objectGeometryCoords,
  }) : super(key: key);
  final List<LayerModel> layers;
  final Map<String, double> layerIdsMap;
  final List<String> activeLayers;
  final MapController controller;
  final MapCachingManager mcm;
  final List<LatLng> objectGeometryCoords;
  final Distance dist = const Distance(roundResult: false);

  @override
  Widget build(BuildContext context) {
    return Consumer2<GeneralProvider, BulkDownloadProvider>(
      builder: (context, p, bdp, _) {
        final String? source =
            p.persistent!.getString('${mcm.storeName}: sourceURL') ??
                'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
        return FutureBuilder<void>(
          future: controller.onReady,
          builder: (context, _) {
            return FlutterMap(
              mapController: controller,
              options: MapOptions(
                center: objectGeometryCoords.isNotEmpty
                    ? objectGeometryCoords[0]
                    : LatLng(59.94231, 30.328849),
                zoom: 10,
                interactiveFlags: InteractiveFlag.all & ~InteractiveFlag.rotate,
              ),
              layers: [
                TileLayerOptions(
                  urlTemplate: source,
                  subdomains: ['a', 'b', 'c'],
                  tileProvider: const NonCachingNetworkTileProvider(),
                  maxZoom: 20,
                  reset: p.resetController.stream,
                  tileBuilder: (_, child, __) => Container(
                    decoration: BoxDecoration(border: Border.all()),
                    child: child,
                  ),
                ),
                ..._getWmsOverlayOptions(layers, layerIdsMap, activeLayers),
                PolylineLayerOptions(
                  polylines: [
                    Polyline(
                      points: objectGeometryCoords,
                      strokeWidth: 5,
                      color: Colors.purple,
                    ),
                    /*Polyline(
                        points: boundedBox,
                        strokeWidth: 5,
                        borderColor: Colors.red[300]!.withOpacity(0.5),
                        color: Colors.red,
                      ),*/
                  ],
                ),
              ],
            );
          },
        );
      },
    );
  }

  List<TileLayerOptions> _getWmsOverlayOptions(List<LayerModel> layers,
      Map<String, double> layerIdsMap, List<String> activeLayers) {
    print("ACTIVE LAYERS = $activeLayers");

    /// Get Test String
    String getTestString(List<Map<String, dynamic>> configs) {
      String testString = "";
      for (final config in configs) {
        if (config["nick"] != "layers") {
          testString += "${config["value"]};";
        }
      }
      return testString;
    }

    /// Transform config to Map
    Map<String, dynamic> configsListToMap(List<Map<String, dynamic>> configs) {
      Map<String, dynamic> res = {};
      for (final config in configs) {
        res[config["nick"]] = config["value"];
      }

      return res;
    }

    List<Map<String, dynamic>> layersGroup = [];
    Map<String, String> idToLayer = {};
    for (final layer in layers) {
      String testString = getTestString(layer.mapProjectLayerConfigs);
      var foundIndex = -1;
      for (int i = 0; i < layersGroup.length; i++) {
        if (layersGroup[i]["testString"] == testString &&
            layersGroup[i]["uri"] == layer.uri) {
          foundIndex = i;
          break;
        }
      }
      final configsMap = configsListToMap(layer.mapProjectLayerConfigs);
      idToLayer[layer.id] = configsMap["layers"];
      if (foundIndex == -1) {
        layersGroup.add(<String, dynamic>{
          'testString': testString,
          'layers': <String>[configsMap["layers"]],
          'uri': layer.uri,
          for (final mapEntry in configsMap.entries)
            if (mapEntry.key != 'layers') mapEntry.key: mapEntry.value,
        });
      } else {
        layersGroup[foundIndex]["layers"].add(configsMap['layers']);
      }
    }

    final List<TileLayerOptions> tileLayerOptions = [];

    for (final layerGroup in layersGroup) {
      List<String> _layerIds = [];
      List<double> opacities = [];
      for (String _layerId in layerIdsMap.keys) {
        _layerIds.add(idToLayer[_layerId]!);
        opacities.add(layerIdsMap[_layerId]!);
      }
      print("LAYERS IDS = $opacities");
      //print("LAYERS = $_layers");
      /*for (String _layerId in _layerIds) {
        tileLayerOptions.add(TileLayerOptions(
          opacity: layerIdsMap[_layerId]!,
          backgroundColor: Colors.transparent,
          wmsOptions: WMSTileLayerOptions(
            baseUrl: layerGroup['uri'] + "?",
            layers: [idToLayer[_layerId]!],
            format: layerGroup['format'],
            version: "1.12.2",
            transparent: layerGroup['transparent'],
            otherParameters: {
              for (final option in layerGroup.entries)
                if (!["layers", "format", "transparent", "uri"]
                    .contains(option.key))
                  option.key: option.value.toString(),
            },
          ),
        ));
      }*/

      tileLayerOptions.add(
        TileLayerOptions(
          opacity: opacities.isNotEmpty ? opacities[0] : 1,
          backgroundColor: Colors.transparent,
          //keepBuffer: 1,
          wmsOptions: WMSTileLayerOptions(
            baseUrl: layerGroup['uri'] + "?",
            layers: _layerIds,
            format: layerGroup['format'],
            version: "1.12.2",
            transparent: layerGroup['transparent'],
            otherParameters: {
              for (final option in layerGroup.entries)
                if (!["layers", "format", "transparent", "uri"]
                    .contains(option.key))
                  option.key: option.value.toString(),
            },
          ),
        ),
      );
    }
    return tileLayerOptions;
  }

  Marker _buildCrosshairMarker(LatLng point) {
    return Marker(
      point: point,
      builder: (context) {
        return Stack(
          children: [
            Center(
              child: Container(
                color: Colors.black,
                height: 1,
                width: 10,
              ),
            ),
            Center(
              child: Container(
                color: Colors.black,
                height: 10,
                width: 1,
              ),
            )
          ],
        );
      },
    );
  }
}
