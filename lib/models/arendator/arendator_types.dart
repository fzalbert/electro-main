import 'package:electro/models/arendator/types_model.dart';

class Arendator {
  final int total;
  final List<dynamic> rows;

  Arendator({required this.total, required this.rows});
  Map<String, dynamic> toMap() {
    return {
      'total': total,
      'rows': rows,
    };
  }

  factory Arendator.fromJson(Map<String, dynamic> json) {
    return Arendator(
      total: json["total"],
      rows: json["rows"]
          .map((jsonElement) => Type.fromJson(jsonElement))
          .toList(),
    );
  }
}
