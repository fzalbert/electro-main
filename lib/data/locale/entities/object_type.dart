import 'package:electro/models/object/draw_type.dart';
import 'package:floor/floor.dart';



@Entity(tableName: ObjectTypeEntity.TABLE_NAME)
class ObjectTypeEntity extends ObjectType {

  @PrimaryKey()
  String id;

  ///parent directory id [DrawTypeResponseDto]
  String directoryId;

  String title;

  @ignore
  static const TABLE_NAME = "object_type";


  ObjectTypeEntity({
    required this.id,
    required this.directoryId,
    required this.title
  });

  @override
  String getId() {
    return id;
  }

  @override
  String getParentId() {
    return directoryId;
  }

  @override
  String getTitle() {
    return title;
  }

}

class ObjectTypeModel extends ObjectType {

  String id;

  ///parent directory id [DrawTypeResponseDto]
  String directoryId;

  String title;

  ObjectTypeModel({
    required this.id,
    required this.directoryId,
    required this.title
  });

  @override
  String getId() {
    return id;
  }

  @override
  String getParentId() {
    return directoryId;
  }

  @override
  String getTitle() {
    return title;
  }

  factory ObjectTypeModel.fromEntity(ObjectTypeEntity entity){
    return ObjectTypeModel(
        id: entity.id,
        directoryId: entity.directoryId,
        title: entity.title
    );
  }
}

abstract class ObjectType {
  ///id of this data
  String getId();

  /// parentId of a child
  String getParentId();

  /// Text displayed on the parent/child tile
  String getTitle();
}