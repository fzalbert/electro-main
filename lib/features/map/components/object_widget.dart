import 'package:electro/bloc/object/object_cubit.dart';
import 'package:electro/data/utils/functions.dart';
import 'package:electro/features/crete-user/pages/error_page.dart';
import 'package:electro/features/crete-user/pages/loading_page.dart';
import 'package:electro/features/draw_type/draw_type_tree_widget.dart';
import 'package:electro/features/draw_type/object/preview_object_widget.dart';
import 'package:electro/features/map/components/types_for_click.dart';
import 'package:electro/features/map/preview_object_info.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'dart:ui' as ui;

import 'package:latlong2/latlong.dart';

class ObjectInfoWidget extends StatefulWidget {
  final LatLng pos;
  final List<String> layerIds;

  const ObjectInfoWidget({
    Key? key,
    required this.pos,
    required this.layerIds,
  }) : super(key: key);

  @override
  _ObjectInfoWidgetState createState() => _ObjectInfoWidgetState();
}

class _ObjectInfoWidgetState extends State<ObjectInfoWidget> {
  @override
  Widget build(BuildContext context) {
    context
        .read<ObjectCubit>()
        .getTypesObject(pos: widget.pos, layerIds: widget.layerIds);
    return BlocListener(
      bloc: BlocProvider.of<ObjectCubit>(context),
      listener: (BuildContext context, ObjectState state) async {
        if (state is ErrorObjectState) {
          Navigator.of(context).pop();
        }
      },
      child: BlocBuilder(
        bloc: BlocProvider.of<ObjectCubit>(context),
        builder: (BuildContext context, ObjectState state) {
          if (state is LoadingObjectState) {
            return const LoadingPage(
              height: 0,
            );
          }
          if (state is LoadedTypesObjectState) {
            return TypeObjects(
              typeIds: state.typeIds,
              layerIds: widget.layerIds,
            );
          } else if (state is LoadedObjectState) {
            return Container(
              height: MediaQuery.of(context).size.height * 0.6,
              child: PreviewObjectInfoWidget(object: state.object),
            );
          } else {
            return Container(
              height: 0,
            );
          }
        },
      ),
    );
  }
}
