import 'package:electro/app/theme/theme.dart';
import 'package:electro/bloc/arendator/arendator_cubit.dart';
import 'package:electro/features/crete-user/fields/image_upload.dart';
import 'package:electro/features/projects/components/editing_textfield.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/src/provider.dart';

class TypesOfDocuments extends StatelessWidget {
  final List<dynamic> typesArendator;
  const TypesOfDocuments({Key? key, required this.typesArendator})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.fromLTRB(16, 44, 16, 0),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            for (var arendator in typesArendator)
              CupertinoButton(
                onPressed: () {
                  print("ID = ${arendator.id}");

                  context
                      .read<ArendatorCubit>()
                      .getDocumentsForArendator(arendatorId: arendator.id);
                },
                padding: const EdgeInsets.all(0),
                child: Column(
                  children: [
                    Text(
                      arendator.name,
                      style: Theme.of(context).textTheme.headline3!.copyWith(
                            fontSize: 20,
                          ),
                    ),
                    const SizedBox(height: 28),
                  ],
                ),
              ),
          ],
        ),
      ),
    );
  }
}
