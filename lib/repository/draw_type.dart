import 'package:dio/dio.dart';
import 'package:electro/data/singletons/dio_settings.dart';
import 'package:electro/data/singletons/storage.dart';
import 'package:electro/models/object/draw_type.dart';
import 'package:flutter/material.dart';

class DrawTypeRepository {
  Future<List<DrawTypeResponseDto>> getAll() async {
    await StorageRepository.getInstance();
    Dio _dio = DioSettings.createDio(connectTimeout: 30000);
    final Response response = await _dio.get(
      'GetDrawTypes/',
      options: Options(headers: {
        'Authorization': 'Bearer ${StorageRepository.getString('token')}'
      }),
    );

    try {
      if (response.statusCode! >= 200 && response.statusCode! < 300) {
        List<DrawTypeResponseDto> list = [];
        List<dynamic> result = response.data;
        //print(result[1]["ChildrenDirectories"][0]["Layers"][2]);
        for (var element in result) {
          list.add(DrawTypeResponseDto.fromJson(element));
        }
        return list;
      } else {
        throw NetworkException(response: response);
      }
    } catch (e) {
      var context = GlobalInfo.key.currentContext;
      if (context != null) {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text(e.toString()),
          duration: const Duration(milliseconds: 700),
        ));
      }

      print(e.toString());
      rethrow;
    }
  }

  Future<String?> getDrawTypeId() {
    return getAll().then((items) {
      return items.isEmpty ? null : items.first.id;
    });
  }

  Future<Map<dynamic, String?>> getDrawTypeIdForRoute() {
    return getAll().then((items) {
      return {
        'typeId': items.isEmpty
            ? null
            : items.first.objectTypes.first.childrenTypes
                .firstWhere((el) => el.name == "Маршрут")
                .getId()
                .toString(),
        'groupId': items.isEmpty ? null : items.first.getId().toString()
      };
    });
  }
}

class NetworkException implements Exception {
  Response response;

  NetworkException({required this.response});
}
