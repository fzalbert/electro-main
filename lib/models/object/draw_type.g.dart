// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'draw_type.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DrawTypeResponseDto _$DrawTypeResponseDtoFromJson(Map<String, dynamic> json) =>
    DrawTypeResponseDto(
      id: json['Id'] as String,
      directoryName: json['DirectoryName'] as String,
      orderNumber: json['OrderNumber'] as int,
      objectTypes: (json['ObjectTypes'] as List<dynamic>)
          .map((e) => ObjectTypeResponseDto.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$DrawTypeResponseDtoToJson(
        DrawTypeResponseDto instance) =>
    <String, dynamic>{
      'Id': instance.id,
      'DirectoryName': instance.directoryName,
      'OrderNumber': instance.orderNumber,
      'ObjectTypes': instance.objectTypes,
    };
