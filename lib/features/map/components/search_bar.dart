import 'dart:convert';
import 'dart:ui';

import 'package:electro/bloc/search/search_cubit.dart';
import 'package:electro/features/crete-user/pages/error_page.dart';
import 'package:electro/features/crete-user/pages/loading_page.dart';
import 'package:electro/features/map/components/search/search_text_field.dart';
import 'package:electro/app/theme/theme.dart';
import 'package:electro/models/columns/layer.dart';
import 'package:electro/models/search/search_request.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:latlong2/latlong.dart';
import 'package:provider/src/provider.dart';

class SearchBar extends StatefulWidget {
  final VoidCallback? onSettingsTapped;
  final List<LayerModel> layerModels;
  final Function callback;

  const SearchBar(
      {this.onSettingsTapped,
      required this.layerModels,
      required this.callback,
      Key? key})
      : super(key: key);

  @override
  _SearchBarState createState() => _SearchBarState();
}

class _SearchBarState extends State<SearchBar> {
  final _searchFieldFocusNode = FocusNode();

  bool _isCollapsed = true;
  int? settingsValue = 8;
  bool openSettings = false;
  bool chooseLayer = false;
  String layerId = "";
  int? layersValue = -1;
  Map<String, dynamic> settingsParameters = {
    "1": "По кадастровому номеру",
    "2": "По адресу",
    "3": "По правообладателю",
    "5": "По родительскому кадастровому номеру",
    "6": "По кварталу",
    "7": "По идентификатору",
    "8": "По координатам",
    "9": "По наименованию",
  };
  int maxKey = 0;

  String _currentFilter = "";
  double latitude = 0.0;
  double longitude = 0.0;
  bool isPrecise = false;

  @override
  void initState() {
    _searchFieldFocusNode.addListener(_focusHandler);
    initSettings();
    super.initState();
  }

  void initSettings() async {
    Map<String, dynamic> params =
        await context.read<SearchCubit>().initializeSearchParameters();

    for (String key in params.keys) {
      if (int.parse(key) > maxKey) {
        setState(() {
          maxKey = int.parse(key);
        });
      }
    }
    print("MAX KEY = $maxKey");
    if (params.isNotEmpty) {
      setState(() {
        settingsParameters = params;
      });
    }
  }

  @override
  void dispose() {
    _searchFieldFocusNode.removeListener(_focusHandler);
    _searchFieldFocusNode.dispose();
    super.dispose();
  }

  _focusHandler() {
    setState(() {
      _isCollapsed = !_searchFieldFocusNode.hasFocus || _currentFilter.isEmpty;
    });
  }

  @override
  Widget build(BuildContext context) {
    //initSettings();
    return Container(
      decoration: BoxDecoration(
        gradient: const LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            Color.fromRGBO(255, 255, 255, 0.5),
            Color.fromRGBO(215, 224, 237, 0.5),
          ],
        ),
        borderRadius: BorderRadius.circular(40.0),
        border: Border.all(color: Colors.white),
        boxShadow: const [
          BoxShadow(
            color: Color(0x1A193D83),
            offset: Offset(30, 30),
            blurRadius: 50,
          )
        ],
      ),
      child: AnimatedSize(
        alignment: Alignment.topCenter,
        duration: const Duration(milliseconds: 350),
        curve: Curves.easeOut,
        child: Column(
          children: [
            SearchTextField(
              onChanged: (value) {
                setState(() {
                  _currentFilter = value;
                  _isCollapsed =
                      !_searchFieldFocusNode.hasFocus || _currentFilter.isEmpty;
                  openSettings = false;
                });

                context.read<SearchCubit>().searchByLayerText(
                      requestBody: SearchRequest(
                        layerId: layerId,
                        searchType: settingsValue! + 1,
                        searchText: _currentFilter,
                        equalSearch: isPrecise,
                      ),
                    );
              },
              onLat: (value) {
                setState(() {
                  latitude = double.parse(value);
                  _isCollapsed = !(latitude != 0.0);
                  openSettings = false;
                });

                context.read<SearchCubit>().searchByLayerText(
                      requestBody: SearchRequest(
                        layerId: layerId,
                        searchType: settingsValue! + 1,
                        searchText: _currentFilter,
                        searchLat: latitude,
                        searchLng: longitude,
                        equalSearch: isPrecise,
                      ),
                    );
              },
              onLng: (value) {
                setState(() {
                  longitude = double.parse(value);
                  _isCollapsed = !(longitude != 0.0);
                  openSettings = false;
                });

                context.read<SearchCubit>().searchByLayerText(
                      requestBody: SearchRequest(
                        layerId: layerId,
                        searchType: settingsValue! + 1,
                        searchText: _currentFilter,
                        searchLat: latitude,
                        searchLng: longitude,
                        equalSearch: isPrecise,
                      ),
                    );
              },
              isAddress: settingsValue! + 1 == 8 ? true : false,
              onSettingsTapped: () {
                initSettings();
                setState(() {
                  openSettings = !openSettings;
                });
              },
              focusNode: _searchFieldFocusNode,
            ),
            if (openSettings)
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    padding: const EdgeInsets.fromLTRB(21, 20, 12, 0),
                    child: Row(
                      children: [
                        Checkbox(
                          value: isPrecise,
                          onChanged: (bool? newValue) {
                            setState(() {
                              isPrecise = newValue!;
                            });
                          },
                        ),
                        Text(
                          "Точное совпадение",
                          style: Theme.of(context)
                              .textTheme
                              .headline4!
                              .copyWith(fontSize: 16),
                        ),
                      ],
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        chooseLayer = !chooseLayer;
                      });
                    },
                    child: Container(
                      padding: const EdgeInsets.fromLTRB(41, 20, 12, 0),
                      child: Text(
                        "Выбрать слой",
                        style: Theme.of(context)
                            .textTheme
                            .headline4!
                            .copyWith(fontSize: 16),
                      ),
                    ),
                  ),
                  Divider(),
                  if (chooseLayer)
                    SizedBox(
                      height: MediaQuery.of(context).size.height * 0.25,
                      child: Scrollbar(
                        isAlwaysShown: true,
                        child: ListView.builder(
                          padding: const EdgeInsets.fromLTRB(21, 20, 12, 0),
                          itemCount: widget.layerModels.length,
                          itemBuilder: (context, index) {
                            return Padding(
                              padding: const EdgeInsets.symmetric(vertical: 2),
                              child: Row(
                                children: [
                                  Radio(
                                    groupValue: layersValue,
                                    value: index,
                                    onChanged: (int? newValue) {
                                      setState(() {
                                        layerId = widget.layerModels[index].id;
                                        layersValue = newValue;
                                      });
                                    },
                                  ),
                                  Expanded(
                                    child: FutureBuilder<Widget>(
                                      future: loadItem(
                                          widget.layerModels[index].legendLink,
                                          widget.layerModels[index].name),
                                      builder: (BuildContext context,
                                          AsyncSnapshot<Widget> snapshot) {
                                        if (snapshot.hasData) {
                                          return snapshot.data!;
                                        } else if (snapshot.hasError) {
                                          return loadItemWithoutImage(
                                              widget.layerModels[index].name);
                                        } else {
                                          return loadItemWithoutImage(
                                              widget.layerModels[index].name);
                                        }
                                      },
                                    ),
                                  ),
                                ],
                              ),
                            );
                          },
                        ),
                      ),
                    ),
                  if (chooseLayer) Divider(),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.25,
                    child: Scrollbar(
                      isAlwaysShown: true,
                      child: ListView.builder(
                        padding: const EdgeInsets.fromLTRB(21, 20, 12, 0),
                        itemCount: maxKey,
                        itemBuilder: (context, index) {
                          if (settingsParameters[(index + 1).toString()] ==
                              null) {
                            return Container();
                          } else {
                            return Padding(
                              padding: const EdgeInsets.symmetric(vertical: 2),
                              child: Row(
                                children: [
                                  Radio(
                                    groupValue: settingsValue,
                                    value: index,
                                    onChanged: (int? newValue) {
                                      setState(() {
                                        settingsValue = newValue;
                                      });
                                    },
                                  ),
                                  Expanded(
                                    child: Text(
                                      settingsParameters[
                                              (index + 1).toString()] ??
                                          "",
                                      style: Theme.of(context)
                                          .textTheme
                                          .headline4!
                                          .copyWith(fontSize: 16),
                                    ),
                                  ),
                                ],
                              ),
                            );
                          }
                        },
                      ),
                    ),
                  ),
                ],
              ),
            if (!_isCollapsed)
              Container(
                color: const Color(0xFFF7F9FC),
                height: 1,
              ),
            if (!_isCollapsed)
              SizedBox(
                height: 242,
                child: BlocBuilder(
                  bloc: BlocProvider.of<SearchCubit>(context),
                  builder: (BuildContext context, SearchState state) {
                    if (state is SearchingState) {
                      return const LoadingPage();
                    }
                    if (state is LoadedSearchState) {
                      if (state.items.isNotEmpty) {
                        return ListView.separated(
                            padding: const EdgeInsets.fromLTRB(51, 20, 12, 0),
                            itemCount: state.items.length,
                            separatorBuilder: (context, index) => const Divider(
                                  color: Color(0xFFF8F8F8),
                                ),
                            itemBuilder: (context, index) {
                              return CupertinoButton(
                                onPressed: () {
                                  print(
                                      "\n\nGEOM = ${state.items[index].geometry}\n\n");

                                  Map<String, dynamic> geoMap =
                                      jsonDecode(state.items[index].geometry!);

                                  List<double> coords;
                                  if (geoMap["type"] == "GeometryCollection") {
                                    Map<String, dynamic> initGeoMap =
                                        geoMap["geometries"][0];
                                    if (initGeoMap["type"] == "Point") {
                                      coords = (initGeoMap["coordinates"]
                                              as List<dynamic>)
                                          .cast<double>();
                                    } else {
                                      coords = (initGeoMap["coordinates"][0]
                                              as List<dynamic>)
                                          .cast<double>();
                                    }
                                  } else if (geoMap["type"] == "Point") {
                                    coords =
                                        (geoMap["coordinates"] as List<dynamic>)
                                            .cast<double>();
                                  } else {
                                    print("HERE ${geoMap["coordinates"][0]}");
                                    coords = (geoMap["coordinates"][0]
                                            as List<dynamic>)
                                        .cast<double>();
                                  }

                                  widget.callback(
                                      LatLng(coords[1], coords[0]), layerId);
                                },
                                padding: EdgeInsets.all(0),
                                child: Padding(
                                  padding:
                                      const EdgeInsets.symmetric(vertical: 8),
                                  child: Row(
                                    children: [
                                      Expanded(
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          mainAxisSize: MainAxisSize.max,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              state.items[index].keyIdentifier,
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .headline4!
                                                  .copyWith(fontSize: 16),
                                            ),
                                            Text(
                                              state.items[index].result,
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .headline3!
                                                  .copyWith(
                                                      fontSize: 12,
                                                      color: AppTheme
                                                          .textGreyDark),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              );
                            });
                      } else {
                        return const ErrorPage(errorText: "Ничего не найдено");
                      }
                    } else if (state is ErrorSearching) {
                      return ErrorPage(
                        errorText: state.errorText,
                      );
                    } else {
                      return const ErrorPage();
                    }
                  },
                ),
              ),
          ],
        ),
      ),
    );
  }

  Future<Widget> loadItem(String uri, String name) async {
    return Container(
      width: MediaQuery.of(context).size.width * 0.5,
      child: Row(
        children: [
          Image.network(
            uri,
            width: 20,
            height: 20,
          ),
          const SizedBox(
            width: 12,
          ),
          Expanded(
            child: Text(
              name,
              style: Theme.of(context)
                  .textTheme
                  .headline4!
                  .copyWith(fontSize: 16, color: AppTheme.regularText),
            ),
          ),
        ],
      ),
    );
  }

  Widget loadItemWithoutImage(String name) {
    return Container(
      width: MediaQuery.of(context).size.width * 0.5,
      child: Row(
        children: [
          Expanded(
            child: Text(
              name,
              style: Theme.of(context)
                  .textTheme
                  .headline4!
                  .copyWith(fontSize: 16, color: AppTheme.regularText),
            ),
          ),
        ],
      ),
    );
  }
}
