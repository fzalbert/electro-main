import 'package:electro/data/singletons/stop_and_go.dart';
import 'package:electro/data/utils/functions.dart';
import 'package:electro/features/add-objects/inventory_bottomsheet.dart';
import 'package:electro/utils/bottom_sheet_helper.dart';
import 'package:electro/widgets/btns/rounded_button.dart';
import 'package:flutter/cupertino.dart';

Widget inventoryBtnCreate(BuildContext context, bool isStarted) {
  return Padding(
    padding: const EdgeInsets.only(bottom: 50),
    child: Row(
      children: [
        const SizedBox(
          width: 16,
        ),
        Expanded(
          child: RoundedButton(
            title: isStarted ? "Инвентаризация" : "Продолжить",
            height: 48,
            cornerRadius: 10,
            onTap: () {
              FocusScope.of(context).requestFocus(FocusNode());
              if (isStarted) {
                _showInventoryActions(context);
              } else {
                _continueSession(context);
              }
            },
          ),
        ),
        const SizedBox(
          width: 16,
        )
      ],
    ),
  );
}

void _showInventoryActions(BuildContext context) {
  MyFunctions.popBottomSheet(
      InventoryBottomSheet(
        isActive: StopAndGo.isStarted,
      ),
      true,
      context);
}

void _continueSession(BuildContext context) {
  BottomSheetHelper.showSelectable(
    context,
    text: "Вы уверены что хотите продолжить сессию?",
    approve: () {
      StopAndGo.startPrevious();
    },
  );
}
