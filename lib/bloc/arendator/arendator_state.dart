part of 'arendator_cubit.dart';

abstract class ArendatorState extends Equatable {}

class InitialArendatorState extends ArendatorState {
  @override
  List<Object> get props => [];
}

class LoadingArendatorState extends ArendatorState {
  @override
  List<Object> get props => [];
}

class LoadedTypesArendatorState extends ArendatorState {
  LoadedTypesArendatorState(this.typesArendator);

  final List<dynamic> typesArendator;

  @override
  List<Object> get props => [typesArendator];
}

class LoadedCreateDocumentState extends ArendatorState {
  LoadedCreateDocumentState(this.documents, this.typesId);

  final List<dynamic> documents;
  final String typesId;

  @override
  List<Object> get props => [documents, typesId];
}

class EditDocumentState extends ArendatorState {
  EditDocumentState(this.documents, this.typesId, this.arendatorId);

  final List<dynamic> documents;
  final String typesId;
  final String arendatorId;

  @override
  List<Object> get props => [documents, typesId, arendatorId];
}

class LoadedDocumentsState extends ArendatorState {
  LoadedDocumentsState(this.documents, this.typesId);

  final List<Map> documents;
  final String typesId;

  @override
  List<Object> get props => [documents, typesId];
}

class ErrorArendatorState extends ArendatorState {
  @override
  List<Object> get props => [];
}
