part of 'object_cubit.dart';

abstract class ObjectState extends Equatable {}

class InitialObjectState extends ObjectState {
  @override
  List<Object> get props => [];
}

class LoadingObjectState extends ObjectState {
  @override
  List<Object> get props => [];
}

class LoadedTypesObjectState extends ObjectState {
  LoadedTypesObjectState(this.typeIds);

  final List<List<String>> typeIds;

  @override
  List<Object> get props => [typeIds];
}

class LoadedObjectState extends ObjectState {
  LoadedObjectState(
    this.object,
  );

  final ObjectInfo object;

  @override
  List<Object> get props => [object];
}

class ErrorObjectState extends ObjectState {
  @override
  List<Object> get props => [];
}
