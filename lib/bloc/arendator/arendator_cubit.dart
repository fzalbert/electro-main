import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:electro/models/arendator/arendator_types.dart';
import 'package:electro/repository/arendator.dart';
import 'package:electro/repository/file.dart';
import 'package:equatable/equatable.dart';

part 'arendator_state.dart';

class ArendatorCubit extends Cubit<ArendatorState> {
  ArendatorCubit({required this.repository, required this.fileRepository})
      : super(InitialArendatorState()) {
    initializeArendator();
  }
  final ArendatorRepository repository;
  final FileRepository fileRepository;

  void initializeArendator() async {
    try {
      emit(LoadingArendatorState());
      List<dynamic> typesArendator = await repository.getTypesArendator();
      if (typesArendator.isEmpty) {
        print("ERROR = empty response");
        emit(ErrorArendatorState());
        return;
      }
      emit(LoadedTypesArendatorState(typesArendator));
    } catch (e) {
      print("ERROR = $e");
      emit(ErrorArendatorState());
    }
  }

  void getDocumentStructureForArendator({required String arendatorId}) async {
    try {
      emit(LoadingArendatorState());
      List<dynamic> documentStructure = await repository
          .getDocumentStructureForArendator(arendatorId: arendatorId);

      emit(LoadedCreateDocumentState(documentStructure, arendatorId));
    } catch (e) {
      print("ERROR = $e");
      emit(ErrorArendatorState());
    }

  }

  void getDocumentEditForArendator(
      {required String arendatorId, required String typesId}) async {
    try {
      emit(LoadingArendatorState());
      List<dynamic> documentStructure =
          await repository.getDocumentEditForArendator(
              arendatorId: arendatorId, typesId: typesId);

      emit(EditDocumentState(documentStructure, typesId, arendatorId));
    } catch (e) {
      print("ERROR = $e");
      emit(ErrorArendatorState());
    }
  }

  void getDocumentsForArendator({required String arendatorId}) async {
    try {
      emit(LoadingArendatorState());

      List<Map> documents =
          await repository.getDocumentsForArendator(arendatorId: arendatorId);
      if (documents.isEmpty) {
        documents = [
          {"arendatorId": arendatorId}
        ];
      }
      emit(LoadedDocumentsState(documents, arendatorId));
    } catch (e) {
      print("ERROR = $e");
      emit(ErrorArendatorState());
    }
  }

  void postDocumentToApi(
      {required String arendatorId, required Map documentMap}) async {
    try {
      emit(LoadingArendatorState());

      await repository.postDocument(documentMap: documentMap);
      List<Map> documents =
          await repository.getDocumentsForArendator(arendatorId: arendatorId);
      emit(LoadedDocumentsState(documents, arendatorId));
    } catch (e) {
      print("ERROR = $e");
      emit(ErrorArendatorState());
    }
  }

  void postEditDocumentToApi(
      {required String arendatorId, required Map documentMap}) async {
    try {
      emit(LoadingArendatorState());

      print("EDIT DOC = ${documentMap}");

      await repository.postEditDocument(documentMap: documentMap);
      List<Map> documents =
          await repository.getDocumentsForArendator(arendatorId: arendatorId);
      print("DODS = $documents and ID = $arendatorId");
      emit(LoadedDocumentsState(documents, arendatorId));
    } catch (e) {
      print("ERROR = $e");
      emit(ErrorArendatorState());
    }
  }

  Future<Map> postFileToApi({required File file}) async {
    Map fileInfo = await fileRepository.postFile(file: file);
    return fileInfo;
  }

  Future<File> dowloadFile(
      {required String idFile, required String arendatorId}) async {
    File file = await fileRepository.downloadFile(
        idFile: idFile, arendatorId: arendatorId);

    return file;
  }
}
