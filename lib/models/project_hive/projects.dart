import 'package:electro/models/columns/directory.dart';
import 'package:electro/models/project_hive/directory_response_model.dart';
import 'package:hive/hive.dart';

part "projects.g.dart";

@HiveType(typeId: 0)
class ProjectsModel extends HiveObject {
  ProjectsModel({required this.projects});

  @HiveField(0)
  List<DirectoryResponseDtoHiveModel> projects;
}
