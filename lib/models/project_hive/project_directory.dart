import 'package:electro/models/columns/layer_directory.dart';
import 'package:electro/models/project_hive/layer_directory.dart';
import 'package:hive/hive.dart';
import 'package:json_annotation/json_annotation.dart';

part 'project_directory.g.dart';

@HiveType(typeId: 2)
class ProjectDirectoryResponseDtoHiveModel extends HiveObject {
  @HiveField(0)
  final String id;

  @HiveField(1)
  final String mapProjectId;

  @HiveField(2)
  final String mapProjectDirectoryId;

  @HiveField(3)
  final String name;

  @HiveField(4)
  final bool activated;

  @HiveField(5)
  final int orderNumber;

  @HiveField(6)
  final String serviceId;

  @HiveField(7)
  final bool rootServiceDirectory;

  @HiveField(8)
  final String createdBy;

  @HiveField(9)
  final String timeCreated;

  @HiveField(10)
  final bool enabledDefault;

  @HiveField(11)
  final List<LayerDirectoryHiveModel> layers;

  @HiveField(12)
  final List<ProjectDirectoryResponseDtoHiveModel> childrenDirectories;

  ProjectDirectoryResponseDtoHiveModel(
      {required this.activated,
      required this.createdBy,
      required this.layers,
      required this.childrenDirectories,
      required this.id,
      required this.enabledDefault,
      required this.mapProjectDirectoryId,
      required this.mapProjectId,
      required this.name,
      required this.orderNumber,
      required this.rootServiceDirectory,
      required this.serviceId,
      required this.timeCreated});
}
