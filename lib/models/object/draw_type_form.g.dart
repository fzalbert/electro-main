// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'draw_type_form.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DrawTypeForm _$DrawTypeFormFromJson(Map<String, dynamic> json) => DrawTypeForm(
      id: json['Id'] as String,
      mapObjectTypeParentId: json['MapObjectTypeParentId'] as String?,
      name: json['Name'] as String,
      isCanDraw: json['IsCanDraw'] as bool,
      filesEnabled: json['FilesEnabled'] as bool,
      filesMaxSize: json['FilesMaxSize'],
      filesMultiply: json['FilesMultiply'] as bool,
      filesBoundsEnabled: json['FilesBoundsEnabled'] as bool,
      parameters: (json['Parameters'] as List<dynamic>)
          .map((e) => Parameter.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$DrawTypeFormToJson(DrawTypeForm instance) =>
    <String, dynamic>{
      'Id': instance.id,
      'MapObjectTypeParentId': instance.mapObjectTypeParentId,
      'Name': instance.name,
      'IsCanDraw': instance.isCanDraw,
      'FilesEnabled': instance.filesEnabled,
      'FilesMaxSize': instance.filesMaxSize,
      'FilesMultiply': instance.filesMultiply,
      'FilesBoundsEnabled': instance.filesBoundsEnabled,
      'Parameters': instance.parameters,
    };
