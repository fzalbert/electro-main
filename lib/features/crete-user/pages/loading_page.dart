import 'package:electro/app/theme/theme.dart';
import 'package:electro/features/crete-user/fields/image_upload.dart';
import 'package:electro/features/projects/components/editing_textfield.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class LoadingPage extends StatelessWidget {
  final double? height;
  const LoadingPage({Key? key, this.height}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height ?? MediaQuery.of(context).size.height * 0.4,
      padding: const EdgeInsets.fromLTRB(16, 44, 16, 0),
      child: const Center(
        child: CircularProgressIndicator(),
      ),
    );
  }
}
