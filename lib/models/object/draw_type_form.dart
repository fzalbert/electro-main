import 'package:electro/models/object/parameter.dart';
import 'package:json_annotation/json_annotation.dart';

part 'draw_type_form.g.dart';

@JsonSerializable()
class DrawTypeForm {
  DrawTypeForm({
    required this.id,
    this.mapObjectTypeParentId,
    required this.name,
    required this.isCanDraw,
    required this.filesEnabled,
    this.filesMaxSize,
    required this.filesMultiply,
    required this.filesBoundsEnabled,
    required this.parameters,
  });


  ///motId
  @JsonKey(name: 'Id')
  String id;
  @JsonKey(name: 'MapObjectTypeParentId')
  String? mapObjectTypeParentId;
  @JsonKey(name: 'Name')
  String name;
  @JsonKey(name: 'IsCanDraw')
  bool isCanDraw;
  @JsonKey(name: 'FilesEnabled')
  bool filesEnabled;
  @JsonKey(name: 'FilesMaxSize')
  dynamic filesMaxSize;
  @JsonKey(name: 'FilesMultiply')
  bool filesMultiply;
  @JsonKey(name: 'FilesBoundsEnabled')
  bool filesBoundsEnabled;
  @JsonKey(name: 'Parameters')
  List<Parameter> parameters;

  factory DrawTypeForm.fromJson(Map<String, dynamic> json) =>
      _$DrawTypeFormFromJson(json);

  Map<String,dynamic> toJson() => _$DrawTypeFormToJson(this);

}