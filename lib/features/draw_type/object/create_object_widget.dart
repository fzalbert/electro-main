import 'dart:io';

import 'package:electro/app/constants/colors.dart';
import 'package:electro/app/theme/theme.dart';
import 'package:electro/data/locale/entities/file.dart';
import 'package:electro/data/locale/entities/map_object_value.dart';
import 'package:electro/data/singletons/stop_and_go.dart';
import 'package:electro/data/utils/functions.dart';
import 'package:electro/features/crete-user/fields/image_upload.dart';
import 'package:electro/features/crete-user/user_bottom_sheet.dart';
import 'package:electro/features/draw_type/file_service.dart';
import 'package:electro/features/draw_type/object/loader_widget.dart';
import 'package:electro/features/draw_type/object/save_btn.dart';
import 'package:electro/features/draw_type/object_form_service.dart';
import 'package:electro/features/projects/components/editing_textfield.dart';
import 'package:electro/features/sessions/service/object_service.dart';
import 'package:electro/models/arendator/renter.dart';
import 'package:electro/models/object/parameter.dart';
import 'package:electro/repository/draw_type_form.dart';
import 'package:electro/utils/bottom_sheet_helper.dart';
import 'package:electro/utils/guid_helper.dart';
import 'package:electro/utils/string_helper.dart';
import 'package:electro/widgets/btns/rounded_button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:geolocator/geolocator.dart';
import 'package:latlong2/latlong.dart';

import 'edit_text_with_header.dart';

class CreateObjectWidget extends StatefulWidget implements ScrollableWidget {
  @override
  State<StatefulWidget> createState() => _CreateObjectState();

  final String? sessionId;
  final String mapObjectTypeId;
  final String parentDrawTypeId;
  ScrollController? scrollController;

  CreateObjectWidget(
      {Key? key,
      this.sessionId,
      required this.parentDrawTypeId,
      required this.mapObjectTypeId,
      this.scrollController})
      : super(key: key);

  @override
  void setScrollController(ScrollController controller) {
    scrollController = controller;
  }
}

class _CreateObjectState extends State<CreateObjectWidget> {
  final DrawTypeFormRepository _repository = DrawTypeFormRepository();
  final ObjectFormService _formService = ObjectFormService();

  ///map object type
  late String motId;
  bool filesEnabled = false;

  List<Parameter> parameters = <Parameter>[];
  List<FileModel> filesInfo = [];
  bool _isLoading = true;

  Map<String, TextEditingController> editingsControls =
      <String, TextEditingController>{};

  List<Map> documentIds = [];

  @override
  void initState() {
    _isLoading = true;
    updateData();

    super.initState();
  }

  void updateImageController(List<File> files) async {
    filesInfo = [];
    for (File file in files) {
      var fileInfo = await _repository.postFile(file: file);
      FileModel model;
      model = fileInfo ?? FileModel.fromFile(file);
      filesInfo.add(model);
      print("FILE INFO = $model");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.7,
      padding: EdgeInsets.only(top: 10),
      child: _isLoading
          ? const LoaderWidget()
          : Padding(
              padding: MediaQuery.of(context).viewInsets,
              child: ListView.builder(
                  controller: widget.scrollController,
                  itemCount: parameters.length,
                  itemBuilder: (context, index) {
                    var item = parameters[index];
                    print("ITEM = ${item.required}");
                    switch (item.parameterType) {
                      case "Btn":
                        return Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            documentIds.isNotEmpty
                                ? choosedArendator()
                                : chooseArendator(),
                            createSaveBtn(context),
                          ],
                        );
                      case "ImagePicker":
                        //print("PARAN = ${}");
                        return Padding(
                          padding: const EdgeInsets.only(
                              left: 16.0, right: 16.0, top: 12.0),
                          child: ImageUploadItem(
                            loadImages: updateImageController,
                          ),
                        );
                      default:
                        return GestureDetector(
                          child: Padding(
                            padding: const EdgeInsets.symmetric(
                                vertical: 12, horizontal: 16),
                            child: withHeader(parameters[index],
                                isRequired: item.required),
                          ),
                          onTap: () {},
                        );
                    }
                  }),
            ),
    );
  }

  returnDocumentId(
      String _documentId, String renterTypeId, String arendatorName) {
    setState(() {
      documentIds.add({
        "documentId": _documentId,
        "renterTypeId": renterTypeId,
        "arendatorName": arendatorName,
      });
    });
    print(documentIds);
  }

  Widget choosedArendator() {
    return CupertinoButton(
      onPressed: () {
        MyFunctions.popBottomSheet(
            UserBottomSheet(
              callback: returnDocumentId,
              choosedDocumentIds: documentIds,
            ),
            true,
            context,
            isRenter: true);
        documentIds = [];
      },
      padding: EdgeInsets.all(0),
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "арендатор",
              style: Theme.of(context)
                  .textTheme
                  .headline3!
                  .copyWith(fontSize: 12, color: AppTheme.textGreyDark),
            ),
            const SizedBox(
              height: 8,
            ),
            SizedBox(
              height: 50,
              child: ListView.builder(
                itemCount: documentIds.length,
                itemBuilder: (context, index) {
                  return Text(
                    documentIds[index]["arendatorName"],
                    style: Theme.of(context)
                        .textTheme
                        .headline3!
                        .copyWith(fontSize: 14, color: AppTheme.textGrey),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget chooseArendator() {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: RoundedButton(
        title: "Добавить арендатора",
        height: 48,
        color: Colors.transparent,
        borderColor: mainColor,
        titleColor: mainColor,
        width: double.infinity,
        cornerRadius: 10,
        isLoading: false,
        onTap: () {
          MyFunctions.popBottomSheet(
              UserBottomSheet(
                callback: returnDocumentId,
              ),
              true,
              context,
              isRenter: true);
        },
      ),
    );
  }

  Widget createSaveBtn(BuildContext context) {
    return SaveBtn(action: () => _saveObject());
  }

  Widget withHeader(Parameter parameter, {required bool isRequired}) {
    var _controller = editingsControls[parameter.id]!;
    return EditTextWithHeader(
      controller: _controller,
      type: parameter.parameterType,
      title: parameter.parameterName,
      value: parameter.defaultValue,
      optionSelect: parameter.optionSelect,
      isRequired: isRequired,
    );
  }

  void updateData() async {
    setState(() {
      parameters.clear();
    });

    try {
      var result = await _formService.findById(widget.mapObjectTypeId);
      if (result == null) {
        BottomSheetHelper.showSelectable(context,
            text: "Что то пошло не так");
        return;
      }

      motId = result.id;
      filesEnabled = result.filesEnabled;

      for (var parameter in result.parameters) {
        editingsControls[parameter.id] = TextEditingController();
      }

      if(filesEnabled) {
        result.parameters.add(
          Parameter(id: "id", parameterName: "", parameterType: "ImagePicker"));
      }

      result.parameters.add(Parameter(
          id: "id", parameterName: "Сохранить", parameterType: "Btn"));

      setState(() {
        _isLoading = false;
        parameters.addAll(result.parameters);
      });
    } catch (e) {
      setState(() {
        _isLoading = false;
      });
      print(e);
    }
  }

  void _saveObject() async {
    var service = MapObjectService();
    Position location = (await Geolocator.getLastKnownPosition())!;

    List<MapObjectValueModel> values = [];

    var paramsIds = editingsControls.keys;
    for (var id in paramsIds) {
      var control = editingsControls[id]!;
      var parameter = parameters.firstWhere((element) => element.id == id);
      if (parameter.required && control.text == "") {
        MyFunctions.showErrorMessage(
            errorMessage: "Необходимо ввести обязательные поля",
            context: context);
        return;
      }
      String value = control.value.text;

      if(parameter.parameterType == 'select'){
        var items = parameter.optionSelect;
        var id = items.keys.firstWhere((element) =>  items[element] == value);
        value = id;
      }
      print("CONTROLS = ${editingsControls[id]!.value.text}");
      values.add(MapObjectValueModel(
          parameterId: parameter.id,
          value: value,
          title: parameter.parameterName,
          parameterType: parameter.parameterType,
          id: GuidHelper.timeBased()));
    }

    List<RenterModel> renters = [];
    for (var document in documentIds) {
      renters.add(
        RenterModel(
          renterId: document["documentId"],
          renterTypeId: document["renterTypeId"],
        ),
      );
    }

    print("FILES INFO = $filesInfo");
    await service.create(
      sessionId: widget.sessionId,
      drawTypeId: widget.parentDrawTypeId,
      position: LatLng(location.latitude, location.longitude),
      objectTypeId: widget.mapObjectTypeId,
      values: values,
      renters: renters,
      filesInfo: filesInfo,
    );

    Navigator.of(context).pop();
  }
}
