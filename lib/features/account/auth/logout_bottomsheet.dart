import 'package:electro/app/constants/colors.dart';
import 'package:electro/app/theme/theme.dart';
import 'package:electro/bloc/authentication/auth_bloc.dart';
import 'package:electro/bloc/authentication/auth_events.dart';
import 'package:electro/bloc/authentication/auth_state.dart';
import 'package:electro/data/singletons/storage.dart';
import 'package:electro/features/account/auth/auth.dart';
import 'package:electro/features/map/components/map_body.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:formz/formz.dart';

class LogOutBottomSheet extends StatelessWidget {
  final BuildContext parentContext;
  final String title;

  const LogOutBottomSheet(
      {Key? key, required this.parentContext, required this.title})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<AuthBloc, AuthState>(listener: (context, state) {
      if (state.logoutStatus == FormzStatus.submissionSuccess) {
        Navigator.pushAndRemoveUntil(
            context,
            CupertinoPageRoute(builder: (x) => AuthScreenWidget()),
            (route) => false);
      }
    }, builder: (context, state) {
      print(state.logoutStatus);
      return Container(
        padding: const EdgeInsets.fromLTRB(16, 44, 16, 30),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              title,
              style:
                  Theme.of(context).textTheme.headline3!.copyWith(fontSize: 18),
            ),
            const SizedBox(
              height: 28,
            ),
            state.logoutStatus == FormzStatus.submissionInProgress
                ? const Center(
                    child: CupertinoActivityIndicator(),
                  )
                : Row(
                    children: [
                      const SizedBox(
                        width: 30,
                      ),
                      Expanded(
                        child: GestureDetector(
                          onTap: () {
                            print("GLOBAL KEY = ${ModalRoute.of(context)}");
                            Navigator.of(context).pop();
                            Navigator.of(context).maybePop();
                          },
                          child: Container(
                            height: 48,
                            child: Center(
                              child: Text(
                                'Нет',
                                style: Theme.of(context)
                                    .textTheme
                                    .headline4!
                                    .copyWith(
                                        fontSize: 20,
                                        color: AppTheme.regularTextDarkBlue),
                              ),
                            ),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                border: Border.all(color: mainColor)),
                          ),
                        ),
                      ),
                      const SizedBox(
                        width: 30,
                      ),
                      Expanded(
                        child: GestureDetector(
                          onTap: () {
                            context.read<AuthBloc>().add(LogOutUserEvent());
                          },
                          child: Container(
                            child: Center(
                              child: Text(
                                'Да',
                                style: Theme.of(context)
                                    .textTheme
                                    .headline4!
                                    .copyWith(
                                        fontSize: 20, color: Colors.white),
                              ),
                            ),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: mainColor),
                            height: 48,
                          ),
                        ),
                      ),
                      const SizedBox(
                        width: 30,
                      ),
                    ],
                  )
          ],
        ),
      );
    });
  }
}

class ErrorBottomSheet extends StatelessWidget {
  final BuildContext parentContext;
  final String title;

  const ErrorBottomSheet(
      {Key? key, required this.parentContext, required this.title})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.fromLTRB(16, 44, 16, 30),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            title,
            style:
                Theme.of(context).textTheme.headline3!.copyWith(fontSize: 18),
          ),
          const SizedBox(
            height: 28,
          ),
          Row(
            children: [
              Expanded(
                child: GestureDetector(
                  onTap: () {
                    Navigator.of(context).pop();
                  },
                  child: Container(
                    child: Center(
                      child: Text(
                        'Ок',
                        style: Theme.of(context)
                            .textTheme
                            .headline4!
                            .copyWith(fontSize: 20, color: Colors.white),
                      ),
                    ),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: mainColor),
                    height: 48,
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}

class SelectableBottomSheet extends StatelessWidget {
  final BuildContext parentContext;
  final String text;

  final GestureTapCallback? approve;
  final GestureTapCallback? cancel;

  const SelectableBottomSheet(
      {Key? key,
      required this.parentContext,
      required this.text,
      this.approve,
      this.cancel})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.fromLTRB(16, 44, 16, 30),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            text,
            style:
                Theme.of(context).textTheme.headline3!.copyWith(fontSize: 18),
            textAlign: TextAlign.center,
          ),
          const SizedBox(
            height: 28,
          ),
          Row(
            children: [
              const SizedBox(
                width: 30,
              ),
              Expanded(
                child: GestureDetector(
                  onTap: () {
                    Navigator.of(context).pop();
                    cancel?.call();
                  },
                  child: Container(
                    height: 48,
                    child: Center(
                      child: Text(
                        'Нет',
                        style: Theme.of(context).textTheme.headline4!.copyWith(
                            fontSize: 20, color: AppTheme.regularTextDarkBlue),
                      ),
                    ),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(color: mainColor)),
                  ),
                ),
              ),
              const SizedBox(
                width: 30,
              ),
              Expanded(
                child: GestureDetector(
                  onTap: () {
                    Navigator.of(context).pop();
                    approve?.call();
                  },
                  child: Container(
                    child: Center(
                      child: Text(
                        'Да',
                        style: Theme.of(context)
                            .textTheme
                            .headline4!
                            .copyWith(fontSize: 20, color: Colors.white),
                      ),
                    ),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: mainColor),
                    height: 48,
                  ),
                ),
              ),
              const SizedBox(
                width: 30,
              ),
            ],
          )
        ],
      ),
    );
  }
}
