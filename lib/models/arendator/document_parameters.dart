class DocumentParameters {
  String? id;
  String? typeParametersId;
  int? specificationType;
  int? conditionType;
  int? searchType;
  String? name;
  bool? isRequired;
  int? weight;
  String? stringData;
  int? numberData;
  double? doubleData;
  String? dateData;
  String? dateTimeData;
  String? coords;
  String? parameterOptionsId;
  List<dynamic>? files;
  List<dynamic>? parameterOptionsIds;
  List<dynamic>? parameterOptions;
  List<dynamic>? parameterOptionsSelect;
  Parameters? typeParameters;

  DocumentParameters({
    this.id,
    this.typeParametersId,
    this.specificationType,
    this.conditionType,
    this.searchType,
    this.name,
    this.isRequired,
    this.weight,
    this.stringData,
    this.numberData,
    this.doubleData,
    this.dateData,
    this.dateTimeData,
    this.coords,
    this.parameterOptionsId,
    this.files,
    this.parameterOptionsIds,
    this.parameterOptions,
    this.parameterOptionsSelect,
    this.typeParameters,
  });

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'typeParameters': typeParameters!.toMap(),
    };
  }

  factory DocumentParameters.fromJson(Map<String, dynamic> json) {
    return DocumentParameters(
      id: json["id"],
      typeParametersId: json["typeParametersId"],
      specificationType: json["specificationType"],
      conditionType: json["conditionType"],
      searchType: json["searchType"],
      name: json["name"],
      isRequired: json["required"],
      weight: json["weight"],
      stringData: json["stringData"],
      numberData: json["numberData"],
      doubleData: json["doubleData"],
      dateData: json["dateData"],
      dateTimeData: json["dateTimeData"],
      coords: json["coords"],
      parameterOptionsId: json["parameterOptionsId"],
      files: json["files"],
      parameterOptionsIds: json["parameterOptionsIds"],
      parameterOptions: json["parameterOptions"],
      parameterOptionsSelect: json["parameterOptionsSelect"],
      typeParameters: Parameters.fromJson(json["typeParameters"]),
    );
  }
}

class Parameters {
  String? id;
  String? typesId;
  String? specificationsId;
  String? name;
  bool? isRequired;
  String? conditionsId;
  int? weight;
  String? searchTypeId;
  String? regExp;
  String? regExpErrText;
  String? stringData;
  String? fStringData;
  String? sStringData;
  int? numberData;
  int? fNumberData;
  int? sNumberData;
  double? doubleData;
  double? fDoubleData;
  double? sDoubleData;
  String? dateData;
  String? fDateData;
  String? sDateData;
  String? dateTimeData;
  String? fDateTimeData;
  String? sDateTimeData;
  bool? deleted;
  List<dynamic>? typeParameterOptions;

  Parameters({
    this.id,
    this.typesId,
    this.specificationsId,
    this.conditionsId,
    this.searchTypeId,
    this.name,
    this.isRequired,
    this.weight,
    this.stringData,
    this.numberData,
    this.doubleData,
    this.dateData,
    this.dateTimeData,
    this.typeParameterOptions,
    this.fDateData,
    this.fDateTimeData,
    this.fDoubleData,
    this.fNumberData,
    this.regExp,
    this.fStringData,
    this.regExpErrText,
    this.sDateData,
    this.sDateTimeData,
    this.sDoubleData,
    this.sNumberData,
    this.sStringData,
    this.deleted,
  });

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'typeParameterOptions': typeParameterOptions,
      'typesId': typesId,
      'specificationsId': specificationsId,
      'searchTypeId': searchTypeId,
      'name': name,
      'isRequired': isRequired,
      'weight': weight,
      'stringData': stringData,
      'numberData': numberData,
      'doubleData': doubleData,
      'dateData': dateData,
      'dateTimeData': dateTimeData,
      'fDateData': fDateData,
      'fDateTimeData': fDateTimeData,
      'fDoubleData': fDoubleData,
      'fNumberData': fNumberData,
      'regExp': regExp,
      'fStringData': fStringData,
      'regExpErrText': regExpErrText,
      'sDateData': sDateData,
      'sDoubleData': sDoubleData,
      'sNumberData': sNumberData,
      'sStringData': sStringData,
      'deleted': deleted,
    };
  }

  factory Parameters.fromJson(Map<String, dynamic> json) {
    return Parameters(
      id: json["id"],
      typesId: json["typesId"],
      specificationsId: json["specificationsId"],
      conditionsId: json["id"],
      searchTypeId: json["searchTypeId"],
      name: json["name"],
      isRequired: json["required"],
      weight: json["weight"],
      stringData: json["stringData"],
      numberData: json["numberData"],
      doubleData: json["doubleData"],
      dateData: json["dateData"],
      dateTimeData: json["dateTimeData"],
      typeParameterOptions: json["typeParameterOptions"],
      fDateData: json["fDateData"],
      fDateTimeData: json["fDateTimeData"],
      fDoubleData: json["fDoubleData"],
      fNumberData: json["fNumberData"],
      regExp: json["regExp"],
      fStringData: json["fStringData"],
      regExpErrText: json["regExpErrText"],
      sDateData: json["sDateData"],
      sDateTimeData: json["sDateTimeData"],
      sDoubleData: json["sDoubleData"],
      sNumberData: json["sNumberData"],
      sStringData: json["sStringData"],
      deleted: json["deleted"],
    );
  }
}
