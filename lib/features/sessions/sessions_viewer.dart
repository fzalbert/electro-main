import 'package:electro/app/theme/theme.dart';
import 'package:electro/data/locale/entities/stop_go.dart';
import 'package:electro/data/singletons/stop_and_go.dart';
import 'package:electro/features/sessions/service/stop_go_session_service.dart';
import 'package:electro/utils/bottom_sheet_helper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class SessionsViewerWidget extends StatefulWidget implements ScrollableWidget {
  @override
  State<StatefulWidget> createState() => _SessionsViewerState();

  ScrollController? scrollController;

  SessionsViewerWidget({Key? key, this.scrollController}) : super(key: key);

  @override
  void setScrollController(ScrollController controller) {
    scrollController = controller;
  }
}

class _SessionsViewerState extends State<SessionsViewerWidget> {
  bool _isLoading = true;
  final StopGoSessionService _stopGoSessionService = StopGoSessionService();

  final List<StopGoSessionEntity> items = <StopGoSessionEntity>[];

  @override
  void initState() {
    _isLoading = true;
    updateData();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return _isLoading
        ? const SizedBox(
            height: 200,
            child: Center(
              child: CupertinoActivityIndicator(),
            ),
          )
        : Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: ListView.separated(
                controller: widget.scrollController,
                itemCount: items.length + 1,
                separatorBuilder: (context, index) {
                  return index == 0 ? const SizedBox.shrink() : const Divider();
                },
                itemBuilder: (context, index) {
                  return buildItem(context, index);
                }),
          );
  }

  void updateData() async {
    var list = await _stopGoSessionService.getAll();

    setState(() {
      _isLoading = false;
      items.clear();
      items.addAll(list);
    });
  }

  Widget buildItem(BuildContext context, int index) {
    if (index == 0) {
      return createHeader(context);
    } else {
      return createItem(context, items[index - 1]);
    }
  }

  Widget createItem(BuildContext context, StopGoSessionEntity item) {
    var date = item.createdDate;

    var dateStr = DateFormat("dd.MM.yy").format(date);
    var timeStr = DateFormat("Hm").format(date);
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () {
        _onItemClicked(item);
      },
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text("id: " + item.id,
                    overflow: TextOverflow.ellipsis,
                    style: AppTheme.headline1.copyWith(fontSize: 14)),
                const SizedBox(
                  height: 10,
                ),
                Text("Дата создания: " + dateStr, style: AppTheme.headline4),
                Text("Время: " + timeStr, style: AppTheme.headline4),
              ],
            ),
            const Spacer(),
            Text(item.statusText, style: AppTheme.headline4),
          ],
        ),
      ),
    );
  }

  Widget createHeader(BuildContext context) {
    return const Padding(
      padding: EdgeInsets.only(left: 16.0, top: 8.0, bottom: 8.0),
      child: Text(
        "Stop and go предыдущие сессии",
        style: AppTheme.headline900,
      ),
    );
  }

  void _onItemClicked(StopGoSessionEntity item) {
    StopAndGo.select(item.id);
    Navigator.of(context).pop();
  }
}
