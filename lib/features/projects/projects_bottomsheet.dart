import 'package:electro/bloc/project-list/projects_bloc.dart';
import 'package:electro/bloc/project-list/projects_state.dart';
import 'package:electro/features/projects/components/textfield.dart';
import 'package:electro/app/theme/theme.dart';
import 'package:electro/features/projects/tree_librirary/flutter_tree.dart';
import 'package:electro/models/columns/directory.dart';
import 'package:electro/models/columns/layer_directory.dart';
import 'package:electro/models/columns/project_directory.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:formz/formz.dart';
import 'package:logger/logger.dart';

class ProjectsBottomSheet extends StatefulWidget {
  final List<String> activeLayers;
  const ProjectsBottomSheet({Key? key, required this.activeLayers})
      : super(key: key);

  @override
  _ProjectsBottomSheetState createState() => _ProjectsBottomSheetState();
}

class _ProjectsBottomSheetState extends State<ProjectsBottomSheet> {
  @override
  Widget build(BuildContext context) {
    return Container(
        height: MediaQuery.of(context).size.height * 0.8,
        decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20.0),
            topRight: Radius.circular(20.0),
          ),
        ),
        padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
        child: SingleChildScrollView(
          child: BlocBuilder<ProjectListBloc, ProjectListState>(
              builder: (context, state) {
            return Container(
              color: Colors.white,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(
                    height: 44,
                  ),
                  Text(
                    'Проекты',
                    style: Theme.of(context)
                        .textTheme
                        .headline3!
                        .copyWith(fontSize: 20),
                  ),
                  const SizedBox(height: 20),
                  Row(
                    children: [
                      Expanded(child: ProjectsTextField()),
                      const SizedBox(
                        width: 12,
                      ),
                      GestureDetector(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Text(
                          'Отменить',
                          style: Theme.of(context)
                              .textTheme
                              .headline4!
                              .copyWith(
                                  fontSize: 14, color: AppTheme.regularText),
                        ),
                      ),
                    ],
                  ),
                  state.status == FormzStatus.submissionFailure
                      ? Center(
                          child: Container(
                            margin: EdgeInsets.only(top: 20),
                            child: Text(
                              'Ошибка при загрузке',
                              style: Theme.of(context)
                                  .textTheme
                                  .headline3!
                                  .copyWith(fontSize: 20),
                            ),
                          ),
                        )
                      : state.status == FormzStatus.submissionInProgress
                          ? Center(
                              child: Container(
                                margin: EdgeInsets.only(top: 20),
                                child: CupertinoActivityIndicator(),
                              ),
                            )
                          : state.status == FormzStatus.submissionSuccess
                              ? projectTree(
                                  context,
                                  state.projectList,
                                  state.layerIds!,
                                  widget.activeLayers,
                                )
                              : projectTree(
                                  context,
                                  state.projectList,
                                  state.layerIds!,
                                  widget.activeLayers,
                                )
                ],
              ),
            );
          }),
        ));
  }
}

var logger = Logger();

List<Map<String, dynamic>> iteractionTree(
  ProjectDirectoryResponseDto childrenDirectory,
  List<Map<String, dynamic>> treeData,
  int parentId,
  Map<String, double> layerIdsMap,
  List<String> activeLayers,
  bool disable,
) {
  bool _disable = false;
  List<String> layerIds = [];
  for (String _layerId in layerIdsMap.keys) {
    layerIds.add(_layerId);
  }
  print("CHILDREN NODE NAME = ${childrenDirectory.name}");
  treeData.add({
    "parentId": parentId,
    "uri": null,
    "name": childrenDirectory.name,
    "layerId": "node",
    "disable": disable,
    "activated": false,
    "id": int.parse("$parentId${treeData.length}"),
  });
  parentId = int.parse("$parentId${treeData.length - 1}");

  if (childrenDirectory.layers.isNotEmpty) {
    for (int i = 0; i < childrenDirectory.layers.length; i++) {
      LayerDirectoryModel layer = childrenDirectory.layers[i];
      if (!activeLayers.contains(layer.id) && activeLayers.isNotEmpty) {
        _disable = true;
      }
      treeData.add({
        "parentId": parentId,
        "uri": layer.legendLink,
        "name": layer.name,
        "layerId": layer.id,
        "disable": activeLayers.contains(layer.id)
            ? false
            : activeLayers.isEmpty
                ? false
                : true,
        "value": layerIds.contains(layer.id) ? layerIdsMap[layer.id] : 1.0,
        "activated": layerIds.contains(layer.id) ? true : false,
        "id": int.parse("$parentId$i"),
      });
    }
  }
  if (childrenDirectory.childrenDirectories.isNotEmpty) {
    for (ProjectDirectoryResponseDto _childrenDirectory
        in childrenDirectory.childrenDirectories) {
      treeData = iteractionTree(
        _childrenDirectory,
        treeData,
        parentId,
        layerIdsMap,
        activeLayers,
        _disable,
      );
    }
  }

  //logger.v(treeData);

  return treeData;
}

Widget projectTree(BuildContext context, List<DirectoryResponseDto> projectList,
    Map<String, double> layerIdsMap, List<String> activeLayers) {
  List<Map<String, dynamic>> treeListData = [];
  List<String> layerIds = [];
  for (String _layerId in layerIdsMap.keys) {
    layerIds.add(_layerId);
  }

  treeListData.add({
    "parentId": 0,
    "uri": null,
    "name": "Проекты",
    "id": 1,
    "disable": false,
    "activated": false,
    "layerId": "node",
  });

  for (int i = 0; i < projectList.length; i++) {
    bool _disable = false;
    DirectoryResponseDto project = projectList[i];
    // logger.v("$project");

    treeListData.add({
      "parentId": 1,
      "uri": null,
      "name": project.name,
      "layerId": "node",
      "disable": false,
      "activated": false,
      "id": int.parse("1$i${treeListData.length}"),
    });

    int parentId = int.parse("1$i${treeListData.length - 1}");

    if (project.mapProjectLayers.isNotEmpty) {
      for (int j = 0; j < project.mapProjectLayers.length; j++) {
        LayerDirectoryModel layer = project.mapProjectLayers[j];
        if (!activeLayers.contains(layer.id) && activeLayers.isNotEmpty) {
          _disable = true;
        }
        treeListData.add({
          "parentId": parentId,
          "uri": layer.legendLink,
          "name": layer.name,
          "layerId": layer.id,
          "disable": activeLayers.contains(layer.id)
              ? false
              : activeLayers.isEmpty
                  ? false
                  : true,
          "value": layerIds.contains(layer.id) ? layerIdsMap[layer.id] : 1.0,
          "activated": layerIds.contains(layer.id) ? true : false,
          "id": int.parse("$parentId$j"),
        });
      }
    }

    for (int j = 0; j < project.mapProjectDirectories.length; j++) {
      treeListData = iteractionTree(
        project.mapProjectDirectories[j],
        treeListData,
        parentId,
        layerIdsMap,
        activeLayers,
        _disable,
      );
    }
  }

  //logger.v(pr);

  return FlutterTree(
    listData: treeListData,
    config: const Config(
      parentId: 'parentId',
      dataType: DataType.DataList,
      uri: 'uri',
      layerId: "layerId",
      name: 'name',
      value: 'value',
      activated: 'activated',
      disable: "disable",
    ),
    onChecked: (Map<String, double> checkedList) async {
      print("CHECKED LIST = $checkedList");
      context.read<ProjectListBloc>().updateLayerIds(checkedList);
      // logger.v(checkedList);
    },
  );
}
