import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:electro/bloc/arendator/arendator_cubit.dart';
import 'package:electro/models/object/object_info.dart';
import 'package:electro/repository/file.dart';
import 'package:electro/repository/projects.dart';
import 'package:equatable/equatable.dart';
import 'package:latlong2/latlong.dart';

part 'object_state.dart';

class ObjectCubit extends Cubit<ObjectState> {
  ObjectCubit({
    required this.repository,
    required this.fileRepository,
  }) : super(InitialObjectState());

  final ProjectsRepository repository;
  final FileRepository fileRepository;

  Future<List<List<String>>> getTypesObject(
      {required LatLng pos, required List<String> layerIds}) async {
    try {
      emit(LoadingObjectState());
      List<List<String>> typeIds = await repository.getObjectsByCoordAndLayers(
          pos: pos, layerIds: layerIds);
      print("DYNA here = $typeIds");
      if (typeIds.length == 1) {
        getObject(typeId: typeIds[0][0], layerIds: layerIds);
        return [[]];
      }

      emit(LoadedTypesObjectState(typeIds));
      return typeIds;
    } catch (e) {
      print("ERROR = $e");
      emit(ErrorObjectState());
      return [];
    }
  }

  void getObject(
      {required String typeId, required List<String> layerIds}) async {
    try {
      emit(LoadingObjectState());
      ObjectInfo object =
          await repository.getObjectByType(typeId: typeId, layerIds: layerIds);

      emit(LoadedObjectState(object));
    } catch (e) {
      print("ERROR = $e");
      emit(ErrorObjectState());
    }
  }

  Future<File> dowloadFile(
      {required String idFile, required String objectId}) async {
    File file = await fileRepository.downloadFile(
        idFile: idFile, arendatorId: objectId);

    return file;
  }
}
