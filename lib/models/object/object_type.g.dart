// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'object_type.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ObjectTypeResponseDto _$ObjectTypeResponseDtoFromJson(
        Map<String, dynamic> json) =>
    ObjectTypeResponseDto(
      id: json['Id'] as String,
      mapObjectTypeParentId: json['MapObjectTypeParentId'] as String,
      name: json['Name'] as String,
      isCanDraw: json['IsCanDraw'] as bool,
      drawDirectoryId: json['DrawDirectoryId'] as String,
      mplId: json['MplId'] as String,
      childrenTypes: (json['ChildrenTypes'] as List<dynamic>)
          .map((e) => ObjectTypeResponseDto.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$ObjectTypeResponseDtoToJson(
        ObjectTypeResponseDto instance) =>
    <String, dynamic>{
      'Id': instance.id,
      'MapObjectTypeParentId': instance.mapObjectTypeParentId,
      'Name': instance.name,
      'IsCanDraw': instance.isCanDraw,
      'DrawDirectoryId': instance.drawDirectoryId,
      'MplId': instance.mplId,
      'ChildrenTypes': instance.childrenTypes,
    };
