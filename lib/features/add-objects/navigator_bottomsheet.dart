import 'package:electro/data/singletons/stop_and_go.dart';
import 'package:electro/data/utils/functions.dart';
import 'package:electro/features/projects/editing_bottomsheet.dart';
import 'package:electro/app/theme/theme.dart';
import 'package:flutter/material.dart';

class NavigatorBottomSheet extends StatelessWidget {
  final bool isGeoLocation;
  const NavigatorBottomSheet({Key? key, required this.isGeoLocation})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 180,
      padding: const EdgeInsets.fromLTRB(16, 44, 16, 0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Создание объекта',
            style: Theme.of(context).textTheme.headline3!.copyWith(
                  fontSize: 20,
                ),
          ),
          const SizedBox(height: 28),
          GestureDetector(
            onTap: () {
              Navigator.of(context).pop();
              if (isGeoLocation) {
                MyFunctions.popBottomSheet(
                    EditingBottomSheet(isEditing: false), true, context);
              } else {
                if (StopAndGo.isStarted) {
                  StopAndGo.finish();
                }
                else {
                  StopAndGo.start();
                }
                /*MyFunctions.popBottomSheet(
                    const NavigatorBottomSheet(isGeoLocation: true),
                    true,
                    context);*/
              }
            },
            child: Row(
              children: [
                Expanded(
                    child: Container(
                  height: 48,
                  decoration: BoxDecoration(
                      color: isGeoLocation
                          ? const Color(0xff2B61A3)
                          : Colors.white,
                      borderRadius: BorderRadius.circular(10),
                      border:
                          Border.all(color: const Color(0xff2B61A3), width: 1)),
                  child: Center(
                    child: Text(
                      isGeoLocation ? 'Установить геопозицию' : (StopAndGo.isStarted ? 'stop and go (stop)' : 'stop and go (start)'),
                      style: Theme.of(context).textTheme.headline4!.copyWith(
                          fontSize: 20,
                          color: isGeoLocation
                              ? Colors.white
                              : AppTheme.regularTextDarkBlue),
                    ),
                  ),
                )),
              ],
            ),
          ),
          const SizedBox(
            height: 30,
          )
        ],
      ),
    );
  }
}
