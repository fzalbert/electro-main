// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'map_object_data_value.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MapObjectDataValue _$MapObjectDataValueFromJson(Map<String, dynamic> json) =>
    MapObjectDataValue(
      mopId: json['MopId'] as String,
      value: json['Value'] as String?,
      values:
          (json['Values'] as List<dynamic>?)?.map((e) => e as String).toList(),
    );

Map<String, dynamic> _$MapObjectDataValueToJson(MapObjectDataValue instance) =>
    <String, dynamic>{
      'MopId': instance.mopId,
      'Value': instance.value,
      'Values': instance.values,
    };
