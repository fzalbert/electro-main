import 'package:electro/data/locale/app_database.dart';
import 'package:electro/data/locale/dao/position.dart';
import 'package:electro/data/locale/entities/position.dart';
import 'package:electro/utils/guid_helper.dart';
import 'package:latlong2/latlong.dart';


class PositionService{

  Future<List<PositionEntity>> findBySessionId(String sessionId) async {
    var dao = await _getDao();
    return dao.findBySessionId(sessionId);
  }

  void addToSession(String sessionId, LatLng position) async {
    var dao = await _getDao();
    return dao.insert(
        PositionEntity(
            id: GuidHelper.timeBased(),
            sessionId: sessionId,
            latitude: position.latitude,
            longitude: position.longitude, )
    );
  }

  Future<PositionDao> _getDao() async {
    var _database = await DatabaseBuilder().create();
    return _database.positionDao;
  }
}