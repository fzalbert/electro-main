import 'package:path/path.dart' as path;
import 'package:sqflite/sqflite.dart' as sql;
import 'package:sqflite/sqflite.dart';

class Database {
  static const String directoryQuery =
      'CREATE TABLE IF NOT EXISTS directoryList(id INTEGER PRIMARY KEY,localId TEXT, name TEXT, createdBy TEXT,timeCreated TEXT,center TEXT, zoomLevel INTEGER, activated INTEGER, orderNumber INTEGER, moduleId TEXT)';
  static const String projectDirectoryQuery =
      'CREATE TABLE IF NOT EXISTS projectList(id INTEGER PRIMARY KEY,localId TEXT, mapProjectId TEXT, mapProjectDirectoryId TEXT,name TEXT,activated INTEGER, orderNumber INTEGER, serviceId TEXT, rootServiceDirectory INTEGER, createdBy TEXT, timeCreated TEXT, enabledDefault INTEGER)';
  static const String layerDirectoryQuery =
      'CREATE TABLE IF NOT EXISTS layerList(id INTEGER PRIMARY KEY,localId TEXT, mapProjectId TEXT, mapProjectDirectoryId TEXT,name TEXT,uri TEXT, protocol TEXT, enabledDefault INTEGER, activated INTEGER, createdBy TEXT, orderNumber INTEGER, legendLink TEXT, timeCreated TEXT, geoserverLayerId TEXT, moduleId TEXT, cqlFilter TEXT, dataSearchMode INTEGER, showInfoBlock INTEGER)';
  static const String childProjectDirectoryQuery =
      'CREATE TABLE IF NOT EXISTS childProjectList(id INTEGER PRIMARY KEY,localId TEXT, mapProjectId TEXT, mapProjectDirectoryId TEXT,name TEXT,activated INTEGER, orderNumber INTEGER, serviceId TEXT, rootServiceDirectory INTEGER, createdBy TEXT, timeCreated TEXT, enabledDefault INTEGER)';
  static const String childLayerDirectoryQuery =
      'CREATE TABLE IF NOT EXISTS childLayerList(id INTEGER PRIMARY KEY,localId TEXT, mapProjectId TEXT, mapProjectDirectoryId TEXT,name TEXT,uri TEXT, protocol TEXT, enabledDefault INTEGER, activated INTEGER, createdBy TEXT, orderNumber INTEGER, legendLink TEXT, timeCreated TEXT, geoserverLayerId TEXT, moduleId TEXT, cqlFilter TEXT, dataSearchMode INTEGER, showInfoBlock INTEGER)';

  static Future<sql.Database> checkDatabaseStatus(String query) async {
    final dbPath = await sql.getDatabasesPath();
    // use query parameter to create a table in database
    return sql.openDatabase(
      path.join(dbPath, 'good_database.db'),
      onCreate: (db, version) {
        db.execute(directoryQuery);
        db.execute(projectDirectoryQuery);
        db.execute(layerDirectoryQuery);
        db.execute(childLayerDirectoryQuery);
        db.execute(childProjectDirectoryQuery);
        return db.execute(query);
      },
      version: 1,
    );
  }

  // insert item to database
  static Future<void> insert(
      {required String table,
      required Map<String, dynamic> data,
      required String query}) async {
    final db = await Database.checkDatabaseStatus(query);
    await db.insert(table, data,
        conflictAlgorithm: sql.ConflictAlgorithm.replace);
  }

  // get all items from database
  static Future<List<Map<String, dynamic>>> getItems(
      {required String table, required String query}) async {
    final db = await Database.checkDatabaseStatus(query);
    return db.query(
      table,
    );
  }

  static Future<List<Map<String, dynamic>>> getItemsWithId(
      {required String table, required String query, required int id}) async {
    final db = await Database.checkDatabaseStatus(
      query,
    );
    return db.rawQuery(
        'SELECT COUNT(*) FROM $table WHERE mapProjectDirectoryId == $id ');
  }

  static Future<bool> tableIsEmpty({
    required String query,
    required String table,
  }) async {
    var db = await Database.checkDatabaseStatus(query);

    int? count = Sqflite.firstIntValue(await db.rawQuery(
      'SELECT COUNT(*) FROM $table',
    ));
    if (count == null || count == 0) {
      return true;
    } else {
      return false;
    }
  }

  static Future<void> update({
    required String checkDatabase,
    required String table,
    required Map<String, dynamic> dataToUpdate,
    required String id,
  }) async {
    final db = await Database.checkDatabaseStatus(checkDatabase);
    await db.update(table, dataToUpdate, where: 'localId= ?', whereArgs: [id]);
  }

  // get single item from database
  static Future<List<Map<String, dynamic>>> getItem(
      {required String table, required int id, required String query}) async {
    final db = await Database.checkDatabaseStatus(query);
    return await db.rawQuery('SELECT * FROM $table WHERE id = $id');
  }

  static Future<void> deleteItem(
      {required String table, required int id, required String query}) async {
    final db = await Database.checkDatabaseStatus(query);
    db.rawDelete('DELETE FROM $table WHERE id=$id');
  }
}
