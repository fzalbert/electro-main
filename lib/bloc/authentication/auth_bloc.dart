import 'package:electro/bloc/authentication/auth_events.dart';
import 'package:electro/bloc/authentication/auth_state.dart';
import 'package:electro/repository/auth.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:formz/formz.dart';


class AuthBloc extends Bloc<AuthEvents, AuthState> {
  final AuthenticationRepository repository;
  AuthBloc({required this.repository})
      : super(const AuthState(
            loginStatus: FormzStatus.pure, logoutStatus: FormzStatus.pure)) {
    on<LoginUserEvent>((event, emit) async {
      emit(state.copyWith(loginStatus: FormzStatus.submissionInProgress));

      var result =
          await repository.logIn(login: event.login, password: event.password);
      if (result == 1) {
        emit(state.copyWith(loginStatus: FormzStatus.submissionSuccess));
      } else if (result == 2) {
        emit(state.copyWith(loginStatus: FormzStatus.invalid));

      } else {
        emit(state.copyWith(loginStatus: FormzStatus.submissionFailure));
      }
    });

    on<LogOutUserEvent>((event, emit) async {
      emit(state.copyWith(logoutStatus: FormzStatus.submissionInProgress));
      var result = await repository.logOut();
      if (result == 1) {
        emit(state.copyWith(logoutStatus: FormzStatus.submissionSuccess));
      } else if (result == 2) {
        emit(state.copyWith(logoutStatus: FormzStatus.invalid));
      } else {
        emit(state.copyWith(logoutStatus: FormzStatus.submissionFailure));
      }
    });
  }
}
