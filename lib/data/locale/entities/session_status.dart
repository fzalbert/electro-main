enum SessionStatus{
  inProgress,
  done,
  sended,
  paused
}

const Map<SessionStatus, String> statusNames = {
  SessionStatus.inProgress: "В процессе",
  SessionStatus.done: "Закончен",
  SessionStatus.sended: "Отправлен",
  SessionStatus.paused: "Пауза",
};
