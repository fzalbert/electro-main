import 'package:electro/data/locale/app_database.dart';
import 'package:electro/data/locale/entities/file.dart';
import 'package:electro/data/locale/entities/map_object.dart';
import 'package:electro/data/locale/entities/map_object_value.dart';
import 'package:electro/data/locale/entities/position.dart';
import 'package:electro/data/singletons/stop_and_go.dart';
import 'package:electro/features/sessions/service/stop_go_session_service.dart';
import 'package:electro/features/sessions/view_models/stop_go_session.dart';
import 'package:electro/models/arendator/renter.dart';
import 'package:electro/repository/map_object_repository.dart';
import 'package:electro/utils/guid_helper.dart';
import 'package:latlong2/latlong.dart';

class MapObjectService {
  MapObjectService();

  Future<MapObjectEntity?> create({
    String? sessionId,
    required String drawTypeId,
    required LatLng position,
    required String objectTypeId,
    required List<MapObjectValueModel> values,
    List<RenterModel>? renters,
    List<FileModel>? filesInfo,
  }) async {
    print("FILES INFO = $filesInfo");
    if (sessionId != null && sessionId.isNotEmpty) {
      return _createBySessionId(
        sessionId: sessionId,
        drawTypeId: drawTypeId,
        position: position,
        objectTypeId: objectTypeId,
        values: values,
        renters: renters,
        filesInfo: filesInfo,
      );
    } else {
      return _createSingle(
        drawTypeId: drawTypeId,
        position: position,
        objectTypeId: objectTypeId,
        values: values,
        renters: renters,
        filesInfo: filesInfo,
      );
    }
  }

  Future<List<MapObjectModel>> findBySessionId(String sessionId) async {
    var bd = await _getDatabase();
    var dao = bd.mapObjectDao;
    var coorDao = bd.positionDao;

    var items = (await dao.findAll())
        .where((element) => element.sessionId == sessionId);

    var positionsId = items.map((e) => e.positionId).toList();
    var positions = (await bd.positionDao.findAll())
        .where((element) => positionsId.contains(element.id));

    var values = await bd.mapObjectValueDao.findAll();
    var itemsCoor = (await coorDao.findAll())
        .where((element) => element.sessionId == sessionId)
        .toList();

    var renters = await bd.renter.findAll();

    var files = await bd.file.findAll();

    return items.where((element) => element.sessionId == sessionId).map((item) {
      var position =
          positions.firstWhere((element) => element.id == item.positionId);

      var filesInfo = files
          .where((element) => element.objectId == item.id)
          .map((e) => FileModel.fromEntity(e))
          .toList();

      var objectValues = values
          .where((e) => e.mapObjectId == item.id)
          .map((e) => MapObjectValueModel.fromEntity(e))
          .toList();

      var rentersModels = renters
          .where((element) => element.objectId == item.id)
          .map((e) => RenterModel.fromEntity(e))
          .toList();

      var objectCoords = position.latitude == 0.0 && position.longitude == 0.0
          ? itemsCoor
          : <PositionEntity>[];

      return MapObjectModel(
        id: item.id,
        sessionId: item.sessionId,
        positionId: item.positionId,
        objectTypeId: item.objectTypeId,
        parentId: item.parentId,
        filesInfo: filesInfo,
        coords: objectCoords,
        values: objectValues,
        position: LatLng(position.latitude, position.longitude),
        renters: rentersModels,
      );
    }).toList();
  }

  Future<MapObjectModel> findById(String objectId) async {
    var bd = await _getDatabase();
    var dao = bd.mapObjectDao;
    var item =
        (await dao.findAll()).firstWhere((element) => element.id == objectId);

    var position = (await bd.positionDao.findAll())
        .firstWhere((element) => element.id == item.positionId);

    var values = await bd.mapObjectValueDao.findAll();

    var files = (await bd.file.findByMapObjectId(item.id))
        .map((e) => FileModel.fromEntity(e))
        .toList();

    return MapObjectModel(
        id: item.id,
        sessionId: item.sessionId,
        positionId: item.positionId,
        objectTypeId: item.objectTypeId,
        parentId: item.parentId,
        filesInfo: files,
        values: values
            .where((e) => e.mapObjectId == item.id)
            .map((e) => MapObjectValueModel.fromEntity(e))
            .toList(),
        position: LatLng(position.latitude, position.longitude),
        coords: [position]);
  }

  Future<MapObjectEntity?> update(
      {required String objectId,
      required List<MapObjectValueModel> values}) async {
    var bd = await _getDatabase();

    for (var element in values) {
      await bd.mapObjectValueDao.update(MapObjectValueEntity(
          id: element.id,
          parameterId: element.parameterId,
          mapObjectId: objectId,
          value: element.value,
          title: element.title,
          parameterType: element.parameterType));
    }
  }

  Future<MapObjectEntity?> _createBySessionId({
    required String sessionId,
    required String drawTypeId,
    required LatLng position,
    required String objectTypeId,
    required List<MapObjectValueModel> values,
    required List<RenterModel>? renters,
    List<FileModel>? filesInfo,
  }) async {
    var bd = await _getDatabase();
    var dao = bd.mapObjectDao;
    var fileDao = bd.file;
    var positionDao = bd.positionDao;
    StopGoSessionService _sessionService = StopGoSessionService();
    StopGoSession? session = await _sessionService.findById(sessionId);

    if (session == null) {
      return null;
    }

    await _sessionService.updateDrawId(sessionId, drawTypeId);

    var positionEntity = PositionEntity(
      id: GuidHelper.timeBased(),
      latitude: position.latitude,
      longitude: position.longitude,
      sessionId: "",
    );

    await positionDao.insert(positionEntity);

    var object = MapObjectEntity(
      id: GuidHelper.timeBased(),
      sessionId: sessionId,
      parentId: drawTypeId,
      objectTypeId: objectTypeId,
      positionId: positionEntity.id,
    );

    var objectId = object.id;
    await dao.insert(object);

    /*if (renters == null) {
      var renterEntity = RenterEntity.fromModel(renters!, id);
      bd.renter.insert(renterEntity);
    }*/

    if (filesInfo != null) {
      for (FileModel file in filesInfo) {
        await fileDao.insert(FileEntity.fromModel(file, objectId));
      }
    }

    for (var element in values) {
      await bd.mapObjectValueDao.insert(MapObjectValueEntity(
          id: element.id,
          parameterId: element.parameterId,
          mapObjectId: objectId,
          value: element.value,
          title: element.title,
          parameterType: element.parameterType));
    }

    StopAndGo.newObjectCreated(MapObjectModel(
        id: object.id,
        position: position,
        positionId: object.positionId,
        objectTypeId: object.objectTypeId,
        sessionId: object.sessionId,
        parentId: object.parentId,
        filesInfo: filesInfo ?? [],
        values: values,
        coords: []));
    return object;
  }

  Future<MapObjectEntity?> _createSingle({
    required String drawTypeId,
    required LatLng position,
    required String objectTypeId,
    required List<MapObjectValueModel> values,
    required List<RenterModel>? renters,
    required List<FileModel>? filesInfo,
  }) async {
    var bd = await _getDatabase();
    var dao = bd.mapObjectDao;

    var positionEntity = PositionEntity(
        id: GuidHelper.timeBased(),
        latitude: position.latitude,
        longitude: position.longitude,
        sessionId: "");

    var object = MapObjectEntity(
      id: GuidHelper.timeBased(),
      parentId: drawTypeId,
      objectTypeId: objectTypeId,
      positionId: positionEntity.id,
    );

    var mapObjectRepository = MapObjectRepository();

    var model = MapObjectModel.fromEntity(
        object, values, position, renters ?? [], filesInfo ?? []);

    await mapObjectRepository.send(model, drawTypeId);

    // if (filesInfo != null) {
    //   for (FileModel file in filesInfo) {
    //     bd.file.insert(FileEntity.fromModel(file, object.id));
    //   }
    // }
    // StopAndGo.newObjectCreated(MapObjectModel(
    //     id: object.id,
    //     position: position,
    //     positionId: object.positionId,
    //     objectTypeId: object.objectTypeId,
    //     parentId: object.parentId,
    //     values: values,
    //     coords: []));

    return object;
  }

  Future<AppDatabase> _getDatabase() async {
    return await DatabaseBuilder().create();
  }
}
