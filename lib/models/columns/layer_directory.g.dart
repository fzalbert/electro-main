// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'layer_directory.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LayerDirectoryModel _$LayerDirectoryModelFromJson(Map<String, dynamic> json) =>
    LayerDirectoryModel(
      activated: json['Activated'] as bool,
      cqlFilter: json['CqlFilter'] as String? ?? '',
      createdBy: json['CreatedBy'] as String? ?? '',
      dataSearchMode: json['DataSearchMode'] as int? ?? 0,
      enabledDefault: json['EnabledDefault'] as bool,
      geoserverLayerId: json['GeoserverLayerId'] as String? ?? '',
      id: json['Id'] as String? ?? '',
      legendLink: json['LegendLink'] as String? ?? '',
      mapProjectDirectoryId: json['MapProjectDirectoryId'] as String? ?? '',
      mapProjectId: json['MapProjectId'] as String? ?? '',
      moduleId: json['ModuleId'] as String? ?? '',
      name: json['Name'] as String? ?? '',
      orderNumber: json['OrderNumber'] as int? ?? 0,
      protocol: json['Protocol'] as String? ?? '',
      showInfoBlock: json['ShowInfoBlock'] as bool,
      timeCreated: json['TimeCreated'] as String? ?? '',
      uri: json['Uri'] as String? ?? '',
    );

Map<String, dynamic> _$LayerDirectoryModelToJson(
        LayerDirectoryModel instance) =>
    <String, dynamic>{
      'Id': instance.id,
      'MapProjectId': instance.mapProjectId,
      'MapProjectDirectoryId': instance.mapProjectDirectoryId,
      'Name': instance.name,
      'Uri': instance.uri,
      'Protocol': instance.protocol,
      'EnabledDefault': instance.enabledDefault,
      'Activated': instance.activated,
      'CreatedBy': instance.createdBy,
      'OrderNumber': instance.orderNumber,
      'LegendLink': instance.legendLink,
      'TimeCreated': instance.timeCreated,
      'GeoserverLayerId': instance.geoserverLayerId,
      'ModuleId': instance.moduleId,
      'CqlFilter': instance.cqlFilter,
      'DataSearchMode': instance.dataSearchMode,
      'ShowInfoBlock': instance.showInfoBlock,
    };
