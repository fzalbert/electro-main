import 'dart:io';

import 'package:electro/data/locale/entities/map_object.dart';
import 'package:electro/utils/guid_helper.dart';
import 'package:floor/floor.dart';
import 'package:image_picker/image_picker.dart';

@Entity(tableName: FileEntity.TABLE_NAME, foreignKeys: [
  ForeignKey(
    childColumns: ["objectId"],
    parentColumns: ["id"],
    entity: MapObjectEntity,
    onDelete: ForeignKeyAction.cascade,
  )
])
class FileEntity {
  @PrimaryKey()
  final String id;
  final String? uri;

  ///map object id
  final String objectId;

  final double size;
  ///name from api
  final String? sourceName;
  final String originalName;

  FileEntity(
      {required this.id,
      required this.objectId,
      required this.size,
      required this.sourceName,
      required this.originalName,
      this.uri});

  static FileEntity fromModel(FileModel model, String mapObjectId) {
    return FileEntity(
      id: model.id ?? GuidHelper.timeBased(),
      objectId: mapObjectId,
      size: model.size.toDouble(),
      sourceName: model.sourceName,
      originalName: model.originalName,
      uri: model.uri,
    );
  }

  @ignore
  static const TABLE_NAME = "file_object";
}

class FileModel {
  final String? id;
  final String? uri;

  final int size;

  ///название с api
  final String? sourceName;
  final String originalName;

  FileModel(
      {this.id,
      required this.size,
      this.sourceName,
      required this.originalName,
      this.uri});

  static FileModel fromEntity(FileEntity entity) {
    return FileModel(
      id: entity.id,
      uri: entity.uri,
      size: entity.size.toInt(),
      sourceName: entity.sourceName,
      originalName: entity.originalName,
    );
  }

  static FileModel fromFile(File file) {
    String fileName = file.path.split('/').last;
    var path = file.path;
    return FileModel(
        size: 0,
        originalName: fileName,
        uri: path,
        sourceName: null,
        id: GuidHelper.timeBased()
    );
  }

  static FileModel fromXFile(XFile file){
    String fileName = file.path.split('/').last;
    var path = file.path;
    return FileModel(
        size: 0,
        originalName: fileName,
        uri: path,
        sourceName: null,
        id: GuidHelper.timeBased()
    );
  }
}
