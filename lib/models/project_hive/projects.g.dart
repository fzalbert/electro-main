// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'projects.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class ProjectsModelAdapter extends TypeAdapter<ProjectsModel> {
  @override
  final int typeId = 0;

  @override
  ProjectsModel read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return ProjectsModel(
      projects: (fields[0] as List).cast<DirectoryResponseDtoHiveModel>(),
    );
  }

  @override
  void write(BinaryWriter writer, ProjectsModel obj) {
    writer
      ..writeByte(1)
      ..writeByte(0)
      ..write(obj.projects);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ProjectsModelAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
