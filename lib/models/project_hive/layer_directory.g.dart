// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'layer_directory.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class LayerDirectoryHiveModelAdapter
    extends TypeAdapter<LayerDirectoryHiveModel> {
  @override
  final int typeId = 3;

  @override
  LayerDirectoryHiveModel read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return LayerDirectoryHiveModel(
      activated: fields[7] as bool,
      cqlFilter: fields[14] as String,
      createdBy: fields[8] as String,
      dataSearchMode: fields[15] as int,
      enabledDefault: fields[6] as bool,
      geoserverLayerId: fields[12] as String,
      id: fields[0] as String,
      legendLink: fields[10] as String,
      mapProjectDirectoryId: fields[2] as String,
      mapProjectId: fields[1] as String,
      moduleId: fields[13] as String,
      name: fields[3] as String,
      orderNumber: fields[9] as int,
      protocol: fields[5] as String,
      showInfoBlock: fields[16] as bool,
      timeCreated: fields[11] as String,
      uri: fields[4] as String,
    );
  }

  @override
  void write(BinaryWriter writer, LayerDirectoryHiveModel obj) {
    writer
      ..writeByte(17)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.mapProjectId)
      ..writeByte(2)
      ..write(obj.mapProjectDirectoryId)
      ..writeByte(3)
      ..write(obj.name)
      ..writeByte(4)
      ..write(obj.uri)
      ..writeByte(5)
      ..write(obj.protocol)
      ..writeByte(6)
      ..write(obj.enabledDefault)
      ..writeByte(7)
      ..write(obj.activated)
      ..writeByte(8)
      ..write(obj.createdBy)
      ..writeByte(9)
      ..write(obj.orderNumber)
      ..writeByte(10)
      ..write(obj.legendLink)
      ..writeByte(11)
      ..write(obj.timeCreated)
      ..writeByte(12)
      ..write(obj.geoserverLayerId)
      ..writeByte(13)
      ..write(obj.moduleId)
      ..writeByte(14)
      ..write(obj.cqlFilter)
      ..writeByte(15)
      ..write(obj.dataSearchMode)
      ..writeByte(16)
      ..write(obj.showInfoBlock);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is LayerDirectoryHiveModelAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
