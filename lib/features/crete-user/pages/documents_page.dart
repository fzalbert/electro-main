import 'dart:io';

import 'package:electro/app/theme/theme.dart';
import 'package:electro/bloc/arendator/arendator_cubit.dart';
import 'package:electro/features/crete-user/fields/fields.dart';
import 'package:electro/features/crete-user/fields/image_upload.dart';
import 'package:electro/features/projects/components/editing_textfield.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/src/provider.dart';

// ignore: must_be_immutable
class DocumentsView extends StatefulWidget {
  final Function? callback;
  final List<Map>? choosedDocumentIds;
  final String arendatorId;
  List<Map> documents;
  DocumentsView({
    Key? key,
    required this.documents,
    required this.arendatorId,
    this.callback,
    this.choosedDocumentIds,
  }) : super(key: key);

  @override
  _DocumentsViewState createState() => _DocumentsViewState();
}

class _DocumentsViewState extends State<DocumentsView> {
  late String arendatorId;
  List<String> documentIds = [];
  List<Map> documents = [];
  List<String> choosedIds = [];

  @override
  void initState() {
    documents = widget.documents;
    print("DCO = $documents");
    if (widget.choosedDocumentIds != null) {
      for (var initDoc in widget.choosedDocumentIds!) {
        choosedIds.add(initDoc["documentId"]);
      }
    }

    if (documents.length == 1) {
      for (Map document in documents) {
        try {
          arendatorId = document["arendatorId"];
          documentIds.add(document["documentId"]);
          document.removeWhere((key, value) => key == "arendatorId");
          document.removeWhere((key, value) => key == "documentId");
        } catch (_) {
          arendatorId = document["arendatorId"];
          document.removeWhere((key, value) => key == "arendatorId");
        }
      }
      //documents.removeLast();
    } else {
      for (Map document in documents) {
        documentIds.add(document["documentId"]);
        print("ID = ${document["documentId"]}");
        arendatorId = document["arendatorId"];
        document.removeWhere((key, value) => key == "documentId");
        document.removeWhere((key, value) => key == "arendatorId");
      }
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (documentIds.isEmpty) {
      documents.removeLast();
    }
    print("DOCUMENTS HERE = ${documents}");
    print("DOCUMENTS IDS HERE = ${documentIds}");
    //sleep(Duration(seconds: 5));
    return Container(
      height: MediaQuery.of(context).size.height * 0.8,
      padding: const EdgeInsets.fromLTRB(16, 44, 16, 0),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Создание арендатора',
              style: Theme.of(context).textTheme.headline3!.copyWith(
                    fontSize: 20,
                  ),
            ),
            const SizedBox(height: 28),
            if (documents.isNotEmpty)
              for (int i = 0; i < documents.length; i++)
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    CupertinoButton(
                      onPressed: () {
                        if (choosedIds.contains(documentIds[i])) {
                          choosedIds.remove(documentIds[i]);
                        } else {
                          choosedIds.add(documentIds[i]);
                        }
                        setState(() {});
                      },
                      padding: EdgeInsets.all(0),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Radio(
                            groupValue: true,
                            value: choosedIds.contains(documentIds[i])
                                ? true
                                : false,
                            onChanged: (_) {
                              if (choosedIds.contains(documentIds[i])) {
                                choosedIds.remove(documentIds[i]);
                              } else {
                                choosedIds.add(documentIds[i]);
                              }
                            },
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              for (var name in documents[i].keys)
                                Container(
                                  width:
                                      MediaQuery.of(context).size.width * 0.6,
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        name,
                                        style: Theme.of(context)
                                            .textTheme
                                            .headline3!
                                            .copyWith(
                                              fontSize: 15,
                                            ),
                                      ),
                                      Text(
                                        documents[i][name],
                                        style: Theme.of(context)
                                            .textTheme
                                            .headline3!
                                            .copyWith(
                                                fontSize: 14,
                                                color: AppTheme.textGrey),
                                      ),
                                    ],
                                  ),
                                ),
                              const SizedBox(
                                height: 40,
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                    const Spacer(),
                    CupertinoButton(
                      child: const Icon(
                        Icons.edit,
                        color: AppTheme.regularTextDarkBlack,
                      ),
                      onPressed: () {
                        context
                            .read<ArendatorCubit>()
                            .getDocumentEditForArendator(
                                arendatorId: widget.arendatorId,
                                typesId: documentIds[i]);
                        print("EDIT");
                      },
                      padding: EdgeInsets.all(0),
                    )
                  ],
                ),
            documents.isNotEmpty
                ? createButton(
                    fieldName: "Выбрать",
                    context: context,
                    onPressed: () {
                      for (String documentId in choosedIds) {
                        String name = "";
                        for (int i = 0; i < documents.length; i++) {
                          if (documentIds[i] == documentId) {
                            for (var value in documents[i].values) {
                              name = value;
                              break;
                            }
                          }
                        }

                        widget.callback!(documentId, widget.arendatorId, name);
                      }
                      Navigator.of(context).pop();
                    })
                : Container(),
            const SizedBox(
              height: 10,
            ),
            createButton(
                fieldName: "Создать",
                context: context,
                onPressed: () {
                  context
                      .read<ArendatorCubit>()
                      .getDocumentStructureForArendator(
                          arendatorId: widget.arendatorId);
                }),
            const SizedBox(
              height: 20,
            )
          ],
        ),
      ),
    );
  }
}
