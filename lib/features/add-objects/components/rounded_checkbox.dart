import 'package:flutter/material.dart';

class RoundedCheckBox extends StatefulWidget {
  RoundedCheckBox({Key? key}) : super(key: key);

  @override
  _RoundedCheckBoxState createState() => _RoundedCheckBoxState();
}

class _RoundedCheckBoxState extends State<RoundedCheckBox> {
  bool currentValue = false;
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Checkbox(materialTapTargetSize: MaterialTapTargetSize.padded,
        checkColor: Colors.white,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
        activeColor: const Color(0xff2B61A3),
        value: currentValue,
        onChanged: (newValue) {
          setState(() {
            currentValue = newValue!;
          });
        },
      ),
    );
  }
}
