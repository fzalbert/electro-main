

import 'package:electro/data/locale/entities/object_type.dart';
import 'package:floor/floor.dart';

@dao
abstract class ObjectTypeDao {

  @Query('SELECT * FROM ${ObjectTypeEntity.TABLE_NAME}')
  Future<List<ObjectTypeEntity>> findAll();

  @Query('SELECT * FROM ${ObjectTypeEntity.TABLE_NAME} WHERE id = :id')
  Stream<ObjectTypeEntity?> findById(String id);

  @Insert()
  Future<void> insert(ObjectTypeEntity type);
}
