

import 'package:electro/data/locale/entities/map_object.dart';
import 'package:floor/floor.dart';

@dao
abstract class MapObjectDao {

  @Query('SELECT * FROM map_object')
  Future<List<MapObjectEntity>> findAll();

  @Query('SELECT * FROM map_object WHERE id = :id')
  Stream<MapObjectEntity?> findById(int id);

  @Insert()
  Future<void> insert(MapObjectEntity person);

}