import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_map_location_marker/flutter_map_location_marker.dart';
import 'package:latlong2/latlong.dart';

import 'package:flutter_map_marker_popup/flutter_map_marker_popup.dart';

const int _enabledInteractive = InteractiveFlag.drag |
    InteractiveFlag.flingAnimation |
    InteractiveFlag.pinchMove |
    InteractiveFlag.pinchZoom |
    InteractiveFlag.doubleTapZoom;

class MapView extends StatefulWidget {
  final Function onPressed;
  final dynamic document;
  final Map? controllers;
  final Function? callback;
  const MapView(
      {Key? key,
      required this.onPressed,
      this.callback,
      this.document,
      this.controllers})
      : super(key: key);

  @override
  _MapViewState createState() => _MapViewState();
}

class _MapViewState extends State<MapView> {
  final PopupController _popupLayerController = PopupController();
  List<LatLng> checkedPosition = [];

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.5,
      child: FlutterMap(
        options: MapOptions(
          minZoom: 6,
          maxZoom: 18.4,
          zoom: 13,
          interactiveFlags: _enabledInteractive,
          center: LatLng(59.94231, 30.328849),
          onTap: (_, LatLng pos) {
            checkedPosition = [LatLng(pos.latitude, pos.longitude)];
            _popupLayerController.hideAllPopups();
            _popupLayerController.showPopupsAlsoFor(_markers);
            setState(() {});
            if (widget.callback != null) {
              if (widget.callback != null) {
                widget.callback!(
                  widget.document.typeParameters.specificationsId,
                  widget.document.typeParameters.id,
                  pos,
                );
              }
            }
          },
        ),
        children: [
          TileLayerWidget(
            options: TileLayerOptions(
              urlTemplate: 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
              subdomains: ['a', 'b', 'c'],
            ),
          ),
          PopupMarkerLayerWidget(
            options: PopupMarkerLayerOptions(
              popupController: _popupLayerController,
              markers: _markers,
              markerRotateAlignment:
                  PopupMarkerLayerOptions.rotationAlignmentFor(AnchorAlign.top),
              popupBuilder: (BuildContext context, Marker marker) =>
                  CupertinoButton(
                child: Card(child: ExamplePopup()),
                onPressed: () {
                  widget.onPressed();
                },
              ),
            ),
          ),
        ],
      ),
    );
  }

  List<Marker> get _markers => checkedPosition
      .map(
        (markerPosition) => Marker(
          point: markerPosition,
          width: 40,
          height: 40,
          builder: (_) => Icon(Icons.location_on, size: 40),
          anchorPos: AnchorPos.align(AnchorAlign.top),
        ),
      )
      .toList();
}

class ExamplePopup extends StatefulWidget {
  const ExamplePopup({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _ExamplePopupState();
}

class _ExamplePopupState extends State<ExamplePopup> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10),
      child: Container(
        constraints: BoxConstraints(minWidth: 100, maxWidth: 200),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: const <Widget>[
            Text(
              'Верная позиция',
              overflow: TextOverflow.fade,
              softWrap: false,
              style: TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: 14.0,
              ),
            ),
            Padding(padding: EdgeInsets.symmetric(vertical: 4.0)),
          ],
        ),
      ),
    );
  }
}
