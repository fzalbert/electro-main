import 'package:electro/app/theme/theme.dart';
import 'package:electro/bloc/authentication/auth_bloc.dart';
import 'package:electro/bloc/authentication/auth_events.dart';
import 'package:electro/bloc/authentication/auth_state.dart';
import 'package:electro/data/singletons/stop_and_go.dart';
import 'package:electro/features/account/auth/auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:electro/app/constants/colors.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:formz/formz.dart';

class CancelStopGoBottomSheet extends StatelessWidget {
  final BuildContext parentContext;
  const CancelStopGoBottomSheet({Key? key, required this.parentContext})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<AuthBloc, AuthState>(listener: (context, state) {
      if (state.logoutStatus == FormzStatus.submissionSuccess) {
        Navigator.pushAndRemoveUntil(context, CupertinoPageRoute(builder: (x)=>AuthScreenWidget()), (route) => false);
      }
    }, builder: (context, state) {
      print(state.logoutStatus);
      return Container(
        padding: const EdgeInsets.fromLTRB(16, 44, 16, 30),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              'Вы действительно хотите выйти из режима Stop and Go?',
              style:
              Theme.of(context).textTheme.headline3!.copyWith(fontSize: 18),
              textAlign: TextAlign.center,
            ),
            const SizedBox(
              height: 28,
            ),
            state.logoutStatus == FormzStatus.submissionInProgress
                ? const Center(
              child: CupertinoActivityIndicator(),
            )
                : Row(
              children: [
                const SizedBox(
                  width: 30,
                ),
                Expanded(
                  child: GestureDetector(
                    onTap: () {
                      _cancel(context);
                    },
                    child: Container(
                      height: 48,
                      child: Center(
                        child: Text(
                          'Нет',
                          style: Theme.of(context)
                              .textTheme
                              .headline4!
                              .copyWith(
                              fontSize: 20,
                              color: AppTheme.regularTextDarkBlue),
                        ),
                      ),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          border: Border.all(color: mainColor)),
                    ),
                  ),
                ),
                const SizedBox(
                  width: 30,
                ),
                Expanded(
                  child: GestureDetector(
                    onTap: () {
                      _accept(context);
                    },
                    child: Container(
                      child: Center(
                        child: Text(
                          'Да',
                          style: Theme.of(context)
                              .textTheme
                              .headline4!
                              .copyWith(
                              fontSize: 20, color: Colors.white),
                        ),
                      ),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: mainColor),
                      height: 48,
                    ),
                  ),
                ),
                const SizedBox(
                  width: 30,
                ),
              ],
            )
          ],
        ),
      );
    });
  }

  void _cancel(BuildContext context) {
    Navigator.of(context).pop();
  }

  void _accept(BuildContext context) {
    StopAndGo.cancel();
    Navigator.of(context).pop();
  }
}
