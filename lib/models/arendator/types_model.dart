class Type {
  final String id;
  final String name;

  Type({
    required this.id,
    required this.name,
  });
  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
    };
  }

  factory Type.fromJson(Map<String, dynamic> json) {
    return Type(
      id: json["id"],
      name: json["name"],
    );
  }
}
