import 'package:dio/dio.dart';
import 'package:electro/data/singletons/dio_interceptor.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

class DioSettings {
  static const String TEST_URL = 'https://dev.wgsm.pro-itech.ru/api/MobileApi/';
  static const String PROD_URL = 'https://wgsm.pro-itech.ru/api/MobileApi/';
  static const String INIT_URL = "https://cp-vm-volsapp2/api/MobileApi/";

  static const String BASE_URL = PROD_URL;

  BaseOptions _dioBaseOptions = BaseOptions(
    baseUrl: BASE_URL,
    connectTimeout: 30000,
    receiveTimeout: 30000,
    followRedirects: false,
    validateStatus: (status) {
      return status != null && status <= 500;
    },
  );

  void setBaseOptions({String? lang}) {
    _dioBaseOptions = BaseOptions(
      baseUrl: BASE_URL,
      connectTimeout: 30000,
      receiveTimeout: 30000,
      followRedirects: false,
      validateStatus: (status) {
        return status != null && status <= 500;
      },
    );
  }

  BaseOptions get dioBaseOptions => _dioBaseOptions;

  BaseOptions getDioBaseOptions({int connectTimeout = 5000}) {
    var options = _dioBaseOptions;
    options.connectTimeout = connectTimeout;
    options.receiveTimeout = connectTimeout;
    return options;
  }

  static Dio createDio({int connectTimeout = 30000}) {
    Dio _dio =
        Dio(DioSettings().getDioBaseOptions(connectTimeout: connectTimeout));

    _dio.interceptors.add(PrettyDioLogger(
      requestHeader: true,
      requestBody: true,
    ));
    _dio.interceptors.add(AppInterceptors());
    return _dio;
  }
}
