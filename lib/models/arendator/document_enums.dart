enum SpecificationTypes {
  NotSelected,
  Number,
  RealNumber,
  Text,
  SmallText,
  Checkbox,
  Radio,
  Select,
  MultiSelect,
  Date,
  DateTime,
  File,
  Geometry,
}

enum ConditionTypes {
  NotSelected,
  Equals,
  NotEquals,
  More,
  MoreAndEqual,
  Smaller,
  SmallerAndEqual,
  Interval,
  RegularExpression,
}

enum searchTypes {
  NotSelected,
  CadastralNumber,
  Address,
  Owner,
}
