import 'package:electro/data/locale/entities/object_form_entity.dart';
import 'package:floor/floor.dart';


@dao
abstract class ObjectFormDao {

  @Query('SELECT * FROM ${ObjectFormEntity.TABLE_NAME}')
  Future<List<ObjectFormEntity>> findAll();

  @Query('SELECT * FROM ${ObjectFormEntity.TABLE_NAME} WHERE id = :id')
  Future<ObjectFormEntity?> findById(String id);

  @Insert()
  Future<void> insert(ObjectFormEntity type);
}
