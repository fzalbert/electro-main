import 'package:electro/data/locale/entities/map_object.dart';
import 'package:electro/utils/guid_helper.dart';
import 'package:floor/floor.dart';
import 'package:json_annotation/json_annotation.dart';

part 'renter.g.dart';

@JsonSerializable()
class RenterRequestDto {
  RenterRequestDto({
    required this.idList,
    required this.typeId,
  });

  @JsonKey(name: 'RenterIds')
  List<String> idList;
  @JsonKey(name: 'RenterTypesId')
  String typeId;


  static RenterRequestDto fromModel(RenterModel model){
    return RenterRequestDto(
        idList: [model.renterId],
        typeId: model.renterTypeId
    );
  }

  factory RenterRequestDto.fromJson(Map<String, dynamic> json) =>
      _$RenterRequestDtoFromJson(json);

  Map<String, dynamic> toJson() => _$RenterRequestDtoToJson(this);
}

@Entity(tableName: "renter_table", foreignKeys: [
  ForeignKey(
    childColumns: ["objectId"],
    parentColumns: ["id"],
    entity: MapObjectEntity,
    onDelete: ForeignKeyAction.cascade,
  )
])
class RenterEntity {
  @PrimaryKey()
  final String id;

  String objectId;

  String renterId;

  String renterTypeId;

  RenterEntity({
    required this.id,
    required this.objectId,
    required this.renterId,
    required this.renterTypeId,
  });

  static RenterEntity fromModel(RenterModel model, String objectId){
    return RenterEntity(
        id: model.id ?? GuidHelper.timeBased(),
        objectId: objectId,
        renterId: model.renterId,
        renterTypeId: model.renterTypeId
    );
  }
}

class RenterModel {

  String? id;

  String renterId;

  String renterTypeId;

  RenterModel({
    required this.renterId,
    required this.renterTypeId,
  });


  static RenterModel fromEntity(RenterEntity entity){
    return RenterModel(
        renterId: entity.renterId,
        renterTypeId: entity.renterTypeId
    );
  }
}
