import 'package:electro/app/constants/app_icons.dart';
import 'package:electro/app/theme/theme.dart';
import 'package:electro/data/utils/functions.dart';
import 'package:electro/features/add-objects/post_bottomsheet.dart';
import 'package:electro/features/crete-user/fields/image_upload.dart';
import 'package:electro/features/projects/components/editing_textfield.dart';
import 'package:flutter/material.dart';

class EditingBottomSheet extends StatelessWidget {
  final bool isEditing;

  EditingBottomSheet({Key? key, required this.isEditing}) : super(key: key);
  final TextEditingController _nameController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.9,
      padding: const EdgeInsets.fromLTRB(16, 44, 16, 0),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            isEditing
                ? Text(
                    'Редактировать',
                    style: Theme.of(context).textTheme.headline3!.copyWith(
                          fontSize: 20,
                        ),
                  )
                : Text(
                    'Создать',
                    style: Theme.of(context).textTheme.headline3!.copyWith(
                          fontSize: 20,
                        ),
                  ),
            const SizedBox(height: 28),
            _createSelector(context),
            Text(
              'Название',
              style: Theme.of(context)
                  .textTheme
                  .headline3!
                  .copyWith(fontSize: 12, color: AppTheme.textGreyDark),
            ),
            const SizedBox(
              height: 8,
            ),
            GestureDetector(
              onTap: () {
                if (!isEditing) {
                  Navigator.pop(context);
                  MyFunctions.popBottomSheet(
                      const PostBottomSheet(), true, context);
                } else {}
              },
              child: AbsorbPointer(
                absorbing: !isEditing,
                child: Row(
                  children: [
                    Expanded(
                        child: EditingTextField(
                      controller: _nameController,
                      currentText: 'Столб',
                    )),
                  ],
                ),
              ),
            ),
            const SizedBox(height: 28),
            Text(
              'Название',
              style: Theme.of(context)
                  .textTheme
                  .headline3!
                  .copyWith(fontSize: 12, color: AppTheme.textGreyDark),
            ),
            const SizedBox(
              height: 8,
            ),
            Row(
              children: [
                Expanded(
                    child: EditingTextField(
                  controller: _nameController,
                  currentText: 'Муфта',
                )),
              ],
            ),
            const SizedBox(height: 28),
            Text(
              'Крепеж к столбу',
              style: Theme.of(context)
                  .textTheme
                  .headline3!
                  .copyWith(fontSize: 12, color: AppTheme.textGreyDark),
            ),
            const SizedBox(
              height: 8,
            ),
            Row(
              children: [
                Expanded(child: EditingTextField(controller: _nameController)),
              ],
            ),
            const SizedBox(height: 28),
            Text(
              'Текущая геопозиция',
              style: Theme.of(context)
                  .textTheme
                  .headline3!
                  .copyWith(fontSize: 12, color: AppTheme.textGreyDark),
            ),
            const SizedBox(
              height: 8,
            ),
            Row(
              children: [
                Expanded(
                    child: EditingTextField(
                  suffix: AppIcons.location,
                  controller: _nameController,
                  currentText: '123456',
                )),
              ],
            ),
            const SizedBox(height: 28),
            Text(
              'Номер',
              style: Theme.of(context)
                  .textTheme
                  .headline3!
                  .copyWith(fontSize: 12, color: AppTheme.textGreyDark),
            ),
            const SizedBox(
              height: 8,
            ),
            Row(
              children: [
                Expanded(
                    child: EditingTextField(
                  controller: _nameController,
                  currentText: '123456464',
                )),
              ],
            ),
            const SizedBox(height: 28),
            Text(
              'Марка кабеля',
              style: Theme.of(context)
                  .textTheme
                  .headline3!
                  .copyWith(fontSize: 12, color: AppTheme.textGreyDark),
            ),
            const SizedBox(
              height: 8,
            ),
            Row(
              children: [
                Expanded(
                    child: EditingTextField(
                  controller: _nameController,
                  currentText: 'Россия',
                )),
              ],
            ),
            const SizedBox(height: 28),
            const Text('Название '),
            const SizedBox(
              height: 8,
            ),
            Row(
              children: [
                Expanded(child: EditingTextField(controller: _nameController)),
              ],
            ),
            const SizedBox(
              height: 28,
            ),
            ImageUploadItem(),
            Row(
              children: [
                Expanded(
                  child: Container(
                    decoration: BoxDecoration(
                      color: const Color(0xff2B61A3),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    height: 48,
                    child: Center(
                      child: Text(
                        !isEditing ? 'Установить геопозицию' : 'Создать',
                        style: Theme.of(context)
                            .textTheme
                            .headline3!
                            .copyWith(fontSize: 16, color: Colors.white),
                      ),
                    ),
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 15,
            )
          ],
        ),
      ),
    );
  }

  _createSelector(BuildContext context) {
    final List<String> _dropdownValues = [
      "One",
      "Two",
      "Three",
      "Four",
      "Five"
    ];

    return Container(
      padding: const EdgeInsets.symmetric(vertical: 0, horizontal: 10),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(9.0),
          border: Border.all(
              color: const Color(0xffF3F3F3),
              style: BorderStyle.solid,
              width: 1),
          color: const Color(0xffF8F8F8)),
      child: DropdownButton(
        items: _dropdownValues
            .map((value) => DropdownMenuItem(
                  child: Text(value),
                  value: value,
                ))
            .toList(),
        onChanged: (value) {},
        underline: const SizedBox(),
        isExpanded: true,
        value: _dropdownValues.first,
        style: Theme.of(context).textTheme.headline4!.copyWith(
              fontSize: 16,
            ),
      ),
    );
  }
}
