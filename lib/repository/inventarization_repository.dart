import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:electro/data/singletons/dio_settings.dart';
import 'package:electro/data/singletons/storage.dart';
import 'package:electro/models/inventorization/item.dart';
import 'package:electro/repository/draw_type.dart';
import 'package:electro/utils/geojson_helper.dart';
import 'package:latlong2/latlong.dart';

class InventarizationRepository {
  final Dio _dio = DioSettings.createDio();

  Future<Item?> get() async {
    String application = StorageRepository.getString("ApplicationItem");
    Item? prevItem;
    if (application != "") {
      Map<String, dynamic> itemMap = jsonDecode(application);
      prevItem = Item.fromJson(itemMap);
    }

    try {
      final Response response = await _dio.get(
        'GetInventarization/',
        options: Options(headers: {
          'Authorization': 'Bearer ${StorageRepository.getString('token')}'
        }),
      );
      if (response.statusCode! >= 200 && response.statusCode! < 300) {
        var res = response.data;
        if (res["item"] != null) {
          Item item = Item.fromJson(res["item"]);
          //sleep(Duration(seconds: 5));

          if (prevItem != null) {
            if (prevItem.id != item.id) {
              await StorageRepository.deleteString("ApplicationItem");
              if (item.statusId == "2" || item.statusId == "3") {
                StorageRepository.putString(
                    "ApplicationItem", jsonEncode(item.toJson()));
                return item;
              }
              return null;
            } else {
              if (prevItem.statusId == "2" || prevItem.statusId == "3") {
                return prevItem;
              }
              return null;
            }
          }
          if (item.statusId == "2" || item.statusId == "3") {
            print("Назначены на заявку");
            await StorageRepository.deleteString("ApplicationItem");
            StorageRepository.putString(
                "ApplicationItem", jsonEncode(item.toJson()));
            return item;
          }
          return null;
        }
        await StorageRepository.deleteString("ApplicationItem");
        return null;
      } else {
        return null;
      }
    } catch (e) {
      print(e.toString());
      return prevItem;
    }
  }

  void changeStatus(String id, String statusId) async {
    final Response response = await _dio.get(
      'InventarizationUpdateStatus/',
      queryParameters: {
        "id": id,
        "statusId": statusId,
      },
      options: Options(headers: {
        'Authorization': 'Bearer ${StorageRepository.getString('token')}'
      }),
    );
    if (statusId == "4") {
      await StorageRepository.deleteString("ApplicationItem");
    }
  }
}
