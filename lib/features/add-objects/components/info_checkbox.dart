import 'package:flutter/material.dart';

class InfoCheckBox extends StatefulWidget {
  InfoCheckBox({Key? key}) : super(key: key);

  @override
  _InfoCheckBoxState createState() => _InfoCheckBoxState();
}

class _InfoCheckBoxState extends State<InfoCheckBox> {
  bool currentValue = false;
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Checkbox(
        checkColor: Colors.white,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4)),
        activeColor: const Color(0xff2B61A3),
        value: currentValue,
        onChanged: (newValue) {
          setState(() {
            currentValue = newValue!;
          });
        },
      ),
    );
  }
}
