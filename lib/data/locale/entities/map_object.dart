import 'package:electro/data/locale/entities/map_object_value.dart';
import 'package:electro/data/locale/entities/position.dart';
import 'package:electro/models/arendator/renter.dart';
import 'package:electro/models/object/object_type.dart';
import 'package:floor/floor.dart';
import 'package:latlong2/latlong.dart';
import 'file.dart';
import 'stop_go.dart';

@Entity(tableName: "map_object", foreignKeys: [
  ForeignKey(
    childColumns: ["sessionId"],
    parentColumns: ["id"],
    entity: StopGoSessionEntity,
    onDelete: ForeignKeyAction.cascade,
  ),
  ForeignKey(
    childColumns: ["positionId"],
    parentColumns: ["id"],
    entity: PositionEntity,
    onDelete: ForeignKeyAction.cascade,
  )
])
class MapObjectEntity {
  @PrimaryKey()
  final String id;

  final String parentId;

  /// [StopGoSessionEntity.id] id
  String? sessionId;

  final String positionId;

  /// MotId Идентификатор типа объекта. Идетификатор можно взять из модели запроса [ObjectTypeResponseDto].
  final String objectTypeId;

  MapObjectEntity(
      {required this.id,
      required this.parentId,
      this.sessionId,
      required this.positionId,
      required this.objectTypeId});
}

class MapObjectModel {
  final String id;
  final String? sessionId;
  final String positionId;

  ///motId
  final String objectTypeId;
  final String parentId;
  final Iterable<PositionEntity> coords;

  final List<MapObjectValueModel> values;

  final LatLng position;

  final List<RenterModel>? renters;
  List<FileModel> filesInfo;

  MapObjectModel({
    required this.id,
    this.sessionId,
    required this.positionId,
    required this.objectTypeId,
    required this.values,
    required this.position,
    required this.parentId,
    required this.coords,
    this.renters,
    required this.filesInfo,
  });

  static MapObjectModel fromEntity(
    MapObjectEntity entity,
    List<MapObjectValueModel> values,
    LatLng coords,
    List<RenterModel> renters,
    List<FileModel>? filesInfo,
  ) {
    return MapObjectModel(
      id: entity.id,
      positionId: entity.positionId,
      objectTypeId: entity.objectTypeId,
      values: values,
      position: coords,
      parentId: entity.parentId,
      coords: [],
      renters: renters,
      filesInfo: filesInfo ?? [],
    );
  }
}
