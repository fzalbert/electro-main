class ObjectInfo {
  final String? title;
  final List<List> layerItems;
  final List<dynamic>? files;

  ObjectInfo({required this.title, required this.layerItems, this.files});

  factory ObjectInfo.fromJson(Map<String, dynamic> json) {
    List<List> _layerItems = [];

    json["LayerItems"].forEach((var mapElement) {
      _layerItems.add([mapElement["Key"], mapElement["Value"]]);
    });

    return ObjectInfo(
      title: json["LayerName"],
      layerItems: _layerItems,
      files: json["Files"],
    );
  }
}
