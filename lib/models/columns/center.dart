
import 'package:json_annotation/json_annotation.dart';

part 'center.g.dart';
@JsonSerializable()
class CenterModel {

  @JsonKey(defaultValue: '')
  final String type;
  @JsonKey(defaultValue: [])
  final List<double> coordinates;


  CenterModel({required this.coordinates, required this.type});


   factory CenterModel.fromJson(Map<String, dynamic> json) =>
      _$CenterModelFromJson(json);
}
