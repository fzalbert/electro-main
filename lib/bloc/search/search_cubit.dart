import 'package:bloc/bloc.dart';
import 'package:electro/bloc/arendator/arendator_cubit.dart';
import 'package:electro/models/search/search_response.dart';
import 'package:electro/models/object/object_info.dart';
import 'package:electro/models/search/search_request.dart';
import 'package:electro/repository/projects.dart';
import 'package:electro/repository/search.dart';
import 'package:equatable/equatable.dart';
import 'package:latlong2/latlong.dart';

part 'search_state.dart';

class SearchCubit extends Cubit<SearchState> {
  SearchCubit({
    required this.repository,
  }) : super(InitialSearchState()) {
    initializeSearchParameters();
  }

  final SearchRepository repository;

  Future<Map<String, dynamic>> initializeSearchParameters() async {
    Map<String, dynamic> settingsParameters = await repository.getSearchTypes();
    print("HERE PARAMS = $settingsParameters");
    return settingsParameters;
  }

  void searchByLayerText({required SearchRequest requestBody}) async {
    try {
      emit(SearchingState());
      List<SearchItem> items =
          await repository.searchByLayerText(requestBody: requestBody);

      emit(LoadedSearchState(items));
    } catch (e) {
      print("ERROR JJr= $e");
      if (e.toString().startsWith("FormatException"))
        emit(ErrorSearching("Идентификатор слоя должен быть заполнен"));
      else
        emit(ErrorSearching(e.toString().split(": ")[1]));
    }
  }
}
