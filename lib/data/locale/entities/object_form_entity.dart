import 'package:electro/models/object/draw_type_form.dart';
import 'package:electro/models/object/parameter.dart';
import 'package:floor/floor.dart';

@Entity(tableName: ObjectFormEntity.TABLE_NAME)
class ObjectFormEntity {
  @PrimaryKey()
  String id;

  String name;

  String mapObjectTypeId;

  String parameters;

  bool filesEnabled;

  ObjectFormEntity(
      {
        required this.id,
        required this.name,
        required this.parameters,
        required this.mapObjectTypeId,
        required this.filesEnabled
      });

  @ignore
  static const TABLE_NAME = "object_form";

  factory ObjectFormEntity.fromDto(DrawTypeForm dto) {
    var paramsStr = Parameter.toJsonString(dto.parameters);

    return ObjectFormEntity(
        id: dto.id,
        name: dto.name,
        parameters: paramsStr,
        mapObjectTypeId: dto.mapObjectTypeParentId ?? "",
        filesEnabled: dto.filesEnabled,
    );
  }
}

class ObjectFormModel {
  String id;

  String name;

  bool filesEnabled;

  List<Parameter> parameters;

  ObjectFormModel(
      {
        required this.id,
        required this.name,
        required this.parameters,
        required this.filesEnabled,
      });

  factory ObjectFormModel.fromEntity(ObjectFormEntity entity) {
    var parameters = Parameter.fromJsonString(entity.parameters);

    return ObjectFormModel(
        id: entity.id,
        name: entity.name,
        parameters: parameters,
        filesEnabled: entity.filesEnabled
    );
  }
}
