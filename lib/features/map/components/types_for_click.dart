import 'package:electro/bloc/object/object_cubit.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/src/provider.dart';

class TypeObjects extends StatelessWidget {
  final List<List<String>> typeIds;

  final List<String> layerIds;
  const TypeObjects({Key? key, required this.typeIds, required this.layerIds})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(3),
      height: MediaQuery.of(context).size.height * 0.6,
      //margin: EdgeInsets.all(5),
      decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(15))),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Container(
                  margin: EdgeInsets.only(left: 15, bottom: 20, top: 20),
                  child: Text(
                    "Объекты",
                    style: Theme.of(context).textTheme.headline3!.copyWith(
                          fontSize: 20,
                        ),
                  ),
                ),
              ],
            ),
            for (List<String> typeId in typeIds)
              GestureDetector(
                onTap: () {
                  context
                      .read<ObjectCubit>()
                      .getObject(typeId: typeId[1], layerIds: layerIds);
                },
                child: Container(
                  margin: EdgeInsets.only(left: 15, bottom: 10),
                  child: Text(typeId[0]),
                ),
              ),
          ],
        ),
      ),
    );
  }
}
