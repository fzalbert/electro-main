import 'package:electro/data/utils/functions.dart';
import 'package:electro/features/projects/components/switcher.dart';
import 'package:electro/features/projects/detail_bottom_sheet.dart';
import 'package:electro/app/constants/app_icons.dart';
import 'package:electro/app/theme/theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:electro/models/columns/layer_directory.dart';

class ColumnItem extends StatelessWidget {
  final Widget prefix;
  final String title;
  final bool? startingState;
  const ColumnItem({
    Key? key,
    required this.prefix,
    required this.title,this.startingState
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(behavior: HitTestBehavior.translucent,
      onTap: () {
        MyFunctions.popBottomSheet(
            DetailBottomSheet(
              prefix: prefix,
              title: title,
            ),
            true,
            context);
      },
      child: Container(
        child: Row(
          children: [
            prefix,
            const SizedBox(
              width: 12,
            ),
            Text(
              title,
              style: Theme.of(context)
                  .textTheme
                  .headline4!
                  .copyWith(fontSize: 16, color: AppTheme.regularText),
            ),
            const Spacer(),
            ProjectsSwitcher(
              startingState: startingState??false,
              prefix: prefix,
              title: title,
            ),
          ],
        ),
      ),
    );
  }
}
