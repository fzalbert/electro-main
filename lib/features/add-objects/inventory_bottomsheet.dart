import 'package:electro/app/constants/colors.dart';
import 'package:electro/data/singletons/general_provider.dart';
import 'package:electro/data/singletons/stop_and_go.dart';
import 'package:electro/data/singletons/storage.dart';
import 'package:electro/data/utils/functions.dart';
import 'package:electro/features/draw_type/draw_type_tree_widget.dart';
import 'package:electro/repository/stopgo/stop_go.dart';
import 'package:electro/widgets/btns/rounded_button.dart';
import 'package:electro/widgets/dialog/ErrorDialog.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:connectivity/connectivity.dart';

class InventoryBottomSheet extends StatefulWidget {
  _InventoryBottomSheetState createState() => _InventoryBottomSheetState();
  final bool isActive;
  const InventoryBottomSheet({Key? key, required this.isActive})
      : super(key: key);
}

class _InventoryBottomSheetState extends State<InventoryBottomSheet> {
  String application = "";

  bool isSessionSended = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    application = StorageRepository.getString("ApplicationItem");
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    double height;
    List<Widget> childrens;

    if (widget.isActive) {
      height = 304;
      childrens = <Widget>[
        Text(
          application != "" ? 'Инвентаризация по заявке' : "Инвентаризация",
          style: Theme.of(context).textTheme.headline3!.copyWith(
                fontSize: 20,
              ),
        ),
        const SizedBox(height: 20),
        _createIndicator(context),
        const SizedBox(height: 23),
        _objectBtnCreate(context),
        const SizedBox(height: 16),
        _createOutlineBtn("Финиш", isSessionSended, () async {
          var session = StopAndGo.getCurrentSession();
          if (session != null) {
            if (StopAndGo.isPaused) {
              StopAndGo.resume();
            }
            var repository = StopGoSessionRepository();
            setState(() {
              isSessionSended = true;
            });
            var status = await repository.newSend(session);

            if (status < 200 || status > 300) {
              Navigator.of(context).pop();
              showDialog(
                context: context,
                builder: (BuildContext context) => ErrorDialog(
                  title: "Ошибка",
                  description: status == 501
                      ? "У вас нет соединенения повторите позже"
                      : "Что то пошло не так!",
                  buttonText: "Okay",
                ),
              );
            }
          } else
            Navigator.of(context).pop();
        }),
        const SizedBox(
          height: 30,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Switch(
              onChanged: (bool newVal) async {
                final GeneralProvider provider =
                    Provider.of<GeneralProvider>(context, listen: false);
                var connectivityResult =
                    await (Connectivity().checkConnectivity());
                if (connectivityResult == ConnectivityResult.mobile ||
                    connectivityResult == ConnectivityResult.wifi) {
                  provider.cachingEnabled = false;
                } else {
                  provider.cachingEnabled = newVal;
                  provider.resetMap();
                }
              },
              value: Provider.of<GeneralProvider>(context).cachingEnabled,
            ),
          ],
        ),
      ];
    } else {
      height = 300;
      childrens = <Widget>[
        Text(
          application != "" ? 'Инвентаризация по заявке' : "Инвентаризация",
          style: Theme.of(context).textTheme.headline3!.copyWith(
                fontSize: 20,
              ),
        ),
        const SizedBox(height: 28),
        _objectBtnCreate(context),
        const SizedBox(height: 16),
        _createOutlineBtn("Старт", false, () {
          StopAndGo.start();
          Navigator.of(context).pop();
        }),
        const SizedBox(
          height: 30,
        ),
        Row(
          children: [
            Text(
              "Использовать сохранённые карты",
              style: Theme.of(context).textTheme.headline3!.copyWith(
                    fontSize: 15,
                  ),
            ),
            Spacer(),
            Switch(
              onChanged: (bool newVal) async {
                final GeneralProvider provider =
                    Provider.of<GeneralProvider>(context, listen: false);
                var connectivityResult =
                    await (Connectivity().checkConnectivity());
                if (connectivityResult == ConnectivityResult.mobile ||
                    connectivityResult == ConnectivityResult.wifi) {
                  provider.cachingEnabled = false;
                  MyFunctions.showErrorMessage(
                      errorMessage:
                          "Вы можете использовать кешированную карту только при нестабильном/отключенном подключении к интернeт",
                      context: context);
                } else {
                  provider.cachingEnabled = newVal;
                  provider.resetMap();
                }
              },
              value: Provider.of<GeneralProvider>(context).cachingEnabled,
            ),
          ],
        ),
      ];
    }

    return Container(
      height: height,
      padding: const EdgeInsets.fromLTRB(16, 44, 16, 0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: childrens,
      ),
    );
  }

  Widget _createOutlineBtn(
      String title, bool isLoading, VoidCallback? onPressed) {
    return RoundedButton(
      height: 48,
      width: double.infinity,
      cornerRadius: 10,
      title: title,
      isLoading: isLoading,
      onTap: onPressed,
    );
  }

  Widget _createIndicator(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 10),
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            'Stop and go',
            style:
                Theme.of(context).textTheme.headline4!.copyWith(fontSize: 16),
          ),
          IconButton(
              icon: Icon(
                !StopAndGo.isPaused ? Icons.pause : Icons.play_arrow,
                color: mainColor,
              ),
              onPressed: () {
                if (!StopAndGo.isPaused) {
                  StopAndGo.pause();
                } else {
                  StopAndGo.resume();
                }

                setState(() {});
              }),
        ],
      ),
    );
  }

  Widget _objectBtnCreate(BuildContext context) {
    return RoundedButton(
      title: "Создать объект",
      height: 48,
      width: double.infinity,
      cornerRadius: 10,
      onTap: () async {
        if (StopAndGo.isStarted) StopAndGo.pause();
        setState(() {});
        FocusScope.of(context).requestFocus(FocusNode());
        MyFunctions.popBottomSheet(DrawTypeTreeWidget(), true, context)
            .whenComplete(() {
          //
        });
      },
    );
  }
}
