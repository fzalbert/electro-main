import 'package:electro/data/singletons/storage.dart';
import 'package:electro/features/account/auth/auth.dart';
import 'package:electro/features/map/components/map_body.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MapScreenWidget extends StatefulWidget {
  static MapBodyWidget mapBodyWidget = MapBodyWidget();

  @override
  State<MapScreenWidget> createState() => _MapScreenWidgetState();
}

class _MapScreenWidgetState extends State<MapScreenWidget> {
  Future check() async {
    await StorageRepository.getInstance();

    var result = StorageRepository.getString('token');
    print(result.toString() + ' from MAP');
    if (result.isEmpty) {
      Navigator.pushAndRemoveUntil(
          context,
          CupertinoPageRoute(builder: (c) => AuthScreenWidget()),
          (route) => false);
    } else {
      Navigator.pushAndRemoveUntil(
          context,
          CupertinoPageRoute(builder: (c) => MapBodyWidget()),
          (route) => false);
    }
  }

  @override
  void initState() {
    WidgetsBinding.instance!.addPostFrameCallback((_) => check());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      resizeToAvoidBottomInset: false,
      body: Center(
        child: CupertinoActivityIndicator(),
      ),
    );
  }
}
