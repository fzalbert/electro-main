class SearchItem {
  final String keyIdentifier;
  final String? cadastralNumber;
  final String? address;
  final String? geometry;
  final String? parentCadastralNumber;
  final String result;

  SearchItem({
    required this.keyIdentifier,
    required this.result,
    this.cadastralNumber,
    this.address,
    this.geometry,
    this.parentCadastralNumber,
  });
}
