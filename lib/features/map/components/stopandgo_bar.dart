import 'dart:ui';

import 'package:electro/app/constants/app_icons.dart';
import 'package:electro/app/constants/colors.dart';
import 'package:electro/app/theme/theme.dart';
import 'package:electro/bloc/search/search_cubit.dart';
import 'package:electro/data/singletons/stop_and_go.dart';
import 'package:electro/data/utils/functions.dart';
import 'package:electro/features/map/components/search/search_text_field.dart';
import 'package:electro/features/map/components/search_bar.dart';
import 'package:electro/models/columns/layer.dart';
import 'package:electro/repository/search.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';

class StopAndGoBar extends StatefulWidget {
  final VoidCallback? onSettingsTapped;
  final List<LayerModel> layerModels;
  final Function callback;

  const StopAndGoBar(
      {this.onSettingsTapped,
      required this.layerModels,
      required this.callback,
      Key? key})
      : super(key: key);

  @override
  _StopAndGoBarState createState() => _StopAndGoBarState();
}

class _StopAndGoBarState extends State<StopAndGoBar> {
  final _searchFieldFocusNode = FocusNode();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    _searchFieldFocusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: 60,
      left: 16,
      right: 16,
      child: Column(
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(40),
            child: BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 12, sigmaY: 12),
              child: Container(
                  decoration: BoxDecoration(
                    gradient: const LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [
                        Color.fromRGBO(255, 255, 255, 0.5),
                        Color.fromRGBO(215, 224, 237, 0.5),
                      ],
                    ),
                    borderRadius: BorderRadius.circular(40.0),
                    border: Border.all(color: Colors.white),
                    boxShadow: const [
                      BoxShadow(
                        color: Color(0x1A193D83),
                        offset: Offset(30, 30),
                        blurRadius: 50,
                      )
                    ],
                  ),
                  child: Column(children: [
                    _createContent(context),
                    BlocProvider(
                      create: (BuildContext context) =>
                          SearchCubit(repository: SearchRepository()),
                      child: SearchBar(
                        layerModels: widget.layerModels,
                        callback: widget.callback,
                      ),
                    ),
                  ])),
            ),
          ),
        ],
      ),
    );
  }

  Widget _createContent(BuildContext context) {
    return SizedBox(
        height: 52,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          //change here don't //worked
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            IconButton(
              icon: const Icon(
                Icons.arrow_back_ios,
                size: 15,
                color: iconGrey,
              ),
              onPressed: () {
                MyFunctions.showStopAndGoCancel(context: context);
              },
            ),
            const Spacer(),
            SvgPicture.asset(AppIcons.stopAndGo),
            const SizedBox(width: 5),
            Text("Stop&Go",
                style: Theme.of(context)
                    .textTheme
                    .headline4!
                    .copyWith(fontSize: 16, color: iconGrey)),
            const SizedBox(width: 18),
          ],
        ));
  }
}
