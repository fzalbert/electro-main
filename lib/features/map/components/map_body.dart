import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';
import 'dart:ui' as ui;
import 'dart:ui';
import 'package:provider/provider.dart';

import 'package:electro/app/constants/app_icons.dart';
import 'package:electro/app/constants/colors.dart';
import 'package:electro/bloc/authentication/auth_bloc.dart';
import 'package:electro/bloc/project-list/projects_bloc.dart';
import 'package:electro/bloc/project-list/projects_events.dart';
import 'package:electro/bloc/project-list/projects_state.dart';
import 'package:electro/bloc/search/search_cubit.dart';
import 'package:electro/data/database/database.dart';
import 'package:electro/data/locale/entities/session_status.dart';
import 'package:electro/data/singletons/general_provider.dart';
import 'package:electro/data/singletons/stop_and_go.dart';
import 'package:electro/data/singletons/storage.dart';
import 'package:electro/data/utils/functions.dart';
import 'package:electro/features/add-objects/post_bottomsheet.dart';
import 'package:electro/features/crete-user/pages/loading_page.dart';
import 'package:electro/features/draw_type/object_type_service.dart';
import 'package:electro/features/map/components/inventory_btn.dart';
import 'package:electro/features/map/components/object_widget.dart';
import 'package:electro/features/map/components/rounded_btn.dart';
import 'package:electro/features/map/components/search_bar.dart';
import 'package:electro/features/map/components/stopandgo_bar.dart';
import 'package:electro/features/map/marker_helper.dart';
import 'package:electro/features/sessions/service/object_service.dart';
import 'package:electro/features/sessions/service/stop_go_session_service.dart';
import 'package:electro/features/sessions/sessions_viewer.dart';
import 'package:electro/features/sessions/view_models/stop_go_session.dart';
import 'package:electro/models/columns/layer.dart';
import 'package:electro/models/inventorization/item.dart';
import 'package:electro/repository/auth.dart';
import 'package:electro/repository/draw_type.dart';
import 'package:electro/repository/inventarization_repository.dart';
import 'package:electro/repository/projects.dart';
import 'package:electro/repository/search.dart';
import 'package:electro/repository/stopgo/stop_go.dart';
import 'package:electro/utils/bottom_sheet_helper.dart';
import 'package:electro/utils/geojson_helper.dart';
import 'package:electro/widgets/dialog/ErrorDialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_map_location_marker/flutter_map_location_marker.dart';
import 'package:flutter_map_tile_caching/flutter_map_tile_caching.dart';
import 'package:formz/formz.dart';
import 'package:geolocator/geolocator.dart';
import 'package:latlong2/latlong.dart';
import 'package:location/location.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:electro/repository/arendator.dart';
import 'package:electro/repository/file.dart';

import 'map_bottom_btns.dart';

const int _enabledInteractive = InteractiveFlag.drag |
    InteractiveFlag.flingAnimation |
    InteractiveFlag.pinchMove |
    InteractiveFlag.pinchZoom |
    InteractiveFlag.doubleTapZoom;

class MapBodyWidget extends StatefulWidget {
  MapBodyWidget({Key? key}) : super(key: key);

  @override
  _MapBodyState createState() => _MapBodyState();
}

class _MapBodyState extends State<MapBodyWidget> {
  final ObjectTypeService _objectTypeService = ObjectTypeService();
  final InventarizationRepository _inventarizationRepository =
      InventarizationRepository();

  late ProjectListBloc _projectListBloc;
  final ProjectsRepository repository = ProjectsRepository();
  final DrawTypeRepository _drawTypeRepository = DrawTypeRepository();
  late final StorageRepository storageRepository;
  int GetDrawTypesUpdated = 0;
  int GetProjectListUpdated = 0;
  List<LatLng> checkedPosition = [];
  List<String> layerIds = [];
  bool show = false;
  List<LayerModel> layerModels = [];

  bool _centeringLocation = true;

  late AuthBloc authBloc;

  final MapController _mapController = MapController();

  Map<String, double> searchLayer = {};

  double _zoom = 13.0;

  bool _isStopAndGoStarted = false;

  StopGoSession? _session;

  final StreamController<double> _centeringController = StreamController();
  late LocationData _currentLocation;

  final List<LatLng> _userPoints = [];
  bool isApplication = false;
  Item? item;
  List<LatLng> objectGeometryCoords = [];
  List<LatLng> boundedBox = [];
  bool cacheUpdating = false;
  int cacheUpdateState = 2;

  void _initApplication() async {
    await StorageRepository.getInstance();

    Item? res = await _inventarizationRepository.get();

    if (res != null) {
      initObjectAndBBox(res);
      await StorageRepository.deleteString("ApplicationItem");
      StorageRepository.putString("ApplicationItem", jsonEncode(res.toJson()));
      setState(() {
        item = res;
      });
    } else {
      await StorageRepository.deleteString("ApplicationItem");
      setState(() {
        item = null;
      });
    }
  }

  void initObjectAndBBox(Item res) {
    Map<String, dynamic> objectCoords = jsonDecode(res.objectGeometry);

    for (List coord in objectCoords["geometries"][0]["coordinates"][0]) {
      objectGeometryCoords
          .add(GeoJsonCreator.createCoord(coord.cast<double>()));
      setState(() {});
    }

    Map<String, dynamic> boxCoords = jsonDecode(res.objectBbox);

    for (List coord in boxCoords["coordinates"][0]) {
      boundedBox.add(GeoJsonCreator.createCoord(coord.cast<double>()));
      setState(() {});
    }

    setState(() {
      isApplication = true;
      item = res;
    });
    _mapController.move(objectGeometryCoords[0], _zoom + 2);

    MyFunctions.showErrorMessage(
        errorMessage: "Вас назначили на выполнение заявки", context: context);
    _inventarizationRepository.changeStatus(res.id, "3");
  }

  @override
  void initState() {
    _initApplication();

    updateCache();

    StopAndGo.onNewPoints =
        (Iterable<LatLng> items) => setState(() => _userPoints.addAll(items));

    StopAndGo.onStart = (session) => setState(() {
          _userPoints.clear();
          _isStopAndGoStarted = true;
          _session = session;
        });

    StopAndGo.onSelected = (StopGoSession session) => setState(() {
          _userPoints.clear();

          _isStopAndGoStarted = false;
          _session = session;

          MapObjectService moService = MapObjectService();
          moService.findBySessionId(session.id).then((value) {
            value.map((e) => print("map object = " + e.toString()));
          });
        });

    StopAndGo.onEnd = (items) {
      setState(() {
        _userPoints.clear();
        _isStopAndGoStarted = false;
        _session = null;
      });
    };

    StopAndGo.onCancel = () {
      setState(() {
        _userPoints.clear();

        _isStopAndGoStarted = false;
        _session = null;
      });
    };

    StopAndGo.onNewObject = (object) {
      setState(() {
        _session?.objects.add(object);
      });
    };

    /*
    _drawTypeRepository.getAll().then((value) {
      return value;
    }).whenComplete(() => null);

     */

    if (Platform.isAndroid) {
      WebView.platform = SurfaceAndroidWebView();
    }

    authBloc = AuthBloc(repository: AuthenticationRepository());

    //_projectListBloc = ProjectListBloc(repository, Database())
    //  ..add(GetProjectListEvent());

    SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
      statusBarBrightness: Brightness.dark,
    ));

    super.initState();
  }

  @override
  void dispose() {
    _centeringController.close();
    super.dispose();
  }

  void checkCacheState() {
    if (GetDrawTypesUpdated != 0 && GetProjectListUpdated != 0) {
      if (GetDrawTypesUpdated == 2 && GetProjectListUpdated == 2) {
        cacheUpdating = false;

        print('check update cache');
        cacheUpdateState = 2;
      } else {
        print('check update cache 1' +
            GetDrawTypesUpdated.toString() +
            '--' +
            GetProjectListUpdated.toString());
        cacheUpdating = false;
        cacheUpdateState = 0;
      }
    }
  }

  Future<void> getArendatorCache() async {
    List<dynamic> typesArendator =
        await ArendatorRepository().getTypesArendator();
    for (var arendator in typesArendator) {
      //await ArendatorRepository().getDocumentStructureForArendator(arendatorId: arendator.id);
      await ArendatorRepository()
          .getDocumentsForArendator(arendatorId: arendator.id);
      await ArendatorRepository().getColumns(arendatorId: arendator.id);
      //await ArendatorRepository().getDocumentEditForArendator();
    }
  }

  void updateCache() async {
    setState(() {
      cacheUpdating = true;
    });

    _projectListBloc = ProjectListBloc(repository, Database())
      ..add(GetProjectListEvent());

    await ArendatorRepository().getTypesArendator();

    bool checkGetDrawTypesUpdated = await _objectTypeService.update();

    if (checkGetDrawTypesUpdated) {
      bool checkGetDrawTypesUpdated = await _objectTypeService.update();

      if (checkGetDrawTypesUpdated) {
        GetDrawTypesUpdated = 2;
      } else {
        GetDrawTypesUpdated = 1;
      }

      checkCacheState();

      await getArendatorCache();
    }
  }

  void _showUserPoints(List<Position> positions) {
    final positionsString =
        positions.map((e) => '${e.latitude}, ${e.longitude}').join('\n');
    showDialog<void>(
      context: context,
      builder: (BuildContext context) => AlertDialog(
        title: const Text('Список координат'),
        content: SizedBox(
          height: 360,
          width: 260,
          child: SingleChildScrollView(
            child: SelectableText(positionsString),
          ),
        ),
        actions: <Widget>[
          TextButton(
            onPressed: () {
              Navigator.pop(context);
            },
            child: const Text('OK'),
          ),
        ],
      ),
    );
  }

  // MARK: - ACTIONS

  Future<void> _goToMyLocation() async {
    _centeringController.add(_mapController.zoom);
  }

  Future<void> _zoomInDidTap() async {
    _zoom += 1;
    _animateCamera();
  }

  Future<void> _zoomOutDidTap() async {
    _zoom -= 1;
    _animateCamera();
  }

  Future<void> _animateCamera() async {
    _mapController.move(
        LatLng(_currentLocation.latitude!, _currentLocation.longitude!), _zoom);
  }

  // MARK: - FUNCTIONS

  void _animateTo(LatLng pos, String id) async {
    searchLayer = {};
    searchLayer[id] = 1;
    _centeringLocation = false;
    setState(() {});
    _mapController.move(pos, _zoom);
  }

  _getTasks(LatLng center) async {}

  showProjectsSheet() {
    showModalBottomSheet(
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(30), topRight: Radius.circular(30))),
        context: context,
        builder: (x) {
          return
              //  ProjectsBottomSheet()
              // EditiginBottomSheet()
              // InfoBottomSheet();
              // DetailBottomSheet();
              const PostBottomSheet();
        });
  }

  List<TileLayerOptions> _getWmsOverlayOptions(List<LayerModel> layers,
      Map<String, double> layerIdsMap, List<String> activeLayers) {
    print("ACTIVE LAYERS = $activeLayers");
    layerModels = layers;

    /// Get Test String
    String getTestString(List<Map<String, dynamic>> configs) {
      String testString = "";
      for (final config in configs) {
        if (config["nick"] != "layers") {
          testString += "${config["value"]};";
        }
      }
      return testString;
    }

    /// Transform config to Map
    Map<String, dynamic> configsListToMap(List<Map<String, dynamic>> configs) {
      Map<String, dynamic> res = {};
      for (final config in configs) {
        res[config["nick"]] = config["value"];
      }

      return res;
    }

    List<Map<String, dynamic>> layersGroup = [];
    Map<String, String> idToLayer = {};
    for (final layer in layers) {
      String testString = getTestString(layer.mapProjectLayerConfigs);
      var foundIndex = -1;
      for (int i = 0; i < layersGroup.length; i++) {
        if (layersGroup[i]["testString"] == testString &&
            layersGroup[i]["uri"] == layer.uri) {
          foundIndex = i;
          break;
        }
      }
      final configsMap = configsListToMap(layer.mapProjectLayerConfigs);
      idToLayer[layer.id] = configsMap["layers"];
      if (foundIndex == -1) {
        layersGroup.add(<String, dynamic>{
          'testString': testString,
          'layers': <String>[configsMap["layers"]],
          'uri': layer.uri,
          for (final mapEntry in configsMap.entries)
            if (mapEntry.key != 'layers') mapEntry.key: mapEntry.value,
        });
      } else {
        layersGroup[foundIndex]["layers"].add(configsMap['layers']);
      }
    }

    final List<TileLayerOptions> tileLayerOptions = [];

    for (final layerGroup in layersGroup) {
      List<String> _layerIds = [];
      List<double> opacities = [];
      for (String _layerId in layerIdsMap.keys) {
        _layerIds.add(idToLayer[_layerId]!);
        opacities.add(layerIdsMap[_layerId]!);
      }
      print("LAYERS IDS = $opacities");
      //print("LAYERS = $_layers");
      /*for (String _layerId in _layerIds) {
        tileLayerOptions.add(TileLayerOptions(
          opacity: layerIdsMap[_layerId]!,
          backgroundColor: Colors.transparent,
          wmsOptions: WMSTileLayerOptions(
            baseUrl: layerGroup['uri'] + "?",
            layers: [idToLayer[_layerId]!],
            format: layerGroup['format'],
            version: "1.12.2",
            transparent: layerGroup['transparent'],
            otherParameters: {
              for (final option in layerGroup.entries)
                if (!["layers", "format", "transparent", "uri"]
                    .contains(option.key))
                  option.key: option.value.toString(),
            },
          ),
        ));
      }*/

      tileLayerOptions.add(
        TileLayerOptions(
          opacity: opacities.isNotEmpty ? opacities[0] : 1,
          backgroundColor: Colors.transparent,
          //keepBuffer: 1,
          wmsOptions: WMSTileLayerOptions(
            baseUrl: layerGroup['uri'] + "?",
            layers: _layerIds,
            format: layerGroup['format'],
            version: "1.12.2",
            transparent: layerGroup['transparent'],
            otherParameters: {
              for (final option in layerGroup.entries)
                if (!["layers", "format", "transparent", "uri"]
                    .contains(option.key))
                  option.key: option.value.toString(),
            },
          ),
        ),
      );
    }
    return tileLayerOptions;
  }

  void _hideKeyboard() => FocusScope.of(context).requestFocus(FocusNode());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: GlobalInfo.mapKey,
      resizeToAvoidBottomInset: false,
      body: Consumer<GeneralProvider>(
        builder: (context, provider, _) {
          // Layer 1: Get the caching directory
          return FutureBuilder<CacheDirectory>(
            future: MapCachingManager.temporaryCache,
            builder: (context, cacheDir) {
              if (!cacheDir.hasData) {
                return LoadingPage();
              }
              // Layer 2: Get the shared preferences instance
              return FutureBuilder<SharedPreferences>(
                future: SharedPreferences.getInstance(),
                builder: (context, prefs) {
                  if (!prefs.hasData) {
                    return LoadingPage();
                  }

                  // Setup provider & default values
                  provider.parentDirectory ??= cacheDir.data!;
                  provider.persistent ??= prefs.data!;

                  provider.storeNameQuiet =
                      provider.persistent!.getString('lastUsedStore') ??
                          'Default Store';

                  // Layer 3: Wait for the map controller to be ready
                  return FutureBuilder<void>(
                    future: _mapController.onReady,
                    builder: (context, snapshot) {
                      return standartMap(provider);
                    },
                  );
                },
              );
            },
          );
        },
      ),
    );
  }

  Widget standartMap(GeneralProvider provider) {
    return BlocProvider.value(
      value: _projectListBloc,
      child: BlocBuilder<ProjectListBloc, ProjectListState>(
          builder: (context, state) {
        if (state.status == FormzStatus.submissionSuccess) {
          GetProjectListUpdated = 2;
          checkCacheState();
        } else if (state.status == FormzStatus.invalid) {
          GetProjectListUpdated = 1;
          checkCacheState();
        }
        print("COORDS = ${boundedBox}");

        List<Widget> children = <Widget>[
          Consumer<GeneralProvider>(
            builder: (context, provider, _) {
              final MapCachingManager mcm = MapCachingManager(
                  provider.parentDirectory!, provider.storeName);
              final String? source = provider.persistent!
                  .getString('${provider.storeName}: sourceURL');
              final String? cacheBehaviour = provider.persistent!
                  .getString('${provider.storeName}: cacheBehaviour');
              final int? validDuration = provider.persistent!
                  .getInt('${provider.storeName}: validDuration');
              final int? maxTiles = provider.persistent!
                  .getInt('${provider.storeName}: maxTiles');

              return FlutterMap(
                mapController: _mapController,
                options: MapOptions(
                  minZoom: 6,
                  maxZoom: 18.4,
                  zoom: 18,
                  interactiveFlags: _enabledInteractive,
                  onPositionChanged: (MapPosition position, bool hasGesture) {
                    if (hasGesture) {
                      show = false;
                      setState(() => _centeringLocation = false);
                    }
                  },
                  //w
                  center: isApplication
                      ? objectGeometryCoords[0]
                      : LatLng(59.94231, 30.328849),
                  onTap: (_, LatLng pos) async {
                    _hideKeyboard();
                    List<String> layerIds = [];
                    for (String _layerId in state.layerIds!.keys) {
                      layerIds.add(_layerId);
                    }
                    checkedPosition = [pos];

                    try {
                      setState(() {
                        show = true;
                      });
                      var res = await repository.getObjectsByCoordAndLayers(
                          pos: pos, layerIds: layerIds);
                      setState(() {
                        show = false;
                      });
                      MyFunctions.popBottomSheet(
                        ObjectInfoWidget(
                          pos: pos,
                          layerIds: layerIds,
                        ),
                        true,
                        context,
                        isObject: true,
                      );
                    } catch (e) {}

                    setState(() {
                      show = false;
                    });
                  },
                  plugins: [
                    LocationMarkerPlugin(
                      centerCurrentLocationStream: _centeringController.stream,
                      centerOnLocationUpdate:
                          (_centeringLocation && !isApplication) ||
                                  _isStopAndGoStarted
                              ? CenterOnLocationUpdate.always
                              : CenterOnLocationUpdate.never,
                    )
                  ],
                ),
                layers: [
                  TileLayerOptions(
                    urlTemplate:
                        "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
                    subdomains: ['a', 'b', 'c'],
                    tileProvider: provider.cachingEnabled
                        ? StorageCachingTileProvider.fromMapCachingManager(
                            mcm,
                            behavior: cacheBehaviour == 'cacheFirst'
                                ? CacheBehavior.cacheFirst
                                : cacheBehaviour == 'cacheOnly'
                                    ? CacheBehavior.cacheOnly
                                    : cacheBehaviour == 'onlineFirst'
                                        ? CacheBehavior.onlineFirst
                                        : CacheBehavior.cacheFirst,
                            cachedValidDuration:
                                Duration(days: validDuration ?? 16),
                            maxStoreLength: maxTiles ?? 20000,
                          )
                        : const NonCachingNetworkTileProvider(),
                    reset: provider.resetController.stream,
                  ),

                  // if (!isApplication)
                  //   ..._getWmsOverlayOptions(state.layerModels, state.layerIds!, [])
                  // else
                  //   ..._getWmsOverlayOptions(
                  //       state.layerModels, state.layerIds!, item!.layersId),
                  ..._getWmsOverlayOptions(state.layerModels, state.layerIds!,
                      item == null ? [] : item!.layersId),
                  ..._getWmsOverlayOptions(state.layerModels, searchLayer, []),

                  ///stop and go path
                  PolylineLayerOptions(
                    polylines: [
                      Polyline(
                        points: item != null ? objectGeometryCoords : [],
                        strokeWidth: 5,
                        color: Colors.purple,
                      ),
                      /*Polyline(
                        points: boundedBox,
                        strokeWidth: 5,
                        borderColor: Colors.red[300]!.withOpacity(0.5),
                        color: Colors.red,
                      ),*/
                      Polyline(
                        points: _userPoints,
                        strokeWidth: 8,
                        color: Colors.red,
                      ),
                    ],
                  ),
                  LocationMarkerLayerOptions(
                    marker: const DefaultLocationMarker(
                      color: Color(0xFF2B61A3),
                    ),
                    markerSize: const Size(14, 14),
                    accuracyCircleColor: const Color(0x4D2B61A3),
                    showHeadingSector: false,
                  ),
                  MarkerLayerOptions(
                      rotate: true,
                      markers: _session?.objects
                              .map((e) => MarkerHelper.createObjectMarker(
                                    context: context,
                                    id: e.id,
                                    position: e.position,
                                  ))
                              .toList() ??
                          _markers),
                ],
              );
            },
          ),
          _createCashAndLoadBtn(
            context,
            provider,
            state.layerModels,
            state.layerIds!,
            item == null ? [] : item!.layersId,
          ),
          Positioned.fill(
              child: Listener(
            onPointerDown: (_) {
              // FocusManager.instance.primaryFocus!.unfocus();
            },
            behavior: HitTestBehavior.translucent,
          )),
          _createTopWidget(context),
        ];

        var widget = _createBottomWidget(context);
        if (widget != null) {
          children.add(widget);
        }

        return Stack(
          children: children,
        );
      }),
    );
  }

  List<Marker> get _markers => checkedPosition
      .map(
        (markerPosition) => Marker(
          point: markerPosition,
          width: 100,
          height: 100,
          builder: (_) {
            if (show) {
              return Container(
                //margin: EdgeInsets.only(left: 20),
                child: LoadingPage(),
              );
            } else {
              return Container();
            }
          },
          anchorPos: AnchorPos.align(AnchorAlign.center),
        ),
      )
      .toList();

  void _historyShow(BuildContext context) {
    BottomSheetHelper.show(SessionsViewerWidget(), context);
  }

  showLoaderDialog(BuildContext context) {
    AlertDialog alert = AlertDialog(
      content: Row(
        children: [
          const CircularProgressIndicator(
            color: mainColor,
          ),
          Container(
              margin: const EdgeInsets.only(left: 7),
              child: const Text("Loading...")),
        ],
      ),
    );
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  void _loadSession(StopGoSession session) async {
    var repository = StopGoSessionRepository();
    MyFunctions.showLoaderDialog(context);
    var result = await repository.newSend(session);
    Navigator.pop(context);
    if (result < 200 || result > 300) {
      showDialog(
        context: context,
        builder: (BuildContext context) => ErrorDialog(
          title: "Ошибка",
          description: result == 501
              ? "У вас нет соединенения повторите позже"
              : "Что то пошло не так!",
          buttonText: "Okay",
        ),
      );
    } else {
      showDialog(
        context: context,
        builder: (BuildContext context) => const ErrorDialog(
          title: "Успешно",
          description: "Ваша инвентаризация успешно отправлена",
          buttonText: "Okay",
        ),
      );
    }

    print(result);
  }

  Widget? _createBottomWidget(BuildContext context) {
    Widget? widget;

    if (_session != null) {
      //done or sended
      if (_session!.status != SessionStatus.inProgress) {
        widget = bottomBtnsCreate(_goToMyLocation, _zoomInDidTap,
            _zoomOutDidTap, _zoomOutDidTap, showProjectsSheet, context,
            activeLayers: [],
            cacheUpdating: cacheUpdating,
            cacheUpdateState: cacheUpdateState,
            updateCacheAction: updateCache);
        return Align(
          alignment: Alignment.bottomCenter,
          child: widget,
        );
      } else {
        widget = inventoryBtnCreate(context, _isStopAndGoStarted);
      }
    } else {
      if (isApplication) {
        widget = bottomBtnsCreate(_goToMyLocation, _zoomInDidTap,
            _zoomOutDidTap, _zoomOutDidTap, showProjectsSheet, context,
            activeLayers: item!.layersId,
            cacheUpdating: cacheUpdating,
            cacheUpdateState: cacheUpdateState,
            updateCacheAction: updateCache);
      } else {
        widget = bottomBtnsCreate(_goToMyLocation, _zoomInDidTap,
            _zoomOutDidTap, _zoomOutDidTap, showProjectsSheet, context,
            activeLayers: [],
            cacheUpdating: cacheUpdating,
            cacheUpdateState: cacheUpdateState,
            updateCacheAction: updateCache);
      }
    }

    return Align(
      alignment: Alignment.bottomCenter,
      child: widget,
    );
  }

  Widget _createTopWidget(BuildContext context) {
    return _session != null
        ? StopAndGoBar(
            layerModels: layerModels,
            callback: _animateTo,
          )
        : BlocProvider(
            create: (BuildContext context) =>
                SearchCubit(repository: SearchRepository()),
            child: Positioned(
              top: 60,
              left: 16,
              right: 16,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(40),
                child: BackdropFilter(
                  filter: ImageFilter.blur(sigmaX: 12, sigmaY: 12),
                  child:
                      SearchBar(layerModels: layerModels, callback: _animateTo),
                ),
              ),
            ),
          );
  }

  void objectTap(String objectId) {}

  Widget _createCashAndLoadBtn(
    BuildContext context,
    GeneralProvider provider,
    List<LayerModel> layers,
    Map<String, double> layerIdsMap,
    List<String> activeLayers,
  ) {
    return Align(
      alignment: Alignment.centerRight,
      child: Padding(
          padding: const EdgeInsets.only(right: 15.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.only(bottom: 4.0),
                child: RoundedBtn(
                  onTap: () => _session != null &&
                          _session!.status != SessionStatus.inProgress
                      ? _loadSession(_session!)
                      : _historyShow(context),
                  asset:
                      _session != null && _session!.status == SessionStatus.done
                          ? AppIcons.load
                          : AppIcons.plus,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 4.0, bottom: 4),
                child: RoundedBtn(
                  onTap: () {
                    final MapCachingManager mcm = MapCachingManager(
                        provider.parentDirectory!, provider.storeName);
                    Navigator.popAndPushNamed(
                      context,
                      '/bulkDownloader',
                      arguments: [
                        mcm,
                        layers,
                        layerIdsMap,
                        activeLayers,
                        objectGeometryCoords,
                      ],
                    );
                  },
                  child: Icon(Icons.map, color: mainColor),
                ),
              ),
              isApplication
                  ? Padding(
                      padding: const EdgeInsets.only(top: 4.0, bottom: 4),
                      child: RoundedBtn(
                        onTap: () {
                          _inventarizationRepository.changeStatus(
                              item!.id, "4");
                          setState(() {
                            item = null;
                            isApplication = false;
                          });
                        },
                        child: Icon(Icons.done_all, color: mainColor),
                      ),
                    )
                  : Padding(
                      padding: const EdgeInsets.only(top: 4.0, bottom: 4),
                      child: Container(
                        width: 1,
                      ),
                    ),
            ],
          )
          //RoundedBtn(onTap: () {}, asset: AppIcons.location),
          ),
    );
  }
}

Future<Uint8List> getBytesFromAsset(String path, int width) async {
  ByteData data = await rootBundle.load(path);
  ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(),
      targetWidth: width);
  ui.FrameInfo fi = await codec.getNextFrame();
  return (await fi.image.toByteData(format: ui.ImageByteFormat.png))!
      .buffer
      .asUint8List();
}
