import 'package:electro/models/columns/directory.dart';
import 'package:electro/models/columns/layer.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:formz/formz.dart';

class ProjectListState extends Equatable {
  final FormzStatus status;
  final List<DirectoryResponseDto> projectList;
  final List<LayerModel> layerModels;
  final List<Marker> markers;
  final Marker? selectedMarker;
  final Map<String, double>? layerIds;

  const ProjectListState({
    this.selectedMarker,
    required this.projectList,
    required this.layerModels,
    required this.status,
    required this.markers,
    this.layerIds,
  });

  ProjectListState copyWith({
    FormzStatus? status,
    Marker? selectedMarker,
    List<Marker>? markers,
    List<DirectoryResponseDto>? projectList,
    List<LayerModel>? layerModels,
    Map<String, double>? layerIds,
  }) {
    return ProjectListState(
      markers: markers ?? this.markers,
      selectedMarker: selectedMarker ?? this.selectedMarker,
      projectList: projectList ?? this.projectList,
      status: status ?? this.status,
      layerModels: layerModels ?? this.layerModels,
      layerIds: layerIds ?? this.layerIds,
    );
  }

  @override
  List<Object?> get props => [status, projectList, layerModels, layerIds];
}
