import 'package:electro/app/constants/colors.dart';
import 'package:electro/bloc/arendator/arendator_cubit.dart';
import 'package:electro/bloc/authentication/auth_bloc.dart';
import 'package:electro/bloc/object/object_cubit.dart';
import 'package:electro/bloc/project-list/projects_bloc.dart';
import 'package:electro/features/account/auth/logout_bottomsheet.dart';
import 'package:electro/features/draw_type/object/edit_text_with_header.dart';
import 'package:electro/repository/arendator.dart';
import 'package:electro/repository/auth.dart';
import 'package:electro/repository/file.dart';
import 'package:electro/repository/projects.dart';
import 'package:electro/widgets/cancel_stopgo_bottomsheet.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class MyFunctions {
  static popBottomSheet(
    Widget bottomSheet,
    bool isDynamic,
    BuildContext context, {
    bool? isProjectList,
    bool? isRenter,
    bool? isObject,
    Color? color,
  }) {
    if (isDynamic) {
      return showModalBottomSheet(
          backgroundColor: color,
          isScrollControlled: true,
          shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(30), topRight: Radius.circular(30))),
          context: context,
          builder: (x) {
            return
                // ProjectsBottomSheet()
                // EditiginBottomSheet()
                // InfoBottomSheet();
                // DetailBottomSheet();
                isProjectList != null && isProjectList
                    ? BlocProvider.value(
                        value: BlocProvider.of<ProjectListBloc>(context),
                        child: bottomSheet)
                    : isRenter != null && isRenter
                        ? BlocProvider(
                            create: (BuildContext context) => ArendatorCubit(
                              repository: ArendatorRepository(),
                              fileRepository: FileRepository(),
                            ),
                            child: bottomSheet,
                          )
                        : isObject != null && isObject
                            ? BlocProvider(
                                create: (BuildContext context) => ObjectCubit(
                                    repository: ProjectsRepository(),
                                    fileRepository: FileRepository()),
                                child: bottomSheet,
                              )
                            : bottomSheet;
          });
    } else {
      return showModalBottomSheet(
          shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(30), topRight: Radius.circular(30))),
          context: context,
          builder: (x) {
            return bottomSheet;
          });
    }
  }

  static showLogOut({
    String? title = 'Вы действительно хотите выйти?',
    required BuildContext context,
  }) {
    showModalBottomSheet(
        isScrollControlled: true,
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(30), topRight: Radius.circular(30))),
        isDismissible: false,
        enableDrag: false,
        context: context,
        builder: (x) {
          return BlocProvider(
              create: (c) => AuthBloc(repository: AuthenticationRepository()),
              child: LogOutBottomSheet(
                title: title!,
                parentContext: context,
              ));
        });
  }

  static showErrorMessage({
    required String errorMessage,
    required BuildContext context,
  }) {
    showModalBottomSheet(
      isScrollControlled: true,
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(30), topRight: Radius.circular(30))),
      context: context,
      builder: (x) {
        return ErrorBottomSheet(parentContext: context, title: errorMessage);
      },
    );
  }

  static showSelector({
    required String title,
    required BuildContext context,
    required List<String> values,
    required Function callback,
  }) {
    showModalBottomSheet(
      isScrollControlled: true,
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(30), topRight: Radius.circular(30))),
      context: context,
      builder: (x) {
        return SelectorSheet(
          title: title,
          values: values,
          callback: callback,
        );
      },
    );
  }

  static showStopAndGoCancel({
    required BuildContext context,
  }) {
    showModalBottomSheet(
        isScrollControlled: true,
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(30), topRight: Radius.circular(30))),
        context: context,
        builder: (x) {
          return BlocProvider(
              create: (c) => AuthBloc(repository: AuthenticationRepository()),
              child: CancelStopGoBottomSheet(
                parentContext: context,
              ));
        });
  }

  static showLoaderDialog(BuildContext context) {
    AlertDialog alert = AlertDialog(
      content: new Row(
        children: [
          CircularProgressIndicator(
            color: mainColor,
          ),
          Container(
              margin: EdgeInsets.only(left: 7), child: Text("Loading...")),
        ],
      ),
    );
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
