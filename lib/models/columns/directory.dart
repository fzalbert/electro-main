import 'package:electro/models/columns/layer_directory.dart';
import 'package:electro/models/columns/project_directory.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:electro/models/columns/center.dart';

part 'directory.g.dart';

@JsonSerializable()
class DirectoryResponseDto {
  @JsonKey(defaultValue: '', name: 'Id')
  final String id;

  @JsonKey(defaultValue: '', name: 'Name')
  final String name;

  @JsonKey(defaultValue: '', name: 'CreatedBy')
  final String createdBy;

  @JsonKey(defaultValue: '', name: 'TimeCreated')
  final String timeCreated;

  @JsonKey(defaultValue: '', name: 'Center')
  final String center;

  @JsonKey(defaultValue: 0, name: 'ZoomLevel')
  final int zoomLevel;

  @JsonKey(name: 'Activated')
  final bool activated;

  @JsonKey(defaultValue: 0, name: 'OrderNumber')
  final int orderNumber;

  @JsonKey(defaultValue: '', name: 'ModuleId')
  final String moduleId;

  @JsonKey(defaultValue: [], name: 'ChildrenDirectories')
  final List<ProjectDirectoryResponseDto> mapProjectDirectories;

  @JsonKey(defaultValue: [], name: 'Layers')
  final List<LayerDirectoryModel> mapProjectLayers;

  DirectoryResponseDto({
    required this.activated,
    required this.center,
    required this.createdBy,
    required this.id,
    required this.mapProjectDirectories,
    required this.moduleId,
    required this.name,
    required this.orderNumber,
    required this.timeCreated,
    required this.zoomLevel,
    required this.mapProjectLayers,
  });
  static DirectoryResponseDto empty() => DirectoryResponseDto(
        activated: false,
        center: '',
        createdBy: '',
        id: '',
        mapProjectDirectories: [],
        moduleId: '',
        name: '',
        orderNumber: 0,
        timeCreated: '',
        zoomLevel: 0,
        mapProjectLayers: [],
      );

  factory DirectoryResponseDto.fromJson(Map<String, dynamic> json) =>
      _$DirectoryResponseDtoFromJson(json);
}
