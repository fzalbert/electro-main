class AppIcons {
  static const search = 'assets/icons/projects/search.svg';
  static const sync = 'assets/icons/sync_icon.svg';
  static const cancel = 'assets/icons/projects/cancel.svg';
  static const eyeOff = 'assets/icons/projects/eye_off.svg';
  static const eyeOn = 'assets/icons/projects/eye_on.svg';
  static const location = 'assets/icons/projects/location.svg';
  static const pen = 'assets/icons/projects/pen.svg';
  static const videoCamera = 'assets/icons/projects/video_camera.svg';
  static const camera = 'assets/icons/projects/camera.svg';
  static const arrowUp = 'assets/icons/projects/arrow_up.svg';
  static const arrowDown = 'assets/icons/projects/arrow_down.svg';
  static const filter = 'assets/icons/projects/filter.svg';
  static const arrowRight = 'assets/icons/projects/arrow_right.svg';
  static const stroke = 'assets/icons/projects/stroke.svg';
  static const pin = 'assets/icons/projects/pin.svg';
  static const stopAndGo = 'assets/icons/ic_stop_and_go.svg';

  static const plus = 'assets/icons/ic_plus.png';
  static const load = 'assets/icons/ic_load.png';
}
