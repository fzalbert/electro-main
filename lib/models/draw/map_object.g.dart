// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'map_object.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MapObject _$MapObjectFromJson(Map<String, dynamic> json) => MapObject(
      token: json['Token'] as String?,
      directoryId: json['DirectoryId'] as String,
      groupId: json['GroupId'] as String,
      mapObjectData: (json['MapObjectData'] as List<dynamic>)
          .map((e) => MapObjectData.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$MapObjectToJson(MapObject instance) => <String, dynamic>{
      'Token': instance.token,
      'DirectoryId': instance.directoryId,
      'GroupId': instance.groupId,
      'MapObjectData': instance.mapObjectData,
    };
