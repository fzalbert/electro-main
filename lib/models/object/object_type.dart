import 'package:electro/data/locale/entities/object_type.dart';
import 'package:json_annotation/json_annotation.dart';

part 'object_type.g.dart';

///response
@JsonSerializable()
class ObjectTypeResponseDto extends ObjectType {
  ObjectTypeResponseDto({
    required this.id,
    required this.mapObjectTypeParentId,
    required this.name,
    required this.isCanDraw,
    required this.drawDirectoryId,
    required this.mplId,
    required this.childrenTypes,
  });

  @JsonKey(name: 'Id')
  String id;
  @JsonKey(name: 'MapObjectTypeParentId')
  String mapObjectTypeParentId;
  @JsonKey(name: 'Name')
  String name;
  @JsonKey(name: 'IsCanDraw')
  bool isCanDraw;
  @JsonKey(name: 'DrawDirectoryId')
  String drawDirectoryId;
  @JsonKey(name: 'MplId')
  String mplId;
  @JsonKey(name: 'ChildrenTypes')
  List<ObjectTypeResponseDto> childrenTypes;

  factory ObjectTypeResponseDto.fromJson(Map<String, dynamic> json) =>
      _$ObjectTypeResponseDtoFromJson(json);

  Map<String,dynamic> toJson() => _$ObjectTypeResponseDtoToJson(this);

  @override
  Map<String, dynamic> getExtraData() {
    return {
      "id": id
    };
  }

  @override
  String getId() {
    return id;
  }

  @override
  String getParentId() {
    return drawDirectoryId;
  }

  @override
  String getTitle() {
    return name;
  }
}