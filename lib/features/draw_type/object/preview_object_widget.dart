import 'dart:io';

import 'package:electro/app/constants/app_icons.dart';
import 'package:electro/data/locale/app_database.dart';
import 'package:electro/data/locale/entities/file.dart';
import 'package:electro/data/locale/entities/map_object_value.dart';
import 'package:electro/features/crete-user/fields/image_upload.dart';
import 'package:electro/features/draw_type/object/save_btn.dart';
import 'package:electro/features/projects/components/photo_placeholder.dart';
import 'package:electro/features/sessions/service/object_service.dart';
import 'package:electro/repository/draw_type_form.dart';
import 'package:electro/utils/bottom_sheet_helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/svg.dart';
import 'package:mime/mime.dart';
import 'edit_text_with_header.dart';
import 'loader_widget.dart';

class PreviewObjectWidget extends StatefulWidget implements ScrollableWidget {
  @override
  State<StatefulWidget> createState() => _PreviewObjectState();

  final String objectId;
  ScrollController? scrollController;
  bool isEditable;

  PreviewObjectWidget({
    Key? key,
    required this.objectId,
    this.isEditable = false,
    this.scrollController,
  }) : super(key: key);

  @override
  void setScrollController(ScrollController controller) {
    scrollController = controller;
  }
}

class _PreviewObjectState extends State<PreviewObjectWidget> {
  final MapObjectService _service = MapObjectService();

  List<MapObjectValueModel> parameters = <MapObjectValueModel>[];

  bool _isLoading = true;
  bool _isEditable = false;
  List<File> allFiles = [];

  ///parameterId, controller
  Map<String, TextEditingController> editingControls =
      <String, TextEditingController>{};

  @override
  void initState() {
    _isLoading = true;
    _isEditable = widget.isEditable;

    setState(
      () {
        allFiles = [];
      },
    );
    updateData();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.7,
      child: _isLoading
          ? const LoaderWidget()
          : Padding(
              padding: MediaQuery.of(context).viewInsets,
              child: Container(
                padding: const EdgeInsets.only(top: 8.0),
                child: ListView.builder(
                  controller: widget.scrollController,
                  itemCount: parameters.length + 1,
                  itemBuilder: (context, index) {
                    if (index == 0) {
                      return createHeader();
                    } else {
                      var item = parameters[index - 1];
                      return _createItem(item);
                    }
                  },
                ),
              ),
            ),
    );
  }

  Widget createSaveBtn(BuildContext context) {
    return SaveBtn(action: () => _saveObject());
  }

  Widget withHeader(MapObjectValueModel parameter) {
    var _controller = editingControls[parameter.parameterId]!;
    return EditTextWithHeader(
      controller: _controller,
      type: parameter.parameterType == "select"
          ? "textarea"
          : parameter.parameterType,
      value: parameter.value,
      title: parameter.title,
      isEditable: _isEditable,
    );
  }

  Widget createHeader() {
    return Padding(
      padding: const EdgeInsets.only(left: 16.0, right: 16.0, top: 12.0),
      child: Row(
        children: [
          const Text("Информация"),
          const Spacer(),
          IconButton(
            icon: SvgPicture.asset(AppIcons.pen),
            iconSize: 30,
            onPressed: () {
              changeEditingStatus();
            },
          )
        ],
      ),
    );
  }

  void updateImageController(List<File> files) async {
    allFiles = [];
    for (File file in files) {
      allFiles.add(file);
    }
  }

  Widget _createItem(MapObjectValueModel item) {
    switch (item.parameterType) {
      case "Btn":
        return createSaveBtn(context);
      case "ImagePicker":
        return Padding(
          padding: const EdgeInsets.only(left: 16.0, right: 16.0, top: 12.0),
          child: ImageUploadItem(
            files: allFiles,
            loadImages: updateImageController,
          ),
        );
      case "Images":
        if (_isEditable) {
          return Container();
        } else {
          return GridView.builder(
              shrinkWrap: true,
              itemCount: allFiles.length,
              physics: const NeverScrollableScrollPhysics(),
              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 3,
                  crossAxisSpacing: 20,
                  mainAxisSpacing: 10,
                  childAspectRatio: 103 / 103),
              itemBuilder: (context, index) {
                return PhotoPlaceholder(
                  child: lookupMimeType(allFiles[index].path.toString())!
                          .startsWith('image/')
                      ? Image.file(allFiles[index])
                      : Text(allFiles[index].path.toString().split("/").last),
                  isUploaded: true,
                );
              });
        }
      default:
        return GestureDetector(
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 16),
            child: withHeader(item),
          ),
          onTap: () {},
        );
    }
  }

  void changeEditingStatus() {
    if (_isEditable) {
      BottomSheetHelper.showSelectable(context,
          text:
              "Изменения будут отменены. Вы уверены что хотите прекратить редактировние?",
          approve: () {
        _isEditable = false;
        allFiles = [];
        updateData();
      });

      // parameters.removeWhere((element) => element.id == "id");
    } else {
      setState(() {
        parameters.add(MapObjectValueModel(
            id: "id",
            parameterType: "ImagePicker",
            value: '',
            title: '',
            parameterId: ''));

        parameters.add(MapObjectValueModel(
            id: "id",
            parameterType: "Btn",
            value: '',
            title: '',
            parameterId: ''));

        _isEditable = true;
      });
    }
  }

  Future<AppDatabase> _getDatabase() async {
    return await DatabaseBuilder().create();
  }

  void updateData() async {
    setState(() {
      parameters.clear();
    });

    try {
      var objectService = MapObjectService();

      var result = await objectService.findById(widget.objectId);
      var bd = await _getDatabase();
      var files = await bd.file.findByMapObjectId(widget.objectId);

      for (var file in files) {
        if (file.uri != null) allFiles.add(File(file.uri!));
      }

      for (var value in result.values) {
        editingControls[value.parameterId] = TextEditingController();
        editingControls[value.parameterId]!.text = value.value;
        print("value = ${value.value}");
      }

      setState(() {
        _isLoading = false;
        parameters.addAll(result.values);
        parameters.add(
          MapObjectValueModel(
            id: "id",
            parameterType: "Images",
            value: '',
            title: '',
            parameterId: '',
          ),
        );
      });
    } catch (e) {
      setState(() {
        _isLoading = false;
      });
      print(e.toString());
    }
  }

  void _saveObject() async {
    var clearParameters = parameters.where((element) => element.id != "id");
    for (final parameter in clearParameters) {
      parameter.value = editingControls[parameter.parameterId]!.value.text;
    }

    _service.update(
      objectId: widget.objectId,
      values: clearParameters.toList(),
    );

    Navigator.of(context).pop();
  }
}
