part of 'search_cubit.dart';

abstract class SearchState extends Equatable {}

class InitialSearchState extends SearchState {
  @override
  List<Object> get props => [];
}

class SearchingState extends SearchState {
  @override
  List<Object> get props => [];
}

class LoadedSearchState extends SearchState {
  LoadedSearchState(this.items);

  final List<SearchItem> items;

  @override
  List<Object> get props => [items];
}

class ErrorSearching extends SearchState {
  ErrorSearching(this.errorText);
  final String errorText;

  @override
  List<Object> get props => [errorText];
}
