import 'package:json_annotation/json_annotation.dart';

part 'layer_directory.g.dart';

@JsonSerializable()
class LayerDirectoryModel {
  @JsonKey(defaultValue: '',name: 'Id')
  final String id;

  @JsonKey(defaultValue: '',name: 'MapProjectId')
  final String mapProjectId;

  @JsonKey(defaultValue: '',name: 'MapProjectDirectoryId')
  final String mapProjectDirectoryId;

  @JsonKey(defaultValue: '',name: 'Name')
  final String name;

  @JsonKey(defaultValue: '',name: 'Uri')
  final String uri;

  @JsonKey(defaultValue: '',name: 'Protocol')
  final String protocol;

  @JsonKey(name: 'EnabledDefault')
  final bool enabledDefault;

  @JsonKey(name: 'Activated')
  final bool activated;

  @JsonKey(defaultValue: '',name: 'CreatedBy')
  final String createdBy;

  @JsonKey(defaultValue: 0,name: 'OrderNumber')
  final int orderNumber;

  @JsonKey(defaultValue: '',name: 'LegendLink')
  final String legendLink;

  @JsonKey(defaultValue: '',name: 'TimeCreated')
  final String timeCreated;

  @JsonKey(defaultValue: '',name: 'GeoserverLayerId')
  final String geoserverLayerId;

  @JsonKey(defaultValue: '',name: 'ModuleId')
  final String moduleId;

  @JsonKey(defaultValue: '',name: 'CqlFilter')
  final String cqlFilter;

  @JsonKey(defaultValue: 0,name: 'DataSearchMode')
  final int dataSearchMode;

  @JsonKey(name: 'ShowInfoBlock')
  final bool showInfoBlock;

  // final List mapProjectLayerConfigs
  LayerDirectoryModel(
      {required this.activated,
      required this.cqlFilter,
      required this.createdBy,
      required this.dataSearchMode,
      required this.enabledDefault,
      required this.geoserverLayerId,
      required this.id,
      required this.legendLink,
      required this.mapProjectDirectoryId,
      required this.mapProjectId,
      required this.moduleId,
      required this.name,
      required this.orderNumber,
      required this.protocol,
      required this.showInfoBlock,
      required this.timeCreated,
      required this.uri});

  factory LayerDirectoryModel.fromJson(Map<String, dynamic> json) =>
      _$LayerDirectoryModelFromJson(json);
}
