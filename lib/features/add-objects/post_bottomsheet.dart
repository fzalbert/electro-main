import 'package:electro/data/utils/functions.dart';
import 'package:electro/features/add-objects/components/info_checkbox.dart';
import 'package:electro/features/add-objects/components/rounded_checkbox.dart';
import 'package:electro/features/projects/components/editing_textfield.dart';
import 'package:electro/features/projects/editing_bottomsheet.dart';
import 'package:electro/features/projects/info_bottomsheet.dart';
import 'package:electro/app/constants/app_icons.dart';
import 'package:electro/app/theme/theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class PostBottomSheet extends StatelessWidget {
  const PostBottomSheet({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.9,
      padding: const EdgeInsets.fromLTRB(16, 44, 16, 0),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Text(
                  'Столб',
                  style: Theme.of(context).textTheme.headline3!.copyWith(
                        fontSize: 20,
                      ),
                ),
                const Spacer(),
                GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                      MyFunctions.popBottomSheet(
                          EditingBottomSheet(isEditing: true), true, context);
                    },
                    child: SvgPicture.asset(AppIcons.pen))
              ],
            ),
            const SizedBox(
              height: 28,
            ),
            Text(
              'Класс напряжения: 35/10',
              style: Theme.of(context).textTheme.headline4!.copyWith(
                    fontSize: 16,
                  ),
            ),
            const SizedBox(
              height: 20,
            ),
            const Text(
                'Максимальная мощность, разрешенная для технологического присоединения: 0.92 [МВА]Максимальная мощность, разрешенная для технологического присоединения: 0.92 [МВА]Максимальная мощность, разрешенная для технологического присоединения: 0.92'),
            const SizedBox(height: 28),
            Text(
              'Крепеж к столбу',
              style: Theme.of(context)
                  .textTheme
                  .headline3!
                  .copyWith(fontSize: 12, color: AppTheme.textGreyDark),
            ),
            const SizedBox(
              height: 8,
            ),
            Row(
              children: [
                Expanded(
                    child: EditingTextField(
                  controller: TextEditingController(),
                )),
              ],
            ),
            const SizedBox(
              height: 10,
            ),
            Row(
              children: [
                InfoCheckBox(),
                GestureDetector(
                  onTap: () {
                    Navigator.of(context).pop();
                    MyFunctions.popBottomSheet(
                        InfoBottomSheet(), true, context);
                  },
                  child: Text(
                    'Информация',
                    style: Theme.of(context)
                        .textTheme
                        .headline4!
                        .copyWith(fontSize: 12, color: AppTheme.textGreyDark),
                  ),
                )
              ],
            ),
            const SizedBox(
              height: 28,
            ),
            Text(
              'Максимальная мощность, разрешенная для технологического присоединения: 0.92 [МВА] ',
              style: Theme.of(context).textTheme.headline4!.copyWith(
                    fontSize: 16,
                  ),
            ),
            const SizedBox(height: 28),
            Row(
              children: [
                Wrap(
                  direction: Axis.vertical,
                  children: [
                    Text(
                      'Максимальная мощность',
                      style: Theme.of(context).textTheme.headline4!.copyWith(
                            fontSize: 14,
                          ),
                    ),
                    Text(
                      '220v',
                      style: Theme.of(context)
                          .textTheme
                          .headline4!
                          .copyWith(fontSize: 14, color: AppTheme.textGreyDark),
                    ),
                  ],
                ),
                const Spacer(),
                RoundedCheckBox()
              ],
            ),
            Row(
              children: [
                Wrap(
                  direction: Axis.vertical,
                  children: [
                    Text(
                      'Технологическое присоединение',
                      style: Theme.of(context).textTheme.headline4!.copyWith(
                            fontSize: 14,
                          ),
                    ),
                  ],
                ),
                const Spacer(),
                RoundedCheckBox()
              ],
            ),
            const SizedBox(height: 28),
            Text(
              'Крепеж к столбу',
              style: Theme.of(context)
                  .textTheme
                  .headline3!
                  .copyWith(fontSize: 12, color: AppTheme.textGreyDark),
            ),
            const SizedBox(
              height: 8,
            ),
            Row(
              children: [
                Expanded(
                    child: EditingTextField(
                  controller: TextEditingController(),
                )),
              ],
            ),
            const SizedBox(
              height: 10,
            ),
            Row(
              children: [
                InfoCheckBox(),
                Text(
                  'Информация',
                  style: Theme.of(context)
                      .textTheme
                      .headline4!
                      .copyWith(fontSize: 12, color: AppTheme.textGreyDark),
                )
              ],
            ),
            const SizedBox(
              height: 28,
            ),
            Text(
              'Наименование',
              style: Theme.of(context)
                  .textTheme
                  .headline3!
                  .copyWith(fontSize: 12, color: AppTheme.textGreyDark),
            ),
            const SizedBox(
              height: 8,
            ),
            Row(
              children: [
                Expanded(
                    child:
                        EditingTextField(controller: TextEditingController()))
              ],
            ),
            const SizedBox(
              height: 4,
            ),
            Text(
              'Текст',
              style: Theme.of(context)
                  .textTheme
                  .headline4!
                  .copyWith(fontSize: 12, color: AppTheme.textGreyDark),
            ),
            const SizedBox(
              height: 20,
            ),
            Row(
              children: [
                InfoCheckBox(),
                const SizedBox(
                  width: 10,
                ),
                Text(
                  'Информация',
                  style: Theme.of(context)
                      .textTheme
                      .headline4!
                      .copyWith(fontSize: 12, color: AppTheme.textGreyDark),
                )
              ],
            ),
            const SizedBox(
              height: 35,
            )
          ],
        ),
      ),
    );
  }
}
