import 'package:floor/floor.dart';

import 'map_object.dart';

@Entity(tableName: "map_object_value", foreignKeys: [
  ForeignKey(
    childColumns: ["mapObjectId"],
    parentColumns: ["id"],
    entity: MapObjectEntity,
    onDelete: ForeignKeyAction.cascade,
  )
])
class MapObjectValueEntity {
  @PrimaryKey()
  final String id;

  final String mapObjectId;

  final String parameterId;
  final String value;
  final String title;
  final String parameterType;

  MapObjectValueEntity({
    required this.id,
    required this.parameterId,
    required this.mapObjectId,
    required this.value,
    required this.title,
    required this.parameterType,
  });
}

class MapObjectValueModel {
  late final String id;
  late final String parameterId;
  late String value;
  late final String title;
  late final String parameterType;

  MapObjectValueModel({
    required this.id,
    required this.parameterId,
    required this.value,
    required this.title,
    required this.parameterType,
  });

  MapObjectValueModel.fromEntity(MapObjectValueEntity entity) {
    id = entity.id;
    parameterId = entity.parameterId;
    parameterType = entity.parameterType;
    title = entity.title;
    value = entity.value;
  }
}
