import 'package:electro/app/theme/theme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ProjectsSlider extends StatefulWidget {
  final double? currentValue;
  final Function? callback;
  ProjectsSlider({Key? key, this.currentValue, this.callback})
      : super(key: key);

  @override
  _ProjectsSliderState createState() => _ProjectsSliderState();
}

class _ProjectsSliderState extends State<ProjectsSlider> {
  double currentValue = 1.0;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (widget.currentValue != null) currentValue = widget.currentValue!;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            Expanded(
              child: CupertinoSlider(
                thumbColor: AppTheme.regularTextDarkBlue,
                activeColor: AppTheme.regularTextDarkBlue,
                min: 0,
                max: 100,
                value: currentValue * 100,
                onChanged: (value) {
                  if (widget.callback != null) widget.callback!(value / 100);
                  setState(() {
                    currentValue = value / 100;
                  });
                },
              ),
            ),
          ],
        ),
        const SizedBox(
          height: 2,
        ),
        Row(
          children: [
            Text('Непрозрачность',
                style: Theme.of(context)
                    .textTheme
                    .headline4!
                    .copyWith(fontSize: 12, color: AppTheme.textGreyDark)),
            const Spacer(),
            Text(
              (currentValue * 100).toInt().toString() + '%',
              style: Theme.of(context)
                  .textTheme
                  .headline4!
                  .copyWith(fontSize: 12, color: AppTheme.textGreyDark),
            )
          ],
        ),
      ],
    );
  }
}
