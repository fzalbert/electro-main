import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'components/auth_body.dart';

class AuthScreenWidget extends StatelessWidget {
  final AuthBodyWidget body = AuthBodyWidget();

   AuthScreenWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: body
    );
  }
}