import 'package:electro/app/constants/colors.dart';
import 'package:electro/app/theme/theme.dart';
import 'package:electro/bulk_downloader/bulk_downloader.dart';
import 'package:electro/data/singletons/bulk_dowload_provider.dart';
import 'package:electro/data/singletons/general_provider.dart';
import 'package:electro/models/columns/directory.dart';
import 'package:electro/models/project_hive/directory_response_model.dart';
import 'package:electro/models/project_hive/layer_directory.dart';
import 'package:electro/models/project_hive/project_directory.dart';
import 'package:electro/models/project_hive/projects.dart';
import 'package:electro/store_manager/store_manager.dart';
import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:hive/hive.dart';
import 'package:path_provider/path_provider.dart';
import 'data/singletons/storage.dart';
import 'features/account/auth/auth.dart';
import 'features/map/map.dart';
import 'package:provider/provider.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await Hive.initFlutter();
  Hive.registerAdapter(DirectoryResponseDtoHiveModelAdapter());
  Hive.registerAdapter(ProjectsModelAdapter());
  Hive.registerAdapter(ProjectDirectoryResponseDtoHiveModelAdapter());
  Hive.registerAdapter(LayerDirectoryHiveModelAdapter());
  await Hive.openBox<ProjectsModel>("projectList");

  runApp(const MyApp());
}

Future<void> initProjectDatabase() async {
  await Hive.initFlutter();
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<GeneralProvider>(
      create: (context) => GeneralProvider(),
      child: MaterialApp(
        title: 'Flutter Demo',
        navigatorKey: GlobalInfo.key,
        theme: AppTheme.lightTheme(),
        home: MapScreenWidget(),
        routes: {
          '/bulkDownloader': (context) =>
              ChangeNotifierProvider<BulkDownloadProvider>(
                create: (context) => BulkDownloadProvider(),
                child: const BulkDownloader(),
              ),
          '/storeManager': (context) => const StoreManager(),
        },
      ),
    );
  }
}
