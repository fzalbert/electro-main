import 'package:dio/dio.dart';
import 'package:electro/data/singletons/dio_settings.dart';
import 'package:electro/data/singletons/storage.dart';
import 'package:electro/models/search/search_response.dart';
import 'package:electro/models/columns/layer.dart';
import 'package:electro/models/search/search_request.dart';
import 'dart:convert';

class SearchRepository {
  Future<List<SearchItem>> searchByLayerText(
      {required SearchRequest requestBody}) async {
    await StorageRepository.getInstance();
    print("PRES = ${requestBody.equalSearch}");
    Dio _dio = DioSettings.createDio(connectTimeout: 30000);
    final Response response = await _dio.get(
      'SearchByLayerText/',
      queryParameters: requestBody.toJson(),
      options: Options(headers: {
        'Authorization': 'Bearer ${StorageRepository.getString('token')}'
      }),
    );

    try {
      if (response.statusCode! >= 200 && response.statusCode! < 300) {
        List<LayerModel> list = [];
        List<dynamic> result = response.data["items"];
        List<SearchItem> items = [];
        print("RESULT  ${result}");
        for (var item in result) {
          items.add(
            SearchItem(
              keyIdentifier: item["keyIdentifier"],
              result: item["result"],
              cadastralNumber: item["cadastralNumber"],
              address: item["address"],
              geometry: item["geometry"],
              parentCadastralNumber: item["parentCadastralNumber"],
            ),
          );
        }
        return items;
      } else {
        print("ERROR HERE = ${response.data.toString()}");
        throw Exception("${response.data}");
      }
    } catch (e) {
      Map resp = jsonDecode(response.data);
      print("RESPINSE = ${resp["title"] == "Unauthorized"}");

      if (resp["title"] != null && resp["title"] == "Unauthorized") {
        throw Exception("Необходимо авторизоваться");
      } else {
        throw Exception(response.data);
      }
    }
  }

  Future<Map<String, dynamic>> getSearchTypes() async {
    await StorageRepository.getInstance();
    Dio _dio = DioSettings.createDio(connectTimeout: 30000);
    final Response response = await _dio.get(
      'GetSearchTypes/',
      options: Options(headers: {
        'Authorization': 'Bearer ${StorageRepository.getString('token')}'
      }),
    );

    try {
      if (response.statusCode! >= 200 && response.statusCode! < 300) {
        //print("SEARCH TYPES = ${response.data}");

        return response.data;
      } else {
        print("ERROR HERE = ${response.data.toString()}");
        throw Exception("${response.data}");
      }
    } catch (e) {
      print("ERROR = $e");
      throw Exception(response.data);
    }
  }
}
