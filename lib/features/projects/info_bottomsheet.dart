import 'package:electro/data/utils/functions.dart';
import 'package:electro/features/projects/components/editing_textfield.dart';
import 'package:electro/features/projects/editing_bottomsheet.dart';
import 'package:electro/app/constants/app_icons.dart';
import 'package:electro/app/theme/theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class InfoBottomSheet extends StatelessWidget {
  InfoBottomSheet({Key? key}) : super(key: key);

  final TextEditingController _nameController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.fromLTRB(16, 44, 16, 30),
      child: Wrap(
        direction: Axis.vertical,
        children: [
          Wrap(
            direction: Axis.horizontal,
            children: [
              Text(
                'Информация',
                style: Theme.of(context).textTheme.headline3!.copyWith(
                      fontSize: 20,
                    ),
              ),
              const SizedBox(
                width: 200,
              ),
              GestureDetector(
                  onTap: () {
                    Navigator.of(context).pop();
                    MyFunctions.popBottomSheet(
                        EditingBottomSheet(isEditing: true),
                        true,
                        context);
                  },
                  child: SvgPicture.asset(AppIcons.pen))
            ],
          ),
          const SizedBox(
            height: 28,
          ),
          Text(
            'Название',
            style: Theme.of(context)
                .textTheme
                .headline4!
                .copyWith(fontSize: 16, color: AppTheme.textGreyDark),
          ),
          const SizedBox(
            height: 6,
          ),
          Text(
            'Столб',
            style: Theme.of(context)
                .textTheme
                .headline4!
                .copyWith(fontSize: 20, color: AppTheme.regularText),
          ),
          const SizedBox(height: 16),
          Text(
            'Крепеж к столбу',
            style: Theme.of(context)
                .textTheme
                .headline4!
                .copyWith(fontSize: 16, color: AppTheme.textGreyDark),
          ),
          const SizedBox(
            height: 6,
          ),
          Text(
            'уа23456',
            style: Theme.of(context)
                .textTheme
                .headline4!
                .copyWith(fontSize: 20, color: AppTheme.regularText),
          ),
          const SizedBox(height: 16),
          Text(
            'Текущая геопозиция ',
            style: Theme.of(context)
                .textTheme
                .headline4!
                .copyWith(fontSize: 16, color: AppTheme.textGreyDark),
          ),
          const SizedBox(
            height: 6,
          ),
          Text(
            '123468',
            style: Theme.of(context)
                .textTheme
                .headline4!
                .copyWith(fontSize: 20, color: AppTheme.regularText),
          ),
          const SizedBox(height: 16),
          Text(
            'Страна',
            style: Theme.of(context)
                .textTheme
                .headline4!
                .copyWith(fontSize: 16, color: AppTheme.textGreyDark),
          ),
          const SizedBox(
            height: 6,
          ),
          Text(
            'Россия',
            style: Theme.of(context)
                .textTheme
                .headline4!
                .copyWith(fontSize: 20, color: AppTheme.regularText),
          ),
        ],
      ),
    );
  }
}
