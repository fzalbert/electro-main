import 'dart:async';
import 'package:electro/data/locale/dao/object_type_dao.dart';
import 'package:electro/data/locale/dao/renter_dao.dart';
import 'package:electro/models/arendator/renter.dart';
import 'package:floor/floor.dart';
import 'package:sqflite/sqflite.dart' as sqflite;

import 'dao/file_dao.dart';
import 'dao/map_object.dart';
import 'dao/map_object_value.dart';
import 'dao/object_form_dao.dart';
import 'dao/position.dart';
import 'dao/stop_go.dart';
import 'entities/file.dart';
import 'entities/map_object.dart';
import 'entities/map_object_value.dart';
import 'entities/object_form_entity.dart';
import 'entities/object_type.dart';
import 'entities/position.dart';
import 'entities/stop_go.dart';


part 'app_database.g.dart'; // the generated code will be there

@Database(version: 1, entities: [
  StopGoSessionEntity,
  PositionEntity,
  MapObjectValueEntity,
  MapObjectEntity,
  RenterEntity,
  FileEntity,
  ObjectTypeEntity,
  ObjectFormEntity,
])
abstract class AppDatabase extends FloorDatabase {

  StopGoSessionDao get stopGoSessionDao;
  PositionDao get positionDao;
  MapObjectValueDao get mapObjectValueDao;
  MapObjectDao get mapObjectDao;
  RenterDao get renter;
  FileDao get file;
  ObjectTypeDao get objectType;
  ObjectFormDao get objectForm;

}

class DatabaseBuilder {

  Future<AppDatabase> create() async {
    final database = await $FloorAppDatabase
        .databaseBuilder('app_database.db')
        .build();
    return database;
  }
}