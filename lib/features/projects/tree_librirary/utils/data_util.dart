import '../flutter_tree.dart';

class DataUtil {
  static Map<String, dynamic> transformListToMap(List dataList, Config config) {
    Map obj = {};
    int? rootId;
    dataList.forEach((v) {
      if (v[config.parentId] != 0) {
        if (obj[v[config.parentId]] != null) {
          if (obj[v[config.parentId]][config.children] != null) {
            obj[v[config.parentId]][config.children].add(v);
          } else {
            obj[v[config.parentId]][config.children] = [v];
          }
        } else {
          obj[v[config.parentId]] = {
            config.children: [v],
          };
        }
      } else {
        rootId = v[config.id];
      }
      if (obj[v[config.id]] != null) {
        v[config.children] = obj[v[config.id]][config.children];
      }
      obj[v[config.id]] = v;
    });
    return obj[rootId] ?? {};
  }

  Map<String, dynamic> expandMap(Map<String, dynamic> dataMap, Config config) {
    dataMap['open'] = false;
    dataMap['checked'] = 0;
    dataMap.putIfAbsent(dataMap[config.id], () => dataMap);
    (dataMap[config.children] ?? []).forEach((element) {
      expandMap(element, config);
    });
    return {"aaa": ""};
  }
}
