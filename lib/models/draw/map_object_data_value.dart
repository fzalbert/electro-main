import 'package:electro/data/locale/entities/map_object_value.dart';
import 'package:json_annotation/json_annotation.dart';

part 'map_object_data_value.g.dart';


/// параметр в ообъекте
@JsonSerializable()
class MapObjectDataValue {
  MapObjectDataValue({
    required this.mopId,
    this.value,
    this.values,
  });

  @JsonKey(name: 'MopId')
  late String mopId;
  @JsonKey(name: 'Value')
  late String? value;
  @JsonKey(name: 'Values')
  List<String>? values;

  MapObjectDataValue.from(MapObjectValueModel model){
    mopId = model.parameterId;
    value = model.value;
  }

  factory MapObjectDataValue.fromJson(Map<String, dynamic> json) =>
      _$MapObjectDataValueFromJson(json);

  Map<String,dynamic> toJson() => _$MapObjectDataValueToJson(this);
  //
  // Map<String,dynamic> toJson() => {
  //   "Values": values,
  //   "Value":  value,
  //   "MopId":  mopId,
  // };

}
