import 'dart:io';

import 'package:electro/app/theme/theme.dart';
import 'package:electro/features/crete-user/fields/image_upload.dart';
import 'package:electro/features/projects/components/editing_textfield.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'map_view.dart';

Widget textField({
  required BuildContext context,
  required dynamic document,
  required String fieldName,
  required Map controllers,
  TextInputType? keyboardType,
  List<TextInputFormatter>? inputFormatters,
  Function? callback,
}) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      Text(
        fieldName.toUpperCase(),
        style: Theme.of(context).textTheme.headline3!.copyWith(
              fontSize: 12,
            ),
      ),
      const SizedBox(
        height: 8,
      ),
      Row(
        children: [
          Expanded(
            child: EditingTextField(
              onChanged: (String s) {
                if (callback != null) {
                  callback(
                    document.typeParameters.specificationsId,
                    document.typeParameters.id,
                    s,
                  );
                }
              },
              controller: controllers[document.typeParameters.specificationsId]
                  [document.typeParameters.id],
              keyboardType: keyboardType,
              inputFormatters: inputFormatters,
            ),
          ),
        ],
      ),
      const SizedBox(height: 28),
    ],
  );
}

Widget fileField({
  required BuildContext context,
  required String fieldName,
  required List<File> files,
  dynamic document,
  Map? controllers,
  Function? callback,
}) {
  return Column(
    children: [
      ImageUploadItem(
        document: document,
        controllers: controllers,
        callback: callback,
        files: files,
      ),
    ],
  );
}

Widget createButton(
    {required BuildContext context,
    required Function onPressed,
    required String fieldName}) {
  return CupertinoButton(
    onPressed: () {
      onPressed();
    },
    padding: EdgeInsets.all(0),
    child: Row(
      children: [
        Expanded(
          child: Container(
            decoration: BoxDecoration(
              color: const Color(0xff2B61A3),
              borderRadius: BorderRadius.circular(10),
            ),
            height: 48,
            child: Center(
              child: Text(
                fieldName,
                style: Theme.of(context)
                    .textTheme
                    .headline3!
                    .copyWith(fontSize: 16, color: Colors.white),
              ),
            ),
          ),
        ),
      ],
    ),
  );
}

class GeoButton extends StatefulWidget {
  final BuildContext context;
  final dynamic document;
  final Map? controllers;
  final Function? callback;
  const GeoButton({
    Key? key,
    required this.context,
    this.callback,
    this.controllers,
    this.document,
  }) : super(key: key);

  @override
  _GeoButtonState createState() => _GeoButtonState();
}

class _GeoButtonState extends State<GeoButton> {
  bool showMap = false;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        CupertinoButton(
          onPressed: () {
            setState(() {
              showMap = !showMap;
            });
          },
          padding: const EdgeInsets.all(0),
          child: Row(
            children: [
              Expanded(
                child: Container(
                  decoration: BoxDecoration(
                    color: const Color(0xff2B61A3),
                    borderRadius: BorderRadius.circular(10),
                  ),
                  height: 48,
                  child: Center(
                    child: Text(
                      'Указать местоположение',
                      style: Theme.of(context)
                          .textTheme
                          .headline3!
                          .copyWith(fontSize: 16, color: Colors.white),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
        const SizedBox(height: 28),
        if (showMap)
          Column(children: [
            MapView(
              document: widget.document,
              controllers: widget.controllers,
              onPressed: () {
                setState(() {
                  showMap = !showMap;
                });
              },
              callback: widget.callback,
            ),
            const SizedBox(height: 28),
          ]),
      ],
    );
  }
}
