import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';

part 'parameter.g.dart';

@JsonSerializable()
class Parameter {
  Parameter({
    required this.id,
    required this.parameterName,
    required this.parameterType,
    this.required = false,
    this.orderNumber = 0,
    this.minLenght,
    this.maxLength,
    this.regularExpression,
    this.regularExpressionText,
    this.isFixed = false,
    this.defaultValue,
    this.optionSelect = const {},
  });

  @JsonKey(name: 'Id')
  String id;
  @JsonKey(name: 'ParameterName')
  String parameterName;
  @JsonKey(name: 'ParameterType')
  String parameterType;
  @JsonKey(name: 'Required', defaultValue: false)
  bool required;
  @JsonKey(name: 'OrderNumber')
  int orderNumber;
  @JsonKey(name: 'MinLenght')
  int? minLenght;
  @JsonKey(name: 'MaxLength')
  int? maxLength;
  @JsonKey(name: 'RegularExpression', defaultValue: null)
  String? regularExpression;
  @JsonKey(name: 'RegularExpressionText', defaultValue: null)
  String? regularExpressionText;
  @JsonKey(name: 'IsFixed', defaultValue: false)
  bool isFixed;
  @JsonKey(name: 'DefaultValue')
  dynamic defaultValue;
  @JsonKey(name: 'OptionSelect', defaultValue: {})
  Map<String, String> optionSelect;

  factory Parameter.fromJson(Map<String, dynamic> json) =>
      _$ParameterFromJson(json);

  Map<String,dynamic> toJson() => _$ParameterToJson(this);


  static List<Parameter> fromJsonString(String object){
    List<dynamic> rawList = json.decode(object);

    List<Parameter> items = [];
    for(var item in rawList){
      items.add(Parameter.fromJson(item));
    }
    return items;
  }

  static String toJsonString(List<Parameter> items) {
    return json.encode(items);
  }
}