import 'package:electro/data/locale/entities/session_status.dart';
import 'package:electro/utils/guid_helper.dart';
import 'package:floor/floor.dart';

@Entity(tableName: "stop_go_session")
class StopGoSessionEntity {

  @PrimaryKey()
  final String id;

  String drawTypeId;

  final String createdAt;

  final String userId;

  int status;

  StopGoSessionEntity({
    required this.id,
    required this.drawTypeId,
    required this.createdAt,
    required this.status,
    required this.userId
  });


  DateTime get createdDate => DateTime.parse(createdAt);


  SessionStatus get statusValue => SessionStatus.values[status];
  String get statusText => statusNames[statusValue]!;
}
