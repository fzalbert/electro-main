import 'package:electro/app/constants/app_icons.dart';
import 'package:electro/app/theme/theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';

class EditingTextField extends StatefulWidget {
  final TextEditingController controller;
  final String? suffix;
  final String? currentText;
  final TextInputType? keyboardType;
  final List<TextInputFormatter>? inputFormatters;
  final Function? onChanged;
  final bool? enabled;
  final bool? isRequired;

  EditingTextField({
    Key? key,
    required this.controller,
    this.suffix,
    this.currentText,
    this.keyboardType,
    this.inputFormatters,
    this.onChanged,
    this.enabled = true,
    this.isRequired = false,
  }) : super(key: key);

  @override
  _EditingTextFieldState createState() => _EditingTextFieldState();
}

class _EditingTextFieldState extends State<EditingTextField> {
  InputBorder border({required Color color}) {
    return OutlineInputBorder(
        borderRadius: BorderRadius.circular(9),
        borderSide: BorderSide(color: color, width: 1));
  }

  @override
  void initState() {
    if (widget.currentText != null) {
      widget.controller.text = widget.currentText!;
    }
    super.initState();
  }

  @override
  void dispose() {
    widget.controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: TextField(
          enabled: widget.enabled,
          controller: widget.controller,
          style: Theme.of(context).textTheme.headline4!.copyWith(
                fontSize: 16,
              ),
          onChanged: (String s) {
            if (widget.onChanged != null) {
              widget.onChanged!(s);
            }
            setState(() {});
          },
          //autofocus: true,
          textAlignVertical: TextAlignVertical.center,
          keyboardType: widget.keyboardType,
          inputFormatters: widget.inputFormatters,
          decoration: InputDecoration(
            hintText: 'Введите',
            hintStyle: Theme.of(context)
                .textTheme
                .headline3!
                .copyWith(fontSize: 14, color: AppTheme.textGrey),
            filled: true,
            fillColor: Color(0xffF8F8F8),
            counterText: '',
            contentPadding:
                const EdgeInsets.symmetric(vertical: 12, horizontal: 10),
            suffixIcon: widget.suffix != null
                ? GestureDetector(
                    onTap: () {},
                    child: Container(
                        padding: const EdgeInsets.symmetric(vertical: 10),
                        child: SvgPicture.asset(widget.suffix!)),
                  )
                : null,
            focusedBorder: border(
                color: widget.isRequired != null &&
                        widget.isRequired! &&
                        widget.controller.text == ""
                    ? Colors.red
                    : const Color(0xffF3F3F3)),
            border: border(
                color: widget.isRequired != null &&
                        widget.isRequired! &&
                        widget.controller.text == ""
                    ? Colors.red
                    : const Color(0xffF3F3F3)),
            enabledBorder: border(
                color: widget.isRequired != null &&
                        widget.isRequired! &&
                        widget.controller.text == ""
                    ? Colors.red
                    : const Color(0xffF3F3F3)),
            disabledBorder: border(
                color: widget.isRequired != null &&
                        widget.isRequired! &&
                        widget.controller.text == ""
                    ? Colors.red
                    : const Color(0xffF3F3F3)),
          )),
    );
  }
}
