import 'package:electro/data/locale/entities/object_type.dart';
import 'package:electro/data/singletons/stop_and_go.dart';
import 'package:electro/data/utils/functions.dart';
import 'package:electro/features/draw_type/object/create_object_widget.dart';
import 'package:electro/features/draw_type/object_type_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';


class DrawTypeTreeWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _DrawTypeTreeState();
}

class _DrawTypeTreeState extends State<DrawTypeTreeWidget> {
  final ObjectTypeService _service = ObjectTypeService();

  List<ObjectTypeModel> items = <ObjectTypeModel>[];

  bool _isLoading = true;

  @override
  void initState() {
    _isLoading = true;
    updateData();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.7,
      child: _isLoading
          ? const SizedBox(
              height: 200,
              child: Center(
                child: CupertinoActivityIndicator(),
              ),
            )
          : ListView.builder(
              itemCount: items.length,
              itemBuilder: (context, index) {
                return GestureDetector(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 12, horizontal: 16),
                    child: Text(items[index].getTitle(),
                        style: Theme.of(context)
                            .textTheme
                            .headline4!
                            .copyWith(fontSize: 16)),
                  ),
                  onTap: () {
                    var session = StopAndGo.getCurrentSession();

                    Navigator.of(context).pop();
                    if (session != null) {
                      MyFunctions.popBottomSheet(
                          CreateObjectWidget(
                            sessionId: session.id,
                            mapObjectTypeId: items[index].getId(),
                            parentDrawTypeId: items[index].getParentId(),
                          ),
                          true,
                          context);
                    } else {
                      MyFunctions.popBottomSheet(
                          CreateObjectWidget(
                            mapObjectTypeId: items[index].getId(),
                            parentDrawTypeId: items[index].getParentId(),
                          ),
                          true,
                          context);
                    }
                  },
                );
              }),
    );
  }

  void updateData() async {
    setState(() {
      items.clear();
    });

    try {
      var result = await _service.getAll();
      setState(() {
        _isLoading = false;
        items.addAll(result);
      });
    } catch (e) {
      setState(() {
        _isLoading = false;
      });
      print(e);
    }
  }
}

class DrawType extends ObjectType {
  /// map object type id
  String id;
  String directoryId;

  String title;

  DrawType({
    required this.id,
    required this.directoryId,
    required this.title,
  });

  @override
  String getId() {
    return id;
  }

  @override
  String getParentId() {
    return directoryId;
  }

  @override
  String getTitle() {
    return title;
  }
}
