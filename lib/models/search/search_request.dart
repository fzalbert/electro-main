class SearchRequest {
  final String layerId;
  final int searchType;
  final int? page;
  final int? limit;
  final String searchText;
  final bool equalSearch;
  final double? searchLat;
  final double? searchLng;

  SearchRequest({
    required this.layerId,
    required this.searchType,
    required this.searchText,
    this.page = 1,
    this.limit = 10,
    required this.equalSearch,
    this.searchLat,
    this.searchLng,
  });

  Map<String, dynamic> toJson() => <String, dynamic>{
        "layerId": layerId,
        "searchType": searchType,
        "page": page,
        "limit": limit,
        "searchText": searchText,
        "equalSearch": equalSearch,
        "searchLat": searchLat,
        "searchLng": searchLng,
      };
}
