// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'app_database.dart';

// **************************************************************************
// FloorGenerator
// **************************************************************************

// ignore: avoid_classes_with_only_static_members
class $FloorAppDatabase {
  /// Creates a database builder for a persistent database.
  /// Once a database is built, you should keep a reference to it and re-use it.
  static _$AppDatabaseBuilder databaseBuilder(String name) =>
      _$AppDatabaseBuilder(name);

  /// Creates a database builder for an in memory database.
  /// Information stored in an in memory database disappears when the process is killed.
  /// Once a database is built, you should keep a reference to it and re-use it.
  static _$AppDatabaseBuilder inMemoryDatabaseBuilder() =>
      _$AppDatabaseBuilder(null);
}

class _$AppDatabaseBuilder {
  _$AppDatabaseBuilder(this.name);

  final String? name;

  final List<Migration> _migrations = [];

  Callback? _callback;

  /// Adds migrations to the builder.
  _$AppDatabaseBuilder addMigrations(List<Migration> migrations) {
    _migrations.addAll(migrations);
    return this;
  }

  /// Adds a database [Callback] to the builder.
  _$AppDatabaseBuilder addCallback(Callback callback) {
    _callback = callback;
    return this;
  }

  /// Creates the database and initializes it.
  Future<AppDatabase> build() async {
    final path = name != null
        ? await sqfliteDatabaseFactory.getDatabasePath(name!)
        : ':memory:';
    final database = _$AppDatabase();
    database.database = await database.open(
      path,
      _migrations,
      _callback,
    );
    return database;
  }
}

class _$AppDatabase extends AppDatabase {
  _$AppDatabase([StreamController<String>? listener]) {
    changeListener = listener ?? StreamController<String>.broadcast();
  }

  StopGoSessionDao? _stopGoSessionDaoInstance;

  PositionDao? _positionDaoInstance;

  MapObjectValueDao? _mapObjectValueDaoInstance;

  MapObjectDao? _mapObjectDaoInstance;

  RenterDao? _renterInstance;

  FileDao? _fileInstance;

  ObjectTypeDao? _objectTypeInstance;

  ObjectFormDao? _objectFormInstance;

  Future<sqflite.Database> open(String path, List<Migration> migrations,
      [Callback? callback]) async {
    final databaseOptions = sqflite.OpenDatabaseOptions(
      version: 1,
      onConfigure: (database) async {
        await database.execute('PRAGMA foreign_keys = ON');
        await callback?.onConfigure?.call(database);
      },
      onOpen: (database) async {
        await callback?.onOpen?.call(database);
      },
      onUpgrade: (database, startVersion, endVersion) async {
        await MigrationAdapter.runMigrations(
            database, startVersion, endVersion, migrations);

        await callback?.onUpgrade?.call(database, startVersion, endVersion);
      },
      onCreate: (database, version) async {
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `stop_go_session` (`id` TEXT NOT NULL, `drawTypeId` TEXT NOT NULL, `createdAt` TEXT NOT NULL, `userId` TEXT NOT NULL, `status` INTEGER NOT NULL, PRIMARY KEY (`id`))');
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `position` (`id` TEXT NOT NULL, `sessionId` TEXT, `latitude` REAL NOT NULL, `longitude` REAL NOT NULL, PRIMARY KEY (`id`))');
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `map_object_value` (`id` TEXT NOT NULL, `mapObjectId` TEXT NOT NULL, `parameterId` TEXT NOT NULL, `value` TEXT NOT NULL, `title` TEXT NOT NULL, `parameterType` TEXT NOT NULL, FOREIGN KEY (`mapObjectId`) REFERENCES `map_object` (`id`) ON UPDATE NO ACTION ON DELETE CASCADE, PRIMARY KEY (`id`))');
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `map_object` (`id` TEXT NOT NULL, `parentId` TEXT NOT NULL, `sessionId` TEXT, `positionId` TEXT NOT NULL, `objectTypeId` TEXT NOT NULL, FOREIGN KEY (`sessionId`) REFERENCES `stop_go_session` (`id`) ON UPDATE NO ACTION ON DELETE CASCADE, FOREIGN KEY (`positionId`) REFERENCES `position` (`id`) ON UPDATE NO ACTION ON DELETE CASCADE, PRIMARY KEY (`id`))');
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `renter_table` (`id` TEXT NOT NULL, `objectId` TEXT NOT NULL, `renterId` TEXT NOT NULL, `renterTypeId` TEXT NOT NULL, FOREIGN KEY (`objectId`) REFERENCES `map_object` (`id`) ON UPDATE NO ACTION ON DELETE CASCADE, PRIMARY KEY (`id`))');
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `file_object` (`id` TEXT NOT NULL, `uri` TEXT, `objectId` TEXT NOT NULL, `size` REAL NOT NULL, `sourceName` TEXT, `originalName` TEXT NOT NULL, FOREIGN KEY (`objectId`) REFERENCES `map_object` (`id`) ON UPDATE NO ACTION ON DELETE CASCADE, PRIMARY KEY (`id`))');
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `object_type` (`id` TEXT NOT NULL, `directoryId` TEXT NOT NULL, `title` TEXT NOT NULL, PRIMARY KEY (`id`))');
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `object_form` (`id` TEXT NOT NULL, `name` TEXT NOT NULL, `mapObjectTypeId` TEXT NOT NULL, `parameters` TEXT NOT NULL, `filesEnabled` INTEGER NOT NULL, PRIMARY KEY (`id`))');

        await callback?.onCreate?.call(database, version);
      },
    );
    return sqfliteDatabaseFactory.openDatabase(path, options: databaseOptions);
  }

  @override
  StopGoSessionDao get stopGoSessionDao {
    return _stopGoSessionDaoInstance ??=
        _$StopGoSessionDao(database, changeListener);
  }

  @override
  PositionDao get positionDao {
    return _positionDaoInstance ??= _$PositionDao(database, changeListener);
  }

  @override
  MapObjectValueDao get mapObjectValueDao {
    return _mapObjectValueDaoInstance ??=
        _$MapObjectValueDao(database, changeListener);
  }

  @override
  MapObjectDao get mapObjectDao {
    return _mapObjectDaoInstance ??= _$MapObjectDao(database, changeListener);
  }

  @override
  RenterDao get renter {
    return _renterInstance ??= _$RenterDao(database, changeListener);
  }

  @override
  FileDao get file {
    return _fileInstance ??= _$FileDao(database, changeListener);
  }

  @override
  ObjectTypeDao get objectType {
    return _objectTypeInstance ??= _$ObjectTypeDao(database, changeListener);
  }

  @override
  ObjectFormDao get objectForm {
    return _objectFormInstance ??= _$ObjectFormDao(database, changeListener);
  }
}

class _$StopGoSessionDao extends StopGoSessionDao {
  _$StopGoSessionDao(this.database, this.changeListener)
      : _queryAdapter = QueryAdapter(database),
        _stopGoSessionEntityInsertionAdapter = InsertionAdapter(
            database,
            'stop_go_session',
            (StopGoSessionEntity item) => <String, Object?>{
                  'id': item.id,
                  'drawTypeId': item.drawTypeId,
                  'createdAt': item.createdAt,
                  'userId': item.userId,
                  'status': item.status
                }),
        _stopGoSessionEntityUpdateAdapter = UpdateAdapter(
            database,
            'stop_go_session',
            ['id'],
            (StopGoSessionEntity item) => <String, Object?>{
                  'id': item.id,
                  'drawTypeId': item.drawTypeId,
                  'createdAt': item.createdAt,
                  'userId': item.userId,
                  'status': item.status
                });

  final sqflite.DatabaseExecutor database;

  final StreamController<String> changeListener;

  final QueryAdapter _queryAdapter;

  final InsertionAdapter<StopGoSessionEntity>
      _stopGoSessionEntityInsertionAdapter;

  final UpdateAdapter<StopGoSessionEntity> _stopGoSessionEntityUpdateAdapter;

  @override
  Future<List<StopGoSessionEntity>> findAll() async {
    return _queryAdapter.queryList('SELECT * FROM stop_go_session',
        mapper: (Map<String, Object?> row) => StopGoSessionEntity(
            id: row['id'] as String,
            drawTypeId: row['drawTypeId'] as String,
            createdAt: row['createdAt'] as String,
            status: row['status'] as int,
            userId: row['userId'] as String));
  }

  @override
  Future<StopGoSessionEntity?> findById(String id) async {
    return _queryAdapter.query('SELECT * FROM stop_go_session WHERE id = ?1',
        mapper: (Map<String, Object?> row) => StopGoSessionEntity(
            id: row['id'] as String,
            drawTypeId: row['drawTypeId'] as String,
            createdAt: row['createdAt'] as String,
            status: row['status'] as int,
            userId: row['userId'] as String),
        arguments: [id]);
  }

  @override
  Future<void> deleteByObjectId(String id) async {
    await _queryAdapter.queryNoReturn(
        'DELETE FROM stop_go_session WHERE id = ?1',
        arguments: [id]);
  }

  @override
  Future<void> insert(StopGoSessionEntity entity) async {
    await _stopGoSessionEntityInsertionAdapter.insert(
        entity, OnConflictStrategy.abort);
  }

  @override
  Future<void> update(StopGoSessionEntity entity) async {
    await _stopGoSessionEntityUpdateAdapter.update(
        entity, OnConflictStrategy.abort);
  }
}

class _$PositionDao extends PositionDao {
  _$PositionDao(this.database, this.changeListener)
      : _queryAdapter = QueryAdapter(database, changeListener),
        _positionEntityInsertionAdapter = InsertionAdapter(
            database,
            'position',
            (PositionEntity item) => <String, Object?>{
                  'id': item.id,
                  'sessionId': item.sessionId,
                  'latitude': item.latitude,
                  'longitude': item.longitude
                },
            changeListener);

  final sqflite.DatabaseExecutor database;

  final StreamController<String> changeListener;

  final QueryAdapter _queryAdapter;

  final InsertionAdapter<PositionEntity> _positionEntityInsertionAdapter;

  @override
  Future<List<PositionEntity>> findAll() async {
    return _queryAdapter.queryList('SELECT * FROM position',
        mapper: (Map<String, Object?> row) => PositionEntity(
            id: row['id'] as String,
            latitude: row['latitude'] as double,
            longitude: row['longitude'] as double,
            sessionId: row['sessionId'] as String?));
  }

  @override
  Stream<PositionEntity?> findById(String id) {
    return _queryAdapter.queryStream('SELECT * FROM position WHERE id = ?1',
        mapper: (Map<String, Object?> row) => PositionEntity(
            id: row['id'] as String,
            latitude: row['latitude'] as double,
            longitude: row['longitude'] as double,
            sessionId: row['sessionId'] as String?),
        arguments: [id],
        queryableName: 'position',
        isView: false);
  }

  @override
  Future<List<PositionEntity>> findBySessionId(String sessionId) async {
    return _queryAdapter.queryList(
        'SELECT * FROM position WHERE sessionId = ?1',
        mapper: (Map<String, Object?> row) => PositionEntity(
            id: row['id'] as String,
            latitude: row['latitude'] as double,
            longitude: row['longitude'] as double,
            sessionId: row['sessionId'] as String?),
        arguments: [sessionId]);
  }

  @override
  Future<void> insert(PositionEntity person) async {
    await _positionEntityInsertionAdapter.insert(
        person, OnConflictStrategy.abort);
  }
}

class _$MapObjectValueDao extends MapObjectValueDao {
  _$MapObjectValueDao(this.database, this.changeListener)
      : _queryAdapter = QueryAdapter(database, changeListener),
        _mapObjectValueEntityInsertionAdapter = InsertionAdapter(
            database,
            'map_object_value',
            (MapObjectValueEntity item) => <String, Object?>{
                  'id': item.id,
                  'mapObjectId': item.mapObjectId,
                  'parameterId': item.parameterId,
                  'value': item.value,
                  'title': item.title,
                  'parameterType': item.parameterType
                },
            changeListener),
        _mapObjectValueEntityUpdateAdapter = UpdateAdapter(
            database,
            'map_object_value',
            ['id'],
            (MapObjectValueEntity item) => <String, Object?>{
                  'id': item.id,
                  'mapObjectId': item.mapObjectId,
                  'parameterId': item.parameterId,
                  'value': item.value,
                  'title': item.title,
                  'parameterType': item.parameterType
                },
            changeListener);

  final sqflite.DatabaseExecutor database;

  final StreamController<String> changeListener;

  final QueryAdapter _queryAdapter;

  final InsertionAdapter<MapObjectValueEntity>
      _mapObjectValueEntityInsertionAdapter;

  final UpdateAdapter<MapObjectValueEntity> _mapObjectValueEntityUpdateAdapter;

  @override
  Future<List<MapObjectValueEntity>> findAll() async {
    return _queryAdapter.queryList('SELECT * FROM map_object_value',
        mapper: (Map<String, Object?> row) => MapObjectValueEntity(
            id: row['id'] as String,
            parameterId: row['parameterId'] as String,
            mapObjectId: row['mapObjectId'] as String,
            value: row['value'] as String,
            title: row['title'] as String,
            parameterType: row['parameterType'] as String));
  }

  @override
  Stream<MapObjectValueEntity?> findById(String id) {
    return _queryAdapter.queryStream(
        'SELECT * FROM map_object_value WHERE id = ?1',
        mapper: (Map<String, Object?> row) => MapObjectValueEntity(
            id: row['id'] as String,
            parameterId: row['parameterId'] as String,
            mapObjectId: row['mapObjectId'] as String,
            value: row['value'] as String,
            title: row['title'] as String,
            parameterType: row['parameterType'] as String),
        arguments: [id],
        queryableName: 'map_object_value',
        isView: false);
  }

  @override
  Future<void> deleteByObjectId(String objectId) async {
    await _queryAdapter.queryNoReturn(
        'DELETE FROM map_object_value WHERE mapObjectId = ?1',
        arguments: [objectId]);
  }

  @override
  Future<void> insert(MapObjectValueEntity value) async {
    await _mapObjectValueEntityInsertionAdapter.insert(
        value, OnConflictStrategy.abort);
  }

  @override
  Future<void> update(MapObjectValueEntity value) async {
    await _mapObjectValueEntityUpdateAdapter.update(
        value, OnConflictStrategy.abort);
  }
}

class _$MapObjectDao extends MapObjectDao {
  _$MapObjectDao(this.database, this.changeListener)
      : _queryAdapter = QueryAdapter(database, changeListener),
        _mapObjectEntityInsertionAdapter = InsertionAdapter(
            database,
            'map_object',
            (MapObjectEntity item) => <String, Object?>{
                  'id': item.id,
                  'parentId': item.parentId,
                  'sessionId': item.sessionId,
                  'positionId': item.positionId,
                  'objectTypeId': item.objectTypeId
                },
            changeListener);

  final sqflite.DatabaseExecutor database;

  final StreamController<String> changeListener;

  final QueryAdapter _queryAdapter;

  final InsertionAdapter<MapObjectEntity> _mapObjectEntityInsertionAdapter;

  @override
  Future<List<MapObjectEntity>> findAll() async {
    return _queryAdapter.queryList('SELECT * FROM map_object',
        mapper: (Map<String, Object?> row) => MapObjectEntity(
            id: row['id'] as String,
            parentId: row['parentId'] as String,
            sessionId: row['sessionId'] as String?,
            positionId: row['positionId'] as String,
            objectTypeId: row['objectTypeId'] as String));
  }

  @override
  Stream<MapObjectEntity?> findById(int id) {
    return _queryAdapter.queryStream('SELECT * FROM map_object WHERE id = ?1',
        mapper: (Map<String, Object?> row) => MapObjectEntity(
            id: row['id'] as String,
            parentId: row['parentId'] as String,
            sessionId: row['sessionId'] as String?,
            positionId: row['positionId'] as String,
            objectTypeId: row['objectTypeId'] as String),
        arguments: [id],
        queryableName: 'map_object',
        isView: false);
  }

  @override
  Future<void> insert(MapObjectEntity person) async {
    await _mapObjectEntityInsertionAdapter.insert(
        person, OnConflictStrategy.abort);
  }
}

class _$RenterDao extends RenterDao {
  _$RenterDao(this.database, this.changeListener)
      : _queryAdapter = QueryAdapter(database, changeListener),
        _renterEntityInsertionAdapter = InsertionAdapter(
            database,
            'renter_table',
            (RenterEntity item) => <String, Object?>{
                  'id': item.id,
                  'objectId': item.objectId,
                  'renterId': item.renterId,
                  'renterTypeId': item.renterTypeId
                },
            changeListener);

  final sqflite.DatabaseExecutor database;

  final StreamController<String> changeListener;

  final QueryAdapter _queryAdapter;

  final InsertionAdapter<RenterEntity> _renterEntityInsertionAdapter;

  @override
  Future<List<RenterEntity>> findAll() async {
    return _queryAdapter.queryList('SELECT * FROM renter_table',
        mapper: (Map<String, Object?> row) => RenterEntity(
            id: row['id'] as String,
            objectId: row['objectId'] as String,
            renterId: row['renterId'] as String,
            renterTypeId: row['renterTypeId'] as String));
  }

  @override
  Stream<RenterEntity?> findById(int id) {
    return _queryAdapter.queryStream('SELECT * FROM renter_table WHERE id = ?1',
        mapper: (Map<String, Object?> row) => RenterEntity(
            id: row['id'] as String,
            objectId: row['objectId'] as String,
            renterId: row['renterId'] as String,
            renterTypeId: row['renterTypeId'] as String),
        arguments: [id],
        queryableName: 'renter_table',
        isView: false);
  }

  @override
  Future<void> insert(RenterEntity renter) async {
    await _renterEntityInsertionAdapter.insert(
        renter, OnConflictStrategy.abort);
  }
}

class _$FileDao extends FileDao {
  _$FileDao(this.database, this.changeListener)
      : _queryAdapter = QueryAdapter(database, changeListener),
        _fileEntityInsertionAdapter = InsertionAdapter(
            database,
            'file_object',
            (FileEntity item) => <String, Object?>{
                  'id': item.id,
                  'uri': item.uri,
                  'objectId': item.objectId,
                  'size': item.size,
                  'sourceName': item.sourceName,
                  'originalName': item.originalName
                },
            changeListener);

  final sqflite.DatabaseExecutor database;

  final StreamController<String> changeListener;

  final QueryAdapter _queryAdapter;

  final InsertionAdapter<FileEntity> _fileEntityInsertionAdapter;

  @override
  Future<List<FileEntity>> findAll() async {
    return _queryAdapter.queryList('SELECT * FROM file_object',
        mapper: (Map<String, Object?> row) => FileEntity(
            id: row['id'] as String,
            objectId: row['objectId'] as String,
            size: row['size'] as double,
            sourceName: row['sourceName'] as String?,
            originalName: row['originalName'] as String,
            uri: row['uri'] as String?));
  }

  @override
  Stream<FileEntity?> findById(int id) {
    return _queryAdapter.queryStream('SELECT * FROM file_object WHERE id = ?1',
        mapper: (Map<String, Object?> row) => FileEntity(
            id: row['id'] as String,
            objectId: row['objectId'] as String,
            size: row['size'] as double,
            sourceName: row['sourceName'] as String?,
            originalName: row['originalName'] as String,
            uri: row['uri'] as String?),
        arguments: [id],
        queryableName: 'file_object',
        isView: false);
  }

  @override
  Future<List<FileEntity>> findByMapObjectId(String objectId) async {
    return _queryAdapter.queryList(
        'SELECT * FROM file_object WHERE objectId = ?1',
        mapper: (Map<String, Object?> row) => FileEntity(
            id: row['id'] as String,
            objectId: row['objectId'] as String,
            size: row['size'] as double,
            sourceName: row['sourceName'] as String?,
            originalName: row['originalName'] as String,
            uri: row['uri'] as String?),
        arguments: [objectId]);
  }

  @override
  Future<void> insert(FileEntity file) async {
    await _fileEntityInsertionAdapter.insert(file, OnConflictStrategy.abort);
  }
}

class _$ObjectTypeDao extends ObjectTypeDao {
  _$ObjectTypeDao(this.database, this.changeListener)
      : _queryAdapter = QueryAdapter(database, changeListener),
        _objectTypeEntityInsertionAdapter = InsertionAdapter(
            database,
            'object_type',
            (ObjectTypeEntity item) => <String, Object?>{
                  'id': item.id,
                  'directoryId': item.directoryId,
                  'title': item.title
                },
            changeListener);

  final sqflite.DatabaseExecutor database;

  final StreamController<String> changeListener;

  final QueryAdapter _queryAdapter;

  final InsertionAdapter<ObjectTypeEntity> _objectTypeEntityInsertionAdapter;

  @override
  Future<List<ObjectTypeEntity>> findAll() async {
    return _queryAdapter.queryList('SELECT * FROM object_type',
        mapper: (Map<String, Object?> row) => ObjectTypeEntity(
            id: row['id'] as String,
            directoryId: row['directoryId'] as String,
            title: row['title'] as String));
  }

  @override
  Stream<ObjectTypeEntity?> findById(String id) {
    return _queryAdapter.queryStream('SELECT * FROM object_type WHERE id = ?1',
        mapper: (Map<String, Object?> row) => ObjectTypeEntity(
            id: row['id'] as String,
            directoryId: row['directoryId'] as String,
            title: row['title'] as String),
        arguments: [id],
        queryableName: 'object_type',
        isView: false);
  }

  @override
  Future<void> insert(ObjectTypeEntity type) async {
    await _objectTypeEntityInsertionAdapter.insert(
        type, OnConflictStrategy.abort);
  }
}

class _$ObjectFormDao extends ObjectFormDao {
  _$ObjectFormDao(this.database, this.changeListener)
      : _queryAdapter = QueryAdapter(database),
        _objectFormEntityInsertionAdapter = InsertionAdapter(
            database,
            'object_form',
            (ObjectFormEntity item) => <String, Object?>{
                  'id': item.id,
                  'name': item.name,
                  'mapObjectTypeId': item.mapObjectTypeId,
                  'parameters': item.parameters,
                  'filesEnabled': item.filesEnabled ? 1 : 0
                });

  final sqflite.DatabaseExecutor database;

  final StreamController<String> changeListener;

  final QueryAdapter _queryAdapter;

  final InsertionAdapter<ObjectFormEntity> _objectFormEntityInsertionAdapter;

  @override
  Future<List<ObjectFormEntity>> findAll() async {
    return _queryAdapter.queryList('SELECT * FROM object_form',
        mapper: (Map<String, Object?> row) => ObjectFormEntity(
            id: row['id'] as String,
            name: row['name'] as String,
            parameters: row['parameters'] as String,
            mapObjectTypeId: row['mapObjectTypeId'] as String,
            filesEnabled: (row['filesEnabled'] as int) != 0));
  }

  @override
  Future<ObjectFormEntity?> findById(String id) async {
    return _queryAdapter.query('SELECT * FROM object_form WHERE id = ?1',
        mapper: (Map<String, Object?> row) => ObjectFormEntity(
            id: row['id'] as String,
            name: row['name'] as String,
            parameters: row['parameters'] as String,
            mapObjectTypeId: row['mapObjectTypeId'] as String,
            filesEnabled: (row['filesEnabled'] as int) != 0),
        arguments: [id]);
  }

  @override
  Future<void> insert(ObjectFormEntity type) async {
    await _objectFormEntityInsertionAdapter.insert(
        type, OnConflictStrategy.abort);
  }
}
