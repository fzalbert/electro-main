import 'package:electro/widgets/btns/rounded_button.dart';
import 'package:flutter/cupertino.dart';

class SaveBtn extends StatelessWidget {

  final VoidCallback action;
  const SaveBtn({
    Key? key,
    required this.action
  }) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: RoundedButton(
        title: "Сохранить",
        height: 48,
        width: double.infinity,
        cornerRadius: 10,
        isLoading: false,
        onTap: () {
          FocusScope.of(context).requestFocus(FocusNode());
          action.call();
        },
      ),
    );
  }

}