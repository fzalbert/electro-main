import 'dart:io';

import 'package:dio/dio.dart';
import 'package:electro/data/locale/entities/file.dart';
import 'package:electro/data/locale/entities/map_object.dart';
import 'package:electro/data/locale/entities/session_status.dart';
import 'package:electro/data/singletons/dio_settings.dart';
import 'package:electro/data/singletons/stop_and_go.dart';
import 'package:electro/data/singletons/storage.dart';
import 'package:electro/features/draw_type/object_type_service.dart';
import 'package:electro/features/sessions/service/object_service.dart';
import 'package:electro/features/sessions/service/position_service.dart';
import 'package:electro/features/sessions/service/stop_go_session_service.dart';
import 'package:electro/features/sessions/view_models/stop_go_session.dart';
import 'package:electro/models/draw/map_object.dart';
import 'package:electro/utils/guid_helper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:latlong2/latlong.dart';


import '../draw_type.dart';
import '../draw_type_form.dart';

class StopGoSessionRepository {
  final Dio _dio = DioSettings.createDio();
  final PositionService _positionService = PositionService();
  final StopGoSessionService _sessionService = StopGoSessionService();

  final _drawTypeFormRepository = DrawTypeFormRepository();

  Future<int> send(StopGoSession model) async {
    //
    StopGoSession? currentSession;

    StopAndGo.changeStatus(status: SessionStatus.done);
    try {
      currentSession = await _sessionService.findById(model.id);
      if(currentSession == null) {
        return 500;
      }
    }
    catch(ex){
      print(ex.toString());
      return 500;
    }

    var service = MapObjectService();

    final DrawTypeRepository _repository = DrawTypeRepository();
    String? typeId = '';
    String? parentId = '';
    try {
      var result = await _repository.getDrawTypeIdForRoute();

      typeId = result["typeId"];
      parentId = result["groupId"];

      var route = await service.create(
        sessionId   :  currentSession.id ?? '',
        drawTypeId    :  parentId.toString(),
        position    :  LatLng(0.0, 0.0),
        objectTypeId:  typeId.toString(),
        values      : [],
      );
    }catch (ex) {
      print(ex);
    }

    try {
      var currentSession = await _sessionService.findById(model.id);
      if(currentSession == null) {
        return 500;
      }
      else {
        model = currentSession;
      }

      model.coords = await _positionService.findBySessionId(model.id);
      //files

      var data = MapObject.fromSession(model);
      print("StopGoSession " + data.toJson().toString());

      data.mapObjectData.forEach((element) {
        print(element.toJson().toString());
      });


      final Response response = await _dio.post(
          'CreateDrawObject',
          options: Options(headers: {
            'Authorization': 'Bearer ${StorageRepository.getString('token')}'
          }),
          data: data.toJson()
      );

      if(response.statusCode == 200){
        StopAndGo.changeStatus(status: SessionStatus.sended);
      }

      return response.statusCode ?? 500;
    }
    catch(ex){
      print(ex.toString());
      return 500;
    }
  }


  Future<int> newSend(StopGoSession model) async {
    //
    StopGoSession currentSession;

    StopAndGo.changeStatus(status: SessionStatus.done);
    try {

      var session = await _sessionService.findById(model.id);
      if(session == null) {
        return 500;
      }
      else {
        currentSession = session;
      }
    }
    catch(ex){
      print(ex.toString());
      return 500;
    }

    var service = MapObjectService();


    /// добавление пути

    if(currentSession.coords.length > 1) {
      try {
        var result = (await ObjectTypeService().getRoute());

        if (result != null) {
          var typeId = result.id;
          var parentId = result.directoryId;

          var route = MapObjectModel(
              id: GuidHelper.timeBased(),
              positionId: "",
              objectTypeId: typeId.toString(),
              values: [],
              position: LatLng(0.0, 0.0),
              parentId: parentId,
              coords: currentSession.coords,
              filesInfo: []
          );

          currentSession.objects.add(route);
        }
      } catch (ex) {
        print(ex);
        return 500;
      }
    }

    try {
      //files

      var objects = await postObjectFiles(currentSession.objects);
      currentSession.objects = objects;

      var data = MapObject.fromSession(currentSession);
      print("StopGoSession " + data.toJson().toString());

      data.mapObjectData.forEach((element) {
        print(element.toJson().toString());
      });


      final Response response = await _dio.post(
          'CreateDrawObject',
          options: Options(headers: {
            'Authorization': 'Bearer ${StorageRepository.getString('token')}'
          }),
          data: data.toJson()
      );

      if(response.statusCode == 200 && response.statusMessage=="OK"){
        StopAndGo.changeStatus(status: SessionStatus.sended);
        await _sessionService.updateStatus(currentSession.id, SessionStatus.sended);
      }

      return response.statusCode ?? 500;
    }

    on DioError catch(ex){
      return 501;
    }
    catch(ex){
      print(ex.toString());
      return 500;
    }
  }



  Future<List<MapObjectModel>> postObjectFiles(List<MapObjectModel> objects) async {

    //object id and files
    List<Future<_ObjectFilePair?>> objectFiles = [];

    for(var object in objects){
      for(var file in object.filesInfo){
        objectFiles.add(_postFiles(file, object.id));
      }
    }

    return Future.wait(objectFiles).then((List<_ObjectFilePair?> values) {

      ///обновление файлов в объектах на новые загруженные файлы
      for(var object in objects){
        object.filesInfo = [];
      }

      for(var value in values){
        if(value != null){
          var object = objects.firstWhere((element) => element.id == value.objectId);
          object.filesInfo.add(value.file);
        }
      }
      return objects;
    });

  }

  ///
  /// Map of objectId and file list
  Future<_ObjectFilePair?> _postFiles(FileModel file, String objectId) async {

    if(file.sourceName != null && file.sourceName != "") {
      return _ObjectFilePair(
          objectId: objectId,
        file: file
      );
    }

    var response = await _drawTypeFormRepository.postFileModel(file: file);

    return response == null ? null : _ObjectFilePair(
        objectId: objectId,
        file: response,
    );
  }



  // Future<List<MapObjectData>> saveFiles(List<MapObjectModel> objects) async {
  //
  // }

}

class _ObjectFilePair {

  String objectId;
  FileModel file;

  _ObjectFilePair({
    required this.objectId,
    required this.file,
  });

}