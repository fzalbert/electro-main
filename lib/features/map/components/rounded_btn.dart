import 'dart:ui';

import 'package:electro/app/constants/app_icons.dart';
import 'package:electro/app/constants/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class RoundedBtn extends StatelessWidget {
  final GestureTapCallback? onTap;
  final String asset;
  final Widget? child;

  RoundedBtn({Key? key, this.onTap, this.asset = "", this.child})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(50.0),
        child: BackdropFilter(
          filter: ImageFilter.blur(sigmaX: 12, sigmaY: 12),
          child: Container(
            width: 54,
            height: 54,
            decoration: BoxDecoration(
              gradient: const LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  Color.fromRGBO(255, 255, 255, 0.7),
                  Color.fromRGBO(215, 224, 237, 0.7),
                ],
              ),
              border: Border.all(color: Colors.white),
              borderRadius: BorderRadius.circular(50.0),
              boxShadow: const [
                BoxShadow(
                  color: Color(0x1A193D83),
                  offset: Offset(30, 30),
                  blurRadius: 50,
                )
              ],
            ),
            child: asset == AppIcons.arrowRight
                ? Center(
                    child: Container(
                        margin: const EdgeInsets.only(left: 10),
                        child: const Icon(
                          Icons.arrow_back_ios,
                          color: mainColor,
                        )))
                : Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: child ??
                        Image.asset(
                          asset,
                          color: mainColor,
                        ),
                  ),
          ),
        ),
      ),
    );
  }
}
