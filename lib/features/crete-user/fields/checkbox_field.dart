import 'package:electro/app/theme/theme.dart';
import 'package:flutter/material.dart';

class CustomCheckBoxField extends StatefulWidget {
  final List<dynamic> parameterOptions;
  final String fieldName;
  final bool isOne;
  final dynamic document;
  final Map? controllers;
  final Function? callback;
  const CustomCheckBoxField({
    Key? key,
    required this.parameterOptions,
    required this.fieldName,
    required this.isOne,
    this.document,
    this.controllers,
    this.callback,
  }) : super(key: key);

  @override
  _CustomCheckBoxState createState() => _CustomCheckBoxState();
}

class _CustomCheckBoxState extends State<CustomCheckBoxField> {
  int? val = -1;
  late List<bool?> flagFields;

  Color getColor(Set<MaterialState> states) {
    const Set<MaterialState> interactiveStates = <MaterialState>{
      MaterialState.pressed,
      MaterialState.hovered,
      MaterialState.focused,
    };
    if (states.any(interactiveStates.contains)) {
      return Colors.blue;
    }
    return const Color(0xff2B61A3);
  }

  @override
  void initState() {
    super.initState();
    flagFields =
        List<bool?>.generate(widget.parameterOptions.length, (i) => false);
  }

  @override
  Widget build(BuildContext context) {
    if (widget.isOne) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            widget.fieldName.toUpperCase(),
            style: Theme.of(context).textTheme.headline3!.copyWith(
                  fontSize: 12,
                ),
          ),
          for (int i = 0; i < widget.parameterOptions.length; i++)
            Row(
              children: [
                Radio(
                  groupValue: val,
                  value: i,
                  onChanged: (int? newValue) {
                    if (widget.callback != null) {
                      widget.callback!(
                        widget.document.typeParameters.specificationsId,
                        widget.document.typeParameters.id,
                        newValue != -1
                            ? <String>[widget.parameterOptions[newValue!]["id"]]
                            : <String>[""],
                      );
                    }
                    setState(() {
                      val = newValue;
                    });
                  },
                ),
                Text(
                  widget.parameterOptions[i]["optionText"],
                  style: Theme.of(context)
                      .textTheme
                      .headline3!
                      .copyWith(fontSize: 14, color: AppTheme.textGrey),
                ),
              ],
            ),
          const SizedBox(height: 28),
        ],
      );
    } else {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            widget.fieldName.toUpperCase(),
            style: Theme.of(context).textTheme.headline3!.copyWith(
                  fontSize: 12,
                ),
          ),
          for (int i = 0; i < widget.parameterOptions.length; i++)
            Row(
              children: [
                Checkbox(
                  value: flagFields[i],
                  onChanged: (bool? newValue) {
                    flagFields[i] = newValue;

                    if (widget.callback != null) {
                      List<String> ids = [];
                      for (int i = 0; i < flagFields.length; i++) {
                        if (flagFields[i] != null) {
                          if (flagFields[i]!) {
                            ids.add(widget.parameterOptions[i]["id"]);
                          }
                        }
                      }
                      widget.callback!(
                        widget.document.typeParameters.specificationsId,
                        widget.document.typeParameters.id,
                        ids,
                      );
                    }
                    setState(() {});
                  },
                ),
                Text(
                  widget.parameterOptions[i]["optionText"],
                  style: Theme.of(context)
                      .textTheme
                      .headline3!
                      .copyWith(fontSize: 14, color: AppTheme.textGrey),
                ),
              ],
            ),
          const SizedBox(height: 28),
        ],
      );
    }
  }
}
