import 'package:electro/app/theme/theme.dart';
import 'package:flutter/material.dart';

class DetailBottomSheet extends StatelessWidget {
  final String? title;
  final Widget? prefix;
  const DetailBottomSheet({Key? key, this.prefix, this.title})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.fromLTRB(16, 44, 16, 0),
      child: Wrap(
        direction: Axis.vertical,
        children: [
          Text(
            'Легенда',
            style: Theme.of(context).textTheme.headline3!.copyWith(
                  fontSize: 20,
                ),
          ),
          const SizedBox(
            height: 28,
          ),
          prefix ??
              Container(
                width: 16,
                height: 16,
                decoration: BoxDecoration(
                    color: Colors.yellow,
                    border: Border.all(color: Colors.black, width: 2)),
              ),
        ],
      ),
    );
  }
}
