import 'package:electro/data/utils/functions.dart';
import 'package:electro/features/draw_type/object/preview_object_widget.dart';
import 'package:electro/utils/bottom_sheet_helper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong2/latlong.dart';

class MarkerHelper {
  static Marker createObjectMarker({
    required BuildContext context,
    required String id,
    required LatLng position,
  }) {
    var marker = Marker(
        point: position,
        builder: (context) => GestureDetector(
              onTap: () {
                MyFunctions.popBottomSheet(
                    PreviewObjectWidget(objectId: id), true, context);
              },
              child: Image.asset(
                'assets/icons/ic_my_location.png',
                width: 30,
                height: 30,
                color: Colors.red,
              ),
            ));
    return marker;
  }
}
