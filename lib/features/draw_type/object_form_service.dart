import 'package:electro/data/locale/app_database.dart';
import 'package:electro/data/locale/entities/object_form_entity.dart';
import 'package:electro/models/object/draw_type_form.dart';
import 'package:electro/repository/draw_type_form.dart';

class ObjectFormService {
  final _formRepository = DrawTypeFormRepository();

  Future<void> update(Iterable<String> idList) async {
    var db = await _getDb();

    var result = await db.database.delete(ObjectFormEntity.TABLE_NAME);
    var items = <DrawTypeForm>[];
    for(var id in idList) {
      var _formType = await _formRepository.get(id);
      items.add(_formType);
      var entity = ObjectFormEntity.fromDto(_formType);
      await db.objectForm.insert(entity);
    }

    print(items.toString());
  }

  Future<ObjectFormModel?> findById(String id) async {
    var db = await _getDb();

    var all = await db.objectForm.findAll();
    var entity = await db.objectForm.findById(id);

    if(entity != null) {
      return ObjectFormModel.fromEntity(entity);
    } else {
      return null;
    }
  }




  Future<AppDatabase> _getDb() async {
    var _database = await DatabaseBuilder().create();
    return _database;
  }
}