import 'package:electro/bloc/arendator/arendator_cubit.dart';
import 'package:electro/features/crete-user/fields/checkbox_field.dart';
import 'package:electro/features/crete-user/pages/create_page.dart';
import 'package:electro/features/crete-user/pages/documents_page.dart';
import 'package:electro/features/crete-user/pages/error_page.dart';
import 'package:electro/features/crete-user/fields/fields.dart';
import 'package:electro/features/crete-user/pages/types_page.dart';
import 'package:electro/features/crete-user/fields/userlist_field.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_multi_formatter/flutter_multi_formatter.dart';

import 'pages/loading_page.dart';

class UserBottomSheet extends StatefulWidget {
  final Function? callback;
  final List<Map>? choosedDocumentIds;
  const UserBottomSheet({Key? key, this.callback, this.choosedDocumentIds})
      : super(key: key);

  @override
  _UserBottomSheetState createState() => _UserBottomSheetState();
}

class _UserBottomSheetState extends State<UserBottomSheet> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: MediaQuery.of(context).viewInsets,
      child: BlocBuilder<ArendatorCubit, ArendatorState>(
        builder: (context, state) {
          if (state is LoadingArendatorState) {
            return const LoadingPage();
          } else if (state is LoadedTypesArendatorState) {
            return TypesOfDocuments(
              typesArendator: state.typesArendator,
            );
          } else if (state is LoadedCreateDocumentState) {
            return CreateArendatorView(
              documents: state.documents,
              typesId: state.typesId,
              arendatorId: state.typesId,
            );
          } else if (state is LoadedDocumentsState) {
            return DocumentsView(
              documents: state.documents,
              callback: widget.callback,
              choosedDocumentIds: widget.choosedDocumentIds,
              arendatorId: state.typesId,
            );
          } else if (state is EditDocumentState) {
            return CreateArendatorView(
              documents: state.documents,
              typesId: state.typesId,
              arendatorId: state.arendatorId,
              isEdit: true,
            );
          } else {
            return const ErrorPage();
          }
        },
      ),
    );
  }
}
