// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'project_directory.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class ProjectDirectoryResponseDtoHiveModelAdapter
    extends TypeAdapter<ProjectDirectoryResponseDtoHiveModel> {
  @override
  final int typeId = 2;

  @override
  ProjectDirectoryResponseDtoHiveModel read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return ProjectDirectoryResponseDtoHiveModel(
      activated: fields[4] as bool,
      createdBy: fields[8] as String,
      layers: (fields[11] as List).cast<LayerDirectoryHiveModel>(),
      childrenDirectories:
          (fields[12] as List).cast<ProjectDirectoryResponseDtoHiveModel>(),
      id: fields[0] as String,
      enabledDefault: fields[10] as bool,
      mapProjectDirectoryId: fields[2] as String,
      mapProjectId: fields[1] as String,
      name: fields[3] as String,
      orderNumber: fields[5] as int,
      rootServiceDirectory: fields[7] as bool,
      serviceId: fields[6] as String,
      timeCreated: fields[9] as String,
    );
  }

  @override
  void write(BinaryWriter writer, ProjectDirectoryResponseDtoHiveModel obj) {
    writer
      ..writeByte(13)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.mapProjectId)
      ..writeByte(2)
      ..write(obj.mapProjectDirectoryId)
      ..writeByte(3)
      ..write(obj.name)
      ..writeByte(4)
      ..write(obj.activated)
      ..writeByte(5)
      ..write(obj.orderNumber)
      ..writeByte(6)
      ..write(obj.serviceId)
      ..writeByte(7)
      ..write(obj.rootServiceDirectory)
      ..writeByte(8)
      ..write(obj.createdBy)
      ..writeByte(9)
      ..write(obj.timeCreated)
      ..writeByte(10)
      ..write(obj.enabledDefault)
      ..writeByte(11)
      ..write(obj.layers)
      ..writeByte(12)
      ..write(obj.childrenDirectories);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ProjectDirectoryResponseDtoHiveModelAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
