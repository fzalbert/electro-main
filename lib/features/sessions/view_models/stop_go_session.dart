import 'package:electro/data/locale/entities/map_object.dart';
import 'package:electro/data/locale/entities/position.dart';
import 'package:electro/data/locale/entities/session_status.dart';
import 'package:electro/data/locale/entities/stop_go.dart';

class StopGoSession {
  String id;

  String drawTypeId;

  DateTime createdAt;

  SessionStatus status;

  List<PositionEntity> coords;

  List<MapObjectModel> objects;

  StopGoSession(
      {required this.id,
      required this.drawTypeId,
      required this.createdAt,
      required this.status,
      required this.coords,
      required this.objects,
      });

  static StopGoSession fromEntity(
      StopGoSessionEntity entity,
      List<PositionEntity> coords,
      List<MapObjectModel> objects,
      ) {
    return StopGoSession(
        id: entity.id,
        drawTypeId: entity.drawTypeId,
        createdAt: entity.createdDate,
        status: entity.statusValue,
        coords: coords,
      objects: objects,
    );
  }
}
