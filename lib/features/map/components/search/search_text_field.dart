import 'package:electro/app/constants/app_icons.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';

class SearchTextField extends StatefulWidget {
  final VoidCallback? onSettingsTapped;
  final void Function(String)? onChanged;
  final FocusNode? focusNode;
  final bool? isAddress;
  final Function? onLat;
  final Function? onLng;
  const SearchTextField(
      {this.onChanged,
      this.focusNode,
      this.onSettingsTapped,
      this.isAddress = false,
      this.onLat,
      this.onLng,
      Key? key})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _SearchTextField();
  }
}

class _SearchTextField extends State<SearchTextField> {
  final _controller = TextEditingController();
  final latController = TextEditingController();
  final lngController = TextEditingController();

  @override
  void dispose() {
    _controller.dispose();
    latController.dispose();
    lngController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.isAddress!) {
      return Container(
        child: Column(
          children: [
            TextField(
              onChanged: (value) {
                setState(() {});
                widget.onLat!(value);
              },
              controller: latController,
              style:
                  Theme.of(context).textTheme.headline4!.copyWith(fontSize: 16),
              inputFormatters: [FilteringTextInputFormatter.digitsOnly],
              decoration: InputDecoration(
                contentPadding: const EdgeInsets.all(20),
                hintText: 'Широта',
                hintStyle: Theme.of(context)
                    .textTheme
                    .headline4!
                    .copyWith(fontSize: 16),
                suffixIcon: latController.text.isEmpty
                    ? GestureDetector(
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(8, 10, 16, 12),
                          child: SvgPicture.asset(AppIcons.filter),
                        ),
                        onTap: widget.onSettingsTapped,
                      )
                    : GestureDetector(
                        onTap: () {
                          setState(() {});
                          latController.clear();
                        },
                        child: Padding(
                            padding: const EdgeInsets.fromLTRB(8, 10, 16, 12),
                            child: SvgPicture.asset(
                              AppIcons.cancel,
                              color: const Color(0xFF656871),
                            )),
                      ),
                disabledBorder: InputBorder.none,
                focusedBorder: InputBorder.none,
                enabledBorder: InputBorder.none,
                border: InputBorder.none,
              ),
            ),
            TextField(
              onChanged: (value) {
                setState(() {});
                widget.onLng!(value);
              },
              controller: lngController,
              style:
                  Theme.of(context).textTheme.headline4!.copyWith(fontSize: 16),
              inputFormatters: [FilteringTextInputFormatter.digitsOnly],
              decoration: InputDecoration(
                contentPadding: const EdgeInsets.all(20),
                hintText: 'Долгота',
                hintStyle: Theme.of(context)
                    .textTheme
                    .headline4!
                    .copyWith(fontSize: 16),
                suffixIcon: lngController.text.isEmpty
                    ? null
                    : GestureDetector(
                        onTap: () {
                          setState(() {});
                          lngController.clear();
                        },
                        child: Padding(
                            padding: const EdgeInsets.fromLTRB(8, 10, 16, 12),
                            child: SvgPicture.asset(
                              AppIcons.cancel,
                              color: const Color(0xFF656871),
                            )),
                      ),
                disabledBorder: InputBorder.none,
                focusedBorder: InputBorder.none,
                enabledBorder: InputBorder.none,
                border: InputBorder.none,
              ),
            ),
          ],
        ),
      );
    } else {
      return TextField(
        onChanged: widget.onChanged,
        controller: _controller,
        style: Theme.of(context).textTheme.headline4!.copyWith(fontSize: 16),
        focusNode: widget.focusNode,
        decoration: InputDecoration(
          contentPadding: const EdgeInsets.all(20),
          hintText: 'Поиск',
          hintStyle:
              Theme.of(context).textTheme.headline4!.copyWith(fontSize: 16),
          prefixIcon: Padding(
            padding: const EdgeInsets.fromLTRB(16, 10, 8, 12),
            child: SvgPicture.asset(AppIcons.search),
          ),
          suffixIcon: _controller.text.isEmpty
              ? GestureDetector(
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(8, 10, 16, 12),
                    child: SvgPicture.asset(AppIcons.filter),
                  ),
                  onTap: widget.onSettingsTapped,
                )
              : GestureDetector(
                  onTap: () {
                    widget.onChanged?.call("");
                    _controller.clear();
                  },
                  child: Padding(
                      padding: const EdgeInsets.fromLTRB(8, 10, 16, 12),
                      child: SvgPicture.asset(
                        AppIcons.cancel,
                        color: const Color(0xFF656871),
                      )),
                ),
          disabledBorder: InputBorder.none,
          focusedBorder: InputBorder.none,
          enabledBorder: InputBorder.none,
          border: InputBorder.none,
        ),
      );
    }
  }
}
