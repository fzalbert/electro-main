// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'directory.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DirectoryResponseDto _$DirectoryResponseDtoFromJson(
        Map<String, dynamic> json) =>
    DirectoryResponseDto(
      activated: json['Activated'] as bool,
      center: json['Center'] as String? ?? '',
      createdBy: json['CreatedBy'] as String? ?? '',
      id: json['Id'] as String? ?? '',
      mapProjectDirectories: (json['ChildrenDirectories'] as List<dynamic>?)
              ?.map((e) => ProjectDirectoryResponseDto.fromJson(
                  e as Map<String, dynamic>))
              .toList() ??
          [],
      moduleId: json['ModuleId'] as String? ?? '',
      name: json['Name'] as String? ?? '',
      orderNumber: json['OrderNumber'] as int? ?? 0,
      timeCreated: json['TimeCreated'] as String? ?? '',
      zoomLevel: json['ZoomLevel'] as int? ?? 0,
      mapProjectLayers: (json['Layers'] as List<dynamic>?)
              ?.map((e) =>
                  LayerDirectoryModel.fromJson(e as Map<String, dynamic>))
              .toList() ??
          [],
    );

Map<String, dynamic> _$DirectoryResponseDtoToJson(
        DirectoryResponseDto instance) =>
    <String, dynamic>{
      'Id': instance.id,
      'Name': instance.name,
      'CreatedBy': instance.createdBy,
      'TimeCreated': instance.timeCreated,
      'Center': instance.center,
      'ZoomLevel': instance.zoomLevel,
      'Activated': instance.activated,
      'OrderNumber': instance.orderNumber,
      'ModuleId': instance.moduleId,
      'ChildrenDirectories': instance.mapProjectDirectories,
      'Layers': instance.mapProjectLayers,
    };
