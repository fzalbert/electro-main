import 'package:location/location.dart';

class UserLocation {
  static final Location _location = Location();

  static Future<bool> _requestLocation() async {
    var _serviceEnabled = await _location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await _location.requestService();
      if (!_serviceEnabled) {
        print("not enabled");
        return false;
      }
    }

    var _permissionGranted = await _location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await _location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        print("not granted");
        return false;
      }
    }

    return true;
  }
}