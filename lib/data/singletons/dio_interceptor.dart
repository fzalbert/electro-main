import 'dart:async';

import 'package:dio/dio.dart';
import 'package:electro/data/singletons/storage.dart';
import 'package:electro/data/utils/functions.dart';

class AppInterceptors extends Interceptor {
  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) {
    print('REQUEST[${options.method}] => PATH: ${options.path}');
    return super.onRequest(options, handler);
  }

  @override
  Future onResponse(
      Response response, ResponseInterceptorHandler handler) async {
    print(
        'RESPONSE[${response.statusCode}] => PATH: ${response.requestOptions.path}');

    if (response.statusCode == 401 &&
        response.requestOptions.path != "ProjectList/") {
      MyFunctions.showLogOut(
        title:
            'Вы не авторизованы, чтобы обновить информацию перезайдите в приложение',
        context: GlobalInfo.key.currentContext!,
      );
    }
    return super.onResponse(response, handler);
  }
}
