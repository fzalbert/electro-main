

import 'package:electro/data/locale/entities/position.dart';
import 'package:floor/floor.dart';

@dao
abstract class PositionDao {

  @Query('SELECT * FROM position')
  Future<List<PositionEntity>> findAll();

  @Query('SELECT * FROM position WHERE id = :id')
  Stream<PositionEntity?> findById(String id);

  @Query('SELECT * FROM position WHERE sessionId = :sessionId')
  Future<List<PositionEntity>> findBySessionId(String sessionId);

  @Insert()
  Future<void> insert(PositionEntity person);

}