import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

abstract class AppTheme {
  static ThemeData lightTheme() => ThemeData(
        fontFamily: 'Roboto',
        textTheme: const TextTheme(
          headline1: headline1,
          headline2: headline2,
          headline3: headline3,
          headline4: headline4,
        ),
        textSelectionTheme: const TextSelectionThemeData(
          cursorColor: primaryAccent,
          // selectionColor: Colors.transparent,
          // selectionHandleColor: Colors.transparent,
        ),
      );

  static const primaryAccent = Color(0xff2d7af1);
  static const textGrey = Color(0xffACACAC);
    static const textGreyDark = Color(0xff666666);
  static const regularText = Color(0xff1B1B1B);
  static const apptheme = Color(0xffF5F8FC);
  static const regularTextDarkBlack = Color(0xff132133);
  static const regularTextDarkBlue = Color(0xff2B61A3);
  static const regularTextDarkGrey = Color(0xff9E9EA5);
  static const regularTextGrey = Color(0xffD5D5D5);

  static const headline1 = TextStyle(
    fontSize: 11,
    fontWeight: FontWeight.w700,
    color: regularTextDarkBlack,
    fontFamily: 'Roboto',
  );
  static const headline2 = TextStyle(
    fontSize: 11,
    fontWeight: FontWeight.w600,
    color: regularTextDarkBlack,
    fontFamily: 'Roboto',
  );
  static const headline3 = TextStyle(
    fontSize: 11,
    fontWeight: FontWeight.w500,
    color: regularTextDarkBlack,
    fontFamily: 'Roboto',
  );
  static const headline4 = TextStyle(
    fontSize: 14,
    fontWeight: FontWeight.w400,
    color: regularTextDarkBlack,
    fontFamily: 'Roboto',
  );

  static const headline900 = TextStyle(
    fontSize: 20,
    fontWeight: FontWeight.w900,
    color: regularTextDarkBlack,
    fontFamily: 'Roboto',
  );
}
