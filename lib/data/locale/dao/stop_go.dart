import 'package:electro/data/locale/entities/stop_go.dart';
import 'package:floor/floor.dart';
import 'package:flutter/cupertino.dart';

@dao
abstract class StopGoSessionDao {
  @Query('SELECT * FROM stop_go_session')
  Future<List<StopGoSessionEntity>> findAll();

  @Query('SELECT * FROM stop_go_session WHERE id = :id')
  Future<StopGoSessionEntity?> findById(String id);

  @Insert()
  Future<void> insert(StopGoSessionEntity entity);

  @Update()
  Future<void> update(StopGoSessionEntity entity);

  @Query('DELETE FROM stop_go_session WHERE id = :id')
  Future<void> deleteByObjectId(String id);
}
