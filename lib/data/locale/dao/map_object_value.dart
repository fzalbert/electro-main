

import 'package:electro/data/locale/entities/map_object_value.dart';
import 'package:floor/floor.dart';
import 'package:flutter/cupertino.dart';

@dao
abstract class MapObjectValueDao {

  @Query('SELECT * FROM map_object_value')
  Future<List<MapObjectValueEntity>> findAll();

  @Query('SELECT * FROM map_object_value WHERE id = :id')
  Stream<MapObjectValueEntity?> findById(String id);

  @Insert()
  Future<void> insert(MapObjectValueEntity value);

  @Update()
  Future<void> update(MapObjectValueEntity value);

  @Query('DELETE FROM map_object_value WHERE mapObjectId = :objectId')
  Future<void> deleteByObjectId(String objectId);

}