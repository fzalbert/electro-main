import 'package:json_annotation/json_annotation.dart';

part 'layer.g.dart';

@JsonSerializable()
class LayerModel {
  @JsonKey(defaultValue: '', name: 'uri')
  final String uri;

  @JsonKey(defaultValue: '', name: 'id')
  final String id;

  @JsonKey(defaultValue: '', name: 'name')
  final String name;

  @JsonKey(defaultValue: '', name: 'legendLink')
  final String legendLink;

  @JsonKey(defaultValue: '', name: 'protocolName')
  final String protocolName;

  @JsonKey(defaultValue: [], name: 'mapProjectLayerConfigs')
  final List<Map<String, dynamic>> mapProjectLayerConfigs;

  LayerModel({
    required this.uri,
    required this.protocolName,
    required this.mapProjectLayerConfigs,
    required this.id,
    required this.name,
    required this.legendLink,
  });

  factory LayerModel.fromJson(Map<String, dynamic> json) =>
      _$LayerModelFromJson(json);
}
